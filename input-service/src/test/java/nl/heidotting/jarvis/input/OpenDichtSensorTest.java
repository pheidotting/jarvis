package nl.heidotting.jarvis.input;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.ClientParameters;
import com.rabbitmq.http.client.domain.OutboundMessage;
import com.rabbitmq.http.client.domain.QueueInfo;
import com.redis.testcontainers.RedisContainer;
import lombok.SneakyThrows;
import nl.heidotting.jarvis.input.model.OpenDichtInput;
import nl.heidotting.jarvis.input.repository.BatterijRepository;
import nl.heidotting.jarvis.input.repository.BewegingRepository;
import nl.heidotting.jarvis.input.repository.LichtsterkteRepository;
import nl.heidotting.jarvis.input.repository.OpenOfDichtRepository;
import nl.heidotting.jarvis.input.repository.TemperatuurRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.InfluxDBContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.UUID;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class OpenDichtSensorTest {
    @Autowired
    private BewegingRepository bewegingRepository;
    @Autowired
    private BatterijRepository batterijRepository;
    @Autowired
    private LichtsterkteRepository lichtsterkteRepository;
    @Autowired
    private OpenOfDichtRepository openOfDichtRepository;
    @Autowired
    private TemperatuurRepository temperatuurRepository;

    @Value("${queues.zigbee.opendicht.detection.routingkey}")
    private String routingkey;
    @Value("${queues.zigbee.opendicht.detection.exchangenaam}")
    private String exchangeNaam;

    private final String AMQ_TOPIC = "amq.topic";
    private final String QUEUE = this.getClass().getCanonicalName();
    private final static String ORG = "nl.heidotting";
    private final static String BUCKET = "jarvis";
    private final static String TOKEN = UUID.randomUUID().toString();

    private static RabbitMQContainer rabbitmq;
    private static RedisContainer redis;
    private static InfluxDBContainer influxDBContainer;

    static {
        rabbitmq = new RabbitMQContainer();
        rabbitmq.start();
        redis = new RedisContainer(DockerImageName.parse("redis:6.2.6"));
        redis.start();
        influxDBContainer = new InfluxDBContainer<>(
                DockerImageName.parse("influxdb:2.7.0")
        )
                .withOrganization(ORG)
                .withBucket(BUCKET)
                .withAdminToken(TOKEN);
        influxDBContainer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.rabbitmq.host", rabbitmq::getHost);
        registry.add("spring.rabbitmq.port", rabbitmq::getAmqpPort);
        registry.add("spring.rabbitmq.username", rabbitmq::getAdminUsername);
        registry.add("spring.rabbitmq.password", rabbitmq::getAdminPassword);
        registry.add("spring.rabbitmq.ssl.enabled", () -> false);
        registry.add("spring.data.redis.host", redis::getHost);
        registry.add("spring.data.redis.port", () -> redis.getMappedPort(6379));
        registry.add("influx.token", () -> TOKEN);
        registry.add("influx.org", () -> ORG);
        registry.add("influx.bucket", () -> BUCKET);
        registry.add("influx.ip", () -> influxDBContainer.getHost());
        registry.add("influx.port", () -> influxDBContainer.getMappedPort(8086));

        registry.add("queues.smartmeter.energy.queuenaam", () -> "queues.smartmeter.energy.queuenaam");
        registry.add("queues.smartmeter.energy.routingkey", () -> "queues.smartmeter.energy.routingkey");
        registry.add("queues.smartmeter.gasUsage.queuenaam", () -> "queues.smartmeter.gasUsage.queuenaam");
        registry.add("queues.smartmeter.gasUsage.routingkey", () -> "queues.smartmeter.gasUsage.routingkey");
        registry.add("queues.smartmeter.usage.queuenaam", () -> "queues.smartmeter.usage.queuenaam");
        registry.add("queues.smartmeter.usage.routingkey", () -> "queues.smartmeter.usage.routingkey");
        registry.add("queues.zigbee.motion.detection.queuenaam", () -> "queues.zigbee.motion.detection.queuenaam");
        registry.add("queues.zigbee.motion.detection.routingkey", () -> "queues.zigbee.motion.detection.routingkey");
        registry.add("queues.zigbee.opendicht.detection.queuenaam", () -> "queues.zigbee.opendicht.detection.queuenaam");
        registry.add("queues.zigbee.opendicht.detection.routingkey", () -> "queues.zigbee.opendicht.detection.routingkey");
        registry.add("queues.lamp.logeerkamer.statua.queuenaam", () -> "queues.lamp.logeerkamer.statua.queuenaam");
        registry.add("queues.lamp.logeerkamer.statua.routingkey", () -> "queues.lamp.logeerkamer.statua.routingkey");
        registry.add("queues.zigbee.opendicht.detection.exchangenaam", () -> "queues.zigbee.opendicht.detection.exchangenaam");
        registry.add("queues.zigbee.licht.wijziging.of.beweging.gedetecteerd.exchangenaam", () -> "queues.zigbee.licht.wijziging.of.beweging.gedetecteerd.exchangenaam");
    }

    @SneakyThrows
    @Test
    @DisplayName("Test een bericht vanuit de open/dicht sensor")
    public void openDicht() {
        Client c = new Client(
                new ClientParameters()
                        .url("http://" + rabbitmq.getHost() + ":" + rabbitmq.getHttpPort() + "/api/")
                        .username(rabbitmq.getAdminUsername())
                        .password(rabbitmq.getAdminPassword())
        );

        c.declareQueue("/", QUEUE, new QueueInfo(false, true, false));
        c.bindQueue("/", QUEUE, exchangeNaam, null);

        var outboundMessage = new OutboundMessage();
        var openDichtInput = new OpenDichtInput(100, true, 25, 86, 8, 3215, false, false, "0l");
        outboundMessage.payload(new ObjectMapper().writeValueAsString(openDichtInput));
//        outboundMessage.payload("{\"battery\":100,\"contact\":true,\"device_temperature\":24,\"linkquality\":86,\"power_outage_count\":8,\"voltage\":3215}");

        c.publish("/", AMQ_TOPIC, routingkey, outboundMessage);

        await().atMost(60, SECONDS).until(() -> batterijRepository.findById("open-dicht-sensor").isPresent());
        var batterij = batterijRepository.findById("open-dicht-sensor").get();
        assertEquals(100, batterij.battery());
        assertNotNull(batterij.tijdstip());

        await().atMost(60, SECONDS).until(() -> openOfDichtRepository.findById("open-dicht-sensor").isPresent());
        var openOfDicht = openOfDichtRepository.findById("open-dicht-sensor").get();
        assertFalse(openOfDicht.open());
        assertNotNull(openOfDicht.tijdstip());

        await().atMost(1, MINUTES).pollDelay(1, SECONDS).until(() -> {
            var message = c.get("/", QUEUE);
            if (message != null) {
                var payload = message.getPayload();
                assertEquals("{\"id\":\"open-dicht-sensor\"}", payload);
            }
            return message != null;
        });
    }
}
