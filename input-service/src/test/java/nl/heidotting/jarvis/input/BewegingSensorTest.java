package nl.heidotting.jarvis.input;

import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.ClientParameters;
import com.rabbitmq.http.client.domain.OutboundMessage;
import com.rabbitmq.http.client.domain.QueueInfo;
import com.redis.testcontainers.RedisContainer;
import lombok.SneakyThrows;
import nl.heidotting.jarvis.input.repository.BatterijRepository;
import nl.heidotting.jarvis.input.repository.BewegingRepository;
import nl.heidotting.jarvis.input.repository.LichtsterkteRepository;
import nl.heidotting.jarvis.input.repository.TemperatuurRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.InfluxDBContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.UUID;

import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class BewegingSensorTest {
    @Autowired
    private BewegingRepository bewegingRepository;
    @Autowired
    private BatterijRepository batterijRepository;
    @Autowired
    private LichtsterkteRepository lichtsterkteRepository;
    @Autowired
    private TemperatuurRepository temperatuurRepository;

    @Value("${queues.zigbee.motion.detection.routingkey}")
    private String routingkey;
    @Value("${queues.zigbee.licht.wijziging.of.beweging.gedetecteerd.exchangenaam}")
    private String exchangenaam;

    private final String AMQ_TOPIC = "amq.topic";
    private final String QUEUE = this.getClass().getCanonicalName();
    private final static String ORG = "nl.heidotting";
    private final static String BUCKET = "jarvis";
    private final static String TOKEN = UUID.randomUUID().toString();

    private static RabbitMQContainer rabbitmq;
    private static RedisContainer redis;
    private static InfluxDBContainer influxDBContainer;

    static {
        rabbitmq = new RabbitMQContainer();
        rabbitmq.start();
        System.out.println(rabbitmq.getHttpPort());

        redis = new RedisContainer(DockerImageName.parse("redis:6.2.6"));
        redis.start();
        influxDBContainer = new InfluxDBContainer<>(
                DockerImageName.parse("influxdb:2.7.0")
        )
                .withOrganization(ORG)
                .withBucket(BUCKET)
                .withAdminToken(TOKEN);
        influxDBContainer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.rabbitmq.host", rabbitmq::getHost);
        registry.add("spring.rabbitmq.port", rabbitmq::getAmqpPort);
        registry.add("spring.rabbitmq.username", rabbitmq::getAdminUsername);
        registry.add("spring.rabbitmq.password", rabbitmq::getAdminPassword);
        registry.add("spring.rabbitmq.ssl.enabled", () -> false);
        registry.add("spring.data.redis.host", redis::getHost);
        registry.add("spring.data.redis.port", () -> redis.getMappedPort(6379));
        registry.add("influx.token", () -> TOKEN);
        registry.add("influx.org", () -> ORG);
        registry.add("influx.bucket", () -> BUCKET);
        registry.add("influx.ip", () -> influxDBContainer.getHost());
        registry.add("influx.port", () -> influxDBContainer.getMappedPort(8086));

        registry.add("queues.smartmeter.energy.queuenaam", () -> "queues.smartmeter.energy.queuenaam");
        registry.add("queues.smartmeter.energy.routingkey", () -> "queues.smartmeter.energy.routingkey");
        registry.add("queues.smartmeter.gasUsage.queuenaam", () -> "queues.smartmeter.gasUsage.queuenaam");
        registry.add("queues.smartmeter.gasUsage.routingkey", () -> "queues.smartmeter.gasUsage.routingkey");
        registry.add("queues.smartmeter.usage.queuenaam", () -> "queues.smartmeter.usage.queuenaam");
        registry.add("queues.smartmeter.usage.routingkey", () -> "queues.smartmeter.usage.routingkey");
        registry.add("queues.zigbee.motion.detection.queuenaam", () -> "queues.zigbee.motion.detection.queuenaam");
        registry.add("queues.zigbee.motion.detection.routingkey", () -> "queues.zigbee.motion.detection.routingkey");
        registry.add("queues.zigbee.opendicht.detection.queuenaam", () -> "queues.zigbee.opendicht.detection.queuenaam");
        registry.add("queues.zigbee.opendicht.detection.routingkey", () -> "queues.zigbee.opendicht.detection.routingkey");
        registry.add("queues.lamp.logeerkamer.statua.queuenaam", () -> "queues.lamp.logeerkamer.statua.queuenaam");
        registry.add("queues.lamp.logeerkamer.statua.routingkey", () -> "queues.lamp.logeerkamer.statua.routingkey");
        registry.add("queues.zigbee.opendicht.detection.exchangenaam", () -> "queues.zigbee.opendicht.detection.exchangenaam");
        registry.add("queues.zigbee.licht.wijziging.of.beweging.gedetecteerd.exchangenaam", () -> "queues.zigbee.licht.wijziging.of.beweging.gedetecteerd.exchangenaam");
    }

    @SneakyThrows
    @Test
    @DisplayName("Test een bericht vanuit de bewegingssensor")
    public void beweging() {
        Client c = new Client(
                new ClientParameters()
                        .url("http://" + rabbitmq.getHost() + ":" + rabbitmq.getHttpPort() + "/api/")
                        .username(rabbitmq.getAdminUsername())
                        .password(rabbitmq.getAdminPassword())
        );
        c.declareQueue("/", QUEUE, new QueueInfo(false, true, false));
        c.bindQueue("/", QUEUE, exchangenaam, null);

        var outboundMessage = new OutboundMessage();
        outboundMessage.payload("{\"battery\":100,\"detection_interval\":30,\"device_temperature\":26,\"illuminance\":53,\"linkquality\":110,\"motion_sensitivity\":\"high\",\"occupancy\":false,\"power_outage_count\":0,\"trigger_indicator\":false,\"voltage\":3183}");

        c.publish("/", AMQ_TOPIC, routingkey, outboundMessage);

        await().atMost(60, SECONDS).until(() -> batterijRepository.findById("kantoor").isPresent());
        var batterij = batterijRepository.findById("kantoor").get();
        assertEquals(100, batterij.battery());
        assertNotNull(batterij.tijdstip());

        await().atMost(60, SECONDS).until(() -> bewegingRepository.findById("kantoor").isPresent());
        var beweging = bewegingRepository.findById("kantoor").get();
        assertFalse(beweging.occupancy());
        assertNotNull(beweging.tijdstip());

        await().atMost(60, SECONDS).until(() -> lichtsterkteRepository.findById("kantoor").isPresent());
        var lichtsterkte = lichtsterkteRepository.findById("kantoor").get();
        assertEquals(53, lichtsterkte.illuminance());
        assertNotNull(lichtsterkte.tijdstip());

        await().atMost(60, SECONDS).until(() -> temperatuurRepository.findById("kantoor").isPresent());
        var temperatuur = temperatuurRepository.findById("kantoor").get();
        assertEquals(26, temperatuur.device_temperature());
        assertNotNull(temperatuur.tijdstip());

        await().atMost(1, MINUTES).pollDelay(1, SECONDS).until(() -> {
            var message = c.get("/", QUEUE);
            if (message != null) {
                var payload = message.getPayload();
                assertEquals("{}", payload);
            }
            return message != null;
        });

//        var influxDBClient = InfluxDBClientFactory.create("http://"+influxDBContainer.getHost()+":"+influxDBContainer.getMappedPort(8086), TOKEN.toCharArray(), ORG, BUCKET);
//        String flux = "String.format( "from(bucket:\"%s\") |> range(start:0) |> filter(fn: (r) => r[\"_measurement\"] == \"sensor\") |> filter(fn: (r) => r[\"sensor_id\"] == \"TLM0100\"or r[\"sensor_id\"] == \"TLM0101\" or r[\"sensor_id\"] == \"TLM0103\" or r[\"sensor_id\"] == \"TLM0200\") |> sort() |> yield(name: \"sort\")", BUCKET)";
//        String flux = "from(bucket: \""+BUCKET+"\") |> range(start:0)";// +
//        QueryApi queryApi = influxDBClient.getQueryApi();
//        List<FluxTable> tables = queryApi.query(flux);
//        System.out.println(tables);
//        for (FluxTable fluxTable : tables) {
//            System.out.println(fluxTable);
//            List<FluxRecord> records = fluxTable.getRecords();
//            System.out.println(records);
//            for (FluxRecord fluxRecord : records) {
//                System.out.println(fluxRecord);
//                System.out.println(fluxRecord.getValueByKey("sensor_id"));
//            }
//        }
    }
}
