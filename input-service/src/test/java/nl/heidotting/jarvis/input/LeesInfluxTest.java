package nl.heidotting.jarvis.input;

import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.QueryApi;
import com.influxdb.query.FluxRecord;
import com.influxdb.query.FluxTable;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoField;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class LeesInfluxTest {
    @Test
    public void testLeesInflux() {
        InfluxDBClient influxDBClient = InfluxDBClientFactory.create("http://192.168.7.119:8086", "BgQqzIfQgaujWylEHqrmjf1-cYzMYUEtIOtdOcOKz6YpPeWy_unRrTVOgM-SDterThfUjohCYfwj3ETgq1R_pw==".toCharArray(), "nl.heidotting", "J.A.R.V.I.S.");

        QueryApi queryApi = influxDBClient.getQueryApi();

//        queryApi.query()
        String flux = "from(bucket:\"J.A.R.V.I.S.\") |> range(start: -10d)";
//        List<FluxTable> tables = queryApi.query(flux);
//        for (FluxTable fluxTable : tables) {
//            List<FluxRecord> records = fluxTable.getRecords();
//            for (FluxRecord fluxRecord : records) {
//                if ("televisie".equals(fluxRecord.getValueByKey("sensor-id"))) {
//                if ("wattage".equals(fluxRecord.getValueByKey("_field"))) {
//                    System.out.println(fluxRecord.getTime() + ": " + fluxRecord.getValueByKey("_value"));//fluxRecord.getValueByKey("voltage") + ": " + fluxRecord.getValueByKey("wattage"));// + ": " + fluxRecord.getField() + ": " + fluxRecord.getValueByKey("voltage") + ": " + fluxRecord.getValues());
//                }
//                }
//            }
//        }

        Map<Instant, Map<String, Boolean>> waarden = new HashMap<>();
        Map<String, Instant> aanUit = new HashMap<>();

        queryApi.query(flux)
                .stream()
                .map(new Function<FluxTable, List<FluxRecord>>() {
                    @Override
                    public List<FluxRecord> apply(FluxTable fluxTable) {
                        return fluxTable.getRecords();
                    }
                })
                .flatMap(new Function<List<FluxRecord>, Stream<FluxRecord>>() {
                    @Override
                    public Stream<FluxRecord> apply(List<FluxRecord> fluxRecords) {
                        return fluxRecords.stream();
                    }
                })
                .filter(new Predicate<FluxRecord>() {
                    @Override
                    public boolean test(FluxRecord fluxRecord) {
                        return fluxRecord.getValueByKey("sensor-id").toString().startsWith("lamp-bij-de-");
                    }
                })
                .filter(new Predicate<FluxRecord>() {
                    @Override
                    public boolean test(FluxRecord fluxRecord) {
                        return "aanUitStatus1".equals(fluxRecord.getValueByKey("_field"));
                    }
                })
                .sorted(new Comparator<FluxRecord>() {
                    @Override
                    public int compare(FluxRecord o1, FluxRecord o2) {
                        return o1.getTime().compareTo(o2.getTime());
                    }
                })
                .forEach(new Consumer<FluxRecord>() {
                    @Override
                    public void accept(FluxRecord fluxRecord) {
                        var tijdstip = fluxRecord.getTime().with(ChronoField.MILLI_OF_SECOND, 0);
//                        var record = waarden.get(tijdstip);
//                        if(record==null){
//                            record=new HashMap<>();
//                        }
//                        record.put(fluxRecord.getValueByKey("sensor-id").toString(),(Long)fluxRecord.getValueByKey("_value")==1);
//                        waarden.put(tijdstip,record);

                        var key = fluxRecord.getValueByKey("sensor-id").toString() + "-" + fluxRecord.getValueByKey("_value");
                        if (aanUit.get(key) == null) {
                            aanUit.put(key, tijdstip);
                        }

//                        System.out.println(fluxRecord.getTime() + ": " + fluxRecord.getValueByKey("_value"));//fluxRecord.getValueByKey("voltage") + ": " + fluxRecord.getValueByKey("wattage"));// + ": " + fluxRecord.getField() + ": " + fluxRecord.getValueByKey("voltage") + ": " + fluxRecord.getValues());
                    }
                });
        aanUit.forEach(new BiConsumer<String, Instant>() {
            @Override
            public void accept(String s, Instant instant) {
                System.out.println(s + " - " + instant);
            }
        });
//        waarden.keySet()
//                .stream().sorted()
//                .forEach(new Consumer<Instant>() {
//            @Override
//            public void accept(Instant instant) {
//                System.out.println(instant);
//                waarden.get(instant).forEach(new BiConsumer<String, Boolean>() {
//                    @Override
//                    public void accept(String s, Boolean aBoolean) {
//                        System.out.println("                     - "+s+" - "+aBoolean);
//                    }
//                });
//
//            }
//        });
    }
}
