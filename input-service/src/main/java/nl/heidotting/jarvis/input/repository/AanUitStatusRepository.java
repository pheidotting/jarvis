package nl.heidotting.jarvis.input.repository;

import nl.heidotting.jarvis.input.model.AanUitStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AanUitStatusRepository extends CrudRepository<AanUitStatus, String> {
}
