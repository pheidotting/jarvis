package nl.heidotting.jarvis.input.model;

import org.springframework.data.redis.core.RedisHash;

import java.time.LocalDateTime;

@RedisHash(value = "Temperatuur", timeToLive = 2678400)//1 maand
public record Temperatuur(String id,
                          int device_temperature,
                          LocalDateTime tijdstip

) {
}
