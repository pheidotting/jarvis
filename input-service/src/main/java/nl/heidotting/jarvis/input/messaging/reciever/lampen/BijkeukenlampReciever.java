package nl.heidotting.jarvis.input.messaging.reciever.lampen;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.ShellyMiniAanUit;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class BijkeukenlampReciever {
    private final SensorService sensorService;

    @RabbitListener(
            queues = "#{bijkeukenlampQueueConfig.bijkeukenlampQueue()}"
    )
    public void recievedMessage(ShellyMiniAanUit onOff) throws JsonProcessingException {
        log.info("Bijkeuken lamp {}", onOff);
        sensorService.opslaan(onOff.output(), "bijkeuken-lamp");
    }
}
