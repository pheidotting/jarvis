package nl.heidotting.jarvis.input.model;

public record ShellyTemperature(
        double tC,
        double tF
) {
}
