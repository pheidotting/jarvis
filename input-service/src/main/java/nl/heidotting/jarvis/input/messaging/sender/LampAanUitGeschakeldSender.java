package nl.heidotting.jarvis.input.messaging.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.LichtWijzigingOfBewegingGedetecteerd;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LampAanUitGeschakeldSender extends AbstractPublisher<LichtWijzigingOfBewegingGedetecteerd> {
    @Value("${queues.lamp-aan-of-uit-geschakeld.exchangenaam}")
    private String topicNaam;

    @Override
    protected String topic() {
        return topicNaam;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}

