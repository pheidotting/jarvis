package nl.heidotting.jarvis.input.model;

public record Shelly(
        long id,
        String source,
        boolean output,
        double apower,
        double voltage,
        double current,
        ShellyAEnergy aenergy,
        ShellyTemperature temperature
) {
}
