package nl.heidotting.jarvis.input.config;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.QueueConfiger;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class ZigbeeMotionDetectionQueueConfig {
    @Value("${queues.zigbee.motion.detection.queuenaam}")
    private String queueNaam;
    @Value("${queues.zigbee.motion.detection.routingkey}")
    private String routingkey;

    private QueueConfiger queueConfiger;

    @PostConstruct
    public void init() {
        this.queueConfiger = new QueueConfiger()
                .withQueue(queueNaam)
                .withRoutingKey(routingkey)
                .withLogger(log)
                .withBinding()
                .build();
    }

    @Bean
    public Queue zigbeeMotionDetectionQueue() {
        return this.queueConfiger.getQueue();
    }

    @Bean
    public Binding zigbeeMotionDetectionBinding() {
        return this.queueConfiger.getBinding();
    }
}
