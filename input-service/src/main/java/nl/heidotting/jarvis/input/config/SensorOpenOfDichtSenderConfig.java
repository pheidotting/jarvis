package nl.heidotting.jarvis.input.config;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.QueueConfiger;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class SensorOpenOfDichtSenderConfig {
    @Value("${queues.zigbee.opendicht.detection.exchangenaam}")
    private String topicNaam;

    @Bean
    public FanoutExchange sensorOpenOfDichtExchange() {
        return new QueueConfiger()
                .withLogger(log)
                .withFanoutExchange(topicNaam)
                .build()
                .getFanoutExchange();
    }
}
