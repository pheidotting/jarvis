package nl.heidotting.jarvis.input.messaging.reciever.motiondection;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.BewegingInput;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class DetectieRechtsReciever {
    private final SensorService sensorService;

    @RabbitListener(queues = "#{detectieRechtsQueueConfig.detectieRechtsQueue()}")
    public void recievedMessage(BewegingInput bewegingInput) {
        log.info("{}", bewegingInput);
        sensorService.opslaan(bewegingInput, "detectie-rechts");
    }
}
