package nl.heidotting.jarvis.input.model;

public record ShellyAanUitVoordeur(
        String state,
        Long linkquality
) {
}
