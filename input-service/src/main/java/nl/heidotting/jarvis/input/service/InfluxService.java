package nl.heidotting.jarvis.input.service;

import com.google.gson.Gson;
import com.influxdb.client.InfluxDBClient;
import com.influxdb.client.InfluxDBClientFactory;
import com.influxdb.client.WriteApiBlocking;
import com.influxdb.client.domain.WritePrecision;
import com.influxdb.client.write.Point;
import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import lombok.extern.log4j.Log4j2;
import nl.heidotting.jarvis.input.model.AanUitStatus;
import nl.heidotting.jarvis.input.model.Batterij;
import nl.heidotting.jarvis.input.model.Beweging;
import nl.heidotting.jarvis.input.model.Lichtsterkte;
import nl.heidotting.jarvis.input.model.OpenOfDicht;
import nl.heidotting.jarvis.input.model.StroomGebruik;
import nl.heidotting.jarvis.input.model.Temperatuur;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Log4j2
@Service
public class InfluxService {
    @Value("${influx.org}")
    private String influxOrg;
    @Value("${influx.bucket}")
    private String influxBucket;
    @Value("${influx.token}")
    private String influxToken;
    @Value("${influx.ip}")
    private String influxIp;
    @Value("${influx.port}")
    private String influxPort;

    private static final String SENSOR_ID = "sensor-id";

    private WriteApiBlocking writeApi;
    private InfluxDBClient influxDBClient;

    @PostConstruct
    public void init() {
        log.info("Verbinding maken met Influx bucket {}", influxBucket);
        influxDBClient = InfluxDBClientFactory.create("http://" + influxIp + ":" + influxPort, influxToken.toCharArray(), influxOrg, influxBucket);
        writeApi = influxDBClient.getWriteApiBlocking();
    }

    @PreDestroy
    public void sluiten() {
        influxDBClient.close();
    }

    public void schrijfBatterij(Batterij batterij) {
        var point = Point.measurement("batterijpercentage")
                .addTag(SENSOR_ID, batterij.id())
                .addField("batterij", batterij.battery())
                .time(batterij.tijdstip().toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.info("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfBeweging(Beweging beweging) {
        var point = Point.measurement("bewegingdetectie")
                .addTag(SENSOR_ID, beweging.id())
                .addField("beweging1", beweging.occupancy() ? 1 : 0)
                .time(beweging.tijdstip().toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.info("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfLichtsterkte(Lichtsterkte lichtsterkte) {
        var point = Point.measurement("lichtsterkte")
                .addTag(SENSOR_ID, lichtsterkte.id())
                .addField("lichtsterkte", lichtsterkte.illuminance())
                .time(lichtsterkte.tijdstip().toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.info("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfStroomGebruik(StroomGebruik stroomGebruik) {
        var point = Point.measurement("stroomgebruik")
                .addTag(SENSOR_ID, stroomGebruik.id())
                .addField("voltage", stroomGebruik.voltage())
                .addField("wattage", stroomGebruik.wattage())
                .time(stroomGebruik.tijdstip().toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.info("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfOpenOfDicht(OpenOfDicht openOfDicht) {
        var point = Point.measurement("openOfDicht")
                .addTag(SENSOR_ID, openOfDicht.id())
                .addField("openOfGesloten", openOfDicht.open() ? 1 : 0)
                .time(openOfDicht.tijdstip().toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.info("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfTemperatuur(Temperatuur temperatuur) {
        var point = Point.measurement("temperatuur")
                .addTag(SENSOR_ID, temperatuur.id())
                .addField("temperatuur", temperatuur.device_temperature())
                .time(temperatuur.tijdstip().toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.info("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfAanUitStatus(AanUitStatus aanUitStatus) {
        var point = Point.measurement("aanUitStatus")
                .addTag(SENSOR_ID, aanUitStatus.id())
                .addField("aanUitStatus1", aanUitStatus.aan() ? 1 : 0)
                .time(aanUitStatus.tijdstip().toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.info("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfSmartmeterEnergy(JSONObject jsonObject) {
        var tijdstip = LocalDateTime.parse(jsonObject.getString("powerTs"));
        var point = Point.measurement("p1smartmeter")
                .addField("energy", jsonObject.getBigDecimal("currentUsage"))
                .time(tijdstip.toInstant(ZoneOffset.of("+02:00")), WritePrecision.MS);
        log.trace("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfSmartmeterGas(JSONObject jsonObject) {
        var point = Point.measurement("p1smartmeter")
                .addField("gas", jsonObject.getInt("val"))
                .time(jsonObject.getLong("tc"), WritePrecision.MS);
        log.trace("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }

    public void schrijfSmartmeterUsage(JSONObject jsonObject) {
        var point = Point.measurement("p1smartmeter")
                .addField("usage", jsonObject.getInt("val"))
                .time(jsonObject.getLong("tc"), WritePrecision.MS);
        log.trace("Wegschrijven {}", new Gson().toJson(point));
        writeApi.writePoint(point);
    }
}
