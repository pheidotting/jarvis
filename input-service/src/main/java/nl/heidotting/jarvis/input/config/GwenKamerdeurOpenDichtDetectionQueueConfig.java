package nl.heidotting.jarvis.input.config;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.QueueConfiger;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class GwenKamerdeurOpenDichtDetectionQueueConfig {
    @Value("${queues.zigbee.gwen-kamerdeur.detection.queuenaam}")
    private String queueNaam;
    @Value("${queues.zigbee.gwen-kamerdeur.detection.routingkey}")
    private String routingkey;


    private QueueConfiger queueConfiger;

    @PostConstruct
    public void init() {
        this.queueConfiger = new QueueConfiger()
                .withQueue(queueNaam)
                .withRoutingKey(routingkey)
                .withLogger(log)
                .withBinding()
                .build();
    }

    @Bean
    public Queue gwenKamerdeurOpenDichtDetectionQueue() {
        return this.queueConfiger.getQueue();
    }

    @Bean
    public Binding gwenKamerdeurOpenDichtDetectionBinding() {
        return this.queueConfiger.getBinding();
    }
}
