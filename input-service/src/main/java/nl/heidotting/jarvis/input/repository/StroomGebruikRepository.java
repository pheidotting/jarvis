package nl.heidotting.jarvis.input.repository;

import nl.heidotting.jarvis.input.model.StroomGebruik;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StroomGebruikRepository extends CrudRepository<StroomGebruik, String> {
}
