package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.input.model.Temperatuur;
import nl.heidotting.jarvis.input.repository.TemperatuurRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TemperatuurService {
    private final TemperatuurRepository temperatuurRepository;
    private final InfluxService influxService;

    public void save(Temperatuur temperatuur) {
        save(temperatuur, true, true);
    }

    public void save(Temperatuur temperatuur, boolean redis, boolean influx) {
        if (redis) {
            temperatuurRepository.save(temperatuur);
        }
        if (influx) {
            influxService.schrijfTemperatuur(temperatuur);
        }
    }

    public Optional<Temperatuur> findById(String id) {
        return temperatuurRepository.findById(id);
    }
}
