package nl.heidotting.jarvis.input.model;

import org.springframework.data.redis.core.RedisHash;

import java.time.LocalDateTime;

@RedisHash(value = "OpenOfDicht", timeToLive = 2678400)//1 maand
public record OpenOfDicht(String id,
                          boolean open,
                          LocalDateTime tijdstip
) {
}
