package nl.heidotting.jarvis.input.messaging.reciever;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.OpenDichtInput;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class GwenKamerdeurReciever {
    private final SensorService sensorService;

    @RabbitListener(queues = "#{gwenKamerdeurOpenDichtDetectionQueueConfig.gwenKamerdeurOpenDichtDetectionQueue()}")
    public void recievedMessage(OpenDichtInput openDichtInput) {
        log.info("{}", openDichtInput);
        sensorService.opslaan(openDichtInput, "gwen-kamerdeur-sensor");
    }
}
