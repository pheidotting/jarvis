package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.input.model.StroomGebruik;
import nl.heidotting.jarvis.input.repository.StroomGebruikRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StroomGebruikService {
    private final StroomGebruikRepository stroomGebruikRepository;
    private final InfluxService influxService;

    public void save(StroomGebruik stroomGebruik) {
        save(stroomGebruik, true, true);
    }

    public void save(StroomGebruik stroomGebruik, boolean redis, boolean influx) {
        if (redis) {
            stroomGebruikRepository.save(stroomGebruik);
        }
        if (influx) {
            influxService.schrijfStroomGebruik(stroomGebruik);
        }
    }

    public Optional<StroomGebruik> findById(String id) {
        return stroomGebruikRepository.findById(id);
    }
}
