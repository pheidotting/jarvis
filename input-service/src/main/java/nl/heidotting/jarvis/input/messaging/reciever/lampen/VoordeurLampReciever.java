package nl.heidotting.jarvis.input.messaging.reciever.lampen;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.ShellyAanUitVoordeur;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class VoordeurLampReciever {
    //rihkih-Cescud-2kyrka
    private final SensorService sensorService;

    @RabbitListener(
            queues = "#{voordeurLampQueueConfig.voordeurLampQueue()}"
    )
    public void recievedMessage(ShellyAanUitVoordeur onOff) throws JsonProcessingException {
        log.info("VoordeurLamp {}", onOff);
        sensorService.opslaan("on".equalsIgnoreCase(onOff.state()), "voordeur-lamp");
    }
}
