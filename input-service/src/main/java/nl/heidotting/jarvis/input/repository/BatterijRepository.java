package nl.heidotting.jarvis.input.repository;

import nl.heidotting.jarvis.input.model.Batterij;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BatterijRepository extends CrudRepository<Batterij, String> {
}
