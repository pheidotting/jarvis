package nl.heidotting.jarvis.input.messaging.reciever;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.BewegingInput;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class ZigBeeMotionDetectionReciever {
    private final SensorService sensorService;

    @RabbitListener(queues = "#{zigbeeMotionDetectionQueueConfig.zigbeeMotionDetectionQueue()}")
    public void recievedMessage(BewegingInput bewegingInput) {
        sensorService.opslaan(bewegingInput, "kantoor");
    }
}
