package nl.heidotting.jarvis.input.model;

import org.springframework.data.redis.core.RedisHash;

import java.time.LocalDateTime;

@RedisHash(value = "StroomGebruik", timeToLive = 2678400)//1 maand
public record StroomGebruik(
        String id,
        double voltage,
        double wattage,
        LocalDateTime tijdstip
) {

}
