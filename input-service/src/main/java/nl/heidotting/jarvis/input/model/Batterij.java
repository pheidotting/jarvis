package nl.heidotting.jarvis.input.model;

import org.springframework.data.redis.core.RedisHash;

import java.time.LocalDateTime;

@RedisHash(value = "Batterij", timeToLive = 2678400)//1 maand
public record Batterij(String id,
                       int battery,
                       LocalDateTime tijdstip
) {
}
