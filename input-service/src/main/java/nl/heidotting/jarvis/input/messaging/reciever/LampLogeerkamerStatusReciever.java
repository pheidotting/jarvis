package nl.heidotting.jarvis.input.messaging.reciever;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.IkeaLampStatus;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class LampLogeerkamerStatusReciever {
    private final SensorService sensorService;

    @RabbitListener(queues = "#{lampLogeerkamerStatusQueueConfig.lampLogeerkamerStatusQueue()}")
    public void recievedMessage(IkeaLampStatus ikeaLampStatus) {
        sensorService.opslaan(ikeaLampStatus, "logeerkamer");
    }
}
