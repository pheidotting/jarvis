package nl.heidotting.jarvis.input.model;

import java.io.Serializable;

public record IkeaLampStatus(
        int brightness,
        String color_mode,
        int color_temp,
        int linkquality,
        String state,
        IkeaLampStatusUpdate update,
        boolean update_available,
        String power_on_behavior,
        int color_temp_startup
) implements Serializable {
}
