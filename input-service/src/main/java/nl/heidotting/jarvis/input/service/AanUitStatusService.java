package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.input.model.AanUitStatus;
import nl.heidotting.jarvis.input.repository.AanUitStatusRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AanUitStatusService {
    private final AanUitStatusRepository aanUitStatusRepository;
    private final InfluxService influxService;

    public void save(AanUitStatus aanUitStatus) {
        save(aanUitStatus, true, true);
    }

    public void save(AanUitStatus aanUitStatus, boolean redis, boolean influx) {
        if (redis) {
            aanUitStatusRepository.save(aanUitStatus);
        }
        if (influx) {
            influxService.schrijfAanUitStatus(aanUitStatus);
        }
    }

    public Optional<AanUitStatus> findById(String id) {
        return aanUitStatusRepository.findById(id);
    }
}
