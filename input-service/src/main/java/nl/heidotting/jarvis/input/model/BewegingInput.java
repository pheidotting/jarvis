package nl.heidotting.jarvis.input.model;

public record BewegingInput(
        int battery,
        int detection_interval,
        int device_temperature,
        int illuminance,
        int linkquality,
        String motion_sensitivity,
        boolean occupancy,
        int power_outage_count,
        boolean trigger_indicator,
        int voltage,
        NoOccupancySince no_occupancy_since
) {
}
