package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.input.model.Beweging;
import nl.heidotting.jarvis.input.repository.BewegingRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BewegingService {
    private final BewegingRepository bewegingRepository;
    private final InfluxService influxService;

    public void save(Beweging beweging) {
        save(beweging, true, true);
    }

    public void save(Beweging beweging, boolean redis, boolean influx) {
        if (redis) {
            bewegingRepository.save(beweging);
        }
        if (influx) {
            influxService.schrijfBeweging(beweging);
        }
    }

    public Optional<Beweging> findById(String id) {
        return bewegingRepository.findById(id);
    }
}
