package nl.heidotting.jarvis.input.messaging.reciever.lampen;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.JsonObject;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.ShellyAanUit;
import nl.heidotting.jarvis.input.model.ShellyAanUitVoordeur;
import nl.heidotting.jarvis.input.model.ShellyMiniAanUit;
import nl.heidotting.jarvis.input.service.SensorService;
import org.json.JSONObject;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class BijkeukenBuitenlampReciever {
    private final SensorService sensorService;

    @RabbitListener(
            queues = "#{bijkeukenBuitenlampQueueConfig.bijkeukenBuitenlampQueue()}"
    )
    public void recievedMessage(ShellyMiniAanUit onOff) throws JsonProcessingException {
        log.info("Bijkeuken buitenlamp {}", onOff);
        sensorService.opslaan(onOff.output(), "bijkeuken-buitenlamp");
    }
}
