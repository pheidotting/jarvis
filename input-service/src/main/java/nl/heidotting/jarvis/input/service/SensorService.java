package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import nl.heidotting.jarvis.input.messaging.sender.LampAanUitGeschakeldSender;
import nl.heidotting.jarvis.input.messaging.sender.LichtWijzigingOfBewegingGedetecteerdSender;
import nl.heidotting.jarvis.input.messaging.sender.SensorOpenOfDichtSender;
import nl.heidotting.jarvis.input.model.AanUitStatus;
import nl.heidotting.jarvis.input.model.Batterij;
import nl.heidotting.jarvis.input.model.Beweging;
import nl.heidotting.jarvis.input.model.BewegingInput;
import nl.heidotting.jarvis.input.model.IkeaLampStatus;
import nl.heidotting.jarvis.input.model.Lichtsterkte;
import nl.heidotting.jarvis.input.model.OpenDichtInput;
import nl.heidotting.jarvis.input.model.OpenOfDicht;
import nl.heidotting.jarvis.input.model.Shelly;
import nl.heidotting.jarvis.input.model.StroomGebruik;
import nl.heidotting.jarvis.input.model.Temperatuur;
import nl.heidotting.jarvis.shared.mqtt.messages.LichtWijzigingOfBewegingGedetecteerd;
import nl.heidotting.jarvis.shared.mqtt.messages.SensorOpenOfDicht;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Log4j2
@Service
@RequiredArgsConstructor
public class SensorService {
    private final BatterijService batterijService;
    private final BewegingService bewegingService;
    private final LichtsterkteService lichtsterkteService;
    private final OpenOfDichtService openOfDichtService;
    private final TemperatuurService temperatuurService;
    private final AanUitStatusService aanUitStatusService;
    private final StroomGebruikService stroomGebruikService;
    private final LichtWijzigingOfBewegingGedetecteerdSender lichtWijzigingOfBewegingGedetecteerdSender;
    private final SensorOpenOfDichtSender sensorOpenOfDichtSender;
    private final LampAanUitGeschakeldSender lampAanUitGeschakeldSender;

    public void opslaan(IkeaLampStatus ikeaLampStatus, String id) {
        log.info("id {}, input {}", id, ikeaLampStatus);
        var tijdstip = LocalDateTime.now();

        var status = new AanUitStatus(id, tijdstip, "ON".equals(ikeaLampStatus.state()));

        var laatsteStatus = aanUitStatusService.findById(id);
        if (laatsteStatus.isPresent() && laatsteStatus.get().aan() != status.aan()) {
            aanUitStatusService.save(status);
        } else if (laatsteStatus.isEmpty()) {
            aanUitStatusService.save(status);
        } else {
            aanUitStatusService.save(status, false, true);
        }
    }

    public void opslaan(boolean aanUit, String id) {
        log.info("id {}, input {}", id, aanUit);
        var tijdstip = LocalDateTime.now();
        var status = new AanUitStatus(id, tijdstip, aanUit);

        var laatsteStatus = aanUitStatusService.findById(id);
        if (laatsteStatus.isPresent() && laatsteStatus.get().aan() != status.aan()) {
            aanUitStatusService.save(status);
            if (!"voordeur-lamp".equals(id)) {
                lampAanUitGeschakeldSender.stuur(new LichtWijzigingOfBewegingGedetecteerd());
            }
        } else if (laatsteStatus.isEmpty()) {
            aanUitStatusService.save(status);
            if (!"voordeur-lamp".equals(id)) {
                lampAanUitGeschakeldSender.stuur(new LichtWijzigingOfBewegingGedetecteerd());
            }
        } else {
            aanUitStatusService.save(status, false, true);
        }
        lichtWijzigingOfBewegingGedetecteerdSender.stuur(new LichtWijzigingOfBewegingGedetecteerd());
    }

    public void opslaan(Shelly shelly, String id) {
        log.info("id {}, input {}", id, shelly);
        var tijdstip = LocalDateTime.now();

        var stroomGebruik = new StroomGebruik(id, shelly.voltage(), shelly.apower(), tijdstip);

        var laatsteStroomGebruik = stroomGebruikService.findById(id);
        if (laatsteStroomGebruik.isPresent() && (laatsteStroomGebruik.get().voltage() != stroomGebruik.voltage() || laatsteStroomGebruik.get().wattage() != stroomGebruik.wattage())) {
            stroomGebruikService.save(stroomGebruik);
        } else if (laatsteStroomGebruik.isEmpty()) {
            stroomGebruikService.save(stroomGebruik);
        } else {
            stroomGebruikService.save(stroomGebruik, false, true);
        }
        lichtWijzigingOfBewegingGedetecteerdSender.stuur(new LichtWijzigingOfBewegingGedetecteerd());
    }

    public void opslaan(BewegingInput bewegingInput, String id) {
        log.info("id {}, input {}", id, bewegingInput);
        var tijdstip = LocalDateTime.now();
        var batterij = new Batterij(id, bewegingInput.battery(), tijdstip);
        var beweging = new Beweging(id, bewegingInput.occupancy(), tijdstip);
        var lichtsterkte = new Lichtsterkte(id, bewegingInput.illuminance(), tijdstip);
        var temperatuur = new Temperatuur(id, bewegingInput.device_temperature(), tijdstip);

        var laatsteBatterij = batterijService.findById(id);
        var laatsteBeweging = bewegingService.findById(id);
        var laatsteLichtsterkte = lichtsterkteService.findById(id);
        var laatsteTemperatuur = temperatuurService.findById(id);

        if (laatsteBatterij.isPresent() && laatsteBatterij.get().battery() != batterij.battery()) {
            batterijService.save(batterij);
        } else if (laatsteBatterij.isEmpty()) {
            batterijService.save(batterij);
        } else {
            batterijService.save(batterij, false, true);
        }
        if (laatsteBeweging.isPresent() && laatsteBeweging.get().occupancy() != beweging.occupancy()) {
            bewegingService.save(beweging);
        } else if (laatsteBeweging.isEmpty()) {
            bewegingService.save(beweging);
        } else {
            bewegingService.save(beweging, false, true);
        }
        if (laatsteLichtsterkte.isPresent() && laatsteLichtsterkte.get().illuminance() != lichtsterkte.illuminance()) {
            lichtsterkteService.save(lichtsterkte);
        } else if (laatsteLichtsterkte.isEmpty()) {
            lichtsterkteService.save(lichtsterkte);
        } else {
            lichtsterkteService.save(lichtsterkte, false, true);
        }
        if (laatsteTemperatuur.isPresent() && laatsteTemperatuur.get().device_temperature() != temperatuur.device_temperature()) {
            temperatuurService.save(temperatuur);
        } else if (laatsteTemperatuur.isEmpty()) {
            temperatuurService.save(temperatuur);
        } else {
            temperatuurService.save(temperatuur, false, true);
        }
        lichtWijzigingOfBewegingGedetecteerdSender.stuur(new LichtWijzigingOfBewegingGedetecteerd());
    }

    public void opslaan(OpenDichtInput openDichtInput, String id) {
        log.info("id {}, input {}", id, openDichtInput);
        var tijdstip = LocalDateTime.now();
        var batterij = new Batterij(id, openDichtInput.battery(), tijdstip);
        var openOfDicht = new OpenOfDicht(id, !openDichtInput.contact(), tijdstip);
        var temperatuur = new Temperatuur(id, openDichtInput.device_temperature(), tijdstip);

        var laatsteBatterij = batterijService.findById(id);
        var laatsteOpenOfDicht = openOfDichtService.findById(id);
        var laatsteTemperatuur = temperatuurService.findById(id);

        if (laatsteBatterij.isPresent() && laatsteBatterij.get().battery() != batterij.battery()) {
            batterijService.save(batterij);
        } else if (laatsteBatterij.isEmpty()) {
            batterijService.save(batterij);
        } else {
            batterijService.save(batterij, false, true);
        }
        if (laatsteOpenOfDicht.isPresent() && laatsteOpenOfDicht.get().open() != openOfDicht.open()) {
            openOfDichtService.save(openOfDicht);
        } else if (laatsteOpenOfDicht.isEmpty()) {
            openOfDichtService.save(openOfDicht);
        } else {
            openOfDichtService.save(openOfDicht, false, true);
        }
        if (laatsteTemperatuur.isPresent() && laatsteTemperatuur.get().device_temperature() != temperatuur.device_temperature()) {
            temperatuurService.save(temperatuur);
        } else if (laatsteTemperatuur.isEmpty()) {
            temperatuurService.save(temperatuur);
        } else {
            temperatuurService.save(temperatuur, false, true);
        }

        sensorOpenOfDichtSender.stuur(new SensorOpenOfDicht(id));
    }
}
