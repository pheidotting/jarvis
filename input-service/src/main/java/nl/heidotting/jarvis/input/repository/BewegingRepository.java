package nl.heidotting.jarvis.input.repository;

import nl.heidotting.jarvis.input.model.Beweging;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BewegingRepository extends CrudRepository<Beweging, String> {
}
