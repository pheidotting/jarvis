package nl.heidotting.jarvis.input.messaging.reciever;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.OpenDichtInput;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

@Slf4j
//@Component
@RequiredArgsConstructor
public class ZigBeeOpenDichtDetectionReciever {
    private final SensorService sensorService;

    @RabbitListener(queues = "#{zigbeeOpenDichtDetectionQueueConfig.zigbeeOpenDichtDetectionQueue()}")
    public void recievedMessage(OpenDichtInput openDichtInput) {
        log.info("{}", openDichtInput);
        sensorService.opslaan(openDichtInput, "open-dicht-sensor");

    }
}
