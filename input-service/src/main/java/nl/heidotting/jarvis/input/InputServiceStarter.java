package nl.heidotting.jarvis.input;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.messaging.converter.StringMessageConverter;
import nl.heidotting.jarvis.shared.services.AbstractStarter;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.rabbit.config.SimpleRabbitListenerContainerFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@Slf4j
@SpringBootApplication(scanBasePackages = {"nl.heidotting.jarvis.input"})
public class InputServiceStarter extends AbstractStarter {
    public static void main(String[] args) {
        SpringApplication.run(InputServiceStarter.class, args);
    }

    @Bean("stringMessageConverter")
    public StringMessageConverter methodMessageConverter() {
        StringMessageConverter converter = new StringMessageConverter();
        return converter;
    }


    @Bean
    public MessageConverter simpleMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(simpleMessageConverter());

        return rabbitTemplate;
    }

    @Bean
    public SimpleRabbitListenerContainerFactory rabbitListenerContainerFactory(ConnectionFactory connectionFactory) {
        SimpleRabbitListenerContainerFactory factory = new SimpleRabbitListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory);
        factory.setMessageConverter(simpleMessageConverter());
//        factory.setConcurrentConsumers(concurrentConsumers);
//        factory.setMaxConcurrentConsumers(maxConcurrentConsumers);
//        factory.setErrorHandler(errorHandler());
        return factory;
    }
}