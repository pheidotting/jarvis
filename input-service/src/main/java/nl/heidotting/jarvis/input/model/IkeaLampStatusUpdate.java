package nl.heidotting.jarvis.input.model;

public record IkeaLampStatusUpdate(
        int latest_version,
        int installed_version,
        String state
) {
}
