package nl.heidotting.jarvis.input.messaging.reciever.lampen;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class BijDeBankReciever {
    //rihkih-Cescud-2kyrka
    private final SensorService sensorService;

    @RabbitListener(
            queues = "#{lampenBijDeBankQueueConfig.lampenBijDeBankQueue()}",
            messageConverter = "stringMessageConverter"
    )
    public void recievedMessageBijDeBank(String onOff) {
        log.info("lampenBijDeBankQueue {}", onOff);
        sensorService.opslaan("on".equalsIgnoreCase(onOff), "lamp-bij-de-bank");
    }

    @RabbitListener(
            queues = "#{lampenBijDeEettafelQueueConfig.lampenBijDeEettafelQueue()}",
            messageConverter = "stringMessageConverter"
    )
    public void recievedMessageBijDeEettafel(String onOff) {
        log.info("lampenBijDeEettafelQueue {}", onOff);
        sensorService.opslaan("on".equalsIgnoreCase(onOff), "lamp-bij-de-eettafel");
    }

    @RabbitListener(
            queues = "#{lampenBijDePuiQueueConfig.lampenBijDePuiQueue()}",
            messageConverter = "stringMessageConverter"
    )
    public void recievedMessageBijDePui(String onOff) {
        log.info("lampenBijDePuiQueue {}", onOff);
        sensorService.opslaan("on".equalsIgnoreCase(onOff), "lamp-bij-de-pui");
    }

    @RabbitListener(
            queues = "#{lampenBijDeTvQueueConfig.lampenBijDeTvQueue()}",
            messageConverter = "stringMessageConverter"
    )
    public void recievedMessageBijDeTv(String onOff) {
        log.info("lampenBijDeTvQueue {}", onOff);
        sensorService.opslaan("on".equalsIgnoreCase(onOff), "lamp-bij-de-tv");
    }
}
