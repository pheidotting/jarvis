package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.input.model.Lichtsterkte;
import nl.heidotting.jarvis.input.repository.LichtsterkteRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class LichtsterkteService {
    private final LichtsterkteRepository lichtsterkteRepository;
    private final InfluxService influxService;

    public void save(Lichtsterkte lichtsterkte) {
        save(lichtsterkte, true, true);
    }

    public void save(Lichtsterkte lichtsterkte, boolean redis, boolean influx) {
        if (redis) {
            lichtsterkteRepository.save(lichtsterkte);
        }
        if (influx) {
            influxService.schrijfLichtsterkte(lichtsterkte);
        }
    }

    public Optional<Lichtsterkte> findById(String id) {
        return lichtsterkteRepository.findById(id);
    }
}
