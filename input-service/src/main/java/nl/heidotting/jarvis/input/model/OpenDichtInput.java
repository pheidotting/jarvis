package nl.heidotting.jarvis.input.model;

public record OpenDichtInput(
        int battery,
        boolean contact,
        int device_temperature,
        int linkquality,
        int power_outage_count,
        int voltage,
        boolean battery_low,
        boolean tamper,
        String id) {
}
