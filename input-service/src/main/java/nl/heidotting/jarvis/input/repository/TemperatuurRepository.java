package nl.heidotting.jarvis.input.repository;

import nl.heidotting.jarvis.input.model.Temperatuur;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TemperatuurRepository extends CrudRepository<Temperatuur, String> {
}
