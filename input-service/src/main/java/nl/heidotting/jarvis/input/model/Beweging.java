package nl.heidotting.jarvis.input.model;

import org.springframework.data.redis.core.RedisHash;

import java.time.LocalDateTime;

@RedisHash(value = "Beweging", timeToLive = 2678400)//1 maand
public record Beweging(
        String id,
        boolean occupancy,
        LocalDateTime tijdstip) {
}
