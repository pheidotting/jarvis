package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.input.model.Batterij;
import nl.heidotting.jarvis.input.repository.BatterijRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BatterijService {
    private final BatterijRepository batterijRepository;
    private final InfluxService influxService;

    public void save(Batterij batterij) {
        save(batterij, true, true);
    }

    public void save(Batterij batterij, boolean redis, boolean influx) {
        if (redis) {
            batterijRepository.save(batterij);
        }
        if (influx) {
            influxService.schrijfBatterij(batterij);
        }
    }

    public Optional<Batterij> findById(String id) {
        return batterijRepository.findById(id);
    }
}
