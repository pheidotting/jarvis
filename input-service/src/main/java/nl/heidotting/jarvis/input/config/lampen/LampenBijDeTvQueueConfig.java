package nl.heidotting.jarvis.input.config.lampen;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.QueueConfiger;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Slf4j
@Configuration
public class LampenBijDeTvQueueConfig {
    @Value("${lampen.bijdetv.queuenaam}")
    private String queueNaam;
    @Value("${lampen.bijdetv.routingkey}")
    private String routingkey;


    private QueueConfiger queueConfiger;

    @PostConstruct
    public void init() {
        this.queueConfiger = new QueueConfiger()
                .withQueue(queueNaam)
                .withRoutingKey(routingkey)
                .withLogger(log)
                .withBinding()
                .build();
    }

    @Bean
    public Queue lampenBijDeTvQueue() {
        return this.queueConfiger.getQueue();
    }

    @Bean
    public Binding lampenBijDeTvBinding() {
        return this.queueConfiger.getBinding();
    }
}
