package nl.heidotting.jarvis.input.model;

public record ShellyAEnergy(
        long total,
        Object by_minute,
        long minute_ts
) {
}
