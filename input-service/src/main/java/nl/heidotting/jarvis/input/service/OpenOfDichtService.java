package nl.heidotting.jarvis.input.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.input.model.OpenOfDicht;
import nl.heidotting.jarvis.input.repository.OpenOfDichtRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class OpenOfDichtService {
    private final OpenOfDichtRepository openOfDichtRepository;
    private final InfluxService influxService;

    public void save(OpenOfDicht openOfDicht) {
        save(openOfDicht, true, true);
    }

    public void save(OpenOfDicht openOfDicht, boolean redis, boolean influx) {
        if (redis) {
            openOfDichtRepository.save(openOfDicht);
        }
        if (influx) {
            influxService.schrijfOpenOfDicht(openOfDicht);
        }
    }

    public Optional<OpenOfDicht> findById(String id) {
        return openOfDichtRepository.findById(id);
    }
}
