package nl.heidotting.jarvis.input.config;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.QueueConfiger;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class LichtWijzigingOfBewegingGedetecteerdConfig {
    @Value("${queues.zigbee.licht.wijziging.of.beweging.gedetecteerd.exchangenaam}")
    private String topicNaam;

    @Bean
    public FanoutExchange lichtWijzigingOfBewegingGedetecteerd() {
        return new QueueConfiger()
                .withLogger(log)
                .withFanoutExchange(topicNaam)
                .build()
                .getFanoutExchange();
    }
}
