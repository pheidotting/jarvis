package nl.heidotting.jarvis.input.repository;

import nl.heidotting.jarvis.input.model.Lichtsterkte;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LichtsterkteRepository extends CrudRepository<Lichtsterkte, String> {
}
