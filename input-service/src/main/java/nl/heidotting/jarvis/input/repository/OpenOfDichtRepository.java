package nl.heidotting.jarvis.input.repository;

import nl.heidotting.jarvis.input.model.OpenOfDicht;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OpenOfDichtRepository extends CrudRepository<OpenOfDicht, String> {
}
