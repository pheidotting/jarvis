package nl.heidotting.jarvis.input.messaging.reciever;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.Shelly;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class TelevisieReciever {
    private final SensorService sensorService;

    @RabbitListener(queues = "#{televisieQueueConfig.televisieQueue()}")
    public void recievedMessage(Shelly shelly) {
        log.info("{}", shelly);
        sensorService.opslaan(shelly, "televisie");

    }
}
