package nl.heidotting.jarvis.input.messaging.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.LichtWijzigingOfBewegingGedetecteerd;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class LichtWijzigingOfBewegingGedetecteerdSender extends AbstractPublisher<LichtWijzigingOfBewegingGedetecteerd> {
    @Value("${queues.zigbee.licht.wijziging.of.beweging.gedetecteerd.exchangenaam}")
    private String topicNaam;

    @Override
    protected String topic() {
        return topicNaam;
    }

    @Override
    protected String routingKey() {
        return null;
    }

    public void stuur() {
        rabbitTemplate.convertAndSend(topic(), routingKey(), "a");
    }
}

