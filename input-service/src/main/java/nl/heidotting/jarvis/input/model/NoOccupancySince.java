package nl.heidotting.jarvis.input.model;

public record NoOccupancySince(
        int undefined) {
}
