package nl.heidotting.jarvis.input.messaging.reciever.lampen;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.model.ShellyMiniAanUit;
import nl.heidotting.jarvis.input.service.SensorService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class BuitenlampSchuurReciever {
    private final SensorService sensorService;

    @RabbitListener(
            queues = "#{buitenlampSchuurQueueConfig.buitenlampSchuurQueue()}"
    )
    public void recievedMessage(ShellyMiniAanUit onOff) throws JsonProcessingException {
        log.info("Bijkeuken buitenlamp {}", onOff);
        sensorService.opslaan(onOff.output(), "buitenlamp-schuur");
    }
}
