package nl.heidotting.jarvis.input.messaging.reciever.smartmeter;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.input.service.InfluxService;
import org.json.JSONObject;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class SmartmeterEnergyReciever {

    private final InfluxService influxService;

    @RabbitListener(queues = "#{smartmeterEnergyQueueConfig.smartmeterEnergyQueue()}")
    public void recievedMessage(final Message message) {
        influxService.schrijfSmartmeterEnergy(new JSONObject(new String(message.getBody())));
    }

}
