package nl.heidotting.jarvis.input.messaging.sender;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.SensorOpenOfDicht;
import org.springframework.amqp.core.Message;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class SensorOpenOfDichtSender extends AbstractPublisher<SensorOpenOfDicht> {
    @Value("${queues.zigbee.opendicht.detection.exchangenaam}")
    private String topicNaam;

    @Override
    protected String topic() {
        return topicNaam;
    }

    @Override
    protected String routingKey() {
        return null;
    }

    @Override
    public void stuur(SensorOpenOfDicht sensorOpenOfDicht) {
        log.info("Topic : {}\nBericht : {}", topic(), sensorOpenOfDicht);
        try {
            Message message = new Message(new ObjectMapper().writeValueAsBytes(sensorOpenOfDicht));
            rabbitTemplate.send(topic(), routingKey(), message);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
//        rabbitTemplate.convertAndSend(topic(), routingKey(), sensorOpenOfDicht);
    }
}
