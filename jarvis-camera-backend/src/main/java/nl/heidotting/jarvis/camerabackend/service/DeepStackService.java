package nl.heidotting.jarvis.camerabackend.service;

import io.github.resilience4j.ratelimiter.RateLimiter;
import io.github.resilience4j.ratelimiter.RateLimiterConfig;
import io.github.resilience4j.ratelimiter.RateLimiterRegistry;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.camerabackend.mqtt.sender.GezichtVerwerktSender;
import nl.heidotting.jarvis.camerabackend.mqtt.sender.PersoonGesignaleerdSender;
import nl.heidotting.jarvis.shared.mqtt.messages.PersoonGesignaleerd;
import org.flaad.deepstack.client.model.FaceDetectionResponse;
import org.flaad.deepstack.client.model.Prediction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.time.Duration;

import static java.awt.Color.RED;
import static javax.imageio.ImageIO.write;

@Slf4j
@Service
public class DeepStackService {
    @Autowired
    private PersoonGesignaleerdSender persoonGesignaleerdSender;
    private RateLimiter rateLimiter;

    @PostConstruct
    public void init() {
        RateLimiterConfig config = RateLimiterConfig.custom()
                .limitRefreshPeriod(Duration.ofMillis(10))
                .limitForPeriod(10)
                .timeoutDuration(Duration.ofSeconds(10))
                .build();

        RateLimiterRegistry rateLimiterRegistry = RateLimiterRegistry.of(config);

        rateLimiter = rateLimiterRegistry
                .rateLimiter("deepStackService");
    }

    @Autowired
    private GezichtVerwerktSender gezichtVerwerktSender;

    @SneakyThrows
    public void verwerkImage(String camera, byte[] imageContent) {
        rateLimiter.acquirePermission();
        log.info("verwerkImage van {}", camera);
        var restTemplate = new RestTemplate();
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var serverUrl = "http://192.168.1.125:32168/v1/vision/face/recognize";

        ByteArrayResource contentsAsResource = new ByteArrayResource(imageContent) {
            @Override
            public String getFilename() {
                return "image"; // Filename has to be returned in order to be able to post.
            }
        };

        var multipartBodyBuilder = new MultipartBodyBuilder();
        multipartBodyBuilder.part("image", contentsAsResource);
        var multipartBody = multipartBodyBuilder.build();
        var httpEntity = new HttpEntity<>(multipartBody, headers);
        ResponseEntity<FaceDetectionResponse> responseEntity = null;
        try {
            log.info("Aanroepen");
            responseEntity = restTemplate.postForEntity(serverUrl, httpEntity, FaceDetectionResponse.class);
            log.info("Klaar met aanroepen");
            log.info("{}", responseEntity);
        } catch (Exception e) {
            log.trace("{}", e);
            log.trace("doorgaan");
        }

        if (responseEntity != null) {
            var faceDetectionResponse = responseEntity.getBody();
            if (faceDetectionResponse != null && faceDetectionResponse.getPredictions() != null && !faceDetectionResponse.getPredictions().isEmpty()) {
                var namenSb = new StringBuilder();
                faceDetectionResponse.getPredictions().stream()
                        .map(Prediction::getUserId)
                        .map(naam -> naam.replace("unknown", "Onbekend Persoon"))
                        .forEachOrdered(naam -> {
                            namenSb.append(naam);
                            namenSb.append(" en ");
                        });
                var namen = namenSb.substring(0, namenSb.length() - 3).trim();

                try {
                    InputStream is = new ByteArrayInputStream(imageContent);
                    BufferedImage[] image = {ImageIO.read(is)};

                    faceDetectionResponse.getPredictions().forEach(prediction -> image[0] = verwerk(image[0], prediction));
//                    createDirectories(Paths.get("/home/pi/faces/" + namen));
//                    var outputFileString = "/home/pi/faces/" + namen + separator + filename;
//
//                    log.info("Schrijf naar {}", outputFileString);
//                    var outputfile = new File(outputFileString);
//                    outputfile.createNewFile();
//                    write(image[0], "jpg", outputfile);

                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    write(image[0], "jpg", baos);
                    byte[] bytes = baos.toByteArray();

                    log.info(namen + " gesignaleerd op camera " + camera);
                    persoonGesignaleerdSender.stuur(new PersoonGesignaleerd(namen, camera, bytes));
                } catch (Exception e) {
                    log.error(e.getMessage());
                }
            }
        }

    }

    @SneakyThrows
    private BufferedImage verwerk(BufferedImage originalImg, Prediction prediction) {
        var naam = prediction.getUserId().replace("unknown", "Onbekend Persoon");
        log.info(naam);

        if (!"onbelangrijk".equalsIgnoreCase(naam)) {
            BufferedImage subImg;

            try {
                subImg = tekenVierkantMetNaam(originalImg, prediction, naam);
            } catch (Exception e) {
                log.error("cropImage ging niet goed");
                subImg = originalImg.getSubimage(prediction.getXMin(), prediction.getYMin(), prediction.getXMax() - prediction.getXMin(), prediction.getYMax() - prediction.getYMin());
            }
            return subImg;
        }
        return originalImg;
    }

    private static FileSystemResource getImage(String imageName) {
        return new FileSystemResource(new File(imageName));
    }

    @SneakyThrows
    private BufferedImage tekenVierkantMetNaam(BufferedImage originalImg, Prediction prediction, String naam) {
        int xMin = prediction.getXMin() < 10 ? 0 : prediction.getXMin() - 10;
        int yMin = prediction.getYMin() < 10 ? 0 : prediction.getYMin() - 10;
        int xMax = prediction.getXMax() + 10 > originalImg.getWidth() ? originalImg.getWidth() : prediction.getXMax() + 10;
        int yMax = prediction.getYMax() + 10 > originalImg.getHeight() ? originalImg.getHeight() : prediction.getYMax() + 10;

        var gp = originalImg.createGraphics();
        gp.setStroke(new BasicStroke(4));
        gp.setColor(RED);
        gp.drawRect(xMin, yMin, xMax - xMin, yMax - yMin);
        gp.setStroke(new BasicStroke(6));
        gp.drawString(naam, xMin, yMin - 10);

        return originalImg;
    }

    private byte[]  enhanceImage(byte[] imageContent){
        var restTemplate = new RestTemplate();
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var serverUrl = "http://deepstack:5000/v1/vision/face/enhance";

        ByteArrayResource contentsAsResource = new ByteArrayResource(imageContent) {
            @Override
            public String getFilename() {
                return "image"; // Filename has to be returned in order to be able to post.
            }
        };

        var multipartBodyBuilder = new MultipartBodyBuilder();
        multipartBodyBuilder.part("image", contentsAsResource);
        var multipartBody = multipartBodyBuilder.build();
        var httpEntity = new HttpEntity<>(multipartBody, headers);
        ResponseEntity<FaceDetectionResponse> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(serverUrl, httpEntity, FaceDetectionResponse.class);
        } catch (Exception e) {
            log.trace("{}", e);
        }

        return imageContent;
    }
}
