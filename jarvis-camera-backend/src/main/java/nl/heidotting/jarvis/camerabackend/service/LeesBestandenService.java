package nl.heidotting.jarvis.camerabackend.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Stream;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static java.util.stream.Collectors.toList;

@Slf4j
@Service
public class LeesBestandenService {
    private final String baseDir = "/home/motioneye";

    public Stream<String> leesBestanden(String camera) {
        List<File> directories = getDirs(new File(baseDir));
        List<File> gefilterdeDirectories = directories.stream().filter(file -> file.getName().startsWith(camera)).collect(toList());

        return gefilterdeDirectories.stream().map(this::getBestanden)
                .flatMap((Function<List<String>, Stream<?>>) Collection::stream)
                .map(o -> (String) o);
    }

    private List<String> getBestanden(File base) {
        Set<String> result = newHashSet();

        File basis = new File(baseDir + "/" + base);

        String[] dirs = basis.list((current, name) -> new File(current, name).getName().endsWith(".jpg"));

        for (int i = 0; i < dirs.length; i++) {
            result.add(baseDir + "/" + base + "/" + new File(dirs[i]).getName());
        }

        return newArrayList(result);
    }

    private List<File> getDirs(File base) {
        List<File> result = newArrayList();

        String[] dirs = base.list((current, name) -> new File(current, name).isDirectory());

        for (int i = 0; i < dirs.length; i++) {
            result.add(new File(dirs[i]));
        }

        return result;
    }

    public String getBaseDir() {
        return baseDir;
    }
}
