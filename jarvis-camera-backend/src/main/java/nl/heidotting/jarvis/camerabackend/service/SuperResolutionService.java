package nl.heidotting.jarvis.camerabackend.service;

import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.json.JSONObject;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.web.client.RestTemplate;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;

@Log4j2
public class SuperResolutionService {
    @SneakyThrows
    public static void main(String[] args) {
        File file = new File("/Users/patrickheidotting/Documents/Test/faces/Patrick Heidotting/Voorkant - 2022-08-22-13-47-03-0--1.jpg");
        byte[] bytes = new byte[(int) file.length()];

        FileInputStream fis = null;
        try {

            fis = new FileInputStream(file);

            //read file into bytes[]
            fis.read(bytes);

        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (fis != null) {
                fis.close();
            }
        }
        var restTemplate = new RestTemplate();
        var headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        var serverUrl = "http://192.168.1.125:32168/v1/image/superresolution";

        ByteArrayResource contentsAsResource = new ByteArrayResource(bytes) {
            @Override
            public String getFilename() {
                return "image"; // Filename has to be returned in order to be able to post.
            }
        };

        var multipartBodyBuilder = new MultipartBodyBuilder();
        multipartBodyBuilder.part("image", contentsAsResource);
        var multipartBody = multipartBodyBuilder.build();
        var httpEntity = new HttpEntity<>(multipartBody, headers);
        log.info("Aanroepen");
        ResponseEntity<String> responseEntity = restTemplate.postForEntity(serverUrl, httpEntity, String.class);
        JSONObject jsonObject = new JSONObject(responseEntity.getBody());

        byte[] output = jsonObject.get("imageBase64").toString().getBytes();
        var outputFileString = "/Users/patrickheidotting/Documents/Test/faces/Patrick Heidotting/Voorkant - 2022-08-22-13-47-03-0--1=2.png";
        log.info("Schrijf naar {}", outputFileString);
        var outputfile = new File(outputFileString);
        outputfile.createNewFile();

        ByteArrayInputStream inStreambj = new ByteArrayInputStream(output);

        BufferedImage image = ImageIO.read(inStreambj);
        ImageIO.write(image, "png", outputfile);

//        write(output, outputfile);
    }
}
