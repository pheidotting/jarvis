package nl.heidotting.jarvis.camerabackend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Queues.*;

@Slf4j
@Configuration
public class Queues {
    @Bean
    public Queue jarvisCameraUploadMovieQueue() {
        log.info("Aanmaken {}",JARVIS_CAMERA_UPLOAD_MOVIE_QUEUE);
        return new Queue(JARVIS_CAMERA_UPLOAD_MOVIE_QUEUE);
    }

    @Bean
    public Queue jarvisCameraUploadPictureQueue() {
        log.info("Aanmaken {}","camera."+JARVIS_CAMERA_UPLOAD_PICTURE_QUEUE);
        return new Queue("camera."+JARVIS_CAMERA_UPLOAD_PICTURE_QUEUE);
    }
    @Bean
    public Queue verwerkGezichtQueue() {
        log.info("Aanmaken {}","camera."+VERWERK_GEZICHT_QUEUE);
        return new Queue("camera."+VERWERK_GEZICHT_QUEUE);
    }
}
