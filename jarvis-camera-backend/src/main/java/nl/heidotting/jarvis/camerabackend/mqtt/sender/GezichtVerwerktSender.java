package nl.heidotting.jarvis.camerabackend.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.GezichtVerwerkt;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Topics.GEZICHT_VERWERKT_EXCHANGE;

@Slf4j
@Service
public class GezichtVerwerktSender extends AbstractPublisher<GezichtVerwerkt> {
    @Override
    protected String topic() {
        return GEZICHT_VERWERKT_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}

