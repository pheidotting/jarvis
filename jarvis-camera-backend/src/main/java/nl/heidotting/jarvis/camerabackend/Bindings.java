package nl.heidotting.jarvis.camerabackend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class Bindings {
    @Bean
    Binding jarvisCameraUploadMovie(Queue jarvisCameraUploadMovieQueue, FanoutExchange jarvisCameraUploadMovieExchange) {
        log.info("Binding tussen {} en {} aanmaken", jarvisCameraUploadMovieQueue.getName(), jarvisCameraUploadMovieExchange.getName());
        return BindingBuilder.bind(jarvisCameraUploadMovieQueue).to(jarvisCameraUploadMovieExchange);
    }
    @Bean
    Binding jarvisCameraUploadPicture(Queue jarvisCameraUploadPictureQueue, FanoutExchange jarvisCameraUploadPictureExchange) {
        log.info("Binding tussen {} en {} aanmaken", jarvisCameraUploadPictureQueue.getName(), jarvisCameraUploadPictureExchange.getName());
        return BindingBuilder.bind(jarvisCameraUploadPictureQueue).to(jarvisCameraUploadPictureExchange);
    }
    @Bean
    Binding verwerkGezicht(Queue verwerkGezichtQueue, FanoutExchange verwerkGezichtExchange) {
        log.info("Binding tussen {} en {} aanmaken", verwerkGezichtQueue.getName(), verwerkGezichtExchange.getName());
        return BindingBuilder.bind(verwerkGezichtQueue).to(verwerkGezichtExchange);
    }
}
