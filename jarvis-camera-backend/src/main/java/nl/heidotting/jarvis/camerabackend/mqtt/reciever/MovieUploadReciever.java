//package nl.heidotting.jarvis.camerabackend.mqtt.reciever;
//
//import lombok.extern.slf4j.Slf4j;
//import nl.heidotting.jarvis.shared.mqtt.AbstractReciever;
//import nl.heidotting.jarvis.shared.mqtt.messages.IncomingFileEvent;
//import org.slf4j.Logger;
//import org.springframework.stereotype.Service;
//
//@Slf4j
//@Service
//public class MovieUploadReciever extends AbstractReciever<IncomingFileEvent> {
//    @Override
//    public String topic() {
//        return "jarvis/camera/upload/movie";
//    }
//
//    @Override
//    protected void verwerk(IncomingFileEvent incomingFileEvent) {
////        videoToImagesService.verwerk(incomingFileEvent.getUsername(), incomingFileEvent.getFileData());
//    }
//
//    @Override
//    protected Class<IncomingFileEvent> clazz() {
//        return IncomingFileEvent.class;
//    }
//
//    @Override
//    protected Logger getLogger() {
//        return log;
//    }
//}
//
