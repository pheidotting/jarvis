package nl.heidotting.jarvis.camerabackend.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.camerabackend.service.InlezenGezichtenService;
import nl.heidotting.jarvis.shared.mqtt.messages.VerwerkGezicht;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.VERWERK_GEZICHT_QUEUE;

@Slf4j
@Service
public class VerwerkGezichtReciever {
    @Autowired
    private InlezenGezichtenService inlezenGezichtenService;

    @RabbitListener(queues = "camera."+VERWERK_GEZICHT_QUEUE)
    protected void verwerk(VerwerkGezicht verwerkGezicht) {
        inlezenGezichtenService.leesGezichtenIn(true, verwerkGezicht.naam(), verwerkGezicht.foto());
    }
}

