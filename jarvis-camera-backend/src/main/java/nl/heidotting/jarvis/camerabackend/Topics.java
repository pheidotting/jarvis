package nl.heidotting.jarvis.camerabackend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Topics.*;

@Slf4j
@Configuration
public class Topics {
    @Bean
    public FanoutExchange jarvisCameraUploadMovieExchange() {
        log.info("Aanmaken {}", JARVIS_CAMERA_UPLOAD_MOVIE_EXCHANGE);
        return new FanoutExchange(JARVIS_CAMERA_UPLOAD_MOVIE_EXCHANGE);
    }

    @Bean
    public FanoutExchange jarvisCameraUploadPictureExchange() {
        log.info("Aanmaken {}", JARVIS_CAMERA_UPLOAD_PICTURE_EXCHANGE);
        return new FanoutExchange(JARVIS_CAMERA_UPLOAD_PICTURE_EXCHANGE);
    }

    @Bean
    public FanoutExchange persoonGesignaleerdExchange() {
        log.info("Aanmaken {}", PERSOON_GESIGNALEERD_EXCHANGE);
        return new FanoutExchange(PERSOON_GESIGNALEERD_EXCHANGE);
    }

    @Bean
    public FanoutExchange verwerkGezichtExchange() {
        log.info("Aanmaken {}", VERWERK_GEZICHT_EXCHANGE);
        return new FanoutExchange(VERWERK_GEZICHT_EXCHANGE);
    }
    @Bean
    public FanoutExchange gezichtVerwerktExchange() {
        log.info("Aanmaken {}", GEZICHT_VERWERKT_EXCHANGE);
        return new FanoutExchange(GEZICHT_VERWERKT_EXCHANGE);
    }
}
