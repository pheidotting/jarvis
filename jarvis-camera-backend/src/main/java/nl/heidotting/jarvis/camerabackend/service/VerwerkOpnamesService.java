//package nl.heidotting.jarvis.camerabackend.service;
//
//import lombok.extern.slf4j.Slf4j;
//import nl.heidotting.jarvis.camerabackend.mqtt.sender.StuurTelegramBerichtSender;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.annotation.PostConstruct;
//import java.time.LocalDateTime;
//import java.time.format.DateTimeFormatter;
//import java.util.HashSet;
//import java.util.Map;
//import java.util.Set;
//import java.util.concurrent.ConcurrentHashMap;
//
//@Slf4j
//@Service
//public class VerwerkOpnamesService {
//    @Autowired
//    private LeesBestandenService leesBestandenService;
//    @Autowired
//    private StuurTelegramBerichtSender stuurTelegramBerichtSender;
//
//    private boolean gezichtsherkenning = false;
//
//    private Map<LocalDateTime, Set<String>> teVerwerkenBestanden = new ConcurrentHashMap<>();
//
//    @PostConstruct
//    public void init() {
//        new Thread(new RoepDeepStackAan(leesBestandenService, this, stuurTelegramBerichtSender)).start();
//    }
//
//    public void verwerkBestanden(String welkeCamera) {
//        leesBestandenService.leesBestanden(welkeCamera).forEach(this::voegToeAanLijst);
//    }
//
//    public Map<LocalDateTime, Set<String>> getTeVerwerkenBestanden() {
//        return teVerwerkenBestanden;
//    }
//
//    public boolean isGezichtsherkenning() {
//        return gezichtsherkenning;
//    }
//
//    public void setGezichtsherkenning(boolean gezichtsherkenning) {
//        this.gezichtsherkenning = gezichtsherkenning;
//    }
//
//    public Set<String> getAndRemove(LocalDateTime key) {
//        var result = getTeVerwerkenBestanden().get(key);
//        getTeVerwerkenBestanden().remove(key);
//        return result;
//    }
//
//    private void voegToeAanLijst(String bestand) {
//        LocalDateTime tijdstip = leesTijdstip(bestand);
//        Set<String> gevonden = teVerwerkenBestanden.get(tijdstip);
//        if (gevonden == null) {
//            gevonden = new HashSet<>();
//        }
//        gevonden.add(bestand);
//        teVerwerkenBestanden.put(tijdstip, gevonden);
//    }
//
//    private LocalDateTime leesTijdstip(String bestand) {
//        // /home/motioneye/Voorkant - 2022-07-28/18-10-36.jpg
//        String[] parts = bestand.split("/");
//        String tijd = parts[parts.length - 1].replace(".jpg", "").replace(".jpeg", "").replace("-", ":");
//        String[] dummy = parts[parts.length - 2].split(" - ");
//        String datum = dummy[dummy.length - 1];
//
//        return LocalDateTime.parse(datum + " " + tijd, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
//    }
//}
