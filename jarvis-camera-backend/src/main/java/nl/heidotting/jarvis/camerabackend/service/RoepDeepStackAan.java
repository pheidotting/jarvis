//package nl.heidotting.jarvis.camerabackend.service;
//
//import lombok.SneakyThrows;
//import lombok.extern.slf4j.Slf4j;
//import nl.heidotting.jarvis.camerabackend.mqtt.sender.StuurTelegramBerichtSender;
//import org.flaad.deepstack.client.model.FaceDetectionResponse;
//import org.flaad.deepstack.client.model.Prediction;
//import org.springframework.core.io.FileSystemResource;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.http.ResponseEntity;
//import org.springframework.http.client.MultipartBodyBuilder;
//import org.springframework.web.client.RestTemplate;
//
//import javax.imageio.ImageIO;
//import java.awt.*;
//import java.awt.image.BufferedImage;
//import java.io.ByteArrayOutputStream;
//import java.io.File;
//import java.nio.file.Paths;
//import java.time.LocalDateTime;
//import java.util.Optional;
//
//import static java.awt.Color.RED;
//import static java.io.File.separator;
//import static java.nio.file.Files.createDirectories;
//import static java.util.Collections.reverseOrder;
//import static javax.imageio.ImageIO.write;
//import static org.awaitility.Awaitility.await;
//
//@Slf4j
//public class RoepDeepStackAan implements Runnable {
//    private LeesBestandenService leesBestandenService;
//    private VerwerkOpnamesService verwerkOpnamesService;
//    private StuurTelegramBerichtSender stuurTelegramBerichtSender;
//
//    public RoepDeepStackAan(LeesBestandenService leesBestandenService, VerwerkOpnamesService verwerkOpnamesService, StuurTelegramBerichtSender stuurTelegramBerichtSender) {
//        this.leesBestandenService = leesBestandenService;
//        this.verwerkOpnamesService = verwerkOpnamesService;
//        this.stuurTelegramBerichtSender = stuurTelegramBerichtSender;
//    }
//
//    @SneakyThrows
//    @Override
//    public void run() {
//        log.info("RoepDeepStackAan");
//        var restTemplate = new RestTemplate();
//        var headers = new HttpHeaders();
//        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//        var serverUrl = "http://deepstack:5000/v1/vision/face/recognize";
//        final int[] aantalThreads = {0};
//
//        while (true) {
//            await().forever().until(() -> verwerkOpnamesService.isGezichtsherkenning());
//            await().forever().until(() -> verwerkOpnamesService.getTeVerwerkenBestanden().size() > 0);
//            var optionalLocalDateTime = leesTijdstip();
//            if (optionalLocalDateTime.isPresent()) {
//                LocalDateTime tijdstip = optionalLocalDateTime.get();
//                var bestanden = verwerkOpnamesService.getAndRemove(tijdstip);
//
//                await().forever().until(() -> aantalThreads[0] < 2);
//                new Thread(() -> {
//                    aantalThreads[0]++;
//                    bestanden.forEach(filename -> {
//                        var multipartBodyBuilder = new MultipartBodyBuilder();
//                        multipartBodyBuilder.part("image", getImage(filename));
//                        var multipartBody = multipartBodyBuilder.build();
//                        var httpEntity = new HttpEntity<>(multipartBody, headers);
//                        ResponseEntity<FaceDetectionResponse> responseEntity = null;
//                        try {
//                            responseEntity = restTemplate.postForEntity(serverUrl, httpEntity, FaceDetectionResponse.class);
//                        } catch (Exception e) {
//                            log.trace("{}", e);
//                            log.trace("doorgaan");
//                        }
//
//                        if (responseEntity != null) {
//                            var faceDetectionResponse = responseEntity.getBody();
//
//                            if (!faceDetectionResponse.getPredictions().isEmpty()) {
//                                var namenSb = new StringBuilder();
//                                faceDetectionResponse.getPredictions().stream()
//                                        .map(Prediction::getUserId)
//                                        .map(naam -> naam.replace("unknown", "Onbekend Persoon"))
//                                        .forEachOrdered(naam -> {
//                                            namenSb.append(naam);
//                                            namenSb.append(" en ");
//                                        });
//                                var namen = namenSb.substring(0, namenSb.length() - 3).trim();
//
//                                try {
//                                    final BufferedImage[] image = {ImageIO.read(new File(filename))};
//
//                                    faceDetectionResponse.getPredictions().forEach(prediction -> image[0] = verwerk(image[0], prediction));
//                                    createDirectories(Paths.get("/home/motioneye/faces/" + namen));
//                                    var outputFileString = "/home/motioneye/faces/" + namen + separator + filename;
//
//                                    log.info("Schrijf naar {}", outputFileString);
//                                    var outputfile = new File(outputFileString);
//                                    outputfile.createNewFile();
//                                    write(image[0], "jpg", outputfile);
//
//                                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                                    write(image[0], "jpg", baos);
//                                    byte[] bytes = baos.toByteArray();
//
//                                    if (stuurTelegramBerichtSender != null) {
//                                        stuurTelegramBerichtSender.stuur(namen + " gesignaleerd!", bytes);
//                                    }
//                                } catch (Exception e) {
//                                }
//                            }
//                            new File(filename).delete();
//                            log.info("Verwijder bestand {}", filename);
//
//                        }
//                    });
//                    aantalThreads[0] = aantalThreads[0] - 1;
//                }).start();
//            }
//        }
//    }
//
//    private Optional<LocalDateTime> leesTijdstip() {
//        return verwerkOpnamesService.getTeVerwerkenBestanden().keySet()
//                .stream()
//                .sorted(reverseOrder())
//                .findFirst();
//    }
//
//    @SneakyThrows
//    private BufferedImage verwerk(BufferedImage originalImg, Prediction prediction) {
//        var naam = prediction.getUserId().replace("unknown", "Onbekend Persoon");
//        log.info(naam);
//
//        if (!"onbelangrijk".equalsIgnoreCase(naam)) {
//            BufferedImage subImg;
//
//            try {
//                subImg = tekenVierkantMetNaam(originalImg, prediction, naam);
//            } catch (Exception e) {
//                log.error("cropImage ging niet goed");
//                subImg = originalImg.getSubimage(prediction.getXMin(), prediction.getYMin(), prediction.getXMax() - prediction.getXMin(), prediction.getYMax() - prediction.getYMin());
//            }
//            return subImg;
//        }
//        return originalImg;
//    }
//
//    private static FileSystemResource getImage(String imageName) {
//        return new FileSystemResource(new File(imageName));
//    }
//
//    @SneakyThrows
//    private BufferedImage tekenVierkantMetNaam(BufferedImage originalImg, Prediction prediction, String naam) {
//        int xMin = prediction.getXMin() < 10 ? 0 : prediction.getXMin() - 10;
//        int yMin = prediction.getYMin() < 10 ? 0 : prediction.getYMin() - 10;
//        int xMax = prediction.getXMax() + 10 > originalImg.getWidth() ? originalImg.getWidth() : prediction.getXMax() + 10;
//        int yMax = prediction.getYMax() + 10 > originalImg.getHeight() ? originalImg.getHeight() : prediction.getYMax() + 10;
//
//        var gp = originalImg.createGraphics();
//        gp.setStroke(new BasicStroke(4));
//        gp.setColor(RED);
//        gp.drawRect(xMin, yMin, xMax - xMin, yMax - yMin);
//        gp.drawString(naam, xMin, yMin - 10);
//
//        return originalImg;
//    }
//}
