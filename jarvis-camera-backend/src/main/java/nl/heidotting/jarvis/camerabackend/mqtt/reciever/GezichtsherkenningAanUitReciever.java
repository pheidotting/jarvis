//package nl.heidotting.jarvis.camerabackend.mqtt.reciever;
//
//import lombok.extern.slf4j.Slf4j;
//import nl.heidotting.jarvis.camerabackend.service.VerwerkOpnamesService;
//import nl.heidotting.jarvis.shared.mqtt.AbstractReciever;
//import nl.heidotting.jarvis.shared.mqtt.messages.GezichtsherkenningAanUit;
//import org.slf4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Slf4j
//@Service
//public class GezichtsherkenningAanUitReciever extends AbstractReciever<GezichtsherkenningAanUit> {
//    @Autowired
//    private VerwerkOpnamesService verwerkOpnamesService;
//
//    @Override
//    public String topic() {
//        return "jarvis/camera/gezichtsherkenningaanuit";
//    }
//
//    @Override
//    protected void verwerk(GezichtsherkenningAanUit gezichtsherkenningAanUit) {
//        verwerkOpnamesService.setGezichtsherkenning(gezichtsherkenningAanUit.aanOfUit());
//    }
//
//    @Override
//    protected Class<GezichtsherkenningAanUit> clazz() {
//        return GezichtsherkenningAanUit.class;
//    }
//
//    @Override
//    protected Logger getLogger() {
//        return log;
//    }
//}
//
