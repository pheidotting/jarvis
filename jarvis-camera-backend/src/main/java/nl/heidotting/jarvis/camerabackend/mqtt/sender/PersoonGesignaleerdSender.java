package nl.heidotting.jarvis.camerabackend.mqtt.sender;

import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.PersoonGesignaleerd;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.PERSOON_GESIGNALEERD_EXCHANGE;

@Component
public class PersoonGesignaleerdSender extends AbstractPublisher<PersoonGesignaleerd> {
    @Override
    protected String topic() {
        return PERSOON_GESIGNALEERD_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
