package nl.heidotting.jarvis.camerabackend.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.camerabackend.mqtt.sender.GezichtVerwerktSender;
import nl.heidotting.jarvis.shared.mqtt.messages.GezichtVerwerkt;
import org.flaad.deepstack.client.model.FaceRegisterResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.util.Arrays;
import java.util.Objects;

import static com.google.common.collect.Lists.newArrayList;

@Slf4j
@Service
public class InlezenGezichtenService {
    private final String baseDir = "/home/motioneye/inleren";
//    private final String baseDir = "/Users/patrickheidotting/Downloads/inleren";

    @Autowired
    private GezichtVerwerktSender gezichtVerwerktSender;

    public void leesGezichtenIn(boolean antwoord, String naam, byte[] foto) {
        log.info("Lees {} in", naam);
        var result = roepAan(null, foto, naam);
        if (antwoord && result != null) {
            gezichtVerwerktSender.stuur(new GezichtVerwerkt(result));
        }
    }

    public void leesGezichtenIn(boolean antwoord) {
        var base = new File(baseDir);
        var namen = newArrayList(Objects.requireNonNull(base.list((dir, name) -> dir.isDirectory())))
                .stream().filter(s -> !".DS_Store".equals(s))
                .toList();

        namen.forEach(naam -> Arrays.stream(new File(baseDir + File.separator + naam).list()).forEach(s -> {
            log.info(baseDir + File.separator + naam + File.separator + s);
            var result = roepAan(baseDir + File.separator + naam + File.separator + s, null, naam);
            if (result == null) {
                result = "Geen gezicht gevonden.";
            }
            if (antwoord) {
                gezichtVerwerktSender.stuur(new GezichtVerwerkt(result));
            }
        }));
    }

    private String roepAan(String filename, byte[] file, String naam) {
        if (naam == null) {
            return "naam was leeg";
        }
        String result = null;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
//        String serverUrl = "http://192.168.1.103:5000/v1/vision/face/register";
        var serverUrl = "http://192.168.1.125:32168/v1/vision/face/register";

        MultipartBodyBuilder multipartBodyBuilder = new MultipartBodyBuilder();
        if (filename != null) {
            multipartBodyBuilder.part("image1", getImage(filename));
        } else if (file != null) {
            multipartBodyBuilder.part("image1", new ByteArrayResource(file) {
                @Override
                public String getFilename() {
                    return naam;
                }
            });
        }
//        multipartBodyBuilder.part("userid", naam);
        MultiValueMap<String, HttpEntity<?>> multipartBody = multipartBodyBuilder.build();
        HttpEntity<MultiValueMap<String, HttpEntity<?>>> httpEntity = new HttpEntity<>(multipartBody, headers);
        ResponseEntity<FaceRegisterResponse> responseEntity = null;
        try {
            responseEntity = restTemplate.postForEntity(serverUrl, httpEntity, FaceRegisterResponse.class);
        } catch (HttpClientErrorException e) {
        } catch (Exception e) {
            log.info("{}", e);
        }

        if (responseEntity != null) {
            var faceRegisterResponse = responseEntity.getBody();
            log.info(faceRegisterResponse.getMessage());
            result = faceRegisterResponse.getMessage();
        }

        return result;
    }

    private static FileSystemResource getImage(String imageName) {
        return new FileSystemResource(new File(imageName));
    }
}
