package nl.heidotting.jarvis.camerabackend.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.camerabackend.service.DeepStackService;
import nl.heidotting.jarvis.shared.mqtt.messages.IncomingFileEvent;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.JARVIS_CAMERA_UPLOAD_PICTURE_QUEUE;

@Slf4j
@Service
public class ImageUploadReciever {
    @Autowired
    private DeepStackService deepStackService;

    @RabbitListener(queues = "camera."+JARVIS_CAMERA_UPLOAD_PICTURE_QUEUE)
    public void recievedMessage(IncomingFileEvent incomingFileEvent) {
        deepStackService.verwerkImage(incomingFileEvent.getUsername(), incomingFileEvent.getFileData());
    }
}

