package nl.heidotting.jarvis.camerabackend;

import nl.heidotting.jarvis.shared.services.AbstractStarter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;


@EnableScheduling
@SpringBootApplication(scanBasePackages = {"nl.heidotting.jarvis.camerabackend", "org.springframework.cloud.context.restart"})
public class JarvisCameraBackendApplication extends AbstractStarter {
    public static void main(String[] args) {
        SpringApplication.run(JarvisCameraBackendApplication.class, args);
    }
}
