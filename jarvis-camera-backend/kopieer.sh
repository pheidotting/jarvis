#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-camera-backend/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-camera-backend/Dockerfile
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-camera-backend/target/jarvis-camera-backend.jar pi@192.168.1.103:/home/pi/images/jarvis-camera-backend/target/jarvis-camera-backend.jar
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-ftp2mq-backend/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-ftp2mq-backend/Dockerfile
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-ftp2mq-backend/target/jarvis-ftp2mq-backend.jar pi@192.168.1.103:/home/pi/images/jarvis-ftp2mq-backend/target/jarvis-ftp2mq-backend.jar
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-keeping-backend/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-keeping-backend/Dockerfile
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-keeping-backend/target/jarvis-keeping-backend.jar pi@192.168.1.103:/home/pi/images/jarvis-keeping-backend/target/jarvis-keeping-backend.jar
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-lampen-backend/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-lampen-backend/Dockerfile
scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-lampen-backend/target/jarvis-lampen-backend.jar pi@192.168.1.103:/home/pi/images/jarvis-lampen-backend/target/jarvis-lampen-backend.jar
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-moneybird-backend/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-moneybird-backend/Dockerfile
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-moneybird-backend/target/jarvis-moneybird-backend.jar pi@192.168.1.103:/home/pi/images/jarvis-moneybird-backend/target/jarvis-moneybird-backend.jar
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-restapi/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-restapi/Dockerfile
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-restapi/target/jarvis-restapi.jar pi@192.168.1.103:/home/pi/images/jarvis-restapi/target/jarvis-restapi.jar
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-sonos-backend/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-sonos-backend/Dockerfile
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-sonos-backend/target/jarvis-sonos-backend.jar pi@192.168.1.103:/home/pi/images/jarvis-sonos-backend/target/jarvis-sonos-backend.jar
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-telegram-backend/Dockerfile pi@192.168.1.103:/home/pi/images/jarvis-telegram-backend/Dockerfile
#scp /Users/patrickheidotting/IdeaProjects/jarvis/jarvis-telegram-backend/target/jarvis-telegram-backend.jar pi@192.168.1.103:/home/pi/images/jarvis-telegram-backend/target/jarvis-telegram-backend.jar

#docker build -t jarvis-camera-backend /home/pi/images/jarvis-camera-backend/. && docker tag jarvis-camera-backend:latest registry.gitlab.com/pheidotting/jarvis/jarvis-camera-backend:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-camera-backend:jarvis-camera-backend && docker compose up -d jarvis-camera-backend
#docker build -t jarvis-ftp2mq-backend /home/pi/images/jarvis-ftp2mq-backend/. && docker tag jarvis-ftp2mq-backend:latest registry.gitlab.com/pheidotting/jarvis/jarvis-ftp2mq-backend:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-ftp2mq-backend:jarvis-camera-backend && docker compose up -d jarvis-ftp2mqtt-backend && docker logs jarvis-ftp2mqtt-backend
#docker build -t jarvis-keeping-backend /home/pi/images/jarvis-keeping-backend/. && docker tag jarvis-keeping-backend:latest registry.gitlab.com/pheidotting/jarvis/jarvis-keeping-backend:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-keeping-backend:jarvis-camera-backend && docker compose up -d jarvis-keeping-backend
#docker build -t jarvis-lampen-backend /home/pi/images/jarvis-lampen-backend/. && docker tag jarvis-lampen-backend:latest registry.gitlab.com/pheidotting/jarvis/jarvis-lampen-backend:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-lampen-backend:jarvis-camera-backend && docker compose up -d jarvis-lampen-backend && docker logs jarvis-lampen-backend -f
#docker build -t jarvis-moneybird-backend /home/pi/images/jarvis-moneybird-backend/. && docker tag jarvis-moneybird-backend:latest registry.gitlab.com/pheidotting/jarvis/jarvis-moneybird-backend:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-moneybird-backend:jarvis-camera-backend && docker compose up -d jarvis-moneybird-backend && docker logs jarvis-moneybird-backend -f
#docker build -t jarvis-restapi /home/pi/images/jarvis-restapi/. && docker tag jarvis-restapi:latest registry.gitlab.com/pheidotting/jarvis/jarvis-restapi:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-restapi:jarvis-camera-backend && docker compose up -d jarvis-restapi
#docker build -t jarvis-sonos-backend /home/pi/images/jarvis-sonos-backend/. && docker tag jarvis-sonos-backend:latest registry.gitlab.com/pheidotting/jarvis/jarvis-sonos-backend:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-sonos-backend:jarvis-camera-backend && docker compose up -d jarvis-sonos-backend && docker logs jarvis-sonos-backend -f
#docker build -t jarvis-telegram-backend /home/pi/images/jarvis-telegram-backend/. && docker tag jarvis-telegram-backend:latest registry.gitlab.com/pheidotting/jarvis/jarvis-telegram-backend:jarvis-camera-backend && docker push registry.gitlab.com/pheidotting/jarvis/jarvis-telegram-backend:jarvis-camera-backend && docker compose up -d jarvis-telegram-backend

#voorkant
#http://camera-voorkant.heidotting.nl:88/cgi-bin/CGIProxy.fcgi?cmd=setFtpConfig&ftpAddr=ftp://jarvis.heidotting.nl&ftpPort=2121&fptuserName=voorkant&ftppassword=secret&mode=1&usr=patrick&pwd=Herman79!
#zijkant rechts
#http://camera-zijkant-rechts.heidotting.nl:88/cgi-bin/CGIProxy.fcgi?cmd=setFtpConfig&ftpAddr=ftp://jarvis.heidotting.nl&ftpPort=2121&fptuserName=zijkant-rechts&ftppassword=secret&mode=1&usr=patrick&pwd=Herman79!
#zijkant links
#http://camera-zijkant-links.heidotting.nl:88/cgi-bin/CGIProxy.fcgi?cmd=setFtpConfig&ftpAddr=ftp://jarvis.heidotting.nl&ftpPort=2121&fptuserName=zijkant-links&ftppassword=secret&mode=1&usr=patrick&pwd=Herman79!
#Achterdeur
#http://camera-achterdeur.heidotting.nl:88/cgi-bin/CGIProxy.fcgi?cmd=setFtpConfig&ftpAddr=ftp://jarvis.heidotting.nl&ftpPort=2121&fptuserName=achterdeur&ftppassword=secret&mode=1&usr=patrick&pwd=Herman79!
#
#
#http://192.168.1.106:88/cgi-bin/CGIProxy.fcgi?cmd=getFtpConfig&usr=patrick&pwd=Herman79!
#http://192.168.1.109:88/cgi-bin/CGIProxy.fcgi?cmd=getFtpConfig&usr=patrick&pwd=Herman79!
#http://192.168.1.108:88/cgi-bin/CGIProxy.fcgi?cmd=getFtpConfig&usr=patrick&pwd=Herman79!
#http://192.168.1.100:88/cgi-bin/CGIProxy.fcgi?cmd=getFtpConfig&usr=patrick&pwd=Herman79!