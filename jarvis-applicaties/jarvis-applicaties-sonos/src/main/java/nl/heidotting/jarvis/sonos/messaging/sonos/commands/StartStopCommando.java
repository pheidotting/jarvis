package nl.heidotting.jarvis.sonos.messaging.sonos.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class StartStopCommando {
    private String command;

    public StartStopCommando(boolean start) {
        this.command = start ? "play" : "stop";
    }
}
