package nl.heidotting.jarvis.sonos.messaging.reciever;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.messaging.service.WoonkamerSonosService;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class WoonkamerReciever {
    private final WoonkamerSonosService woonkamerSonosService;

    @Incoming("woonkamer")
    public void listener(Object p) {
        SonosState state = null;
        try {
            state = new ObjectMapper().readValue((byte[]) p, SonosState.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error(new String((byte[]) p));
        }
        if (state != null) {
            woonkamerSonosService.verwerkVerandering(state);
        }
    }
}
