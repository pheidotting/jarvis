package nl.heidotting.jarvis.sonos.messaging.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.messaging.model.MogelijkeUitgevoerdeActie;
import nl.heidotting.jarvis.sonos.messaging.sender.AbstractSpeakerSender;
import nl.heidotting.jarvis.sonos.messaging.sender.KantoorSender;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Predicate;


@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class KantoorSonosService extends SonosService {
    @ConfigProperty(name = "zwarte-lijst")
    private List<String> zwarteLijst;

    private LocalDate vandaagAlAanGeweest = LocalDate.now().minusDays(1);

    private final KantoorSender kantoorSender;
    private final SonosStatusService sonosStatusService;
    private final UitgevoerdeActieService uitgevoerdeActieService;

    @Override
    public AbstractSpeakerSender getSender() {
        return kantoorSender;
    }

    @Override
    public String getNaam() {
        return "Kantoor";
    }

    @Override
    public SonosStatusService getSonosStatusService() {
        return this.sonosStatusService;
    }

    @Override
    public UitgevoerdeActieService getUitgevoerdeActieService() {
        return uitgevoerdeActieService;
    }

    public void verwerkVeranderingVoorSpeaker(SonosState sonosState) {
        var masterVolume = sonosState.getVolume().getMaster();

        log.info("Artist : {}, Title : {}", artist, title);
        boolean gemute = sonosStatusService.getMuteStatus(getNaam());
        if (zwarteLijst.stream().anyMatch(isEentjeVanDeZwarteLijst(artist, title)) && !gemute) {
            log.info("k*tmuziek, volume lager");
            sonosStatusService.setHoofdVolume(getNaam(), masterVolume);
            this.sonosStatusService.setMuteStatus(getNaam(), true);
            getSender().setVolume(new BigDecimal(1));
            uitgevoerdeActieService.opslaan(new MogelijkeUitgevoerdeActie(getNaam(), MogelijkeUitgevoerdeActie.MogelijkeActie.VOLUME_LAGER_DOOR_ZWARTELIJST_NUMMER));
        } else if (zwarteLijst.stream().noneMatch(isEentjeVanDeZwarteLijst(artist, title)) && gemute) {
            log.info("volume herstellen");
            this.sonosStatusService.setMuteStatus(getNaam(), false);
            getSender().setVolume(new BigDecimal(sonosStatusService.getHoofdVolume(getNaam())));
            uitgevoerdeActieService.opslaan(new MogelijkeUitgevoerdeActie(getNaam(), MogelijkeUitgevoerdeActie.MogelijkeActie.EINDE_VOLUME_LAGER_DOOR_ZWARTELIJST_NUMMER));
        }

//        if (vandaagAlAanGeweest.isBefore(LocalDate.now()) && LocalTime.now().isBefore(LocalTime.of(10, 0))) {
//            log.info("ochtendbegroeting afspelen");
//            ttsService.maakOchtendBegroeting(true);
//            kantoorSender.speelMp3("ochtendbegroeting", true);
//            vandaagAlAanGeweest = LocalDate.now();
//        }
    }

    private Predicate isEentjeVanDeZwarteLijst(String artist, String title) {
        return (Predicate<String>) zwarteLijstItem -> artist.toLowerCase().contains(zwarteLijstItem.toLowerCase()) || title.toLowerCase().contains(zwarteLijstItem.toLowerCase());
    }

}
