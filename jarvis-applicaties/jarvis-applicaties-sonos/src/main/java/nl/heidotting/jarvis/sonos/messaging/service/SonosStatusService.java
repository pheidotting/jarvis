package nl.heidotting.jarvis.sonos.messaging.service;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class SonosStatusService {
    private final RedisDataSource redisDataSource;

    private HashCommands<String, String, Boolean> booleanHashCommands;
    private HashCommands<String, String, LocalDateTime> localDateTimeHashCommands;
    private HashCommands<String, String, String> stringHashCommands;
    private HashCommands<String, String, Integer> integerHashCommands;


    @PostConstruct
    public void init() {
        booleanHashCommands = redisDataSource.hash(Boolean.class);
        localDateTimeHashCommands = redisDataSource.hash(LocalDateTime.class);
        stringHashCommands = redisDataSource.hash(String.class);
        integerHashCommands = redisDataSource.hash(Integer.class);
    }

    public void opslaanStatus(String speaker, boolean speeltAf, String artist, String title, int volume) {
        booleanHashCommands.hset("Sonos:" + speaker, "speelt-af", speeltAf);
        localDateTimeHashCommands.hset("Sonos:" + speaker, "tijdstip", LocalDateTime.now());
        if (artist != null) {
            stringHashCommands.hset("Sonos:" + speaker, "artist", artist);
        } else {
            stringHashCommands.hdel("Sonos:" + speaker, "artist");
        }
        if (title != null) {
            stringHashCommands.hset("Sonos:" + speaker, "title", title);
        } else {
            stringHashCommands.hdel("Sonos:" + speaker, "title");
        }
        integerHashCommands.hset("Sonos:" + speaker, "volume", volume);
    }

    public void setMuteStatus(String speaker, boolean mute) {
        booleanHashCommands.hset("Sonos:" + speaker, "mute", mute);
    }

    public boolean getMuteStatus(String speaker) {
        var mute = booleanHashCommands.hget("Sonos:" + speaker, "mute");
        return mute == null ? false : mute;
    }

    public void setHoofdVolume(String speaker, int hoofdVolume) {
        integerHashCommands.hset("Sonos:" + speaker, "hoofdVolume", hoofdVolume);
    }

    public int getHoofdVolume(String speaker) {
        return integerHashCommands.hget("Sonos:" + speaker, "hoofdVolume");
    }

    public boolean speeltAf(String speaker) {
        return booleanHashCommands.hget("Sonos:" + speaker, "speelt-af");
    }
}
