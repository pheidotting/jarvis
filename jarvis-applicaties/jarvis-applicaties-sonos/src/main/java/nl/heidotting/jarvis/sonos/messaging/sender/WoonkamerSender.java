package nl.heidotting.jarvis.sonos.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@ApplicationScoped
public class WoonkamerSender extends AbstractSpeakerSender {
    @Inject
    @Channel("woonkamer-control")
    private Emitter<Object> woonkamerController;

    @Override
    void stuur(Object o) {
        woonkamerController.send(o);
    }
//    @Override
//    protected String topic() {
//        return new Woonkamer().controlTopic();
//    }
//
//    @Override
//    protected String routingKey() {
//        return new Kantoor().controlRoutingkey();
//    }
}
