package nl.heidotting.jarvis.sonos.messaging.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.messaging.model.MogelijkeUitgevoerdeActie;
import nl.heidotting.jarvis.sonos.messaging.sender.AbstractSpeakerSender;
import nl.heidotting.jarvis.sonos.messaging.sender.WoonkamerSender;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class WoonkamerSonosService extends SonosService {
    private final WoonkamerSender woonkamerSender;
    private final StroomGebruikService stroomGebruikService;
    private final SonosStatusService sonosStatusService;
    private final UitgevoerdeActieService uitgevoerdeActieService;

    @Override
    public void verwerkVeranderingVoorSpeaker(SonosState sonosState) {
        log.info("Artist : {}, Title : {}", artist, title);
    }

    @Override
    public AbstractSpeakerSender getSender() {
        return woonkamerSender;
    }

    @Override
    public String getNaam() {
        return "Woonkamer";
    }

    @Override
    public SonosStatusService getSonosStatusService() {
        return this.sonosStatusService;
    }

    @Override
    public UitgevoerdeActieService getUitgevoerdeActieService() {
        return uitgevoerdeActieService;
    }

    public void eventueelStoppen() {
        var wattageTelevisie = stroomGebruikService.leesWattageVanApparaat("televisie");
        if (wattageTelevisie > 10 && sonosStatusService.speeltAf(getNaam())) {
            woonkamerSender.stuurStopCommand();
            uitgevoerdeActieService.opslaan(new MogelijkeUitgevoerdeActie(getNaam(), MogelijkeUitgevoerdeActie.MogelijkeActie.STOP_AFSPELEN));
        }
    }
}
