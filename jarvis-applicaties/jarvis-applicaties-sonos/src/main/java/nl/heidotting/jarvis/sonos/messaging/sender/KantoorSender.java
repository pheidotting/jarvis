package nl.heidotting.jarvis.sonos.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@Slf4j
@ApplicationScoped
public class KantoorSender extends AbstractSpeakerSender {
    private boolean connected = false;

    @Inject
    @Channel("kantoor-control")
    private Emitter<Object> kantoorController;

    @SneakyThrows
    @Override
    void stuur(Object o) {
        log.info("Stuur {}", o);
        kantoorController.send(o);
    }
}

