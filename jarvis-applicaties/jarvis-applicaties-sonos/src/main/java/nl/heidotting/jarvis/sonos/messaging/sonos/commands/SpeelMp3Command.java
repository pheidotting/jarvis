package nl.heidotting.jarvis.sonos.messaging.sonos.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SpeelMp3Command {
    private String command = "notify";
    private Input input;

    public SpeelMp3Command(Input input) {
        this.input = input;
    }
}

