package nl.heidotting.jarvis.sonos.messaging.model;

public record VolumeGewijzigdDoorZwarteLijstLied(
        int volume,
        String artist,
        String title
) {
}
