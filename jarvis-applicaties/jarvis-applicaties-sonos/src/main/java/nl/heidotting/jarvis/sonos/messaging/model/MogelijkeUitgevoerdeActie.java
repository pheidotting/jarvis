package nl.heidotting.jarvis.sonos.messaging.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class MogelijkeUitgevoerdeActie {
    public enum MogelijkeActie {
        VOLUME_LAGER_DOOR_ZWARTELIJST_NUMMER,
        EINDE_VOLUME_LAGER_DOOR_ZWARTELIJST_NUMMER,
        STOP_AFSPELEN,
        START_AFSPELEN;
    }

    private String speaker;
    private MogelijkeActie mogelijkeActie;

    @Override
    public String toString() {
        return mogelijkeActie + "-" + speaker;
    }
}


