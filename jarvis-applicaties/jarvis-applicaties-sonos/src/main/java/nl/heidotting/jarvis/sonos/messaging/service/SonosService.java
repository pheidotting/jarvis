package nl.heidotting.jarvis.sonos.messaging.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.messaging.sender.AbstractSpeakerSender;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;

import java.math.BigDecimal;

@Slf4j
public abstract class SonosService {
    protected Integer volume;
    protected boolean isPlaying;
    protected String artist;
    protected String title;
    protected int masterVolume;
    protected int volumeVoorMute;

    public abstract void verwerkVeranderingVoorSpeaker(SonosState sonosState);

    public abstract String getNaam();

    public abstract AbstractSpeakerSender getSender();

    public abstract SonosStatusService getSonosStatusService();

    public abstract UitgevoerdeActieService getUitgevoerdeActieService();

    public void verwerkVerandering(SonosState sonosState) {
        var transportState = sonosState.getTransportState();
        this.isPlaying = isPlaying(transportState);
        this.masterVolume = sonosState.getVolume().getMaster();

        var isQmusic = "qmusic".equalsIgnoreCase(sonosState.getEnqueuedMetadata().getTitle());
        artist = isQmusic ? sonosState.getCurrentTrack().getTitle() : sonosState.getCurrentTrack().getArtist();
        title = isQmusic ? sonosState.getCurrentTrack().getArtist() : sonosState.getCurrentTrack().getTitle();

        getSonosStatusService().opslaanStatus(getNaam(), isPlaying, artist, title, this.masterVolume);

        if (isPlaying(transportState) && !"ad break".equalsIgnoreCase(title) && !"qmusic nieuws".equalsIgnoreCase(title)) {
            if (artist != null && title != null) {
                verwerkVeranderingVoorSpeaker(sonosState);
            }
        }
    }

    public void speelQmusic() {
        getSender().setAvTransportUri("radio:s87683");
    }

    protected boolean isPlaying(String transportState) {
        return "playing".equalsIgnoreCase(transportState);
    }

    public void stuurSetVolumeOpdracht(BigDecimal volume) {
        getSender().setVolume(volume);
    }

    public void stuurStopOpdracht() {
        getSender().stuurStopCommand();
    }

    public void stuurStartOpdracht() {
        getSender().stuurStartCommand();
    }

    public void mute() {
        this.volumeVoorMute = this.masterVolume;
        getSender().setVolume(new BigDecimal(5));
    }

    public void unmute() {
        getSender().setVolume(new BigDecimal(this.volumeVoorMute));
    }
}
