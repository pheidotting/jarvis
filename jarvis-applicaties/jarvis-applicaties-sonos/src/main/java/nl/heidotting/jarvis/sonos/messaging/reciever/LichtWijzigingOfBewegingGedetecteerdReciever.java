package nl.heidotting.jarvis.sonos.messaging.reciever;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.messaging.service.WoonkamerSonosService;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class LichtWijzigingOfBewegingGedetecteerdReciever {
    private final WoonkamerSonosService woonkamerSonosService;

    @Incoming("LichtWijzigingOfBewegingGedetecteerd")
    public void listener(Object p) {
        woonkamerSonosService.eventueelStoppen();
    }


}
