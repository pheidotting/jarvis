package nl.heidotting.jarvis.sonos.messaging.sonos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Volume {
    @JsonProperty("Master")
    private int master;
    @JsonProperty("LF")
    private int lf;
    @JsonProperty("RF")
    private int rf;
}
