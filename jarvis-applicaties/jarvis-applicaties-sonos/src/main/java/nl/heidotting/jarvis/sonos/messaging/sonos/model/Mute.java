package nl.heidotting.jarvis.sonos.messaging.sonos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Mute {
    @JsonProperty("Master")
    private boolean master;
    @JsonProperty("LF")
    private boolean lf;
    @JsonProperty("RF")
    private boolean rf;
}
