package nl.heidotting.jarvis.sonos.messaging.sonos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class Track {
    @JsonProperty("Artist")
    private String artist;
    @JsonProperty("AlbumArtUri")
    private String albumArtUri;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("UpnpClass")
    private String upnpClass;
    @JsonProperty("ItemId")
    private String itemId;
    @JsonProperty("ParentId")
    private String parentId;
    @JsonProperty("TrackUri")
    private String trackUri;
    @JsonProperty("ProtocolInfo")
    private String protocolInfo;
    @JsonProperty("Duration")
    private String duration;
    @JsonProperty("Album")
    private String album;
}
