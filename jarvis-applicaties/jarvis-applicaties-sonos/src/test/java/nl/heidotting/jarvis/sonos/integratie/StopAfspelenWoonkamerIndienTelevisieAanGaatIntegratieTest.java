package nl.heidotting.jarvis.sonos.integratie;

import io.quarkus.test.junit.QuarkusTest;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.MetaData;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.Track;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.Volume;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class StopAfspelenWoonkamerIndienTelevisieAanGaatIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Als de televisie wordt aan gezet, dient het afspelen op de woonkamer speaker te worden gestopt.")
    void test() {
        var wattage = Map.of("wattage", "11");
        var volumeGetal = 10;

        var track = new Track();
        track.setArtist("aaa");
        track.setTitle(UUID.randomUUID().toString());
        track.setAlbum(UUID.randomUUID().toString());
        track.setDuration(Duration.of(123, ChronoUnit.SECONDS).toString());
        var volume = new Volume();
        volume.setMaster(volumeGetal);
        var metadata = new MetaData();
        metadata.setTitle(UUID.randomUUID().toString());
        var state = new SonosState();
        state.setCurrentTrack(track);
        state.setVolume(volume);
        state.setEnqueuedMetadata(metadata);
        state.setTransportState("playing");

        woonkamerExchangeQueueUtil.stuurBerichtNaarExchange(state);

        jedis.hset("StroomGebruik:televisie", wattage);

        lichtWijzigingOfBewegingGedetecteerdQueue.stuurBerichtNaarExchange("");

        woonkamerMutatieExchangeQueueUtil.vangBericht()
                .ifPresent(stopCommand -> assertThat(stopCommand.getCommand()).isEqualTo("stop"));
    }

    @Test
    @DisplayName("Als de televisie wordt aan gezet, dient het afspelen op de woonkamer speaker te worden gestopt, maar alleen als er iets speelt")
    void test1() {
        var wattage = Map.of("wattage", "11");
        var volumeGetal = 10;

        var track = new Track();
        track.setArtist(UUID.randomUUID().toString());
        track.setTitle(UUID.randomUUID().toString());
        track.setAlbum(UUID.randomUUID().toString());
        track.setDuration(Duration.of(123, ChronoUnit.SECONDS).toString());
        var volume = new Volume();
        volume.setMaster(volumeGetal);
        var metadata = new MetaData();
        metadata.setTitle(UUID.randomUUID().toString());
        var state = new SonosState();
        state.setCurrentTrack(track);
        state.setVolume(volume);
        state.setEnqueuedMetadata(metadata);
        state.setTransportState("stopped");

        woonkamerExchangeQueueUtil.stuurBerichtNaarExchange(state);

        jedis.hset("StroomGebruik:televisie", wattage);

        lichtWijzigingOfBewegingGedetecteerdQueue.stuurBerichtNaarExchange("");

        woonkamerMutatieExchangeQueueUtil.vangGeenBericht();
    }
}
