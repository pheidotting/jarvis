package nl.heidotting.jarvis.sonos.integratie;

import io.quarkus.test.junit.QuarkusTest;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.MetaData;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.Track;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.Volume;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
class ZwartelijstIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Als er een nummer afgespeeld wordt welke op de zwarte lijst staat, gaat het volume zachter, alleen op de kantoorspeaker")
    void testZwartelijst() {
        var volumeGetal = 10;

        var track = new Track();
        track.setArtist(UUID.randomUUID().toString());
        track.setTitle(UUID.randomUUID().toString());
        track.setAlbum(UUID.randomUUID().toString());
        track.setDuration(Duration.of(123, ChronoUnit.SECONDS).toString());
        var volume = new Volume();
        volume.setMaster(volumeGetal);
        var metadata = new MetaData();
        metadata.setTitle(UUID.randomUUID().toString());
        var state = new SonosState();
        state.setCurrentTrack(track);
        state.setVolume(volume);
        state.setEnqueuedMetadata(metadata);
        state.setTransportState("playing");

        kantoorExchangeQueueUtil.stuurBerichtNaarExchange(state);

        state.getCurrentTrack().setArtist("aaa");

        kantoorExchangeQueueUtil.stuurBerichtNaarExchange(state);

        kantoorMutatieExchangeQueueUtil.vangBericht()
                .ifPresent(volumeCommand -> {
                    assertThat(volumeCommand.getCommand()).isEqualTo("volume");
                    assertThat(volumeCommand.getInput()).isEqualTo(1);
                });

        state.getCurrentTrack().setArtist(UUID.randomUUID().toString());
        state.getVolume().setMaster(1);
        kantoorExchangeQueueUtil.stuurBerichtNaarExchange(state);

        kantoorMutatieExchangeQueueUtil.vangBericht()
                .ifPresent(volumeCommand -> {
                    assertThat(volumeCommand.getCommand()).isEqualTo("volume");
                    assertThat(volumeCommand.getInput()).isEqualTo(volumeGetal);
                });
    }
}
