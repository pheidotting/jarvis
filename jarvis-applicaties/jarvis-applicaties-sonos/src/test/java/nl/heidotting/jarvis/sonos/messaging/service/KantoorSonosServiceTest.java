package nl.heidotting.jarvis.sonos.messaging.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.messaging.sender.KantoorSender;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.MetaData;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.Track;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.Volume;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.List;

import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
class KantoorSonosServiceTest {
    @InjectMocks
    private KantoorSonosService kantoorSonosService;

    @Mock
    private KantoorSender kantoorSender;
    @Mock
    private SonosStatusService sonosStatusService;
    @Mock
    private UitgevoerdeActieService uitgevoerdeActieService;

    @Test
    @DisplayName("Happy flow")
    void verwerkVeranderingVoorSpeaker() throws IllegalAccessException {
        FieldUtils.writeField(kantoorSonosService, "zwarteLijst", List.of(), true);

        kantoorSonosService.verwerkVerandering(maakState("Artiest", "Nummer", "station"));

        verifyNoInteractions(kantoorSender);
    }

    @Test
    @DisplayName("Er wordt een nummer afgespeeld wat op de zwarte lijst staat vervolgens is deze afgelopen")
    void testZwarteLijst() throws IllegalAccessException {
        FieldUtils.writeField(kantoorSonosService, "zwarteLijst", List.of("Abc"), true);

        kantoorSonosService.verwerkVerandering(maakState("Abc", "Nummer", "station"));

        verify(kantoorSender).setVolume(new BigDecimal(1));
        verify(sonosStatusService).setMuteStatus("Kantoor", true);
        verify(sonosStatusService).setHoofdVolume("Kantoor", 14);

        when(sonosStatusService.getMuteStatus("Kantoor")).thenReturn(true);
        when(sonosStatusService.getHoofdVolume("Kantoor")).thenReturn(14);

        kantoorSonosService.verwerkVerandering(maakState("asafd", "Nummer2", "station"));

        verify(kantoorSender).setVolume(new BigDecimal(14));
        verify(sonosStatusService).setMuteStatus("Kantoor", false);
        verify(sonosStatusService).getHoofdVolume("Kantoor");

        verifyNoMoreInteractions(kantoorSender);
    }

    private SonosState maakState(String artist, String title, String station) {
        var sonosState = new SonosState();
        var track = new Track();
        track.setArtist(artist);
        track.setTitle(title);
        sonosState.setCurrentTrack(track);
        var volume = new Volume();
        volume.setMaster(14);
        sonosState.setVolume(volume);
        var metaData = new MetaData();
        metaData.setTitle(station);
        sonosState.setEnqueuedMetadata(metaData);
        sonosState.setTransportState("PLAYING");

        return sonosState;
    }
}