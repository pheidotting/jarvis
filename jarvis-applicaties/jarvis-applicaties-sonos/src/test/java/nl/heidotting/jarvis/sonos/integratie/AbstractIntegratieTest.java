package nl.heidotting.jarvis.sonos.integratie;


import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.common.QueueUtil;
import nl.heidotting.jarvis.sonos.common.TestGegevens;
import nl.heidotting.jarvis.sonos.messaging.sonos.commands.StartStopCommando;
import nl.heidotting.jarvis.sonos.messaging.sonos.commands.VolumeCommand;
import nl.heidotting.jarvis.sonos.messaging.sonos.model.SonosState;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import redis.clients.jedis.Jedis;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
abstract class AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.LichtWijzigingOfBewegingGedetecteerd.exchange.name")
    protected String exchangeLichtWijzigingOfBewegingGedetecteerd;
    @ConfigProperty(name = "mp.messaging.incoming.kantoor.exchange.name")
    protected String kantoorExchange;
    @ConfigProperty(name = "mp.messaging.incoming.kantoor.routing-keys")
    protected String kantoorExchangeRoutingkey;
    @ConfigProperty(name = "mp.messaging.incoming.woonkamer.exchange.name")
    protected String woonkamerExchange;
    @ConfigProperty(name = "mp.messaging.incoming.woonkamer.routing-keys")
    protected String woonkamerExchangeRoutingkey;
    @ConfigProperty(name = "mp.messaging.outgoing.woonkamer-control.exchange.name")
    protected String woonkamerMutatieExchange;
    @ConfigProperty(name = "mp.messaging.outgoing.woonkamer-control.default-routing-key")
    protected String woonkamerMutatieExchangeRoutingkey;
    @ConfigProperty(name = "mp.messaging.outgoing.kantoor-control.exchange.name")
    protected String kantoorMutatieExchange;
    @ConfigProperty(name = "mp.messaging.outgoing.kantoor-control.default-routing-key")
    protected String kantoorMutatieExchangeRoutingkey;
    @ConfigProperty(name = "rabbitmq-host")
    protected String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    protected String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    protected String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    protected String rabbitPassword;
    @ConfigProperty(name = "quarkus.redis.hosts")
    protected String redisHost;

    protected Jedis jedis;
    protected QueueUtil<Void> lichtWijzigingOfBewegingGedetecteerdQueue;
    protected QueueUtil<SonosState> kantoorExchangeQueueUtil;
    protected QueueUtil<VolumeCommand> kantoorMutatieExchangeQueueUtil;
    protected QueueUtil<SonosState> woonkamerExchangeQueueUtil;
    protected QueueUtil<StartStopCommando> woonkamerMutatieExchangeQueueUtil;


    @BeforeEach
    void init() {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        jedis = new Jedis(redisHost);

        lichtWijzigingOfBewegingGedetecteerdQueue = maakLichtWijzigingOfBewegingGedetecteerdQueueUtil();
        kantoorExchangeQueueUtil = maakKantoorExchangeQueueUtil();
        kantoorMutatieExchangeQueueUtil = maakKantoorMutatieExchangeQueueUtil();
        woonkamerExchangeQueueUtil = maakWoonkamerExchangeQueueUtil();
        woonkamerMutatieExchangeQueueUtil = maakWoonkamerMutatieExchangeQueueUtil();
    }

    public void checkBerichtOpQueue(QueueUtil queueUtil, String bericht) {
        queueUtil.vangBericht().ifPresentOrElse(s -> {
            assertThat(s).isInstanceOf(String.class);
            assertThat(s).isEqualTo(bericht);
        }, () -> fail("geen bericht ontvangen op de 'bij de bank' queue"));
    }

    public QueueUtil<Void> maakLichtWijzigingOfBewegingGedetecteerdQueueUtil() {
        return new QueueUtil<Void>() {
            @Override
            public String getExchange() {
                return exchangeLichtWijzigingOfBewegingGedetecteerd;
            }

            @Override
            public Class<Void> getClazz() {
                return Void.class;
            }

            @Override
            public String getRoutingKey() {
                return "";
            }

            @Override
            public Types getType() {
                return Types.FANOUT;
            }
        };
    }

    public QueueUtil<SonosState> maakKantoorExchangeQueueUtil() {
        return new QueueUtil<SonosState>() {
            @Override
            public String getExchange() {
                return kantoorExchange;
            }

            @Override
            public Class<SonosState> getClazz() {
                return SonosState.class;
            }

            @Override
            public String getRoutingKey() {
                return kantoorExchangeRoutingkey;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<VolumeCommand> maakKantoorMutatieExchangeQueueUtil() {
        return new QueueUtil<VolumeCommand>() {
            @Override
            public String getExchange() {
                return kantoorMutatieExchange;
            }

            @Override
            public Class<VolumeCommand> getClazz() {
                return VolumeCommand.class;
            }

            @Override
            public String getRoutingKey() {
                return kantoorMutatieExchangeRoutingkey;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<SonosState> maakWoonkamerExchangeQueueUtil() {
        return new QueueUtil<SonosState>() {
            @Override
            public String getExchange() {
                return woonkamerExchange;
            }

            @Override
            public Class<SonosState> getClazz() {
                return SonosState.class;
            }

            @Override
            public String getRoutingKey() {
                return woonkamerExchangeRoutingkey;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<StartStopCommando> maakWoonkamerMutatieExchangeQueueUtil() {
        return new QueueUtil<StartStopCommando>() {
            @Override
            public String getExchange() {
                return woonkamerMutatieExchange;
            }

            @Override
            public Class<StartStopCommando> getClazz() {
                return StartStopCommando.class;
            }

            @Override
            public String getRoutingKey() {
                return woonkamerMutatieExchangeRoutingkey;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }
}
