package nl.heidotting.jarvis.telegram.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;

import java.math.BigDecimal;
import java.time.LocalDate;

import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;

@ApplicationScoped
@RequiredArgsConstructor
public class UrenNogTeGaanService {
    private final TelegramService telegramService;

    @SneakyThrows
    public void verwerkUrenNogTeGaan(float urenDezeWeek, float urenNogTeGaanVoor32, float urenNogTeGaanVoor36, float urenNogTeGaanVoor40, LocalDate nu) {
        var dezeWeek = new BigDecimal(urenDezeWeek).setScale(1, HALF_UP);
        var dagenNogTeGaan = 5 - nu.getDayOfWeek().getValue();

        var nogTeGaanVoor32 = new BigDecimal(urenNogTeGaanVoor32).setScale(1, HALF_UP);
        var nogTeGaanVoor36 = new BigDecimal(urenNogTeGaanVoor36).setScale(1, HALF_UP);
        var nogTeGaanVoor40 = new BigDecimal(urenNogTeGaanVoor40).setScale(1, HALF_UP);
        var gemiddeldNogVoor32 = ZERO;
        var gemiddeldNogVoor36 = ZERO;
        var gemiddeldNogVoor40 = ZERO;
        if (dagenNogTeGaan > 0) {
            gemiddeldNogVoor32 = new BigDecimal(String.valueOf(nogTeGaanVoor32.divide(new BigDecimal(dagenNogTeGaan)))).setScale(1, HALF_UP);
            gemiddeldNogVoor36 = new BigDecimal(String.valueOf(nogTeGaanVoor36.divide(new BigDecimal(dagenNogTeGaan)))).setScale(1, HALF_UP);
            gemiddeldNogVoor40 = new BigDecimal(String.valueOf(nogTeGaanVoor40.divide(new BigDecimal(dagenNogTeGaan)))).setScale(1, HALF_UP);
        }

        var tekst = new StringBuilder("Deze week heb je ");
        tekst.append(dezeWeek);
        tekst.append(" uur gemaakt, nog te gaan:");
        tekst.append("\n");
        tekst.append("Voor 32 uur: ");
        tekst.append(nogTeGaanVoor32);
        if (gemiddeldNogVoor32.compareTo(ZERO) > 0) {
            tekst.append(", gemiddeld per dag : ");
            tekst.append(gemiddeldNogVoor32);
        }
        tekst.append("\n");
        tekst.append("Voor 36 uur: ");
        tekst.append(nogTeGaanVoor36);
        if (gemiddeldNogVoor36.compareTo(ZERO) > 0) {
            tekst.append(", gemiddeld per dag : ");
            tekst.append(gemiddeldNogVoor36);
        }
        tekst.append("\n");
        tekst.append("Voor 40 uur: ");
        tekst.append(nogTeGaanVoor40);
        if (gemiddeldNogVoor40.compareTo(ZERO) > 0) {
            tekst.append(", gemiddeld per dag : ");
            tekst.append(gemiddeldNogVoor40);
        }

        telegramService.sendMessageToChannel(tekst.toString());
    }
}
