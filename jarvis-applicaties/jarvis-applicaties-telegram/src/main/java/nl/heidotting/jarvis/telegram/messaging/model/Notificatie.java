package nl.heidotting.jarvis.telegram.messaging.model;

import java.io.Serializable;

public record Notificatie(String tekst) implements Serializable {
}
