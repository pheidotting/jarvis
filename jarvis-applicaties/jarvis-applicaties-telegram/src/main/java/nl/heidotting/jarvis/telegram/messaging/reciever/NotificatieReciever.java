package nl.heidotting.jarvis.telegram.messaging.reciever;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.telegram.messaging.model.Notificatie;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class NotificatieReciever extends AbstractReciever {
    private final TelegramService telegramService;

    @Incoming("notificatie")
    public void listener(io.vertx.core.json.JsonObject m) {
        var notificatie = (Notificatie) map(m, Notificatie.class);
        log.info("Binnengekomen {}", notificatie);

        telegramService.sendMessageToChannel(notificatie.tekst());
    }


}
