package nl.heidotting.jarvis.telegram.messaging.reciever;

import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.telegram.messaging.model.UrenNogTeGaan;
import nl.heidotting.jarvis.telegram.service.UrenNogTeGaanService;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import java.time.LocalDate;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class UrenNogTeGaanReciever {
    private final UrenNogTeGaanService urenNogTeGaanService;

    @Incoming("uren-nog-te-gaan")
    public void listener(JsonObject p) {
        UrenNogTeGaan urenNogTeGaan = p.mapTo(UrenNogTeGaan.class);
        log.info("Binnengekomen {}", urenNogTeGaan);

        urenNogTeGaanService.verwerkUrenNogTeGaan(
                urenNogTeGaan.getUrenDezeWeek(),
                urenNogTeGaan.getNogTeGaanVoor32(),
                urenNogTeGaan.getNogTeGaanVoor36(),
                urenNogTeGaan.getNogTeGaanVoor40(),
                LocalDate.now()
        );
    }


}
