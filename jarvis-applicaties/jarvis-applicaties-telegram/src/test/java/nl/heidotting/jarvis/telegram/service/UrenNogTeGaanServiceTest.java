package nl.heidotting.jarvis.telegram.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UrenNogTeGaanServiceTest {
    @Mock
    private TelegramService telegramService;

    @InjectMocks
    private UrenNogTeGaanService urenNogTeGaanService;

    @Test
    void verwerkUrenNogTeGaan() {
        urenNogTeGaanService.verwerkUrenNogTeGaan(1.23456f, 2.3456789f, 3.4567f, 4.56789f, LocalDate.of(2024, 1, 24));

        verify(telegramService).sendMessageToChannel("Deze week heb je 1.2 uur gemaakt, nog te gaan:\nVoor 32 uur: 2.3, gemiddeld per dag : 1.2\nVoor 36 uur: 3.5, gemiddeld per dag : 1.8\nVoor 40 uur: 4.6, gemiddeld per dag : 2.3");
    }
}