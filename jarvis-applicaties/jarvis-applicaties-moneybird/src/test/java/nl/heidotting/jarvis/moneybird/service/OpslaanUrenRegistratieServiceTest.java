package nl.heidotting.jarvis.moneybird.service;

import nl.heidotting.jarvis.moneybird.client.model.project.Project;
import nl.heidotting.jarvis.moneybird.client.model.timeentry.TimeEntry;
import nl.heidotting.jarvis.moneybird.messaging.model.Urenregistratie;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OpslaanUrenRegistratieServiceTest {
    @InjectMocks
    private OpslaanUrenRegistratieService opslaanUrenRegistratieService;

    @Mock
    private ProjectService projectService;
    @Mock
    private UrenService urenService;

    @Test
    @DisplayName("Afgesloten op maandag, met 3 uurtjes gewerkt")
    public void maandagDrieUurtjes() {
        var taakNaam = "taakNaam";
        var taakId = 34L;
        var lijst = new ArrayList<Urenregistratie>();
        var lijstEntries = new ArrayList<TimeEntry>();

        //Maandag
        var urenregistratie1 = maakUrenregistratie(LocalDateTime.of(2024, 2, 5, 7, 5), LocalDateTime.of(2024, 2, 5, 10, 55), Duration.ofHours(3).plusMinutes(50));
        lijst.add(urenregistratie1);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie1, lijst);

        //Maandag (2)
        var urenregistratie2 = maakUrenregistratie(LocalDateTime.of(2024, 2, 5, 11, 30), LocalDateTime.of(2024, 2, 5, 13, 35), Duration.ofHours(2).plusMinutes(5));
        lijst.add(urenregistratie2);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie2, lijst);
        lijstEntries.add(new TimeEntry(1L, "2024-01-05T07:00:00.000Z", "2024-01-05T13:00:00.000Z", null, null, null, false, null));

        //Dinsdag
        var urenregistratie3 = maakUrenregistratie(LocalDateTime.of(2024, 2, 6, 6, 25), LocalDateTime.of(2024, 2, 6, 14, 40), Duration.ofHours(8).plusMinutes(15));
        lijst.add(urenregistratie3);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie3, lijst);
        lijstEntries.add(new TimeEntry(2L, "2024-01-06T07:00:00.000Z", "2024-01-06T15:00:00.000Z", null, null, null, false, null));

        //Woensdag (1)
        var urenregistratie4 = maakUrenregistratie(LocalDateTime.of(2024, 2, 7, 6, 35), LocalDateTime.of(2024, 2, 7, 8, 10), Duration.ofHours(1).plusMinutes(35));
        lijst.add(urenregistratie4);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie4, lijst);

        //Woensdag (2)
        var urenregistratie5 = maakUrenregistratie(LocalDateTime.of(2024, 2, 7, 8, 35), LocalDateTime.of(2024, 2, 7, 12, 00), Duration.ofHours(3).plusMinutes(25));
        lijst.add(urenregistratie5);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie5, lijst);

        //Woensdag (3)
        var urenregistratie6 = maakUrenregistratie(LocalDateTime.of(2024, 2, 7, 13, 5), LocalDateTime.of(2024, 2, 7, 16, 00), Duration.ofHours(2).plusMinutes(55));
        lijst.add(urenregistratie6);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie6, lijst);
        lijstEntries.add(new TimeEntry(3L, "2024-01-07T07:00:00.000Z", "2024-01-07T15:00:00.000Z", null, null, null, false, null));

        //Donderdag
        var urenregistratie7 = maakUrenregistratie(LocalDateTime.of(2024, 2, 8, 6, 5), LocalDateTime.of(2024, 2, 8, 16, 00), Duration.ofHours(9).plusMinutes(55));
        lijst.add(urenregistratie7);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie7, lijst);
        lijstEntries.add(new TimeEntry(4L, "2024-01-08T07:00:00.000Z", "2024-01-08T15:00:00.000Z", null, null, null, false, null));

        //Vrijdag
        var urenregistratie8 = maakUrenregistratie(LocalDateTime.of(2024, 2, 9, 6, 5), LocalDateTime.of(2024, 2, 9, 14, 00), Duration.ofHours(7).plusMinutes(55));
        lijst.add(urenregistratie8);

        when(projectService.ophalenProject(taakNaam)).thenReturn(new Project(taakId, taakNaam, null));
        when(urenService.entriesHuidigeWeek()).thenReturn(lijstEntries);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(taakNaam, urenregistratie8, lijst);
//    lijstEntries.add(new TimeEntry(3L, "2024-01-07T07:00:00.000Z", "2024-01-07T15:00:00.000Z", null, null, null, false, null));

        verify(urenService, times(8)).entriesHuidigeWeek();
        lijstEntries.forEach(timeEntry -> verify(urenService, atLeastOnce()).verwijder(timeEntry.id()));
        verify(urenService, times(1)).opslaan(LocalDateTime.of(2024, 2, 5, 7, 0), LocalDateTime.of(2024, 2, 5, 13, 0), taakId, taakNaam);
        verify(urenService, times(2)).opslaan(LocalDateTime.of(2024, 2, 6, 7, 0), LocalDateTime.of(2024, 2, 6, 15, 0), taakId, taakNaam);
        verify(urenService, times(2)).opslaan(LocalDateTime.of(2024, 2, 7, 7, 0), LocalDateTime.of(2024, 2, 7, 15, 0), taakId, taakNaam);
        verify(urenService, times(2)).opslaan(LocalDateTime.of(2024, 2, 8, 7, 0), LocalDateTime.of(2024, 2, 8, 15, 0), taakId, taakNaam);
        verify(urenService, times(1)).opslaan(LocalDateTime.of(2024, 2, 9, 7, 0), LocalDateTime.of(2024, 2, 9, 15, 0), taakId, taakNaam);

        verify(urenService, times(1)).opslaan(LocalDateTime.of(2024, 2, 5, 7, 0), LocalDateTime.of(2024, 2, 5, 15, 0), taakId, taakNaam);
        verify(urenService, times(2)).opslaan(LocalDateTime.of(2024, 2, 6, 7, 0), LocalDateTime.of(2024, 2, 6, 15, 0), taakId, taakNaam);
        verify(urenService, times(2)).opslaan(LocalDateTime.of(2024, 2, 7, 7, 0), LocalDateTime.of(2024, 2, 7, 15, 0), taakId, taakNaam);
        verify(urenService, times(2)).opslaan(LocalDateTime.of(2024, 2, 8, 7, 0), LocalDateTime.of(2024, 2, 8, 15, 0), taakId, taakNaam);

        verifyNoMoreInteractions(projectService, urenService);
    }

    private Urenregistratie maakUrenregistratie(LocalDateTime start, LocalDateTime eind, Duration verwachteDuration) {
        var urenregistratie = new Urenregistratie(start, eind);
        var duration = Duration.between(urenregistratie.startTijd(), urenregistratie.eindTijd());
        assert duration.equals(verwachteDuration);

        return urenregistratie;
    }
}