package nl.heidotting.jarvis.moneybird.client.model.project;

public record Project(
        Long id,
        String name,
        String state
) {
}
