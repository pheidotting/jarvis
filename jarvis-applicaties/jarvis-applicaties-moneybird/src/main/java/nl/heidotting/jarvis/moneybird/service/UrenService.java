package nl.heidotting.jarvis.moneybird.service;
//
//import lombok.extern.slf4j.Slf4j;
//import org.json.JSONArray;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.http.HttpMethod;
//import org.springframework.stereotype.Service;
//
//import java.time.Duration;
//import java.time.Instant;
//import java.time.LocalDate;
//import java.time.LocalDateTime;
//import java.time.ZoneId;
//import java.time.format.DateTimeFormatter;
//import java.time.temporal.ChronoUnit;
//
//import static com.google.common.collect.Lists.newArrayList;
//
//

import jakarta.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.moneybird.client.MoneyBirdClient;
import nl.heidotting.jarvis.moneybird.client.model.timeentry.OpslaanTimeEntry;
import nl.heidotting.jarvis.moneybird.client.model.timeentry.TimeEntry;
import nl.heidotting.jarvis.moneybird.client.model.timeentry.TimeEntryNieuw;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@ApplicationScoped
public class UrenService {
    @RestClient
    private MoneyBirdClient moneyBirdClient;
    private Long userid = 365712881003529541L;

    public void opslaan(LocalDateTime starttijd, LocalDateTime eindtijd, Long projectId, String projectNaam) {
        var o = ZoneId.systemDefault().getRules().getOffset(Instant.now());
        var formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        var entry = new TimeEntryNieuw(
                userid,
                starttijd.atZone(o).format(formatter) + " UTC",
                eindtijd.atZone(o).format(formatter) + " UTC",
                "Gewerkt " + projectNaam,
                null,
                projectId,
                null,
                true,
                0L
        );
        var opslaan = new OpslaanTimeEntry(entry);

        moneyBirdClient.opslaan(opslaan);
    }

    public List<TimeEntry> entriesHuidigeWeek() {
        return moneyBirdClient.timeEntriesHuidigeWeek();
    }

    public void verwijder(Long id) {
        moneyBirdClient.verwijder(id);
    }
    //    @Value("${moneybird.base.url}")
//    private String baseUrl;
//    @Value("${moneybird.autorisation.token}")
//    private String token;
//
//    @Override
//    protected String getBaseUrl() {
//        return baseUrl;
//    }
//
//    @Override
//    protected String getToken() {
//        return token;
//    }
//
//    private Long userid = 365712881003529541L;
//
//    public Duration bepaalAantalUrenDezeMaandVoorProject(Long projectId) {
//        return bepaalAantalUrenVoorProject(projectId, "period:this_month,");
//    }
//
//    public Duration bepaalAantalUrenVoorProject(Long projectId) {
//        return bepaalAantalUrenVoorProject(projectId, "");
//    }
//
//    private Duration bepaalAantalUrenVoorProject(Long projectId, String datumPeriode) {
//        var entries = new JSONArray(roepMoneyBirdAan(HttpMethod.GET, null, "time_entries.json?filter=" + datumPeriode + "project_id:" + projectId));
//        var o = ZoneId.systemDefault().getRules().getOffset(Instant.now());
//        var minutenTotaal = newArrayList(entries).stream()
//                .map(JSONObject.class::cast)
//                .map(entry -> {
//                    var start = LocalDateTime.parse(entry.getString("started_at").substring(0, 19)).plusSeconds(o.getTotalSeconds());
//                    var eind = LocalDateTime.parse(entry.getString("ended_at").substring(0, 19)).plusSeconds(o.getTotalSeconds());
//                    var pause = entry.getLong("paused_duration");
//                    return ChronoUnit.MINUTES.between(start, eind) - (pause / 60);
//                })
//                .mapToLong(value -> value).sum();
//
//        return Duration.of(minutenTotaal, ChronoUnit.MINUTES);
//    }
//
//    public void opslaanUrenRegistratie(LocalDateTime startTijd, LocalDateTime eindTijd, Long projectId, String projectNaam) {
//        var dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
//        var o = ZoneId.systemDefault().getRules().getOffset(Instant.now());
//        log.info("startTijd : {} , eindTijd : {}", startTijd, eindTijd);
//
//        long pause = 0L;
//        var id = "";
//        var entries = new JSONArray(roepMoneyBirdAan(HttpMethod.GET, null, "time_entries?filter=day:" + LocalDate.now().format(dtf)));
//
//        if (!entries.isEmpty()) {
//            var laatsteRegistratie = entries.getJSONObject(entries.length() - 1);
//            id = laatsteRegistratie.getString("id");
//            var eind = LocalDateTime.parse(laatsteRegistratie.getString("ended_at").substring(0, 19)).plusSeconds(o.getTotalSeconds());
//            long diff = ChronoUnit.SECONDS.between(eind, startTijd);
//            pause = laatsteRegistratie.getLong("paused_duration") + diff;
//            startTijd = LocalDateTime.parse(laatsteRegistratie.getString("started_at").substring(0, 19)).plusSeconds(o.getTotalSeconds());
//        }
//
//        var opslaanTimeEntry = new OpslaanTimeEntry(
//                new TimeEntryNieuw(
//                        userid,
//                        startTijd.atZone(o).toString(),
//                        eindTijd.atZone(o).toString(),
//                        "Gewerkt " + projectNaam,
//                        null,
//                        projectId,
//                        null,
//                        true,
//                        pause
//                )
//        );
//        var wijzigTimeEntry = new WijzigTimeEntry(
//                new TimeEntry(
//                        startTijd.atZone(o).toString(),
//                        eindTijd.atZone(o).toString(),
//                        "Gewerkt " + projectNaam,
//                        null,
//                        projectId,
//                        true,
//                        pause
//                )
//        );
//        if (!"".equals(id)) {
//            roepMoneyBirdAan(HttpMethod.PATCH, wijzigTimeEntry, "time_entries" + "/" + id);
//        } else {
//            roepMoneyBirdAan(HttpMethod.POST, opslaanTimeEntry, "time_entries");
//        }
//    }
//
//    public JSONArray haalAlleUrenregistratiesTussenTweeDatums(LocalDate beginDatum, LocalDate eindDatum) {
//        var dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
//
//        var entries = new JSONArray(roepMoneyBirdAan(HttpMethod.GET, null, "time_entries?filter=period:" + beginDatum.format(dtf) + ".." + eindDatum.format(dtf)));
//
//        if (entries.isEmpty()) {
//            entries = haalAlleUrenregistratiesTussenTweeDatums(beginDatum.plusYears(1), eindDatum.plusYears(1));
//        }
//
//        return entries;
//    }
}
