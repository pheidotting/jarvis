package nl.heidotting.jarvis.moneybird.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.moneybird.client.MoneyBirdClient;
import nl.heidotting.jarvis.moneybird.client.model.project.ProjectOpslaan;
import nl.heidotting.jarvis.moneybird.client.model.project.NieuwProject;
import nl.heidotting.jarvis.moneybird.client.model.project.Project;
import org.eclipse.microprofile.rest.client.inject.RestClient;

@Slf4j
@ApplicationScoped
public class ProjectService {
    @RestClient
    private MoneyBirdClient moneyBirdClient;

//    public String ophalenProjectOpId(Long projectId) {
//        var project = moneyBirdClient.leesProjectOpId(projectId);
//        return project.name();
//    }

    @SneakyThrows
    public Project ophalenProject(String projectNaam) {
        var project = moneyBirdClient.leesProjecten().stream()
                .filter(project1 -> projectNaam.equalsIgnoreCase(project1.name()))
                .findFirst();

        if (project.isEmpty()) {
            var nieuwProject = new ProjectOpslaan(new NieuwProject(projectNaam));
            moneyBirdClient.nieuwProject(nieuwProject);

            return ophalenProject(projectNaam);
        }
        return project.get();
    }

}