package nl.heidotting.jarvis.moneybird.client.model.timeentry;

public record TimeEntryNieuw(
        Long user_id,
        String started_at,
        String ended_at,
        String description,
        Long contact_id,
        Long project_id,
        Long detail_id,
        boolean billable,
        Long paused_duration
) {
}
