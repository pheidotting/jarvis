package nl.heidotting.jarvis.moneybird.messaging.model;

import java.time.LocalDateTime;

public record Urenregistratie(
        LocalDateTime startTijd,
        LocalDateTime eindTijd
) {
}
