package nl.heidotting.jarvis.moneybird.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.moneybird.client.model.timeentry.TimeEntry;
import nl.heidotting.jarvis.moneybird.messaging.model.Urenregistratie;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static java.time.DayOfWeek.*;
import static java.time.temporal.ChronoUnit.HOURS;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.util.List.of;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class OpslaanUrenRegistratieService {
    private final ProjectService projectService;
    private final UrenService urenService;

    private final List<DayOfWeek> MA_DI_WO_DO = of(MONDAY, TUESDAY, WEDNESDAY, THURSDAY);

    public void opslaanUrenRegistratie(String taakNaam, Urenregistratie urenregistratie, List<Urenregistratie> urenregistraties) {
        var project = projectService.ophalenProject(taakNaam);
        log.info("Project gevonden : {}", project);
        var projectId = project.id();

        var bestaandeEntries = urenService.entriesHuidigeWeek();
        if (MA_DI_WO_DO.contains(urenregistratie.startTijd().getDayOfWeek())) {
            bestaandeEntries.stream()
                    .filter(timeEntry -> {
                        var datumTimeEntry = LocalDateTime.parse(timeEntry.started_at().substring(0, 19)).toLocalDate();
                        var datumUrenregistratie = urenregistratie.startTijd().toLocalDate();
                        return datumTimeEntry.equals(datumUrenregistratie);
                    })
                    .map(TimeEntry::id)
                    .forEach(urenService::verwijder);

            var durationHuidigeDag = bepaalDuration(urenregistraties, urenregistratie.startTijd().toLocalDate());

            if (durationHuidigeDag.compareTo(Duration.of(450, MINUTES)) > 0) {
                //8 uur opslaan
                urenService.opslaan(LocalDateTime.of(urenregistratie.startTijd().toLocalDate(), LocalTime.of(7, 0)), LocalDateTime.of(urenregistratie.startTijd().toLocalDate(), LocalTime.of(15, 0)), projectId, taakNaam);
            } else if (durationHuidigeDag.compareTo(Duration.of(330, MINUTES)) > 0) {
                //6
                urenService.opslaan(LocalDateTime.of(urenregistratie.startTijd().toLocalDate(), LocalTime.of(7, 0)), LocalDateTime.of(urenregistratie.startTijd().toLocalDate(), LocalTime.of(13, 0)), projectId, taakNaam);
            }
        } else {
            bestaandeEntries.stream()
                    .map(TimeEntry::id)
                    .forEach(urenService::verwijder);
            var totaleDuration = bepaalTotaleDuration(urenregistraties);
            var datumMaandag = urenregistratie.startTijd().toLocalDate().minusDays(4);
            var zevenUur = LocalTime.of(7, 0);
            var vijftienUur = LocalTime.of(15, 0);
            var twaalfUur = LocalTime.of(12, 0);
            if (totaleDuration.compareTo(Duration.of(39, HOURS)) > 0) {
                urenService.opslaan(LocalDateTime.of(datumMaandag, zevenUur), LocalDateTime.of(datumMaandag, vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(1), zevenUur), LocalDateTime.of(datumMaandag.plusDays(1), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(2), zevenUur), LocalDateTime.of(datumMaandag.plusDays(2), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(3), zevenUur), LocalDateTime.of(datumMaandag.plusDays(3), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(4), zevenUur), LocalDateTime.of(datumMaandag.plusDays(4), vijftienUur), projectId, taakNaam);
            } else if (totaleDuration.compareTo(Duration.of(35, HOURS)) > 0) {
                urenService.opslaan(LocalDateTime.of(datumMaandag, zevenUur), LocalDateTime.of(datumMaandag, vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(1), zevenUur), LocalDateTime.of(datumMaandag.plusDays(1), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(2), zevenUur), LocalDateTime.of(datumMaandag.plusDays(2), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(3), zevenUur), LocalDateTime.of(datumMaandag.plusDays(3), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(4), zevenUur), LocalDateTime.of(datumMaandag.plusDays(4), twaalfUur), projectId, taakNaam);
            } else if (totaleDuration.compareTo(Duration.of(31, HOURS)) > 0) {
                urenService.opslaan(LocalDateTime.of(datumMaandag, zevenUur), LocalDateTime.of(datumMaandag, vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(1), zevenUur), LocalDateTime.of(datumMaandag.plusDays(1), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(2), zevenUur), LocalDateTime.of(datumMaandag.plusDays(2), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(3), zevenUur), LocalDateTime.of(datumMaandag.plusDays(3), vijftienUur), projectId, taakNaam);
            } else if (totaleDuration.compareTo(Duration.of(27, HOURS)) > 0) {
                urenService.opslaan(LocalDateTime.of(datumMaandag, zevenUur), LocalDateTime.of(datumMaandag, vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(1), zevenUur), LocalDateTime.of(datumMaandag.plusDays(1), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(2), zevenUur), LocalDateTime.of(datumMaandag.plusDays(2), vijftienUur), projectId, taakNaam);
                urenService.opslaan(LocalDateTime.of(datumMaandag.plusDays(3), zevenUur), LocalDateTime.of(datumMaandag.plusDays(3), twaalfUur), projectId, taakNaam);
            } else {
                urenregistraties.forEach(u -> urenService.opslaan(u.startTijd(), u.eindTijd(), projectId, taakNaam));
            }
        }
    }

    private Duration bepaalDuration(List<Urenregistratie> entries, LocalDate datum) {
        final Duration[] d = {Duration.ZERO};
        entries.stream()
                .filter(urenregistratie -> urenregistratie.startTijd().toLocalDate().equals(datum))
                .map(urenregistratie -> Duration.between(urenregistratie.startTijd(), urenregistratie.eindTijd()))
                .forEach(duration -> d[0] = d[0].plus(duration));

        return d[0];
    }

    private Duration bepaalTotaleDuration(List<Urenregistratie> entries) {
        final Duration[] d = {Duration.ZERO};
        entries.stream()
                .map(urenregistratie -> Duration.between(urenregistratie.startTijd(), urenregistratie.eindTijd()))
                .forEach(duration -> d[0] = d[0].plus(duration));

        return d[0];
    }
}
