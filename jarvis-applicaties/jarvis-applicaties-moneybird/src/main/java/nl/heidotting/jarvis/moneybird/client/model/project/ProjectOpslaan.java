package nl.heidotting.jarvis.moneybird.client.model.project;

public record ProjectOpslaan(
        NieuwProject project
) {
}
