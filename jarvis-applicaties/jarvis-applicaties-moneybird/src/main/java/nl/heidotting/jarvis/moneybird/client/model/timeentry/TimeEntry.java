package nl.heidotting.jarvis.moneybird.client.model.timeentry;

public record TimeEntry(
        Long id,
        String started_at,
        String ended_at,
        String description,
        Long contact_id,
        Long project_id,
        boolean billable,
        Long paused_duration
) {
}
