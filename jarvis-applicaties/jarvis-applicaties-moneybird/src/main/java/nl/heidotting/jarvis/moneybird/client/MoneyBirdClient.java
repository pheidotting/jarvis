package nl.heidotting.jarvis.moneybird.client;

import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.core.MediaType;
import nl.heidotting.jarvis.moneybird.client.model.project.Project;
import nl.heidotting.jarvis.moneybird.client.model.project.ProjectOpslaan;
import nl.heidotting.jarvis.moneybird.client.model.timeentry.OpslaanTimeEntry;
import nl.heidotting.jarvis.moneybird.client.model.timeentry.TimeEntry;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.reactive.RestQuery;

import java.util.List;

import static jakarta.ws.rs.core.HttpHeaders.*;


@RegisterRestClient(configKey = "moneybird-api")
public interface MoneyBirdClient {
    @GET
    @Path("projects/")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${moneybird.autorisation.token}")
    Project leesProjectOpId(@RestQuery Long id);

    @GET
    @Path("projects")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${moneybird.autorisation.token}")
    List<Project> leesProjecten();

    @POST
    @Path("projects")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${moneybird.autorisation.token}")
    @ClientHeaderParam(name = CONTENT_TYPE, value = MediaType.APPLICATION_JSON)
    @ClientHeaderParam(name = ACCEPT, value = MediaType.APPLICATION_JSON)
    void nieuwProject(ProjectOpslaan project);

    @GET
    @Path("time_entries.json?filter=period:this_week")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${moneybird.autorisation.token}")
    List<TimeEntry> timeEntriesHuidigeWeek();

    @DELETE
    @Path("time_entries/{id}")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${moneybird.autorisation.token}")
    void verwijder(@PathParam("id") Long id);

    @POST
    @Path("time_entries")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${moneybird.autorisation.token}")
    void opslaan(OpslaanTimeEntry opslaanTimeEntry);
}