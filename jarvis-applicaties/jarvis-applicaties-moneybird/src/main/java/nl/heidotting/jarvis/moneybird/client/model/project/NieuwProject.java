package nl.heidotting.jarvis.moneybird.client.model.project;

public record NieuwProject(
        String name
) {
}
