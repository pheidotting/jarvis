package nl.heidotting.jarvis.moneybird.messaging.reciever;

import io.smallrye.common.annotation.Blocking;
import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.moneybird.messaging.model.LopendeUrenRegistratieAfgesloten;
import nl.heidotting.jarvis.moneybird.service.OpslaanUrenRegistratieService;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import java.io.IOException;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class LopendeUrenregistratieAfgeslotenReciever {
//{"startTijd":[2024,2,2,20,36,42,587398000],"eindTijd":[2024,2,2,23,10,42,588606000],"project":"aa","taakId":389370657472775992}

    private final OpslaanUrenRegistratieService opslaanUrenRegistratieService;

    @Incoming("lopende-urenregistratie-afgesloten")
    @Blocking
    public void lopendeUrenRegistratieAfgesloten(JsonObject p) throws IOException {
        log.info("Binnengekomen {}", p);
//        var event = new ObjectMapper().findAndRegisterModules().readValue((byte[]) p, LopendeUrenRegistratieAfgesloten.class);
        var event = p.mapTo(LopendeUrenRegistratieAfgesloten.class);

        opslaanUrenRegistratieService.opslaanUrenRegistratie(
                event.taakNaam(),
                event.afgeslotenUrenregistratie(),
                event.urenregistratiesDezeWeek()
        );
    }
}
