package nl.heidotting.jarvis.integratie;

import io.quarkus.test.junit.QuarkusTest;
import nl.heidotting.jarvis.model.Notificatie;
import nl.heidotting.jarvis.model.SensorOpenOfDicht;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

@QuarkusTest
class RoanKamerdeurIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test een notificatie als Roan z'n kamerdeur open gaat")
    void test() {
        jedis.hset("OpenOfDicht:roan-kamerdeur-sensor", Map.of("open", "true"));
        jedis.hset("Status:roan-kamerdeur-sensor", Map.of("open", "false"));

        sensorOpenOfDichtExchangeQueueUtil.stuurBerichtNaarExchange(new SensorOpenOfDicht("roan-kamerdeur-sensor"));

        checkBerichtOpQueue(notificatieExchangeQueueUtil, new Notificatie("Roan zijn kamerdeur is zojuist geopend"));
    }

    @Test
    @DisplayName("Test een notificatie als Roan z'n kamerdeur dicht gaat")
    void test1() {
        jedis.hset("OpenOfDicht:roan-kamerdeur-sensor", Map.of("open", "false"));
        jedis.hset("Status:roan-kamerdeur-sensor", Map.of("open", "true"));

        sensorOpenOfDichtExchangeQueueUtil.stuurBerichtNaarExchange(new SensorOpenOfDicht("roan-kamerdeur-sensor"));

        checkBerichtOpQueue(notificatieExchangeQueueUtil, new Notificatie("Roan zijn kamerdeur is zojuist gesloten"));
    }

}
