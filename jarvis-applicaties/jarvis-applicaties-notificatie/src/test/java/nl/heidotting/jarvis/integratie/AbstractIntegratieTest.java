package nl.heidotting.jarvis.integratie;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.common.QueueUtil;
import nl.heidotting.jarvis.common.TestGegevens;
import nl.heidotting.jarvis.model.Notificatie;
import nl.heidotting.jarvis.model.SensorOpenOfDicht;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.BeforeEach;
import redis.clients.jedis.Jedis;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
abstract class AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.sensorOpenOfDicht.exchange.name")
    protected String sensorOpenOfDichtExchange;
    @ConfigProperty(name = "mp.messaging.outgoing.notificatie.exchange.name")
    protected String notificatieExchange;
    @ConfigProperty(name = "rabbitmq-host")
    protected String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    protected String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    protected String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    protected String rabbitPassword;
    @ConfigProperty(name = "quarkus.redis.hosts")
    protected String redisHost;

    protected Jedis jedis;
    protected QueueUtil<SensorOpenOfDicht> sensorOpenOfDichtExchangeQueueUtil;
    protected QueueUtil<Notificatie> notificatieExchangeQueueUtil;


    @BeforeEach
    void init() {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        jedis = new Jedis(redisHost);

        sensorOpenOfDichtExchangeQueueUtil = maakSensorOpenOfDichtExchangeQueueUtil();
        notificatieExchangeQueueUtil = maakNotificatieExchangeQueueUtil();
    }

    public void checkBerichtOpQueue(QueueUtil queueUtil, String bericht) {
        queueUtil.vangBericht().ifPresentOrElse(s -> {
            assertThat(s).isInstanceOf(String.class);
            assertThat(s).isEqualTo(bericht);
        }, () -> fail("geen bericht ontvangen op de 'bij de bank' queue"));
    }

    public void checkBerichtOpQueue(QueueUtil queueUtil, Notificatie bericht) {
        queueUtil.vangBericht().ifPresentOrElse(s -> {
            assertThat(s).isInstanceOf(Notificatie.class);
            assertThat(s).isEqualTo(bericht);
        }, () -> fail("geen bericht ontvangen op de 'bij de bank' queue"));
    }

    public QueueUtil<SensorOpenOfDicht> maakSensorOpenOfDichtExchangeQueueUtil() {
        return new QueueUtil<SensorOpenOfDicht>() {
            @Override
            public String getExchange() {
                return sensorOpenOfDichtExchange;
            }

            @Override
            public Class<SensorOpenOfDicht> getClazz() {
                return SensorOpenOfDicht.class;
            }

            @Override
            public String getRoutingKey() {
                return "";
            }

            @Override
            public Types getType() {
                return Types.FANOUT;
            }
        };
    }

    public QueueUtil<Notificatie> maakNotificatieExchangeQueueUtil() {
        return new QueueUtil<Notificatie>() {
            @Override
            public String getExchange() {
                return notificatieExchange;
            }

            @Override
            public Class<Notificatie> getClazz() {
                return Notificatie.class;
            }

            @Override
            public String getRoutingKey() {
                return "";
            }

            @Override
            public Types getType() {
                return Types.FANOUT;
            }
        };
    }
}
