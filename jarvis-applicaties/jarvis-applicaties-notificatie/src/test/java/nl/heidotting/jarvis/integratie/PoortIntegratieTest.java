package nl.heidotting.jarvis.integratie;

import io.quarkus.test.junit.QuarkusTest;
import nl.heidotting.jarvis.model.Notificatie;
import nl.heidotting.jarvis.model.SensorOpenOfDicht;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Map;

@QuarkusTest
class PoortIntegratieTest extends AbstractIntegratieTest {
    @Test
    @DisplayName("Test een notificatie als de poort open gaat")
    void test() {
        jedis.hset("OpenOfDicht:open-dicht-sensor", Map.of("open", "true"));
        jedis.hset("Status:open-dicht-sensor", Map.of("open", "false"));

        sensorOpenOfDichtExchangeQueueUtil.stuurBerichtNaarExchange(new SensorOpenOfDicht("open-dicht-sensor"));

        checkBerichtOpQueue(notificatieExchangeQueueUtil, new Notificatie("De poort is zojuist geopend"));
    }

    @Test
    @DisplayName("Test een notificatie als de poort dicht gaat")
    void test1() {
        jedis.hset("OpenOfDicht:open-dicht-sensor", Map.of("open", "false"));
        jedis.hset("Status:open-dicht-sensor", Map.of("open", "true"));

        sensorOpenOfDichtExchangeQueueUtil.stuurBerichtNaarExchange(new SensorOpenOfDicht("open-dicht-sensor"));

        checkBerichtOpQueue(notificatieExchangeQueueUtil, new Notificatie("De poort is zojuist gesloten"));
    }
}
