package nl.heidotting.jarvis.messaging.reciever;

import io.smallrye.common.annotation.Blocking;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.model.SensorOpenOfDicht;
import nl.heidotting.jarvis.service.VerwerkOpenDichtBerichtService;
import org.eclipse.microprofile.reactive.messaging.Incoming;


@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class SensorOpenOfDichtReciever extends AbstractReciever<SensorOpenOfDicht> {
    private final VerwerkOpenDichtBerichtService verwerkOpenDichtBerichtService;

    @Blocking
    @Incoming("sensorOpenOfDicht")
    public void recievedMessage(byte[] m) {
        var sensorOpenOfDicht = map(m, SensorOpenOfDicht.class);

        verwerkOpenDichtBerichtService.verwerk(sensorOpenOfDicht.id());
    }
}
