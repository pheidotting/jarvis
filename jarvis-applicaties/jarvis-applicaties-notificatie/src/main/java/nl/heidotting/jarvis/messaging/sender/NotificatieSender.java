package nl.heidotting.jarvis.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.model.Notificatie;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@Slf4j
@ApplicationScoped
public class NotificatieSender {
    private Emitter<Object> notificatieEmitter;

    public NotificatieSender(@Channel("notificatie") Emitter<Object> notificatieEmitter) {
        this.notificatieEmitter = notificatieEmitter;
    }

    public void sendNotificatie(Notificatie notificatie) {
        log.info("Stuur {}", notificatie);
        notificatieEmitter.send(notificatie);
    }
}
