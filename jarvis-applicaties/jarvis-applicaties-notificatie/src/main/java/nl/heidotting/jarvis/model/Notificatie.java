package nl.heidotting.jarvis.model;

import java.io.Serializable;

public record Notificatie(String tekst) implements Serializable {
}
