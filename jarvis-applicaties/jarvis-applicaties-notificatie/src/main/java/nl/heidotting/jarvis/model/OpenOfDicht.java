package nl.heidotting.jarvis.model;


import java.time.LocalDateTime;

public record OpenOfDicht(String id,
                          boolean open,
                          LocalDateTime tijdstip
) {
}
