package nl.heidotting.jarvis.service;

import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.messaging.sender.NotificatieSender;
import nl.heidotting.jarvis.model.Notificatie;
import nl.heidotting.jarvis.model.Status;
import nl.heidotting.jarvis.repository.OpenOfDichtRepository;
import nl.heidotting.jarvis.repository.StatusRepository;

import java.util.HashMap;
import java.util.Map;


@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class VerwerkOpenDichtBerichtService {
    private final OpenOfDichtRepository openOfDichtRepository;
    private final StatusRepository statusRepository;
    private final NotificatieSender notificatieSender;
    private final Map<String, String> namenLookUp = new HashMap<>();

    @PostConstruct
    public void init() {
        namenLookUp.put("open-dicht-sensor", "De poort");
        namenLookUp.put("gwen-kamerdeur-sensor", "Gwen haar kamerdeur");
        namenLookUp.put("roan-kamerdeur-sensor", "Roan zijn kamerdeur");
    }

    public void verwerk(String id) {
        log.info("Verwerk open/dicht sensor met id {}", id);
        openOfDichtRepository.leesOpenOfDichtStatus(id).ifPresentOrElse(openOfDicht -> {
            var status = statusRepository.leesStatus(id);
            if (openOfDicht.open()) {
                if (status.isEmpty() || openOfDicht.open()) {
                    statusRepository.save(new Status(id, true));
                    notificatieSender.sendNotificatie(new Notificatie(lookup(id) + " is zojuist geopend"));
                } else if (status.get().open() != openOfDicht.open()) {
                    statusRepository.save(new Status(id, status.get().open()));
                }
            } else {
                if (status.isEmpty() || !openOfDicht.open()) {
                    statusRepository.save(new Status(id, false));
                    notificatieSender.sendNotificatie(new Notificatie(lookup(id) + " is zojuist gesloten"));
                } else if (status.get().open() != openOfDicht.open()) {
                    statusRepository.save(new Status(id, status.get().open()));
                }
            }
        }, () -> {
            throw new UnsupportedOperationException("Open Of Dicht met id " + id + " niet gevonden in Redis");
        });
    }

    private String lookup(String id) {
        var naam = namenLookUp.get(id);
        if (naam == null) {
            return id;
        } else {
            return naam;
        }
    }
}
