package nl.heidotting.jarvis.repository;


import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.model.OpenOfDicht;

import java.time.LocalDateTime;
import java.util.Optional;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class OpenOfDichtRepository {
    private static final String OPEN_OF_DICHT = "OpenOfDicht:";

    private final RedisDataSource redisDataSource;

    private HashCommands<String, String, Boolean> booleanHashCommands;
    private HashCommands<String, String, String> stringHashCommands;

    @PostConstruct
    public void init() {
        booleanHashCommands = redisDataSource.hash(Boolean.class);
        stringHashCommands = redisDataSource.hash(String.class);
    }

    public Optional<OpenOfDicht> leesOpenOfDichtStatus(String id) {
        var tijdstipStr = stringHashCommands.hget(OPEN_OF_DICHT + id, "tijdstip");
        var tijdstip = tijdstipStr == null ? null : LocalDateTime.parse(tijdstipStr);
        var open = booleanHashCommands.hget(OPEN_OF_DICHT + id, "open");
        var id1 = stringHashCommands.hget(OPEN_OF_DICHT + id, "id");

        return Optional.ofNullable(new OpenOfDicht(id1, open, tijdstip));

    }
}
