package nl.heidotting.jarvis.repository;


import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import io.quarkus.redis.datasource.keys.KeyCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.model.Status;

import java.util.Optional;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class StatusRepository {
    private final RedisDataSource redisDataSource;

    private HashCommands<String, String, Boolean> booleanHashCommands;
    private HashCommands<String, String, String> stringHashCommands;
    private KeyCommands<String> keyCommands;
    private KeyCommands<Status> statusCommands;

    @PostConstruct
    public void init() {
        booleanHashCommands = redisDataSource.hash(Boolean.class);
        stringHashCommands = redisDataSource.hash(String.class);
        keyCommands = redisDataSource.key();
        statusCommands = redisDataSource.key(Status.class);
    }

    public Optional<Status> leesStatus(String id) {
        return keyCommands.keys("Status:*")
                .stream()
                .filter(id::equals)
                .map(s -> {
                    var open = booleanHashCommands.hget(s, "open");
                    var id1 = stringHashCommands.hget(s, "id");

                    return new Status(id1, open);
                })
                .findFirst();
    }

    public void save(Status status) {
        statusCommands.persist(status);
    }
}
