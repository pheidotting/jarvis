package nl.heidotting.jarvis.model;

import java.io.Serializable;

public record SensorOpenOfDicht(String id) implements Serializable {
}
