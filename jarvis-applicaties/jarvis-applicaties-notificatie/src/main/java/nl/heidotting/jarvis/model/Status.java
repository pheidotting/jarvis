package nl.heidotting.jarvis.model;

public record Status(
        String id,
        boolean open
) {
}
