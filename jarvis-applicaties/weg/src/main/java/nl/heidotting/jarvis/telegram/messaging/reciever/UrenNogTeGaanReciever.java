package nl.heidotting.jarvis.telegram.messaging.reciever;

import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import java.nio.charset.StandardCharsets;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class UrenNogTeGaanReciever {

    @Incoming("uren-nog-te-gaan")
    public void listener(byte[] p) {
        log.info("Binnengekomen {}", p);
        log.info("Binnengekomen {}", new String(p, StandardCharsets.UTF_8));
        log.info("Binnengekomen {}", new JsonObject(new String(p, StandardCharsets.UTF_8)));


    }


}
