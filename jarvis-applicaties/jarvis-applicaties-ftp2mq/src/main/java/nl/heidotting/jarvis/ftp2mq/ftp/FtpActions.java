package nl.heidotting.jarvis.ftp2mq.ftp;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.ftp2mq.ftp.common.FilesFacade;
import nl.heidotting.jarvis.ftp2mq.messaging.sender.IncomingFileEventSender;
import org.apache.ftpserver.ftplet.DefaultFtpReply;
import org.apache.ftpserver.ftplet.DefaultFtplet;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.FtpReply;
import org.apache.ftpserver.ftplet.FtpRequest;
import org.apache.ftpserver.ftplet.FtpSession;
import org.apache.ftpserver.ftplet.FtpletResult;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class FtpActions extends DefaultFtplet {
    private final IncomingFileEventSender incomingFileEventSender;

    private final FilesFacade filesFacade;

    private static final String SEPARATOR = "/";
    private static final String NO_PERMISSION_MESSAGE = "No permission.";

    @Override
    public FtpletResult onUploadStart(final FtpSession session, final FtpRequest request) throws FtpException, IOException {
        return FtpletResult.DEFAULT;
    }

    @Override
    public FtpletResult onUploadEnd(final FtpSession session, final FtpRequest request) throws FtpException, IOException {
        final String userName = session.getUser().getName();
        final Path filePath = getFilePath(session, request);
        final byte[] fileData = readAndDeleteTemporaryFile(filePath);

        var filename = filePath.getFileName().toString();
        log.info(filename);
        if (filename.toLowerCase().endsWith(".jpg")) {
            incomingFileEventSender.stuurImage(userName, filename, fileData);
        } else {
            incomingFileEventSender.stuurVideo(userName, filename, fileData);
        }

        return super.onUploadEnd(session, request);
    }

    @Override
    public FtpletResult onDeleteStart(final FtpSession session, final FtpRequest request) throws FtpException, IOException {
        session.write(new DefaultFtpReply(FtpReply.REPLY_450_REQUESTED_FILE_ACTION_NOT_TAKEN, NO_PERMISSION_MESSAGE));
        return FtpletResult.SKIP;
    }

    @Override
    public FtpletResult onDownloadStart(final FtpSession session, final FtpRequest request) throws FtpException, IOException {
        session.write(new DefaultFtpReply(FtpReply.REPLY_550_REQUESTED_ACTION_NOT_TAKEN, NO_PERMISSION_MESSAGE));
        return FtpletResult.SKIP;
    }

    private Path getFilePath(final FtpSession session, final FtpRequest request) throws FtpException {
        final String homeDirectory = session.getUser().getHomeDirectory();
        final String workingDirectory = session.getFileSystemView().getWorkingDirectory().getAbsolutePath();
        final String fileArgument = request.getArgument();

        return Paths.get(homeDirectory + workingDirectory + SEPARATOR + fileArgument);
    }

    private byte[] readAndDeleteTemporaryFile(final Path filePath) throws IOException {
        final byte[] fileData = filesFacade.readAllBytes(filePath);
        filesFacade.delete(filePath);

        return fileData;
    }

}
