package nl.heidotting.jarvis.ftp2mq.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.ftp2mq.messaging.model.IncomingFileEvent;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@Slf4j
@ApplicationScoped
public class IncomingFileEventSender {
    @Inject
    @Channel("image-sender")
    private Emitter<IncomingFileEvent> imageSender;

    @Inject
    @Channel("video-sender")
    private Emitter<IncomingFileEvent> videoSender;

    public void stuurImage(String username, String filename, byte[] file) {
        imageSender.send(new IncomingFileEvent(username, filename, file));
    }

    public void stuurVideo(String username, String filename, byte[] file) {
        videoSender.send(new IncomingFileEvent(username, filename, file));
    }
}
