package nl.heidotting.jarvis.ftp2mq.ftp.common;

import jakarta.enterprise.context.ApplicationScoped;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@ApplicationScoped
public class FilesFacade {

    public byte[] readAllBytes(Path path) throws IOException {
        return Files.readAllBytes(path);
    }

    public void delete(Path path) throws IOException {
        Files.delete(path);
    }

    public Path createTempDirectory() throws IOException {
        return Files.createTempDirectory(null);
    }
}
