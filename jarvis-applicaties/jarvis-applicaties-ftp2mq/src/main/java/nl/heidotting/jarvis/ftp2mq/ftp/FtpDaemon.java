package nl.heidotting.jarvis.ftp2mq.ftp;

import io.quarkus.runtime.Startup;
import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import org.apache.ftpserver.FtpServer;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.ftplet.FtpException;
import org.apache.ftpserver.ftplet.Ftplet;
import org.apache.ftpserver.listener.Listener;
import org.apache.ftpserver.listener.ListenerFactory;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Startup
@Singleton
public class FtpDaemon {
    private final FtpUserManager ftpUserManager;
    private final FtpActions ftpActions;
    private final FtpServerFactory ftpServerFactory;
    private final ListenerFactory listenerFactory;
    private final FtpProperties properties;

    private FtpServer ftpServer;

    public FtpDaemon(FtpUserManager ftpUserManager, FtpActions ftpActions, FtpServerFactory ftpServerFactory, ListenerFactory listenerFactory, FtpProperties properties) throws FtpException {
        this.ftpUserManager = ftpUserManager;
        this.ftpActions = ftpActions;
        this.ftpServerFactory = ftpServerFactory;
        this.listenerFactory = listenerFactory;
        this.properties = properties;

        ftpServer = createFtpServer();
        ftpServer.start();
    }

    private FtpServer createFtpServer() {
        ftpServerFactory.addListener("default", createListener());
        ftpServerFactory.setUserManager(ftpUserManager);
        ftpServerFactory.setFtplets(createFtplets());

        return ftpServerFactory.createServer();
    }

    private Listener createListener() {
        listenerFactory.setPort(properties.getPort());

        return listenerFactory.createListener();
    }

    private Map<String, Ftplet> createFtplets() {
        final Map<String, Ftplet> ftpletMap = new HashMap<>();
        ftpletMap.put("ftpActions", ftpActions);

        return ftpletMap;
    }

}