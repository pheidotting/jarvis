package nl.heidotting.jarvis.ftp2mq.ftp;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApplicationScoped
//@ConfigurationProperties("ftp")
public class FtpProperties {

    private int port = 2121;
    private String password = "secret";

}
