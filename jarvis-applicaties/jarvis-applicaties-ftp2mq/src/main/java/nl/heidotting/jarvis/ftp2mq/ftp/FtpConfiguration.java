package nl.heidotting.jarvis.ftp2mq.ftp;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.ws.rs.Produces;
import org.apache.ftpserver.FtpServerFactory;
import org.apache.ftpserver.listener.ListenerFactory;

@Dependent
//@ApplicationScoped
public class FtpConfiguration {

    @Produces
    @ApplicationScoped
    public FtpServerFactory ftpServerFactory() {
        return new FtpServerFactory();
    }

    @Produces
    @ApplicationScoped
    public ListenerFactory listenerFactory() {
        return new ListenerFactory();
    }

}