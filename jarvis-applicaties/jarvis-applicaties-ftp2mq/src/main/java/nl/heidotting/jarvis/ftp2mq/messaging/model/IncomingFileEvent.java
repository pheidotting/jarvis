package nl.heidotting.jarvis.ftp2mq.messaging.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IncomingFileEvent {
    private String username;
    private String filename;
    @ToString.Exclude
    private byte[] fileData;
}
