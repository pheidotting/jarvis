package nl.heidotting.jarvis.amazon.builder;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.context.Dependent;
import jakarta.ws.rs.Produces;
import lombok.Getter;
import org.eclipse.microprofile.config.inject.ConfigProperty;

@Dependent
@Getter
public class ClientBuilder {
    @ConfigProperty(name = "access-key")
    private String accessKey;
    @ConfigProperty(name = "secret-key")
    private String secretKey;

    @Produces
    @ApplicationScoped
    public AmazonS3 amazonS3() {
        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials()))
                .withRegion(Regions.EU_CENTRAL_1)
                .build();
    }

    private BasicAWSCredentials credentials() {
        return new BasicAWSCredentials(accessKey, secretKey);
    }
}
