package nl.heidotting.jarvis.amazon.scheduler;

import io.quarkus.runtime.Startup;
import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.amazon.service.LeesOpnamesUitAmazonService;

@Slf4j
@ApplicationScoped
@Startup
@RequiredArgsConstructor
public class Scheduler {
    private final LeesOpnamesUitAmazonService leesOpnamesUitAmazonService;

    //    @Scheduled(cron = "{check.amazon.cron.expr}")
//    @Scheduled(every="120h")//5dagen
    @Scheduled(every = "13h")
    void opruimenOudeMeuk() {
        leesOpnamesUitAmazonService.verwijderOudeSpullenVanSDrie();
    }
}
