package nl.heidotting.jarvis.amazon.service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.amazon.messaging.model.VerwijderVanS3;
import nl.heidotting.jarvis.amazon.messaging.sender.VerwijderVanS3Sender;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.lang.Thread.sleep;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class LeesOpnamesUitAmazonService {
    @ConfigProperty(name = "bucket-naam")
    private String bucketNaam;

    private final AmazonS3 amazonS3;
    private final VerwijderVanS3Sender verwijderVanS3Sender;

    private String marker = null;

    public static AmazonS3 amazonS3() {
        return AmazonS3ClientBuilder
                .standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials()))
                .withRegion(Regions.EU_CENTRAL_1)
                .build();
    }

    private static BasicAWSCredentials credentials() {
        return new BasicAWSCredentials("AKIAXLIN7JODRETY3YIJ", "mKAmYrAwuKA4WuOEjQqN0375vIHjPupJ3yvlUwTb");
    }

    public static void main(String[] args) {
        new LeesOpnamesUitAmazonService(amazonS3(), null).verwijderOudeSpullenVanSDrie();
    }

    public void verwijderOudeSpullenVanSDrie() {
        this.bucketNaam = "camera-opnames";
        String nextMarker = "";
        var objecten = new ArrayList<S3ObjectSummary>();

        while (nextMarker != null) {
            marker = nextMarker;
            var listObjectsRequest = new ListObjectsRequest(bucketNaam, null, nextMarker, null, 100000);
            var objects = amazonS3.listObjects(listObjectsRequest);
            if (objects.getObjectSummaries().size() > 0) {
                nextMarker = objects.getNextMarker();
                objecten.addAll(objects.getObjectSummaries());
            }
        }

        var datums = objecten.stream()
                .map(s3ObjectSummary -> s3ObjectSummary.getKey().substring(0, 10))
                .collect(Collectors.toSet());

        var eenHalfJaar = LocalDate.now().minusMonths(6);
        var tweeWeken = LocalDate.now().minusWeeks(2);
        var geheelTeVerwijderenDatums = datums.stream()
                .sorted(String::compareTo)
                .filter(s -> LocalDate.parse(s).isBefore(eenHalfJaar))
                .toList();
        var afbeeldingenTeVerwijderenDatums = datums.stream()
                .sorted(String::compareTo)
                .filter(s -> LocalDate.parse(s).isBefore(tweeWeken))
                .toList();

        var teVerwijderenObjecten = objecten.stream()
                .filter(s3ObjectSummary -> geheelTeVerwijderenDatums.contains(s3ObjectSummary.getKey().substring(0, 10)))
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toSet());
        teVerwijderenObjecten.addAll(objecten.stream()
                .filter(s3ObjectSummary -> afbeeldingenTeVerwijderenDatums.contains(s3ObjectSummary.getKey().substring(0, 10)) && s3ObjectSummary.getKey().endsWith(".jpg"))
                .map(S3ObjectSummary::getKey)
                .collect(Collectors.toSet()));

        var counter = new AtomicInteger(0);
        var batchSize = new AtomicInteger(3000);

        teVerwijderenObjecten
                .stream()
                .sorted(String::compareTo)
                .map(s -> new VerwijderVanS3(bucketNaam, s))
                .forEach(verwijderVanS3 -> {
                    if (counter.incrementAndGet() == batchSize.get()) {
                        try {
                            sleep(200000);
                        } catch (InterruptedException e) {
                            throw new RuntimeException(e);
                        }
                        counter.set(0);
                    }
                    verwijderVanS3Sender.stuur(verwijderVanS3);
                });
    }
}
