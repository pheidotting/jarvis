package nl.heidotting.jarvis.amazon.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.amazon.messaging.model.VerwijderVanS3;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@Slf4j
@ApplicationScoped
public class VerwijderVanS3Sender {
    @Inject
    @Channel("verwijder-van-s3-sender")
    private Emitter<VerwijderVanS3> verwijderVanS3Emitter;

    public void stuur(VerwijderVanS3 verwijderVanS3) {
//        log.info("{}", verwijderVanS3);
        try {
            verwijderVanS3Emitter.send(verwijderVanS3);
        } catch (Exception e) {
//            log.info(e.getMessage());
        }
    }
}
