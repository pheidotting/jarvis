package nl.heidotting.jarvis.amazon.messaging.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class VerwijderVanS3 {
    private String bucketNaam;
    private String key;
}
