package nl.heidotting.jarvis.amazon.service;

import com.amazonaws.services.s3.AmazonS3;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class VerwijderVanS3Service {
    private final AmazonS3 amazonS3;

    public void verwijderVanS3(String bucketNaam, String key) {
        log.debug("Verwijder {}", key);
        try {
            amazonS3.deleteObject(bucketNaam, key);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }
}
