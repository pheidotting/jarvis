package nl.heidotting.jarvis.amazon.messaging.reciever;

import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.amazon.messaging.model.VerwijderVanS3;
import nl.heidotting.jarvis.amazon.service.VerwijderVanS3Service;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class VerwijderVanS3Reciever {
    private final VerwijderVanS3Service verwijderVanS3Service;

    @Incoming("verwijder-van-s3-listener")
    public void imageListener(JsonObject p) {
        var verwijderVanS3 = p.mapTo(VerwijderVanS3.class);
        log.info("{}", verwijderVanS3);

        verwijderVanS3Service.verwijderVanS3(verwijderVanS3.getBucketNaam(), verwijderVanS3.getKey());
    }
}
