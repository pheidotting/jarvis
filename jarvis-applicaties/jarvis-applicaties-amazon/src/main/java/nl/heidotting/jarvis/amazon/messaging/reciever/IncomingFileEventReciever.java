package nl.heidotting.jarvis.amazon.messaging.reciever;

import io.vertx.core.json.JsonObject;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.amazon.messaging.model.IncomingFileEvent;
import nl.heidotting.jarvis.amazon.service.UploadNaarS3Service;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class IncomingFileEventReciever {
    private final UploadNaarS3Service uploadNaarS3Service;
    @Incoming("image-listener")
    public void imageListener(JsonObject p) {
        var incomingFileEvent = p.mapTo(IncomingFileEvent.class);
        log.debug("Binnengekomen {}", incomingFileEvent);
        uploadNaarS3Service.upload(incomingFileEvent.getFileData(), incomingFileEvent.getFilename(), incomingFileEvent.getUsername());
    }

    @Incoming("video-listener")
    public void videoListener(JsonObject p) {
        var incomingFileEvent = p.mapTo(IncomingFileEvent.class);
        log.debug("Binnengekomen {}", incomingFileEvent);

        uploadNaarS3Service.upload(incomingFileEvent.getFileData(), incomingFileEvent.getFilename(), incomingFileEvent.getUsername());
    }

}
