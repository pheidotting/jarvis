package nl.heidotting.jarvis.amazon.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class UploadNaarS3Service {
    @ConfigProperty(name = "bucket-naam")
    private String bucketNaam;

    private final AmazonS3 amazonS3;

    public void upload(byte[] file, String fileNaam, String cameraNaam) {
        var dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        var naam = LocalDate.now().format(dtf) + File.separator + cameraNaam + File.separator + fileNaam;
        log.info("uploaden naar {}", naam);
        amazonS3.putObject(
                bucketNaam,
                naam,
                new ByteArrayInputStream(file), new ObjectMetadata());
    }

}
