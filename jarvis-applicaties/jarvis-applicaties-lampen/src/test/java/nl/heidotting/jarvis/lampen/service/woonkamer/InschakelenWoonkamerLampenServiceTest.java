package nl.heidotting.jarvis.lampen.service.woonkamer;

import nl.heidotting.jarvis.lampen.model.DagDeel;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.DagDeelService;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.LichtsterkteService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMPEN_WOONKAMER_INGESCHAKELD;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class InschakelenWoonkamerLampenServiceTest {
    @InjectMocks
    private InschakelenWoonkamerLampenService service;

    @Mock
    private LichtsterkteService lichtsterkteService;
    @Mock
    private LampenSender lampenSender;
    @Mock
    private DagDeelService dagDeelService;
    @Mock
    private UitgevoerdeActieService uitgevoerdeActieService;

    @DisplayName("In de Nacht en Ochtend niets doen")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"NACHT", "OCHTEND"})
    public void test(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService);
    }

    @DisplayName("Middag of Avond, het is nog te licht, dus niets doen")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"MIDDAG", "AVOND"})
    public void test1(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);
        when(lichtsterkteService.gemiddeldeLichtsterkte()).thenReturn(14);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(LocalDateTime.now().minusDays(1));

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(lichtsterkteService).gemiddeldeLichtsterkte();

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService);
    }

    @DisplayName("Middag of Avond, het is precies de lichtsterkte treshold, dus inschakelen")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"MIDDAG", "AVOND"})
    public void test2(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);
        when(lichtsterkteService.gemiddeldeLichtsterkte()).thenReturn(13);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(LocalDateTime.now().minusDays(1));

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(lichtsterkteService).gemiddeldeLichtsterkte();
        verify(lampenSender).bijDeBank(true);
        verify(lampenSender).bijDePui(true);
        verify(lampenSender).eetkamer(true);
        verify(lampenSender).naastDeTv(true);
        verify(uitgevoerdeActieService).opslaan(LAMPEN_WOONKAMER_INGESCHAKELD);

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService);
    }

    @DisplayName("Middag of Avond, lichtsterkte is onder de treshold, dus inschakelen")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"MIDDAG", "AVOND"})
    public void test3(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);
        when(lichtsterkteService.gemiddeldeLichtsterkte()).thenReturn(12);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(LocalDateTime.now().minusDays(1));

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(lichtsterkteService).gemiddeldeLichtsterkte();
        verify(lampenSender).bijDeBank(true);
        verify(lampenSender).bijDePui(true);
        verify(lampenSender).eetkamer(true);
        verify(lampenSender).naastDeTv(true);
        verify(uitgevoerdeActieService).opslaan(LAMPEN_WOONKAMER_INGESCHAKELD);

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService);
    }

    @DisplayName("Middag of Avond, lichtsterkte is onder de treshold, inschakelen, maar is al gezet")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"MIDDAG", "AVOND"})
    public void test4(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);
        when(lichtsterkteService.gemiddeldeLichtsterkte()).thenReturn(12);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(LocalDateTime.now());

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(lichtsterkteService).gemiddeldeLichtsterkte();

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService);
    }
}