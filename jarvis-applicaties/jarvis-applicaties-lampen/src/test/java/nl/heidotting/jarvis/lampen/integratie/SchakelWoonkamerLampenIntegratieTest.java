package nl.heidotting.jarvis.lampen.integratie;

import io.quarkus.test.junit.QuarkusTest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Map;

@Slf4j
@QuarkusTest
public class SchakelWoonkamerLampenIntegratieTest extends AbstractIntegratieTest {

    @SneakyThrows
    @Test
    @DisplayName("Check of de lampen in de woonkamer juist aan gaan")
    public void test() {
        var lichtsterkteWaardenOpTreshold = Map.of("illuminance", "13");
        jedis.hset("Lichtsterkte:detectie-links", lichtsterkteWaardenOpTreshold);
        jedis.hset("Lichtsterkte:detectie-rechts", lichtsterkteWaardenOpTreshold);

        jedis.hset("AanUitStatus:bijkeuken-buitenlamp", uitWaarden);
        jedis.hset("AanUitStatus:bijkeuken-lamp", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-bank", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-eettafel", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-pui", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-tv", uitWaarden);
        jedis.hset("AanUitStatus:voordeur-lamp", uitWaarden);

        zetTijd("12:34");

        var nu = LocalDateTime.now();

        zetBerichtOpWijzigQueue();

        checkBerichtOpQueue(bijDeBankMutatieQueue, "on");
        checkBerichtOpQueue(bijDePuiMutatieQueue, "on");
        checkBerichtOpQueue(eetKamerMutatieQueue, "on");
        checkBerichtOpQueue(naastDeTVMutatieQueue, "on");

        checkUitgevoerdeActie(MogelijkeUitgevoerdeActie.LAMPEN_WOONKAMER_INGESCHAKELD, nu);
//        checkAantalUitgevoerdeActies(2);

//        jedis.hset("AanUitStatus:lamp-bij-de-bank", aanWaarden);
//        jedis.hset("AanUitStatus:lamp-bij-de-eettafel", aanWaarden);
//        jedis.hset("AanUitStatus:lamp-bij-de-pui", aanWaarden);
//        jedis.hset("AanUitStatus:lamp-bij-de-tv", aanWaarden);

//        buitenlampVoordeurMutatieQueue.vangBericht().ifPresentOrElse(s -> {
//            assertThat(s).isInstanceOf(StateOnOff.class);
//            assertThat(s).isEqualTo(new StateOnOff( "ON" ));
//        }, () -> fail("geen bericht ontvangen op de 'buitenlamp' queue"));
//
//        jedis.hset("AanUitStatus:voordeur-lamp", aanWaarden);
//
//        //        var aanUitWaarden = Map.of("aan", "1");
////        jedis.hset("AanUitStatus:voordeur-lamp", aanUitWaarden);
////        lichtWijzigingOfBewegingGedetecteerdQueue.stuurBerichtNaarExchange("");
////
//        buitenlampBijkeukenMutatieQueue.vangBericht().ifPresentOrElse(s -> {
//            assertThat(s).isInstanceOf(String.class);
//            assertThat(s).isEqualTo("on");
//        }, () -> fail("geen bericht ontvangen op de 'buitenlamp bijkeuken' queue"));
    }

}
