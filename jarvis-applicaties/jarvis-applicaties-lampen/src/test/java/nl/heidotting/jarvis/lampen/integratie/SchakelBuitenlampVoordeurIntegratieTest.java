package nl.heidotting.jarvis.lampen.integratie;

import io.quarkus.test.junit.QuarkusTest;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie;
import nl.heidotting.jarvis.lampen.model.StateOnOff;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.util.Map;

@Slf4j
@QuarkusTest
public class SchakelBuitenlampVoordeurIntegratieTest extends AbstractIntegratieTest {

    @SneakyThrows
    @Test
    @DisplayName("Check of de lampen in de woonkamer juist aan gaan")
    public void test() {
        var lichtsterkteWaardenOpTreshold = Map.of("illuminance", "14");
        jedis.hset("Lichtsterkte:detectie-links", lichtsterkteWaardenOpTreshold);
        jedis.hset("Lichtsterkte:detectie-rechts", lichtsterkteWaardenOpTreshold);

        jedis.hset("AanUitStatus:bijkeuken-buitenlamp", uitWaarden);
        jedis.hset("AanUitStatus:bijkeuken-lamp", uitWaarden);
        jedis.hset("AanUitStatus:buitenlamp-schuur", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-bank", aanWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-eettafel", aanWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-pui", aanWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-tv", aanWaarden);
        jedis.hset("AanUitStatus:voordeur-lamp", uitWaarden);

        zetTijd("12:34");

        var nu = LocalDateTime.now();

        zetBerichtOpWijzigQueue();

        checkBerichtOpQueue(buitenlampVoordeurMutatieQueue, new StateOnOff("ON"));

        checkUitgevoerdeActie(MogelijkeUitgevoerdeActie.LAMP_VOORDEUR_INGESCHAKELD, nu);
        checkAantalUitgevoerdeActies(1);

        jedis.hset("AanUitStatus:bijkeuken-buitenlamp", aanWaarden);
        jedis.hset("AanUitStatus:buitenlamp-schuur", aanWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-bank", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-eettafel", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-pui", uitWaarden);
        jedis.hset("AanUitStatus:lamp-bij-de-tv", uitWaarden);
        jedis.hset("AanUitStatus:voordeur-lamp", aanWaarden);

        zetBerichtOpWijzigQueue();

        checkBerichtOpQueue(buitenlampVoordeurMutatieQueue, new StateOnOff("OFF"));

        checkUitgevoerdeActie(MogelijkeUitgevoerdeActie.LAMP_VOORDEUR_UITGESCHAKELD, nu);
        checkAantalUitgevoerdeActies(2);
    }
}
