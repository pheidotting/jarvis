package nl.heidotting.jarvis.lampen.service.woonkamer;

import nl.heidotting.jarvis.lampen.model.DagDeel;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.DagDeelService;
import nl.heidotting.jarvis.lampen.service.redis.GeenActiviteitSindsService;
import nl.heidotting.jarvis.lampen.service.redis.NaarBedTijdService;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.BewegingService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.LichtsterkteService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.StroomGebruikService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.time.LocalTime;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMPEN_WOONKAMER_INGESCHAKELD;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMPEN_WOONKAMER_UITGESCHAKELD;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UitschakelenWoonkamerLampenServiceTest {
    @InjectMocks
    private UitschakelenWoonkamerLampenService service;

    @Mock
    private LichtsterkteService lichtsterkteService;
    @Mock
    private LampenSender lampenSender;
    @Mock
    private DagDeelService dagDeelService;
    @Mock
    private UitgevoerdeActieService uitgevoerdeActieService;
    @Mock
    private GeenActiviteitSindsService geenActiviteitSindsService;
    @Mock
    private BewegingService bewegingService;
    @Mock
    private StroomGebruikService stroomGebruikService;
    @Mock
    private NaarBedTijdService naarBedTijdService;

    @DisplayName("Uitschakelen")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"AVOND", "NACHT"})
    public void test(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);

        var laatsteKeerLampenUitgeschakeld = LocalDateTime.now().plusMinutes(1).minusDays(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(laatsteKeerLampenUitgeschakeld);
        var laatsteKeerLampenIngeschakeld = LocalDateTime.now().minusMinutes(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(laatsteKeerLampenIngeschakeld);

        when(stroomGebruikService.leesWattageVanApparaat("televisie")).thenReturn(120.0);
        when(bewegingService.isBeweging("detectie-links")).thenReturn(true);
        when(bewegingService.isBeweging("detectie-rechts")).thenReturn(true);

        var laatsteGebruikTelevisie = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("televisie")).thenReturn(laatsteGebruikTelevisie);
        var laatsteGebruikDetectieLinks = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-links")).thenReturn(laatsteGebruikDetectieLinks);
        var laatsteGebruikDetectieRechts = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-rechts")).thenReturn(laatsteGebruikDetectieRechts);

        when(naarBedTijdService.bepaalGemiddeldeBedtijd()).thenReturn(LocalTime.now().plusMinutes(29));

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(stroomGebruikService).leesWattageVanApparaat("televisie");
        verify(bewegingService).isBeweging("detectie-links");
        verify(bewegingService).isBeweging("detectie-rechts");

        verify(geenActiviteitSindsService).opslaan("televisie");
        verify(geenActiviteitSindsService).opslaan("detectie-links");
        verify(geenActiviteitSindsService).opslaan("detectie-rechts");

        verify(geenActiviteitSindsService).lees("televisie");
        verify(geenActiviteitSindsService).lees("detectie-links");
        verify(geenActiviteitSindsService).lees("detectie-rechts");

        verify(naarBedTijdService).bepaalGemiddeldeBedtijd();

        verify(lampenSender).bijDeBank(false);
        verify(lampenSender).bijDePui(false);
        verify(lampenSender).eetkamer(false);
        verify(lampenSender).naastDeTv(false);

        verify(uitgevoerdeActieService).opslaan(LAMPEN_WOONKAMER_UITGESCHAKELD);
        verify(naarBedTijdService).opslaan();

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService, geenActiviteitSindsService, bewegingService, stroomGebruikService, naarBedTijdService);
    }

    @DisplayName("Uitschakelen, lege laatsteKeerLampenIngeschakeld")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"AVOND", "NACHT"})
    public void testNullLaatsteKeerLampenIngeschakeld(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);

        LocalDateTime laatsteKeerLampenUitgeschakeld = null;
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(laatsteKeerLampenUitgeschakeld);
        var laatsteKeerLampenIngeschakeld = LocalDateTime.now().minusMinutes(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(laatsteKeerLampenIngeschakeld);

        when(stroomGebruikService.leesWattageVanApparaat("televisie")).thenReturn(120.0);
        when(bewegingService.isBeweging("detectie-links")).thenReturn(true);
        when(bewegingService.isBeweging("detectie-rechts")).thenReturn(true);

        var laatsteGebruikTelevisie = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("televisie")).thenReturn(laatsteGebruikTelevisie);
        var laatsteGebruikDetectieLinks = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-links")).thenReturn(laatsteGebruikDetectieLinks);
        var laatsteGebruikDetectieRechts = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-rechts")).thenReturn(laatsteGebruikDetectieRechts);

        when(naarBedTijdService.bepaalGemiddeldeBedtijd()).thenReturn(LocalTime.now().plusMinutes(29));

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(stroomGebruikService).leesWattageVanApparaat("televisie");
        verify(bewegingService).isBeweging("detectie-links");
        verify(bewegingService).isBeweging("detectie-rechts");

        verify(geenActiviteitSindsService).opslaan("televisie");
        verify(geenActiviteitSindsService).opslaan("detectie-links");
        verify(geenActiviteitSindsService).opslaan("detectie-rechts");

        verify(geenActiviteitSindsService).lees("televisie");
        verify(geenActiviteitSindsService).lees("detectie-links");
        verify(geenActiviteitSindsService).lees("detectie-rechts");

        verify(naarBedTijdService).bepaalGemiddeldeBedtijd();

        verify(lampenSender).bijDeBank(false);
        verify(lampenSender).bijDePui(false);
        verify(lampenSender).eetkamer(false);
        verify(lampenSender).naastDeTv(false);

        verify(uitgevoerdeActieService).opslaan(LAMPEN_WOONKAMER_UITGESCHAKELD);
        verify(naarBedTijdService).opslaan();

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService, geenActiviteitSindsService, bewegingService, stroomGebruikService, naarBedTijdService);
    }

    @DisplayName("Niet Uitschakelen, gemiddelde bedtijd nog niet in beeld")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"AVOND", "NACHT"})
    public void test5(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);

        var laatsteKeerLampenUitgeschakeld = LocalDateTime.now().plusMinutes(1).minusDays(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(laatsteKeerLampenUitgeschakeld);
        var laatsteKeerLampenIngeschakeld = LocalDateTime.now().minusMinutes(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(laatsteKeerLampenIngeschakeld);

        when(stroomGebruikService.leesWattageVanApparaat("televisie")).thenReturn(120.0);
        when(bewegingService.isBeweging("detectie-links")).thenReturn(true);
        when(bewegingService.isBeweging("detectie-rechts")).thenReturn(true);

        var laatsteGebruikTelevisie = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("televisie")).thenReturn(laatsteGebruikTelevisie);
        var laatsteGebruikDetectieLinks = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-links")).thenReturn(laatsteGebruikDetectieLinks);
        var laatsteGebruikDetectieRechts = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-rechts")).thenReturn(laatsteGebruikDetectieRechts);

        when(naarBedTijdService.bepaalGemiddeldeBedtijd()).thenReturn(LocalTime.now().plusMinutes(31));

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(stroomGebruikService).leesWattageVanApparaat("televisie");
        verify(bewegingService).isBeweging("detectie-links");
        verify(bewegingService).isBeweging("detectie-rechts");

        verify(geenActiviteitSindsService).opslaan("televisie");
        verify(geenActiviteitSindsService).opslaan("detectie-links");
        verify(geenActiviteitSindsService).opslaan("detectie-rechts");

        verify(geenActiviteitSindsService).lees("televisie");
        verify(geenActiviteitSindsService).lees("detectie-links");
        verify(geenActiviteitSindsService).lees("detectie-rechts");

        verify(naarBedTijdService).bepaalGemiddeldeBedtijd();

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService, geenActiviteitSindsService, bewegingService, stroomGebruikService, naarBedTijdService);
    }

    @DisplayName("Niet uitschakelen, nog niet lang genoeg geen actie bij 1 van de 3")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"AVOND", "NACHT"})
    public void test4(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);

        var laatsteKeerLampenUitgeschakeld = LocalDateTime.now().plusMinutes(1).minusDays(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(laatsteKeerLampenUitgeschakeld);
        var laatsteKeerLampenIngeschakeld = LocalDateTime.now().minusMinutes(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(laatsteKeerLampenIngeschakeld);

        when(stroomGebruikService.leesWattageVanApparaat("televisie")).thenReturn(120.0);
        when(bewegingService.isBeweging("detectie-links")).thenReturn(true);
        when(bewegingService.isBeweging("detectie-rechts")).thenReturn(true);

        var laatsteGebruikTelevisie = LocalDateTime.now().minusMinutes(29);
        when(geenActiviteitSindsService.lees("televisie")).thenReturn(laatsteGebruikTelevisie);
        var laatsteGebruikDetectieLinks = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-links")).thenReturn(laatsteGebruikDetectieLinks);
        var laatsteGebruikDetectieRechts = LocalDateTime.now().minusMinutes(31);
        when(geenActiviteitSindsService.lees("detectie-rechts")).thenReturn(laatsteGebruikDetectieRechts);

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(stroomGebruikService).leesWattageVanApparaat("televisie");
        verify(bewegingService).isBeweging("detectie-links");
        verify(bewegingService).isBeweging("detectie-rechts");

        verify(geenActiviteitSindsService).opslaan("televisie");
        verify(geenActiviteitSindsService).opslaan("detectie-links");
        verify(geenActiviteitSindsService).opslaan("detectie-rechts");

        verify(geenActiviteitSindsService).lees("televisie");
        verify(geenActiviteitSindsService).lees("detectie-links");
        verify(geenActiviteitSindsService).lees("detectie-rechts");

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService, geenActiviteitSindsService, bewegingService, stroomGebruikService, naarBedTijdService);
    }

    @DisplayName("Niet uitschakelen, nog niet lang genoeg geen actie")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"AVOND", "NACHT"})
    public void test3(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);

        var laatsteKeerLampenUitgeschakeld = LocalDateTime.now().plusMinutes(1).minusDays(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(laatsteKeerLampenUitgeschakeld);
        var laatsteKeerLampenIngeschakeld = LocalDateTime.now().minusMinutes(1);
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(laatsteKeerLampenIngeschakeld);

        when(stroomGebruikService.leesWattageVanApparaat("televisie")).thenReturn(120.0);
        when(bewegingService.isBeweging("detectie-links")).thenReturn(true);
        when(bewegingService.isBeweging("detectie-rechts")).thenReturn(true);

        var laatsteGebruikTelevisie = LocalDateTime.now().minusMinutes(29);
        when(geenActiviteitSindsService.lees("televisie")).thenReturn(laatsteGebruikTelevisie);
        var laatsteGebruikDetectieLinks = LocalDateTime.now().minusMinutes(29);
        when(geenActiviteitSindsService.lees("detectie-links")).thenReturn(laatsteGebruikDetectieLinks);
        var laatsteGebruikDetectieRechts = LocalDateTime.now().minusMinutes(29);
        when(geenActiviteitSindsService.lees("detectie-rechts")).thenReturn(laatsteGebruikDetectieRechts);

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(stroomGebruikService).leesWattageVanApparaat("televisie");
        verify(bewegingService).isBeweging("detectie-links");
        verify(bewegingService).isBeweging("detectie-rechts");

        verify(geenActiviteitSindsService).opslaan("televisie");
        verify(geenActiviteitSindsService).opslaan("detectie-links");
        verify(geenActiviteitSindsService).opslaan("detectie-rechts");

        verify(geenActiviteitSindsService).lees("televisie");
        verify(geenActiviteitSindsService).lees("detectie-links");
        verify(geenActiviteitSindsService).lees("detectie-rechts");

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService, geenActiviteitSindsService, bewegingService, stroomGebruikService, naarBedTijdService);
    }

    @DisplayName("Uitschakelen, maar niet in de ochtend of middag")
    @ParameterizedTest
    @EnumSource(value = DagDeel.class, names = {"OCHTEND", "MIDDAG"})
    public void test1(DagDeel dagDeel) {
        when(dagDeelService.bepaalDagDeel()).thenReturn(dagDeel);

        service.schakelWoonkamerLampen();

        verify(dagDeelService).bepaalDagDeel();

        verifyNoMoreInteractions(lichtsterkteService, lampenSender, dagDeelService, uitgevoerdeActieService, geenActiviteitSindsService, bewegingService, stroomGebruikService, naarBedTijdService);
    }
}