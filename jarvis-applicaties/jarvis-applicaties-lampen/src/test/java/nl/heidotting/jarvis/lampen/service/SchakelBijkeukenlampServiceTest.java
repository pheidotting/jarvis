package nl.heidotting.jarvis.lampen.service;

import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.ApparaatStatusService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SchakelBijkeukenlampServiceTest {
    @InjectMocks
    private SchakelBijkeukenlampService service;


    @Mock
    private LampenSender lampenSender;
    @Mock
    private ApparaatStatusService apparaatStatusService;
    @Mock
    private UitgevoerdeActieService uitgevoerdeActieService;

    @Test
    @DisplayName("De lampen in de woonkamer zijn aan geweest en weer uitgegaan : uitschakelen")
    public void test1() {
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(LocalDateTime.now().minusMinutes(2));
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(LocalDateTime.now().minusMinutes(1));

        when(apparaatStatusService.isApparataatIngeschakeld("bijkeuken-lamp")).thenReturn(true);

        service.verwerk();

        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);

        verify(apparaatStatusService).isApparataatIngeschakeld("bijkeuken-lamp");
        verify(lampenSender).stuurBijkeukenlampEmitter(false);
        verify(uitgevoerdeActieService).opslaan(BIJKEUKEN_LAMP_UITGESCHAKELD);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("De lampen in de woonkamer zijn aan en nog niet uit : niks doen")
    public void test3() {
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(LocalDateTime.now().minusMinutes(2));
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(LocalDateTime.now().minusMinutes(1).minusDays(1));

        service.verwerk();

        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("De lampen in de woonkamer zijn vandaag nog niet aan geweest : niks doen")
    public void test2() {
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD)).thenReturn(LocalDateTime.now().minusMinutes(2).minusDays(1));
        when(uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD)).thenReturn(LocalDateTime.now().minusMinutes(1).minusDays(1));

        service.verwerk();

        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        verify(uitgevoerdeActieService).lees(LAMPEN_WOONKAMER_UITGESCHAKELD);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }
}