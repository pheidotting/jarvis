package nl.heidotting.jarvis.lampen.service;

import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.ApparaatStatusService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMP_VOORDEUR_INGESCHAKELD;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMP_VOORDEUR_UITGESCHAKELD;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SchakelenLampBijVoordeurServiceTest {
    @InjectMocks
    private SchakelenLampBijVoordeurService service;

    @Mock
    private LampenSender lampenSender;
    @Mock
    private ApparaatStatusService apparaatStatusService;
    @Mock
    private UitgevoerdeActieService uitgevoerdeActieService;

    private List<String> relevanteLampen = List.of(
            "lamp-bij-de-tv",
            "lamp-bij-de-eettafel",
            "lamp-bij-de-pui",
            "lamp-bij-de-bank");

    @Test
    @DisplayName("Alle lampen in de woonkamer zijn aan, buitenlamp is nog niet aan, inschakelen")
    public void test() {
        var aanUitStatus1 = new ApparaatStatusService.ApparaatStatus("a", true);
        var aanUitStatus2 = new ApparaatStatusService.ApparaatStatus("b", true);

        when(apparaatStatusService.zijnApparatatenIngeschakeld(relevanteLampen)).thenReturn(List.of(aanUitStatus1, aanUitStatus2));
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(false);

        service.verwerk();

        verify(apparaatStatusService, times(2)).zijnApparatatenIngeschakeld(relevanteLampen);
        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");
        verify(lampenSender).schakelBuitenlampVoordeur(true);
        verify(uitgevoerdeActieService).opslaan(LAMP_VOORDEUR_INGESCHAKELD);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("Alle lampen in de woonkamer zijn aan, buitenlamp is al aan, niets doen")
    public void test2() {
        var aanUitStatus1 = new ApparaatStatusService.ApparaatStatus("a", true);
        var aanUitStatus2 = new ApparaatStatusService.ApparaatStatus("b", true);

        when(apparaatStatusService.zijnApparatatenIngeschakeld(relevanteLampen)).thenReturn(List.of(aanUitStatus1, aanUitStatus2));
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(true);

        service.verwerk();

        verify(apparaatStatusService, times(2)).zijnApparatatenIngeschakeld(relevanteLampen);
        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("Alle lampen in de woonkamer zijn uit, buitenlamp is aan, uitschakelen")
    public void test3() {
        var aanUitStatus1 = new ApparaatStatusService.ApparaatStatus("a", false);
        var aanUitStatus2 = new ApparaatStatusService.ApparaatStatus("b", false);

        when(apparaatStatusService.zijnApparatatenIngeschakeld(relevanteLampen)).thenReturn(List.of(aanUitStatus1, aanUitStatus2));
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(true);

        service.verwerk();

        verify(apparaatStatusService, times(2)).zijnApparatatenIngeschakeld(relevanteLampen);
        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");
        verify(lampenSender).schakelBuitenlampVoordeur(false);
        verify(uitgevoerdeActieService).opslaan(LAMP_VOORDEUR_UITGESCHAKELD);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("Alle lampen in de woonkamer zijn uit, buitenlamp is uit, niets doen")
    public void test4() {
        var aanUitStatus1 = new ApparaatStatusService.ApparaatStatus("a", false);
        var aanUitStatus2 = new ApparaatStatusService.ApparaatStatus("b", false);

        when(apparaatStatusService.zijnApparatatenIngeschakeld(relevanteLampen)).thenReturn(List.of(aanUitStatus1, aanUitStatus2));
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(false);

        service.verwerk();

        verify(apparaatStatusService, times(2)).zijnApparatatenIngeschakeld(relevanteLampen);
        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("Niet alle lampen in de woonkamer zijn aan of uit, dus niets doen")
    public void test1() {
        var aanUitStatus1 = new ApparaatStatusService.ApparaatStatus("a", false);
        var aanUitStatus2 = new ApparaatStatusService.ApparaatStatus("b", true);

        when(apparaatStatusService.zijnApparatatenIngeschakeld(relevanteLampen)).thenReturn(List.of(aanUitStatus1, aanUitStatus2));

        service.verwerk();

        verify(apparaatStatusService, times(2)).zijnApparatatenIngeschakeld(relevanteLampen);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }
}