package nl.heidotting.jarvis.lampen.service;

import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.ApparaatStatusService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.BIJKEUKEN_BUITENLAMP_INGESCHAKELD;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.BIJKEUKEN_BUITENLAMP_UITGESCHAKELD;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SchakelBijkeukenBuitenlampServiceTest {
    @InjectMocks
    private SchakelBijkeukenBuitenlampService service;

    @Mock(strictness = Mock.Strictness.WARN)
    private ApparaatStatusService apparaatStatusService;
    @Mock(strictness = Mock.Strictness.WARN)
    private LampenSender lampenSender;
    @Mock
    private UitgevoerdeActieService uitgevoerdeActieService;

    @Test
    @DisplayName("Buitenlamp voordeur wordt ingeschakeld, bijkeuken buitenlamp is nog niet aan, dus gaat aan")
    public void test() {
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(true);
        when(apparaatStatusService.isApparataatIngeschakeld("bijkeuken-buitenlamp")).thenReturn(false);

        service.verwerk();

        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");
        verify(apparaatStatusService).isApparataatIngeschakeld("bijkeuken-buitenlamp");
        verify(lampenSender).stuurBijkeukenBuitenlamp(true);
        verify(uitgevoerdeActieService).opslaan(BIJKEUKEN_BUITENLAMP_INGESCHAKELD);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("Buitenlamp voordeur wordt ingeschakeld, bijkeuken buitenlamp is al aan, dus gebeurt niets")
    public void test1() {
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(true);
        when(apparaatStatusService.isApparataatIngeschakeld("bijkeuken-buitenlamp")).thenReturn(true);

        service.verwerk();

        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");
        verify(apparaatStatusService).isApparataatIngeschakeld("bijkeuken-buitenlamp");

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("Buitenlamp voordeur wordt uitgeschakeld, bijkeuken buitenlamp is nog  aan, dus gaat uit")
    public void test2() {
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(false);
        when(apparaatStatusService.isApparataatIngeschakeld("bijkeuken-buitenlamp")).thenReturn(true);

        service.verwerk();

        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");
        verify(apparaatStatusService).isApparataatIngeschakeld("bijkeuken-buitenlamp");
        verify(lampenSender).stuurBijkeukenBuitenlamp(false);
        verify(uitgevoerdeActieService).opslaan(BIJKEUKEN_BUITENLAMP_UITGESCHAKELD);

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }

    @Test
    @DisplayName("Buitenlamp voordeur wordt uitgeschakeld, bijkeuken buitenlamp is al uit, dus gebeurt niets")
    public void test3() {
        when(apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")).thenReturn(false);
        when(apparaatStatusService.isApparataatIngeschakeld("bijkeuken-buitenlamp")).thenReturn(false);

        service.verwerk();

        verify(apparaatStatusService).isApparataatIngeschakeld("voordeur-lamp");
        verify(apparaatStatusService).isApparataatIngeschakeld("bijkeuken-buitenlamp");

        verifyNoMoreInteractions(apparaatStatusService, lampenSender, uitgevoerdeActieService);
    }
}