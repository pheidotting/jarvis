package nl.heidotting.jarvis.lampen.integratie;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.common.QueueUtil;
import nl.heidotting.jarvis.lampen.common.TestGegevens;
import nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie;
import nl.heidotting.jarvis.lampen.model.StateOnOff;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import redis.clients.jedis.Jedis;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.function.Consumer;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;
import static org.junit.jupiter.api.Assertions.fail;

@Slf4j
public class AbstractIntegratieTest {
    @ConfigProperty(name = "mp.messaging.incoming.LichtWijzigingOfBewegingGedetecteerd.exchange.name")
    protected String exchangeLichtWijzigingOfBewegingGedetecteerd;
    @ConfigProperty(name = "mp.messaging.outgoing.BijDeBankMutatie.exchange.name")
    protected String exchangeBijDeBankMutatieQueue;
    @ConfigProperty(name = "mp.messaging.outgoing.BijDeBankMutatie.default-routing-key")
    protected String exchangeBijDeBankMutatieQueueRouting;
    @ConfigProperty(name = "mp.messaging.outgoing.BijDePuiMutatie.exchange.name")
    protected String exchangeBijDePuiMutatieQueue;
    @ConfigProperty(name = "mp.messaging.outgoing.BijDePuiMutatie.default-routing-key")
    protected String exchangeBijDePuiMutatieQueueRouting;
    @ConfigProperty(name = "mp.messaging.outgoing.EetKamerMutatie.exchange.name")
    protected String exchangeEetKamerMutatieQueue;
    @ConfigProperty(name = "mp.messaging.outgoing.EetKamerMutatie.default-routing-key")
    protected String exchangeEetKamerMutatieQueueRouting;
    @ConfigProperty(name = "mp.messaging.outgoing.NaastDeTVMutatie.exchange.name")
    protected String exchangeNaastDeTVMutatieQueue;
    @ConfigProperty(name = "mp.messaging.outgoing.NaastDeTVMutatie.default-routing-key")
    protected String exchangeNaastDeTVMutatieQueueRouting;
    @ConfigProperty(name = "mp.messaging.outgoing.BuitenlampVoordeurMutatie.exchange.name")
    protected String exchangeBuitenlampVoordeurMutatieQueue;
    @ConfigProperty(name = "mp.messaging.outgoing.BuitenlampVoordeurMutatie.default-routing-key")
    protected String exchangeBuitenlampVoordeurMutatieQueueRouting;
    @ConfigProperty(name = "mp.messaging.outgoing.BuitenlampBijkeukenMutatie.exchange.name")
    protected String exchangeBuitenlampBijkeukenMutatieQueue;
    @ConfigProperty(name = "mp.messaging.outgoing.BuitenlampBijkeukenMutatie.default-routing-key")
    protected String exchangeBuitenlampBijkeukenMutatieQueueRouting;
    @ConfigProperty(name = "mp.messaging.outgoing.BuitenlampSchuurMutatie.exchange.name")
    protected String exchangeBuitenlampSchuurMutatieQueue;
    @ConfigProperty(name = "mp.messaging.outgoing.BuitenlampSchuurMutatie.default-routing-key")
    protected String exchangeBuitenlampSchuurMutatieQueueRouting;
    @ConfigProperty(name = "rabbitmq-host")
    protected String rabbitHost;
    @ConfigProperty(name = "rabbitmq-port")
    protected String rabbitPort;
    @ConfigProperty(name = "rabbitmq-username")
    protected String rabbitUsername;
    @ConfigProperty(name = "rabbitmq-password")
    protected String rabbitPassword;
    @ConfigProperty(name = "quarkus.redis.hosts")
    protected String redisHost;

    protected Jedis jedis;
    protected QueueUtil<Void> lichtWijzigingOfBewegingGedetecteerdQueue;
    protected QueueUtil<Object> bijDeBankMutatieQueue;
    protected QueueUtil<Object> bijDePuiMutatieQueue;
    protected QueueUtil<Object> eetKamerMutatieQueue;
    protected QueueUtil<Object> naastDeTVMutatieQueue;
    protected QueueUtil<StateOnOff> buitenlampVoordeurMutatieQueue;
    protected QueueUtil<Object> buitenlampBijkeukenMutatieQueue;
    protected QueueUtil<Object> buitenlampSchuurMutatieQueue;

    protected Map<String, String> uitWaarden = Map.of("aan", "0");
    protected Map<String, String> aanWaarden = Map.of("aan", "1");

    protected final static String UITGEVOERDEACTIES = "UitgevoerdeActies";

    @BeforeEach
    public void init() {
        TestGegevens.setRabbitHost(rabbitHost);
        TestGegevens.setRabbitPort(Integer.valueOf(rabbitPort));
        TestGegevens.setRabbitUser(rabbitUsername);
        TestGegevens.setRabbitPassword(rabbitPassword);

        jedis = new Jedis(redisHost);

        leegUitgevoerdeActies();

        lichtWijzigingOfBewegingGedetecteerdQueue = maakLichtWijzigingOfBewegingGedetecteerdQueueUtil();
        bijDeBankMutatieQueue = maakBijDeBankMutatieQueueUtil();
        bijDePuiMutatieQueue = maakBijDePuiMutatieQueueUtil();
        eetKamerMutatieQueue = maakEetKamerMutatieQueueUtil();
        naastDeTVMutatieQueue = maakNaastDeTVMutatieQueueUtil();
        buitenlampVoordeurMutatieQueue = maakBuitenlampVoordeurMutatieQueueUtil();
        buitenlampBijkeukenMutatieQueue = maakBuitenlampBijkeukenMutatieQueueUtil();
        buitenlampSchuurMutatieQueue = maakBuitenlampSchuurMutatieQueueUtil();
    }

    @AfterEach
    public void opruimen() {
        leegUitgevoerdeActies();
    }

    protected void checkUitgevoerdeActie(MogelijkeUitgevoerdeActie mogelijkeUitgevoerdeActie, LocalDateTime naTijdstip) {
        await()
                .atMost(Duration.ofSeconds(10))
                .until(() -> {
                    var uitgevoerdeActie = LocalDateTime.parse(jedis.hget(UITGEVOERDEACTIES, "\"" + mogelijkeUitgevoerdeActie + "\"").replace("\"", ""));
                    if (naTijdstip != null) {
                        assertThat(uitgevoerdeActie).isAfter(naTijdstip);
                    }
                    return true;
                });
    }

    protected void leegUitgevoerdeActies() {
        System.out.println("Verwijderen");
//        Arrays.stream(MogelijkeUitgevoerdeActie.values())
//                .forEach(new Consumer<MogelijkeUitgevoerdeActie>() {
//                    @Override
//                    public void accept(MogelijkeUitgevoerdeActie mogelijkeUitgevoerdeActie) {
        jedis.hscan(UITGEVOERDEACTIES, "0").getResult()
                .stream()
                .forEach(new Consumer<Map.Entry<String, String>>() {
                    @Override
                    public void accept(Map.Entry<String, String> stringStringEntry) {
                        log.info(stringStringEntry.getKey() + ": " + stringStringEntry.getValue());
                        log.info("del : {}", jedis.hdel(UITGEVOERDEACTIES, stringStringEntry.getKey()));
                        log.info("Aantal : {}", jedis.hscan(UITGEVOERDEACTIES, "0").getResult().size());
                    }
                });
//                        jedis.hdel(UITGEVOERDEACTIES, "\"" + mogelijkeUitgevoerdeActie + "\"");
    }
//                });
//    }

    protected void checkAantalUitgevoerdeActies(int verwachtAantal) {
        log.info("Aantal : {}", jedis.hscan(UITGEVOERDEACTIES, "0").getResult().size());
        await()
                .atMost(Duration.ofSeconds(10))
                .until(() -> jedis.hscan(UITGEVOERDEACTIES, "0").getResult().size() == verwachtAantal);
    }

    public void zetTijd(String tijd) {
        given()
                .when()
                .pathParam("tijd", tijd)
                .get("/tijd/{tijd}");
    }

    public void zetBerichtOpWijzigQueue() {
        lichtWijzigingOfBewegingGedetecteerdQueue.stuurBerichtNaarExchange("");
    }

    public void checkBerichtOpQueue(QueueUtil queueUtil, String bericht) {
        queueUtil.vangBericht().ifPresentOrElse(s -> {
            assertThat(s).isInstanceOf(String.class);
            assertThat(s).isEqualTo(bericht);
        }, () -> fail("geen bericht ontvangen op de 'bij de bank' queue"));
    }

    public void checkBerichtOpQueue(QueueUtil queueUtil, StateOnOff stateOnOff) {
        queueUtil.vangBericht().ifPresentOrElse(s -> {
            assertThat(s).isInstanceOf(StateOnOff.class);
            assertThat(s).isEqualTo(stateOnOff);
        }, () -> fail("geen bericht ontvangen op de 'bij de bank' queue"));
    }

    public QueueUtil<Void> maakLichtWijzigingOfBewegingGedetecteerdQueueUtil() {
        return new QueueUtil<Void>() {
            @Override
            public String getExchange() {
                return exchangeLichtWijzigingOfBewegingGedetecteerd;
            }

            @Override
            public Class<Void> getClazz() {
                return Void.class;
            }

            @Override
            public String getRoutingKey() {
                return "";
            }

            @Override
            public Types getType() {
                return Types.FANOUT;
            }
        };
    }

    public QueueUtil<Object> maakBijDeBankMutatieQueueUtil() {
        return new QueueUtil<Object>() {
            @Override
            public String getExchange() {
                return exchangeBijDeBankMutatieQueue;
            }

            @Override
            public Class<Object> getClazz() {
                return Object.class;
            }

            @Override
            public String getRoutingKey() {
                return exchangeBijDeBankMutatieQueueRouting;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<Object> maakBijDePuiMutatieQueueUtil() {
        return new QueueUtil<Object>() {
            @Override
            public String getExchange() {
                return exchangeBijDePuiMutatieQueue;
            }

            @Override
            public Class<Object> getClazz() {
                return Object.class;
            }

            @Override
            public String getRoutingKey() {
                return exchangeBijDePuiMutatieQueueRouting;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<Object> maakEetKamerMutatieQueueUtil() {
        return new QueueUtil<Object>() {
            @Override
            public String getExchange() {
                return exchangeEetKamerMutatieQueue;
            }

            @Override
            public Class<Object> getClazz() {
                return Object.class;
            }

            @Override
            public String getRoutingKey() {
                return exchangeEetKamerMutatieQueueRouting;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<Object> maakNaastDeTVMutatieQueueUtil() {
        return new QueueUtil<Object>() {
            @Override
            public String getExchange() {
                return exchangeNaastDeTVMutatieQueue;
            }

            @Override
            public Class<Object> getClazz() {
                return Object.class;
            }

            @Override
            public String getRoutingKey() {
                return exchangeNaastDeTVMutatieQueueRouting;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<StateOnOff> maakBuitenlampVoordeurMutatieQueueUtil() {
        return new QueueUtil<StateOnOff>() {
            @Override
            public String getExchange() {
                return exchangeBuitenlampVoordeurMutatieQueue;
            }

            @Override
            public Class<StateOnOff> getClazz() {
                return StateOnOff.class;
            }

            @Override
            public String getRoutingKey() {
                return exchangeBuitenlampVoordeurMutatieQueueRouting;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<Object> maakBuitenlampBijkeukenMutatieQueueUtil() {
        return new QueueUtil<Object>() {
            @Override
            public String getExchange() {
                return exchangeBuitenlampBijkeukenMutatieQueue;
            }

            @Override
            public Class<Object> getClazz() {
                return Object.class;
            }

            @Override
            public String getRoutingKey() {
                return exchangeBuitenlampBijkeukenMutatieQueueRouting;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }
        };
    }

    public QueueUtil<Object> maakBuitenlampSchuurMutatieQueueUtil() {
        return new QueueUtil<Object>() {
            @Override
            public String getExchange() {
                return exchangeBuitenlampSchuurMutatieQueue;
            }

            @Override
            public String getRoutingKey() {
                return exchangeBuitenlampSchuurMutatieQueueRouting;
            }

            @Override
            public Types getType() {
                return Types.TOPIC;
            }

            @Override
            public Class<Object> getClazz() {
                return Object.class;
            }
        };
    }
}
