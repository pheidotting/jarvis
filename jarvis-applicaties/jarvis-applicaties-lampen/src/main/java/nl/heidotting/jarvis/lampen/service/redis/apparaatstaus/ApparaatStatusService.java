package nl.heidotting.jarvis.lampen.service.redis.apparaatstaus;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import io.quarkus.redis.datasource.keys.KeyCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class ApparaatStatusService {
    private final RedisDataSource redisDataSource;

    private HashCommands<String, String, Boolean> hcommands;
    private KeyCommands keyCommands;

    @AllArgsConstructor
    @Getter
    public static class ApparaatStatus {
        private String id;
        private boolean aan;
    }

    @PostConstruct
    public void init() {
        hcommands = redisDataSource.hash(Boolean.class);
        keyCommands = redisDataSource.key();
    }

    public Map<String, Boolean> leesLampStatussen() {
        var result = new HashMap<String, Boolean>();

        keyCommands.keys("AanUitStatus:*").stream()
                .forEach((Consumer<String>) s -> result.put(s.replace("AanUitStatus:", ""), hcommands.hget(s, "aan")));

        return result;
    }

    public boolean isApparataatIngeschakeld(String id) {
        return zijnApparatatenIngeschakeld(List.of(id)).stream().findFirst().orElse(new ApparaatStatus("dummy", false)).isAan();
    }

    public List<ApparaatStatus> zijnApparatatenIngeschakeld(List<String> ids) {
        var statussen = leesLampStatussen();
        return statussen.keySet().stream()
                .filter(aanUitStatus -> aanUitStatus != null)
                .filter(aanUitStatus -> ids.contains((aanUitStatus)))
                .map(key -> new ApparaatStatus(key, statussen.get(key)))
                .toList();
    }
}
