package nl.heidotting.jarvis.lampen.service.redis;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class GeenActiviteitSindsService {
    private final RedisDataSource redisDataSource;
    private HashCommands<String, String, LocalDateTime> hcommands;

    @PostConstruct
    public void init() {
        hcommands = redisDataSource.hash(String.class, String.class, LocalDateTime.class);
    }

    public void opslaan(String id) {
        this.hcommands.hset("GeenActiviteitSinds", id, LocalDateTime.now());
    }

    public LocalDateTime lees(String id) {
        return this.hcommands.hget("GeenActiviteitSinds", id);
    }

    public void verwijder(String id) {
        this.hcommands.hdel("GeenActiviteitSinds", id);
    }
}
