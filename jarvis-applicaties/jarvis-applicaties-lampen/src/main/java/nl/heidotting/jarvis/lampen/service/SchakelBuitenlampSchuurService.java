package nl.heidotting.jarvis.lampen.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.ApparaatStatusService;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.BUITENLAMP_SCHUUR_INGESCHAKELD;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.BUITENLAMP_SCHUUR_UITGESCHAKELD;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class SchakelBuitenlampSchuurService {
    private final ApparaatStatusService apparaatStatusService;
    private final LampenSender lampenSender;
    private final UitgevoerdeActieService uitgevoerdeActieService;

    public void verwerk() {
        var voordeurLampIngeschakeld = apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp");
        var buitenlampSchuurIngeschakeld = apparaatStatusService.isApparataatIngeschakeld("buitenlamp-schuur");

        if (voordeurLampIngeschakeld != buitenlampSchuurIngeschakeld) {
            lampenSender.stuurBuitenlampSchuur(voordeurLampIngeschakeld);
            uitgevoerdeActieService.opslaan(voordeurLampIngeschakeld ? BUITENLAMP_SCHUUR_INGESCHAKELD : BUITENLAMP_SCHUUR_UITGESCHAKELD);
        }
    }
}
