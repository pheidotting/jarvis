package nl.heidotting.jarvis.lampen.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.controller.TijdController;
import nl.heidotting.jarvis.lampen.model.DagDeel;

import java.time.LocalTime;

@ApplicationScoped
@RequiredArgsConstructor
public class DagDeelService {
    private final TijdController tijdController;

    public DagDeel bepaalDagDeel() {
        var nu = tijdController.tijd();

        var middernacht = LocalTime.of(0, 0);
        var zesuur = LocalTime.of(6, 0);
        var twaalfuur = LocalTime.of(12, 0);
        var achtienuur = LocalTime.of(18, 0);

        if (nu.isAfter(middernacht) && nu.isBefore(zesuur)) {
            return DagDeel.NACHT;
        } else if (nu.isAfter(zesuur) && nu.isBefore(twaalfuur)) {
            return DagDeel.OCHTEND;
        } else if (nu.isAfter(twaalfuur) && nu.isBefore(achtienuur)) {
            return DagDeel.MIDDAG;
        } else {
            return DagDeel.AVOND;
        }
    }
}
