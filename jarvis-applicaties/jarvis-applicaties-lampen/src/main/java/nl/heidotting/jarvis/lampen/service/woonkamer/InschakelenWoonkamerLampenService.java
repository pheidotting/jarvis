package nl.heidotting.jarvis.lampen.service.woonkamer;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.DagDeelService;
import nl.heidotting.jarvis.lampen.service.redis.GeenActiviteitSindsService;
import nl.heidotting.jarvis.lampen.service.redis.NaarBedTijdService;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.BewegingService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.LichtsterkteService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.StroomGebruikService;

import java.time.LocalDate;

import static nl.heidotting.jarvis.lampen.model.DagDeel.AVOND;
import static nl.heidotting.jarvis.lampen.model.DagDeel.MIDDAG;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMPEN_WOONKAMER_INGESCHAKELD;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class InschakelenWoonkamerLampenService {
    private final LichtsterkteService lichtsterkteService;
    private final LampenSender lampenSender;
    private final DagDeelService dagDeelService;
    private final UitgevoerdeActieService uitgevoerdeActieService;
    private final GeenActiviteitSindsService geenActiviteitSindsService;

    private final static int TRESHOLD_LAMPEN_AAN = 13;
    private final BewegingService bewegingService;
    private final StroomGebruikService stroomGebruikService;
    private final NaarBedTijdService naarBedTijdService;

    public void schakelWoonkamerLampen() {
        var dagdeel = dagDeelService.bepaalDagDeel();
//        log.info("Dagdeel nu {}", dagdeel);

        if (dagdeel == MIDDAG || dagdeel == AVOND) {
            var gemiddeldeLichtsterkte = lichtsterkteService.gemiddeldeLichtsterkte();
//            log.info("gemiddeldeLichtsterkte {} ", gemiddeldeLichtsterkte);
            var ingeschakeld = uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD);

//            log.info("MIDDAG of AVOND");
//            log.info("gemiddeldeLichtsterkte<=TRESHOLD_LAMPEN_AAN                  {}", gemiddeldeLichtsterkte <= TRESHOLD_LAMPEN_AAN);
//            log.info("gemiddeldeLichtsterkte == {}, dat is {} lager of gelijk aan de treshold van {}", gemiddeldeLichtsterkte, (gemiddeldeLichtsterkte <= TRESHOLD_LAMPEN_AAN) ? "wel" : "niet", TRESHOLD_LAMPEN_AAN);
//            log.info("ingeschakeld==null                                           {}", ingeschakeld == null);
//            if (ingeschakeld != null) {
//                log.info("ingeschakeld.isBefore(LocalDate.now())      {}", ingeschakeld.toLocalDate().isBefore(LocalDate.now()));
//            }else{
//                log.info("ingeschakeld dus null");
//            }

//            log.info("(ingeschakeld==null||ingeschakeld.isBefore(LocalDate.now())) {}", (ingeschakeld == null || ingeschakeld.isBefore(LocalDate.now())));
//
            if (gemiddeldeLichtsterkte <= TRESHOLD_LAMPEN_AAN && (ingeschakeld == null || ingeschakeld.toLocalDate().isBefore(LocalDate.now()))) {
                lampenSender.bijDeBank(true);
                lampenSender.bijDePui(true);
                lampenSender.eetkamer(true);
                lampenSender.naastDeTv(true);

                uitgevoerdeActieService.opslaan(LAMPEN_WOONKAMER_INGESCHAKELD);
            }
        }
    }
}
