package nl.heidotting.jarvis.lampen.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.model.StateOnOff;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@Slf4j
@ApplicationScoped
public class LampenSender {
    //Bijkeuken
    @Inject
    @Channel("BuitenlampBijkeukenMutatie")
    private Emitter<Object> bijkeukenBuitenlampEmitter;
    @Inject
    @Channel("BijkeukenlampMutatie")
    private Emitter<Object> bijkeukenlampEmitter;

    //Buiten
    @Inject
    @Channel("BuitenlampSchuurMutatie")
    private Emitter<Object> buitenlampSchuurEmitter;

    //Voordeur
    @Inject
    @Channel("BuitenlampVoordeurMutatie")
    private Emitter<Object> buitenlampVoordeurMutatie;

    //Woonkamer
    @Inject
    @Channel("BijDeBankMutatie")
    private Emitter<Object> bijDeBankMutatie;
    @Inject
    @Channel("BijDePuiMutatie")
    private Emitter<Object> bijDePuiMutatie;
    @Inject
    @Channel("EetKamerMutatie")
    private Emitter<Object> eetKamerMutatie;
    @Inject
    @Channel("NaastDeTVMutatie")
    private Emitter<Object> naastDeTVMutatie;

    public void stuurBijkeukenBuitenlamp(boolean aan) {
        this.bijkeukenBuitenlampEmitter.send(aan ? "on" : "off");
    }

    public void stuurBuitenlampSchuur(boolean aan) {
        this.buitenlampSchuurEmitter.send(aan ? "on" : "off");
    }

    public void stuurBijkeukenlampEmitter(boolean aan) {
        this.bijkeukenlampEmitter.send(aan ? "on" : "off");
    }

    public void schakelBuitenlampVoordeur(boolean aan) {
        this.buitenlampVoordeurMutatie.send(new StateOnOff(aan ? "ON" : "OFF"));
    }

    public void bijDeBank(boolean aan) {
        this.bijDeBankMutatie.send(aan ? "on" : "off");
    }

    public void bijDePui(boolean aan) {
        this.bijDePuiMutatie.send(aan ? "on" : "off");
    }

    public void eetkamer(boolean aan) {
        this.eetKamerMutatie.send(aan ? "on" : "off");
    }

    public void naastDeTv(boolean aan) {
        this.naastDeTVMutatie.send(aan ? "on" : "off");
    }
}
