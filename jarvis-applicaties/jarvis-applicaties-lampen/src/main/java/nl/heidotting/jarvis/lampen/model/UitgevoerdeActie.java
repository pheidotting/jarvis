package nl.heidotting.jarvis.lampen.model;

import java.time.LocalDateTime;

public record UitgevoerdeActie(
        MogelijkeUitgevoerdeActie mogelijkeUitgevoerdeActie,
        LocalDateTime tijdstip
) {
}
