package nl.heidotting.jarvis.lampen.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.ApparaatStatusService;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.BIJKEUKEN_BUITENLAMP_INGESCHAKELD;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.BIJKEUKEN_BUITENLAMP_UITGESCHAKELD;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class SchakelBijkeukenBuitenlampService {
    private final ApparaatStatusService apparaatStatusService;
    private final LampenSender lampenSender;
    private final UitgevoerdeActieService uitgevoerdeActieService;

    public void verwerk() {
        var voordeurLampIngeschakeld = apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp");
        var bijkeukenbuitenlampIngeschakeld = apparaatStatusService.isApparataatIngeschakeld("bijkeuken-buitenlamp");

        if (voordeurLampIngeschakeld != bijkeukenbuitenlampIngeschakeld) {
            lampenSender.stuurBijkeukenBuitenlamp(voordeurLampIngeschakeld);
            uitgevoerdeActieService.opslaan(voordeurLampIngeschakeld ? BIJKEUKEN_BUITENLAMP_INGESCHAKELD : BIJKEUKEN_BUITENLAMP_UITGESCHAKELD);
        }
    }
}
