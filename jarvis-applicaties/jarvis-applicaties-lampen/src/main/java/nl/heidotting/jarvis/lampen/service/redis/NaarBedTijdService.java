package nl.heidotting.jarvis.lampen.service.redis;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class NaarBedTijdService {
    private final RedisDataSource redisDataSource;
    private HashCommands<String, String, List> hcommands;
    private HashCommands<String, String, LocalTime> localTimeCommands;

    @PostConstruct
    public void init() {
        hcommands = redisDataSource.hash(String.class, String.class, List.class);
        localTimeCommands = redisDataSource.hash(String.class, String.class, LocalTime.class);
    }

    public List<LocalTime> alles() {
        return this.hcommands.hget("BedTijden", "tijden")
                .stream()
                .map((Function<Object, String>) object -> (String) object)
                .map((Function<String, LocalTime>) LocalTime::parse)
                .toList();
    }

    public void opslaan() {
        opslaan(LocalTime.now());
    }

    public void opslaan(List<LocalTime> tijden) {
        this.hcommands.hset("BedTijden", "tijden", tijden);
    }

    public void opslaan(LocalTime tijd) {
        var lijst = new ArrayList<LocalTime>();
        lijst.addAll(alles());

        log.info("Huidige lijst {}", lijst);
        log.info("Toevoegen     {}", tijd);
        if (lijst == null) {
            lijst = new ArrayList<LocalTime>();
        }
        lijst.add(tijd);
        this.hcommands.hset("BedTijden", "tijden", lijst);
        this.localTimeCommands.hset("BedTijden", "gemiddelde-bedtijd", bepaalGemiddeldeBedtijd());
    }

    public LocalTime bepaalGemiddeldeBedtijd() {
        var lijst = alles();
        if (lijst.size() > 1) {
            long nanoSum = lijst.get(0).toNanoOfDay();
            int teller = 0;
            for (LocalTime other : lijst) {
                if (teller++ > 0) {
                    nanoSum += other.toNanoOfDay();
                }
            }
            return LocalTime.ofNanoOfDay(nanoSum / lijst.size());
        } else {
            return lijst.get(0);
        }
    }


}
