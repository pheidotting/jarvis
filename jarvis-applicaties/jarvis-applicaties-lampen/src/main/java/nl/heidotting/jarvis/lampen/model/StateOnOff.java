package nl.heidotting.jarvis.lampen.model;

import java.io.Serializable;

public record StateOnOff(String state) implements Serializable {
}
