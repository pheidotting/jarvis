package nl.heidotting.jarvis.lampen.service.redis;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie;

import java.time.LocalDateTime;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class UitgevoerdeActieService {
    private final static String UITGEVOERDEACTIES = "UitgevoerdeActies";
    private final RedisDataSource redisDataSource;
    private HashCommands<String, MogelijkeUitgevoerdeActie, LocalDateTime> hcommands;

    @PostConstruct
    public void init() {
        hcommands = redisDataSource.hash(String.class, MogelijkeUitgevoerdeActie.class, LocalDateTime.class);
    }

    public void opslaan(MogelijkeUitgevoerdeActie mogelijkeUitgevoerdeActie) {
        log.info("Actie uitgevoerd {}", mogelijkeUitgevoerdeActie);
        this.hcommands.hset(UITGEVOERDEACTIES, mogelijkeUitgevoerdeActie, LocalDateTime.now());
    }

    public LocalDateTime lees(MogelijkeUitgevoerdeActie mogelijkeUitgevoerdeActie) {
        return this.hcommands.hget(UITGEVOERDEACTIES, mogelijkeUitgevoerdeActie);
    }
}
