//package nl.heidotting.jarvis.lampen.model;
//
//import org.springframework.data.redis.core.RedisHash;
//
//import java.time.LocalDateTime;
//
//@RedisHash(value = "Lichtsterkte", timeToLive = 2678400)//1 maand
//public record Lichtsterkte(String id,
//                           int illuminance,
//                           LocalDateTime tijdstip
//) {
//}
