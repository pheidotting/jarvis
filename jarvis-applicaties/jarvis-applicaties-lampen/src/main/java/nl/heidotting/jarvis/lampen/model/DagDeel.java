package nl.heidotting.jarvis.lampen.model;

public enum DagDeel {
    OCHTEND, MIDDAG, AVOND, NACHT
}
