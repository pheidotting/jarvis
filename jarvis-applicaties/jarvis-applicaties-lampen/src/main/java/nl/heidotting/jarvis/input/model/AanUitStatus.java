package nl.heidotting.jarvis.input.model;

import java.time.LocalDateTime;

//@RedisHash(value = "AanUitStatus", timeToLive = 2678400)//1 maand
public record AanUitStatus(String id,
                           LocalDateTime tijdstip,
                           boolean aan
) {
}
