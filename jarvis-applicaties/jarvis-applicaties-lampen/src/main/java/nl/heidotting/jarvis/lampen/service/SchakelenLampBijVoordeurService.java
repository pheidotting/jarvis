package nl.heidotting.jarvis.lampen.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.ApparaatStatusService;

import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Stream;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMP_VOORDEUR_INGESCHAKELD;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMP_VOORDEUR_UITGESCHAKELD;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class SchakelenLampBijVoordeurService {
    private final ApparaatStatusService apparaatStatusService;
    private final LampenSender lampenSender;
    private final UitgevoerdeActieService uitgevoerdeActieService;

    private List<String> relevanteLampen = List.of(
            "lamp-bij-de-tv",
            "lamp-bij-de-eettafel",
            "lamp-bij-de-pui",
            "lamp-bij-de-bank");

    public void verwerk() {
        Supplier<Stream<ApparaatStatusService.ApparaatStatus>> apparaatStatussen =
                () -> apparaatStatusService.zijnApparatatenIngeschakeld(relevanteLampen)
                        .stream();

        if (apparaatStatussen
                .get()
                .allMatch(aanUitStatus -> aanUitStatus.isAan())) {
            if (!apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")) {
                lampenSender.schakelBuitenlampVoordeur(true);
                uitgevoerdeActieService.opslaan(LAMP_VOORDEUR_INGESCHAKELD);
            }
        }
        if (apparaatStatussen
                .get()
                .allMatch(aanUitStatus -> !aanUitStatus.isAan())) {
            if (apparaatStatusService.isApparataatIngeschakeld("voordeur-lamp")) {
                lampenSender.schakelBuitenlampVoordeur(false);
                uitgevoerdeActieService.opslaan(LAMP_VOORDEUR_UITGESCHAKELD);
            }
        }
    }
}
