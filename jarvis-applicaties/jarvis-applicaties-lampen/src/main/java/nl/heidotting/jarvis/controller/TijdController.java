package nl.heidotting.jarvis.controller;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalTime;

@Path("/tijd")
@Slf4j
public class TijdController {
    private LocalTime tijd;

    public LocalTime tijd() {
        return tijd == null ? LocalTime.now() : tijd;
    }

    @GET
    @Path("/{tijd}")
    public void setTijd(@PathParam("tijd") LocalTime tijd) {
        log.info("Setting tijd to {}", tijd);
        this.tijd = tijd;
    }
}
