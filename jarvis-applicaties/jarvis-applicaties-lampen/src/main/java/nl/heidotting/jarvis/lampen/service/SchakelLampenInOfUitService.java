package nl.heidotting.jarvis.lampen.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.service.woonkamer.InschakelenWoonkamerLampenService;
import nl.heidotting.jarvis.lampen.service.woonkamer.UitschakelenWoonkamerLampenService;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class SchakelLampenInOfUitService {
    private final SchakelBijkeukenBuitenlampService schakelBijkeukenBuitenlampService;
    private final SchakelenLampBijVoordeurService schakelenLampBijVoordeurService;
    private final InschakelenWoonkamerLampenService inschakelenWoonkamerLampenService;
    private final UitschakelenWoonkamerLampenService uitschakelenWoonkamerLampenService;
    private final SchakelBijkeukenlampService schakelBijkeukenlampService;
    ;
    private final SchakelBuitenlampSchuurService schakelBuitenlampSchuurService;

    public void verwerk() {
        inschakelenWoonkamerLampenService.schakelWoonkamerLampen();
        schakelenLampBijVoordeurService.verwerk();
        schakelBijkeukenBuitenlampService.verwerk();
        schakelBijkeukenlampService.verwerk();
        uitschakelenWoonkamerLampenService.schakelWoonkamerLampen();
        schakelBuitenlampSchuurService.verwerk();
    }
}
