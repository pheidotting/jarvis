package nl.heidotting.jarvis.lampen.service;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.ApparaatStatusService;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.*;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class SchakelBijkeukenlampService {
    private final ApparaatStatusService apparaatStatusService;
    private final LampenSender lampenSender;
    private final UitgevoerdeActieService uitgevoerdeActieService;

    public void verwerk() {
        var lampenWoonkamerIngeschakeld = uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD);
        var lampenWoonkamerUitgeschakeld = uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD);

        var lampenWoonkamerUitgeschakeldIsVoorNU = lampenWoonkamerUitgeschakeld != null && lampenWoonkamerUitgeschakeld.isBefore(LocalDateTime.now());
        var lampenWoonkamerIngeschakeldIsVoorLampenWoonkamerIngeschakeld = lampenWoonkamerUitgeschakeld != null && lampenWoonkamerUitgeschakeld != null && lampenWoonkamerIngeschakeld.isBefore(lampenWoonkamerUitgeschakeld);
        var lampenWoonkamerIngeschakeldIsNogNietVandaag = lampenWoonkamerUitgeschakeld != null && lampenWoonkamerIngeschakeld.toLocalDate().isBefore(LocalDate.now());
        var lampenWoonkamerUitgeschakeldIsNogNietVandaag = lampenWoonkamerUitgeschakeld != null && lampenWoonkamerUitgeschakeld.toLocalDate().isBefore(LocalDate.now());

        if (lampenWoonkamerUitgeschakeldIsVoorNU &&
                lampenWoonkamerIngeschakeldIsVoorLampenWoonkamerIngeschakeld &&
                !lampenWoonkamerIngeschakeldIsNogNietVandaag &&
                !lampenWoonkamerUitgeschakeldIsNogNietVandaag
        ) {

            if (apparaatStatusService.isApparataatIngeschakeld("bijkeuken-lamp")) {
                lampenSender.stuurBijkeukenlampEmitter(false);
                uitgevoerdeActieService.opslaan(BIJKEUKEN_LAMP_UITGESCHAKELD);
            }
        }
    }
}
