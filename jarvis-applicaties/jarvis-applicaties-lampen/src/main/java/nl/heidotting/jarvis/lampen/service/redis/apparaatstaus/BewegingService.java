package nl.heidotting.jarvis.lampen.service.redis.apparaatstaus;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import io.quarkus.redis.datasource.keys.KeyCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class BewegingService {
    private final RedisDataSource redisDataSource;

    private HashCommands<String, String, Boolean> hcommands;
    private KeyCommands keyCommands;

    @AllArgsConstructor
    @Getter
    public static class Beweging {
        private String id;
        private boolean beweging;
    }

    @PostConstruct
    public void init() {
        hcommands = redisDataSource.hash(Boolean.class);
        keyCommands = redisDataSource.key();
    }

    public List<Beweging> leesBewegingen() {
        var result = new ArrayList<Beweging>();

        keyCommands.keys("Beweging:*").stream()
                .forEach((Consumer<String>) s -> result.add(new Beweging(s.replace("Beweging:", ""), hcommands.hget(s, "occupancy"))));

        return result;
    }

    public boolean isBeweging(String id) {
        return leesBewegingen()
                .stream()
                .filter(beweging -> beweging.getId().equals(id))
                .map(Beweging::isBeweging)
                .findFirst()
                .get();
    }
}
