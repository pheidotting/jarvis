package nl.heidotting.jarvis.lampen.service.redis.apparaatstaus;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import io.quarkus.redis.datasource.keys.KeyCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class LichtsterkteService {
    private final RedisDataSource redisDataSource;

    private HashCommands<String, String, Integer> hcommands;
    private KeyCommands keyCommands;

    @AllArgsConstructor
    @Getter
    public static class Lichtsterkte {
        private String id;
        private Integer illuminance;
    }

    @PostConstruct
    public void init() {
        hcommands = redisDataSource.hash(Integer.class);
        keyCommands = redisDataSource.key();
    }

    public List<Lichtsterkte> leesLichtsterktes() {
        var result = new ArrayList<Lichtsterkte>();

        keyCommands.keys("Lichtsterkte:*").stream()
                .forEach((Consumer<String>) s -> result.add(new Lichtsterkte(s.replace("Lichtsterkte:", ""), hcommands.hget(s, "illuminance"))));

        return result;
    }

    public int gemiddeldeLichtsterkte() {
        final int[] totaal = {0};
        final int[] aantal = {0};

        leesLichtsterktes()
                .stream()
                .filter(lichtsterkte -> lichtsterkte != null)
                .filter(lichtsterkte -> lichtsterkte.getId().endsWith("links") || lichtsterkte.getId().endsWith("rechts"))
                .forEach(lichtsterkte -> {
                    totaal[0] = totaal[0] + lichtsterkte.getIlluminance();
                    aantal[0]++;
                });

        return totaal[0] / aantal[0];
    }

    public void opslaan(String id, String veld, Integer waarde) {
        this.hcommands.hset("Lichtsterkte:" + id, veld, waarde);
    }
}
