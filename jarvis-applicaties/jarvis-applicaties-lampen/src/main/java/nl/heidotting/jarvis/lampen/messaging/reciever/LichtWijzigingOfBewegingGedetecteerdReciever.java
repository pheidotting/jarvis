package nl.heidotting.jarvis.lampen.messaging.reciever;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.service.SchakelLampenInOfUitService;
import nl.heidotting.jarvis.lampen.service.redis.NaarBedTijdService;
import org.eclipse.microprofile.reactive.messaging.Incoming;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class LichtWijzigingOfBewegingGedetecteerdReciever {
    private final SchakelLampenInOfUitService schakelLampenInOfUitService;
    private final NaarBedTijdService naarBedTijdService;

    @Incoming("LichtWijzigingOfBewegingGedetecteerd")
    public void listener(Object p) {
        schakelLampenInOfUitService.verwerk();
    }


}
