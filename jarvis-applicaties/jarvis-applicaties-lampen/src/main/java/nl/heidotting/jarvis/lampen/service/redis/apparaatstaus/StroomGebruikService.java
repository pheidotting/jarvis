package nl.heidotting.jarvis.lampen.service.redis.apparaatstaus;

import io.quarkus.redis.datasource.RedisDataSource;
import io.quarkus.redis.datasource.hash.HashCommands;
import io.quarkus.redis.datasource.keys.KeyCommands;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class StroomGebruikService {
    private final RedisDataSource redisDataSource;

    private HashCommands<String, String, Double> hcommands;
    private KeyCommands keyCommands;

    @AllArgsConstructor
    @Getter
    public static class StroomGebruik {
        private String id;
        private Double wattage;
        private Double voltage;
    }

    @PostConstruct
    public void init() {
        hcommands = redisDataSource.hash(Double.class);
        keyCommands = redisDataSource.key();
    }

    public List<StroomGebruikService.StroomGebruik> leesStroomGebruiken() {
        var result = new ArrayList<StroomGebruikService.StroomGebruik>();

        keyCommands.keys("StroomGebruik:*").stream()
                .forEach((Consumer<String>) s -> result.add(new StroomGebruikService.StroomGebruik(s.replace("StroomGebruik:", ""), hcommands.hget(s, "wattage"), hcommands.hget(s, "voltage"))));

        return result;
    }

    public Double leesWattageVanApparaat(String id) {
        return leesStroomGebruiken().stream()
                .filter(stroomGebruik -> stroomGebruik.getId().equals(id))
                .map(StroomGebruik::getWattage)
                .findFirst()
                .get();
    }

}
