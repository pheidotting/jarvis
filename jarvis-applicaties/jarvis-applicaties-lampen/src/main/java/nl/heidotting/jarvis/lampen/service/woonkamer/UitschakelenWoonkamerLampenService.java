package nl.heidotting.jarvis.lampen.service.woonkamer;

import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.lampen.sender.LampenSender;
import nl.heidotting.jarvis.lampen.service.DagDeelService;
import nl.heidotting.jarvis.lampen.service.redis.GeenActiviteitSindsService;
import nl.heidotting.jarvis.lampen.service.redis.NaarBedTijdService;
import nl.heidotting.jarvis.lampen.service.redis.UitgevoerdeActieService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.BewegingService;
import nl.heidotting.jarvis.lampen.service.redis.apparaatstaus.StroomGebruikService;

import java.time.LocalTime;

import static java.time.LocalDateTime.now;
import static nl.heidotting.jarvis.lampen.model.DagDeel.AVOND;
import static nl.heidotting.jarvis.lampen.model.DagDeel.NACHT;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMPEN_WOONKAMER_INGESCHAKELD;
import static nl.heidotting.jarvis.lampen.model.MogelijkeUitgevoerdeActie.LAMPEN_WOONKAMER_UITGESCHAKELD;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class UitschakelenWoonkamerLampenService {
    private final LampenSender lampenSender;
    private final DagDeelService dagDeelService;
    private final UitgevoerdeActieService uitgevoerdeActieService;
    private final GeenActiviteitSindsService geenActiviteitSindsService;

    private final BewegingService bewegingService;
    private final StroomGebruikService stroomGebruikService;
    private final NaarBedTijdService naarBedTijdService;

    public void schakelWoonkamerLampen() {
        var dagdeel = dagDeelService.bepaalDagDeel();
//        log.info("Dagdeel nu {}", dagdeel);

        if (dagdeel == AVOND || dagdeel == NACHT) {
            var laatsteKeerLampenUitgeschakeld = uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_UITGESCHAKELD);
            var laatsteKeerLampenIngeschakeld = uitgevoerdeActieService.lees(LAMPEN_WOONKAMER_INGESCHAKELD);

            var stroomgebruiktelevisie = stroomGebruikService.leesWattageVanApparaat("televisie");
            var bewegingLinks = bewegingService.isBeweging("detectie-links");
            var bewegingRechts = bewegingService.isBeweging("detectie-rechts");

            if (stroomgebruiktelevisie > 1) {
                geenActiviteitSindsService.opslaan("televisie");
            }
            if (bewegingLinks) {
                geenActiviteitSindsService.opslaan("detectie-links");
            }
            if (bewegingRechts) {
                geenActiviteitSindsService.opslaan("detectie-rechts");
            }
            log.info("Moet de boel uit?");
            if (laatsteKeerLampenIngeschakeld != null && laatsteKeerLampenIngeschakeld.isBefore(now())
                    && (laatsteKeerLampenUitgeschakeld == null || laatsteKeerLampenUitgeschakeld.isBefore(laatsteKeerLampenIngeschakeld))) {

                var laatsteGebruikTelevisie = geenActiviteitSindsService.lees("televisie");
                var laatsteGebruikDetectieLinks = geenActiviteitSindsService.lees("detectie-links");
                var laatsteGebruikDetectieRechts = geenActiviteitSindsService.lees("detectie-rechts");

                var halfUurGeleden = now().minusMinutes(30);

                log.info("laatsteGebruikTelevisie {}", laatsteGebruikTelevisie);
                log.info("laatsteGebruikDetectieLinks {}", laatsteGebruikDetectieLinks);
                log.info("laatsteGebruikDetectieRechts {}", laatsteGebruikDetectieRechts);
                if ((laatsteGebruikTelevisie != null && laatsteGebruikTelevisie.isBefore(halfUurGeleden)) &&
                        (laatsteGebruikDetectieLinks != null && laatsteGebruikDetectieLinks.isBefore(halfUurGeleden)) &&
                        (laatsteGebruikDetectieRechts != null && laatsteGebruikDetectieRechts.isBefore(halfUurGeleden))
                ) {
                    var gemiddeldeBedtijd = naarBedTijdService.bepaalGemiddeldeBedtijd();
                    log.info("gemiddeldeBedtijd {}", gemiddeldeBedtijd);

                    if (gemiddeldeBedtijd.minusMinutes(30).isBefore(LocalTime.now())) {
                        log.info("UITSCHAKELEN");

                        lampenSender.bijDeBank(false);
                        lampenSender.bijDePui(false);
                        lampenSender.eetkamer(false);
                        lampenSender.naastDeTv(false);

                        uitgevoerdeActieService.opslaan(LAMPEN_WOONKAMER_UITGESCHAKELD);
                        naarBedTijdService.opslaan();
                    }
                }
            }
        }
    }
}
