package nl.heidotting.jarvis.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.client.KeepingClient;
import nl.heidotting.jarvis.messaging.model.LopendeUrenRegistratieAfgesloten;
import nl.heidotting.jarvis.messaging.model.Urenregistratie;
import nl.heidotting.jarvis.messaging.sender.KeepingUpdateSender;
import nl.heidotting.jarvis.messaging.sender.LopendeUrenRegistratieAfgeslotenSender;
import nl.heidotting.jarvis.messaging.sender.UrenNogTeGaanSender;
import nl.heidotting.jarvis.model.KeepingUpdate;
import nl.heidotting.jarvis.model.TimeEntry;
import nl.heidotting.jarvis.model.UrenNogTeGaan;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.util.stream.Collectors.toSet;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class KeepingUpdateService {
    private final KeepingUpdateSender keepingUpdateSender;
    private final LopendeUrenRegistratieAfgeslotenSender lopendeUrenRegistratieAfgeslotenSender;
    private final UrenNogTeGaanSender urenNogTeGaanSender;

    @RestClient
    private KeepingClient keepingClient;
    @Setter
    private LocalDateTime nu;

    private String laatsteTimeEntriesDezeWeek;
    private boolean alEenLopendeRegistratieAanwezig = false;
    private Long idLopendeAanwezig = null;

    public void checkKeeping() {
        var timeEntriesDezeWeek = getUrenDezeWeek();

        checkOfUpdateGestuurdMoetWorden(timeEntriesDezeWeek);
        stuurAfgesloten(timeEntriesDezeWeek);
    }

    private void stuurAfgesloten(List<TimeEntry> timeEntries) {
        var lopendeAanwezig = timeEntries.stream()
                .filter(TimeEntry::ongoing)
                .findAny();
        if (lopendeAanwezig.isPresent()) {
            this.idLopendeAanwezig = lopendeAanwezig.get().id();
            this.alEenLopendeRegistratieAanwezig = true;
        } else if (this.alEenLopendeRegistratieAanwezig) {
            this.alEenLopendeRegistratieAanwezig = false;
            long id = this.idLopendeAanwezig;
            var aanwezig = timeEntries.stream()
                    .filter(timeEntry -> timeEntry.id() == id)
                    .findAny().get();
            verstuurUrenNogTeGaan(timeEntries);

            var taak = keepingClient.taskOpId(aanwezig.task_id()).task();

            var urenregistratie = new Urenregistratie(
                    aanwezig.start().toLocalDateTime(),
                    aanwezig.end().toLocalDateTime()
            );
            var urenregistraties = timeEntries.stream()
                    .map(timeEntry -> new Urenregistratie(
                            timeEntry.start().toLocalDateTime(),
                            timeEntry.end().toLocalDateTime()))
                    .toList();

            this.lopendeUrenRegistratieAfgeslotenSender
                    .stuur(new LopendeUrenRegistratieAfgesloten(taak == null ? "Justid" : taak.name(), urenregistratie, urenregistraties));
        }
    }

    private void checkOfUpdateGestuurdMoetWorden(List<TimeEntry> timeEntriesDezeWeek) {
        var asJson = naarJson(timeEntriesDezeWeek);
        if (!asJson.equals(laatsteTimeEntriesDezeWeek)) {
            laatsteTimeEntriesDezeWeek = naarJson(timeEntriesDezeWeek);
            var taakIds = timeEntriesDezeWeek
                    .stream()
                    .map(TimeEntry::task_id)
                    .collect(toSet());

            var entriesOpTaakId = new HashMap<Long, List<TimeEntry>>();

            taakIds.forEach(taakId -> entriesOpTaakId.put(taakId, timeEntriesDezeWeek.stream()
                    .filter(timeEntry -> timeEntry.task_id().equals(taakId))
                    .toList()));

            entriesOpTaakId.keySet()
                    .forEach(taakId -> {
                        var entries = entriesOpTaakId.get(taakId);
                        var entriesVandaag = entries.stream()
                                .filter(timeEntry -> timeEntry.date().equals(getNu()))
                                .toList();

                        var durationWeek = bepaalDuration(entries);
                        var durationVandaag = bepaalDuration(entriesVandaag);

                        var lopend = entriesVandaag.stream()
                                .anyMatch(timeEntry -> timeEntry.ongoing());

                        var update = new KeepingUpdate(
                                durationVandaag.toMinutes(),
                                durationWeek.toMinutes(),
                                lopend,
                                taakId
                        );
                        log.info("{}", update);
                        keepingUpdateSender.getSender().send(update);
                    });
        }
    }

    private void verstuurUrenNogTeGaan(List<TimeEntry> timeEntries) {
        var urenDezeWeek = getUrenDezeWeek(timeEntries);
        var urenNogTeGaanVoor32 = 32 - urenDezeWeek;
        var urenNogTeGaanVoor36 = 36 - urenDezeWeek;
        var urenNogTeGaanVoor40 = 40 - urenDezeWeek;
        log.info("urenNogTeGaanVoor32 {}", urenNogTeGaanVoor32);
        log.info("urenNogTeGaanVoor36 {}", urenNogTeGaanVoor36);
        log.info("urenNogTeGaanVoor40 {}", urenNogTeGaanVoor40);
        urenNogTeGaanSender.getSender().send(new UrenNogTeGaan(urenDezeWeek, urenNogTeGaanVoor32, urenNogTeGaanVoor36, urenNogTeGaanVoor40));
    }

    private String naarJson(List<TimeEntry> timeEntries) {
        try {
            return new ObjectMapper().findAndRegisterModules().writeValueAsString(timeEntries);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
            return "";
        }
    }

    private Duration bepaalDuration(List<TimeEntry> entries) {
        return entries.isEmpty() ? Duration.ZERO : entries.stream()
                .map(timeEntry -> {
                    var eind = timeEntry.end() == null ? OffsetDateTime.now() : timeEntry.end();
                    return Duration.between(timeEntry.start(), eind);
                })
                .reduce((duration, duration2) -> duration.plus(duration2))
                .get();
    }

    private List<TimeEntry> getUrenDezeWeek() {
        var result = new ArrayList<TimeEntry>();
        var vandaag = getNu();
        var maandag = getNu();
        while (maandag.getDayOfWeek() != DayOfWeek.MONDAY) {
            maandag = maandag.minusDays(1);
        }
        var dag = maandag;
        while (!dag.isAfter(vandaag)) {
            result.addAll(this.keepingClient.timeEntriesOpDatum(dag.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))).time_entries());
            dag = dag.plusDays(1);
        }
        return result;
    }

    private Double getUrenDezeWeek(List<TimeEntry> timeEntries) {
        return timeEntries.stream()
                .map(TimeEntry::hours)
                .reduce(0.0, (a, b) -> a + b);
    }


    public LocalDate getNu() {
        if (nu == null) {
            return LocalDate.now();
        } else {
            return nu.toLocalDate();
        }
    }
}
