package nl.heidotting.jarvis.messaging.model;

import java.time.LocalDateTime;

public record Urenregistratie(
        LocalDateTime startTijd,
        LocalDateTime eindTijd
) {
}
