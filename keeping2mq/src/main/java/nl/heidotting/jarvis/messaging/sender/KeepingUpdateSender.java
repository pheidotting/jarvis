package nl.heidotting.jarvis.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.Getter;
import nl.heidotting.jarvis.model.KeepingUpdate;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@Getter
@ApplicationScoped
public class KeepingUpdateSender {
    @Inject
    @Channel("keeping-update")
    Emitter<KeepingUpdate> sender;
}
