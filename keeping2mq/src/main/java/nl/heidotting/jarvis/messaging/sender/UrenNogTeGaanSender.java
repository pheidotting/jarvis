package nl.heidotting.jarvis.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.Getter;
import nl.heidotting.jarvis.model.UrenNogTeGaan;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@Getter
@ApplicationScoped
public class UrenNogTeGaanSender {
    @Inject
    @Channel("uren-nog-te-gaan")
    Emitter<UrenNogTeGaan> sender;
}
