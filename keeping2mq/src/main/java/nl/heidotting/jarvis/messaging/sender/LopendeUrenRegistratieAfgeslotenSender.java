package nl.heidotting.jarvis.messaging.sender;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import lombok.Getter;
import nl.heidotting.jarvis.messaging.model.LopendeUrenRegistratieAfgesloten;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;

@ApplicationScoped
public class LopendeUrenRegistratieAfgeslotenSender {
    @Inject
    @Channel("lopende-urenregistratie-afgesloten")
    private Emitter<LopendeUrenRegistratieAfgesloten> sender;

    public void stuur(LopendeUrenRegistratieAfgesloten lopendeUrenRegistratieAfgesloten) {
        this.sender.send(lopendeUrenRegistratieAfgesloten);
    }
}
