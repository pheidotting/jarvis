package nl.heidotting.jarvis.messaging.model;

import java.util.List;

public record LopendeUrenRegistratieAfgesloten(
        String taakNaam,
        Urenregistratie afgeslotenUrenregistratie,
        List<Urenregistratie> urenregistratiesDezeWeek
) {
}
