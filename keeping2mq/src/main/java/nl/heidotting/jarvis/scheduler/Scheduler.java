package nl.heidotting.jarvis.scheduler;

import io.quarkus.scheduler.Scheduled;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.service.KeepingUpdateService;

@Slf4j
@ApplicationScoped
@RequiredArgsConstructor
public class Scheduler {
    private final KeepingUpdateService keepingUpdateService;

    @Scheduled(cron = "{check.keeping.cron.expr}")
    void checkKeeping() {
        log.info("Check Keeping");
            keepingUpdateService.checkKeeping();
    }
}