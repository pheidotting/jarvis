package nl.heidotting.jarvis.client;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.QueryParam;
import nl.heidotting.jarvis.model.TaakResponse;
import nl.heidotting.jarvis.model.TimeEntries;
import org.eclipse.microprofile.rest.client.annotation.ClientHeaderParam;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;
import org.jboss.resteasy.reactive.RestQuery;

import static jakarta.ws.rs.core.HttpHeaders.AUTHORIZATION;


@RegisterRestClient(configKey = "keeping-api")
public interface KeepingClient {
    @GET
    @Path("/time-entries")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${keeping2mq.jwt}")
    TimeEntries timeEntriesOpDatum(@QueryParam("date") String datum);

    @GET
    @Path("/tasks")
    @ClientHeaderParam(name = AUTHORIZATION, value = "Bearer ${keeping2mq.jwt}")
    TaakResponse taskOpId(@RestQuery Long id);
}