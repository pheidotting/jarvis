package nl.heidotting.jarvis.model;


public record KeepingUpdate(
        long dag,
        long week,
        boolean lopend,
        long taskId) {
}
