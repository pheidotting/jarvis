package nl.heidotting.jarvis.model;

import java.time.LocalDateTime;

public record LopendeUrenRegistratieAfgesloten(
        LocalDateTime startTijd,
        LocalDateTime eindTijd,
        String project,
        Long taakId
) {
}
