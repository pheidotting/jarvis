package nl.heidotting.jarvis.model;

public record UrenNogTeGaan(
        double urenDezeWeek,
        double nogTeGaanVoor32,
        double nogTeGaanVoor36,
        double nogTeGaanVoor40
) {
}
