package nl.heidotting.jarvis.model;

import java.time.LocalDate;
import java.time.OffsetDateTime;

public record TimeEntry(
        Long id,
        Long user_id,
        LocalDate date,
        String purpose,
        Long project_id,
        Long task_id,
        String note,
        OffsetDateTime start,
        OffsetDateTime end,
        Double hours,
        Boolean ongoing,
        Boolean locked,
        Boolean is_direct_hours
) {
}
