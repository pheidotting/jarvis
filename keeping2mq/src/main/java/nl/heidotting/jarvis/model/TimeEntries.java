package nl.heidotting.jarvis.model;

import java.util.List;

public record TimeEntries(
        List<TimeEntry> time_entries
) {
}
