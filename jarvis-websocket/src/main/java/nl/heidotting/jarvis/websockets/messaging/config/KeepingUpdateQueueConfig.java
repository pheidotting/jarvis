package nl.heidotting.jarvis.websockets.messaging.config;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.QueueConfiger;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class KeepingUpdateQueueConfig {
    @Value("${queue.keeping.update}")
    private String queue;
    @Value("${topic.keeping.update}")
    private String topic;

    private QueueConfiger queueConfiger;

    @PostConstruct
    public void init() {
        queueConfiger = new QueueConfiger()
                .withLogger(log)
                .withFanoutExchange(topic)
                .withRoutingKey("")
                .withQueue(queue)
                .withBinding()
                .build();
    }

    @Bean
    public FanoutExchange keepingUpdateTopic() {
        return queueConfiger.getFanoutExchange();
    }

    @Bean
    public Queue keepingUpdateQueue() {
        return queueConfiger.getQueue();
    }

    @Bean
    public Binding keepingUpdateBinding() {
        return queueConfiger.getBinding();
    }
}
