package nl.heidotting.jarvis.websockets.controller;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
public class TestController {
    @PostConstruct
    public void init() {
        log.info("Jeojeoje");
    }

    @GetMapping("/joejoe")
    public String joejoe() {
        return "JoeJoe";
    }
}
