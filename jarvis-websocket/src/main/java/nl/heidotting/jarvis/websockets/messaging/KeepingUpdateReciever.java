package nl.heidotting.jarvis.websockets.messaging;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.KeepingUpdate;
import nl.heidotting.jarvis.websockets.socket.KeepingUpdateSender;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class KeepingUpdateReciever {
    private final KeepingUpdateSender keepingUpdateSender;

    @RabbitListener(queues = "#{keepingUpdateQueueConfig.keepingUpdateQueue()}")
    public void recievedMessage(String keepingUpdate) throws JsonProcessingException {
        var update = new ObjectMapper().readValue(keepingUpdate, KeepingUpdate.class);
        log.info("Ontvangen : {}", update);
        keepingUpdateSender.sendMessage(update);
    }
}
