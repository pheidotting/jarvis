package nl.heidotting.jarvis.websockets.socket;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import nl.heidotting.jarvis.shared.mqtt.messages.KeepingUpdate;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.messaging.SessionConnectEvent;

@Log4j2
@Service
@RequiredArgsConstructor
public class KeepingUpdateSender {
    private final SimpMessagingTemplate simpMessagingTemplate;

    private KeepingUpdate ditMoetEigenlijkAnders;

    public void sendMessage(KeepingUpdate keepingUpdate) {
        log.info("Stuur {} naar /topic/keepingupdate/{}", keepingUpdate, keepingUpdate.taskId());
        simpMessagingTemplate.convertAndSend("/topic/keepingupdate/" + keepingUpdate.taskId(), keepingUpdate);
        this.ditMoetEigenlijkAnders = keepingUpdate;
    }

    @EventListener
    private void handleSessionConnected(SessionConnectEvent event) {
        if (ditMoetEigenlijkAnders != null) {
            sendMessage(ditMoetEigenlijkAnders);
        }
    }
}
