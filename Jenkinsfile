import java.text.SimpleDateFormat
import java.util.Date

pipeline {
    environment {
        SONAR_TOKEN = '7f6a00825d74b5369b6f9445f8c07d5ae67dca1d'
        DOCKER_TAG = getGitBranchName()
        VERSIENUMMER = getVersieNummer()
        SNAPSHOTNUMMER = getSnapshotNummer()
    }
    agent any
    parameters {
      booleanParam(name: 'dockerImagesMaken', defaultValue: false, description: 'Docker images maken?')
      booleanParam(name: 'kwaliteitschecks', defaultValue: true, description: 'Kwaliteitschecks uitvoeren?')
    }
    post {
        failure {
            updateGitlabCommitStatus name: 'Compile', state: 'failed'
        }
        success {
            updateGitlabCommitStatus name: 'Compile', state: 'success'
        }
    }
    options {
        gitLabConnection('Gitlab')
        buildDiscarder(logRotator(artifactNumToKeepStr: '5'))
    }
    triggers {
        gitlab(triggerOnPush: true, triggerOnMergeRequest: true)
    }
    tools {
        maven 'maven-3.9.2'
        nodejs 'node'
    }
    stages {
        stage('Start') {
            steps {
                cleanWs()
                checkout scm
                script {
                    currentBuild.description = getReleaseVersion()
                    currentBuild.displayName = BUILD_NUMBER + ' - ' + commitMessage()
                }
            }
        }
        stage('Builden') {
            steps {
                script {
                    try {
                        if(env.BRANCH_NAME == 'development' && commitMessage().startsWith('[maven-release-plugin]')) {
                            sh '''mvn install -DskipTests=true -T 6'''
                        } else if (env.BRANCH_NAME.startsWith("tag/")) {
                            currentBuild.displayName = 'Versie ' + getVersieNummer() + ' builden en deployen'
                            sh '''
                                cd jarvis-applicaties &&
                                mvn install'''
                        } else {
                            sh '''
                                cd jarvis-applicaties &&
                                mvn install '''
                        }
                    } finally {
                    }
                }
            }
        }
        stage('Kwaliteitschecks') {
            when {
                environment name: 'kwaliteitschecks', value: 'true'
            }
            steps {
                script {
                    if(!(env.BRANCH_NAME == 'development' && commitMessage().startsWith('[maven-release-plugin]'))) {
                        sh '''
                            cd jarvis-applicaties &&
                            mvn  org.sonarsource.scanner.maven:sonar-maven-plugin:sonar -Pcoverage -Dsonar.projectKey=pheidotting_jarvis -DskipTests=true -Dsonar.branch.name=''' + env.BRANCH_NAME
                    }
                }
            }
        }
        stage('Release') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return (env.releaseMaken == "true" || branchNaam == 'development') && !commitMessage().startsWith('[maven-release-plugin]') && !commitMessage().startsWith('snapshot versie');
                }
            }
            steps {
                script {
                    currentBuild.displayName = 'Release maken (' + getVersieNummer() + ' en ' + getSnapshotNummer() + ')'
                    sh '''
                        rm docker-compose.yml &&
                        cp docker-compose-base.yml docker-compose.yml &&
                        sed -i "s/{{VERSIE}}/''' + getVersieNummer() + '''/g" docker-compose.yml &&
                        git add docker-compose.yml &&
                        git commit -m 'versie ''' + env.VERSIENUMMER + '''' &&

                        cd jarvis-applicaties &&
                        mvn clean release:clean release:prepare -B release:perform -Darguments=-DskipTests=true -Dmaven.deploy.skip=true -DreleaseVersion=$VERSIENUMMER -DdevelopmentVersion=$SNAPSHOTNUMMER -Dtag=$VERSIENUMMER

                        curl --request POST --header "PRIVATE-TOKEN: glpat-xuDizqtvaAiLdTLFSsNN" \
                          --url "https://gitlab.com/api/v4/projects/33050325/repository/branches?branch=tag%2F$VERSIENUMMER&ref=$VERSIENUMMER"
                        '''
                }
            }
        }
        stage('Build images') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return env.dockerImagesMaken == "true" || branchNaam.startsWith("tag/");
                }
            }
            steps {
                script {
                    sh '''
                        cd jarvis-applicaties &&
                        mvn clean install -Dquarkus.container-image.build=true -Dquarkus.container-image.push=true -Dquarkus.container-image.tag=$DOCKER_TAG -DskipTests=true -T 6'''
                }
            }
        }
        stage('Branch verwijderen') {
            when {
                expression {
                    def branchNaam = env.BRANCH_NAME;
                    return env.dockerImagesMaken == "true" || branchNaam.startsWith("tag/");
                }
            }
            steps {
                script {
                    if (env.BRANCH_NAME.startsWith("tag/")) {
                        sh 'curl --request DELETE --header "PRIVATE-TOKEN: glpat-xuDizqtvaAiLdTLFSsNN" \
                              --url "https://gitlab.com/api/v4/projects/33050325/repository/branches/tag%2F$DOCKER_TAG"'
                    }
                }
            }
        }
        stage('CleanUp Docker') {
            when {
                environment name: 'dockerImagesMaken', value: 'true'
            }
            steps {
                steps {
                    sh '''
                        docker system prune -f
                    '''
                }
            }
        }
   }
}
def getGitBranchName() {
    return env.BRANCH_NAME.replace("tag/","")
}

def getNameSpace() {
    return env.BRANCH_NAME.startsWith("tag/") ? 'prd' : 'tst'
}

def getReleaseVersion() {
    def pom = readMavenPom file: 'jarvis-applicaties/pom.xml'
    return pom.version
}

def getVersieNummer() {
    def h1 = getReleaseVersion();
    def huidig1 =  h1.replace("-SNAPSHOT","")
    def huidig =  huidig1.tokenize('.');
    int volg = 0;
    for (String h : huidig){
        if(h == ':') {
            h = 10;
        }
        volg = h.toInteger();
    }

    int volgnummer = 0;
    def day = new SimpleDateFormat('DDD').format(new Date())
    def year = new SimpleDateFormat('yy').format(new Date())

    if(year == huidig[0] && day == huidig[1]) {
        volgnummer = volg
        echo 'Nu : ' + volgnummer
    }

    echo year + '.' + day + '.' + volgnummer
    return year + '.' + day + '.' + volgnummer
}

def getSnapshotNummer() {
    def versie = getVersieNummer().tokenize('.');

//     echo 'versie[2]' + versie[2]

//     echo versie[0] + '.' + versie[1] + '.' + (++versie[2]) + '-SNAPSHOT'
    return versie[0] + '.' + versie[1] + '.' + (++versie[2]) + '-SNAPSHOT'
}

def commitMessage() {
    sh(returnStdout: true, script: 'git log -1 --pretty=%B').trim();
}