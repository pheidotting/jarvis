package nl.heidotting.jarvis.keepingbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.ClientParameters;
import com.rabbitmq.http.client.domain.ExchangeInfo;
import com.rabbitmq.http.client.domain.OutboundMessage;
import com.rabbitmq.http.client.domain.QueueInfo;
import lombok.SneakyThrows;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingRequest;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.Times;
import org.mockserver.mock.Expectation;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static nl.heidotting.jarvis.shared.mqtt.Queues.START_STOP_URENREGISTRATIE_RESPONSE_QUEUE;
import static nl.heidotting.jarvis.shared.mqtt.Topics.START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE;
import static nl.heidotting.jarvis.shared.mqtt.Topics.START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StopUrenregistratieGeenRegistratieAanwezigTest {
    private final String STOP_URENREGISTRATIE_REQUEST_EXCHANGE = START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE;
    private final String STOP_URENREGISTRATIE_RESPONSE_EXCHANGE = START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE;
    private final String STOP_URENREGISTRATIE_RESPONSE_QUEUE = START_STOP_URENREGISTRATIE_RESPONSE_QUEUE + "_JUNIT";

    private static RabbitMQContainer rabbitmq;
    public static final DockerImageName MOCKSERVER_IMAGE = DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion());

    public static MockServerContainer mockServer;

    static {
        rabbitmq = new RabbitMQContainer();
        rabbitmq.start();
        mockServer = new MockServerContainer(MOCKSERVER_IMAGE);
        mockServer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.rabbitmq.host", rabbitmq::getHost);
        registry.add("spring.rabbitmq.port", rabbitmq::getAmqpPort);
        registry.add("spring.rabbitmq.username", rabbitmq::getAdminUsername);
        registry.add("spring.rabbitmq.password", rabbitmq::getAdminPassword);
        registry.add("spring.rabbitmq.ssl.enabled", () -> false);
        registry.add("keeping.base.url", () -> mockServer.getEndpoint());
        registry.add("check.keeping.interval", () -> 5000);
        registry.add("check.keeping.initial.delay", () -> 120000);
    }

    @SneakyThrows
    @Test
    @DisplayName("Test het stoppen van de enige lopende UrenRegistratie, geen lopende registratie aanwezig")
    public void testStopUrenRegistratie() {
        Client c = new Client(
                new ClientParameters()
                        .url("http://" + rabbitmq.getHost() + ":" + rabbitmq.getHttpPort() + "/api/")
                        .username(rabbitmq.getAdminUsername())
                        .password(rabbitmq.getAdminPassword())
        );

        c.declareExchange("/", STOP_URENREGISTRATIE_REQUEST_EXCHANGE, new ExchangeInfo("fanout", true, false));
        c.declareExchange("/", STOP_URENREGISTRATIE_RESPONSE_EXCHANGE, new ExchangeInfo("fanout", true, false));
        c.declareQueue("/", STOP_URENREGISTRATIE_RESPONSE_QUEUE, new QueueInfo(false, true, false));
        c.bindQueue("/", STOP_URENREGISTRATIE_RESPONSE_QUEUE, STOP_URENREGISTRATIE_RESPONSE_EXCHANGE, null);

        var outboundMessage = new OutboundMessage();
        outboundMessage.payload(new ObjectMapper().findAndRegisterModules()
                .writeValueAsString(
                        new StartStopUrenregistratieKeepingRequest(false)
                )
        );

        var timeEntriesResponse = "{\"time_entries\": [\n" +
                "]}";

        MockServerClient mockServerClient = new MockServerClient(mockServer.getHost(), mockServer.getServerPort());

        var expectations1 = asList(mockServerClient
                .when(request().withPath("/time-entries"),
                        Times.exactly(1)
                )
                .respond(response().withBody(timeEntriesResponse)));

        c.publish("/", STOP_URENREGISTRATIE_REQUEST_EXCHANGE, "", outboundMessage);

        await().atMost(100, SECONDS).until(() -> {
            var message = c.get("/", STOP_URENREGISTRATIE_RESPONSE_QUEUE);
            if (message != null) {
                var startStopUrenregistratieKeepingResponse = new ObjectMapper().findAndRegisterModules().readValue(message.getPayload(), StartStopUrenregistratieKeepingResponse.class);
                System.out.println(startStopUrenregistratieKeepingResponse);
                assertEquals("Er is geen registratie aanwezig om te stoppen", startStopUrenregistratieKeepingResponse.melding());

                expectations1.stream()
                        .map(Expectation::getId)
                        .forEach(mockServerClient::verify);
            }
            return message != null;
        });
    }
}
