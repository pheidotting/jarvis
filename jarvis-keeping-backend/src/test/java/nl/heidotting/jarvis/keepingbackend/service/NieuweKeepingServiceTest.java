//package nl.heidotting.jarvis.keepingbackend.service;
//
//import nl.heidotting.jarvis.keepingbackend.messaging.sender.KeepingUpdateSender;
//import nl.heidotting.jarvis.shared.mqtt.messages.KeepingUpdate;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.Captor;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.jupiter.MockitoExtension;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpMethod;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.client.RestTemplate;
//
//import java.time.LocalDateTime;
//
//import static org.junit.jupiter.api.Assertions.assertEquals;
//import static org.mockito.ArgumentMatchers.eq;
//import static org.mockito.ArgumentMatchers.isA;
//import static org.mockito.Mockito.verify;
//import static org.mockito.Mockito.when;
//
//@ExtendWith(MockitoExtension.class)
//class NieuweKeepingServiceTest {
//
//    @InjectMocks
//    private NieuweKeepingService nieuweKeepingService;
//
//    @Mock
//    private KeepingUpdateSender keepingUpdateSender;
//    @Mock
//    private RestTemplate restTemplate;
//
//    @Mock
//    private ResponseEntity responseEntityMaandag;
//    @Mock
//    private ResponseEntity responseEntityDinsdag;
//    @Mock
//    private ResponseEntity responseEntityWoensdag;
//    @Mock
//    private ResponseEntity responseEntityDonderdag;
//
//    @Captor
//    private ArgumentCaptor<KeepingUpdate> keepingUpdateArgumentCaptor;
//
//    @Test
//    public void test() {
//        nieuweKeepingService.setNu(LocalDateTime.of(2023, 8, 10, 16, 0));
//
//        when(restTemplate.exchange(eq("null/time-entries?date=2023-08-07"), isA(HttpMethod.class), isA(HttpEntity.class), eq(String.class))).thenReturn(responseEntityMaandag);
//        when(responseEntityMaandag.getBody()).thenReturn("{\"time_entries\":[{\"date\":\"2023-08-07\",\"note\":null,\"hours\":4.3908,\"purpose\":\"work\",\"start\":\"2023-08-07T06:30:00+02:00\",\"task_id\":205032,\"ongoing\":false,\"user_id\":7830,\"project_id\":null,\"is_direct_hours\":true,\"end\":\"2023-08-07T10:53:27+02:00\",\"id\":11616083,\"locked\":false,\"external_references\":[]},{\"date\":\"2023-08-07\",\"note\":null,\"hours\":2.3878,\"purpose\":\"work\",\"start\":\"2023-08-07T13:10:56+02:00\",\"task_id\":205032,\"ongoing\":false,\"user_id\":7830,\"project_id\":null,\"is_direct_hours\":true,\"end\":\"2023-08-07T15:34:12+02:00\",\"id\":11619317,\"locked\":false,\"external_references\":[]}]}");
//        when(restTemplate.exchange(eq("null/time-entries?date=2023-08-08"), isA(HttpMethod.class), isA(HttpEntity.class), eq(String.class))).thenReturn(responseEntityDinsdag);
//        when(responseEntityDinsdag.getBody()).thenReturn("{\"time_entries\":[{\"date\":\"2023-08-08\",\"note\":null,\"hours\":1.8225,\"purpose\":\"work\",\"start\":\"2023-08-08T06:26:29+02:00\",\"task_id\":205032,\"ongoing\":false,\"user_id\":7830,\"project_id\":null,\"is_direct_hours\":true,\"end\":\"2023-08-08T08:15:50+02:00\",\"id\":11623380,\"locked\":false,\"external_references\":[]},{\"date\":\"2023-08-08\",\"note\":null,\"hours\":2.5514,\"purpose\":\"work\",\"start\":\"2023-08-08T09:00:00+02:00\",\"task_id\":205032,\"ongoing\":false,\"user_id\":7830,\"project_id\":null,\"is_direct_hours\":true,\"end\":\"2023-08-08T11:33:05+02:00\",\"id\":11624857,\"locked\":false,\"external_references\":[]},{\"date\":\"2023-08-08\",\"note\":null,\"hours\":2.7839,\"purpose\":\"work\",\"start\":\"2023-08-08T13:15:58+02:00\",\"task_id\":205032,\"ongoing\":false,\"user_id\":7830,\"project_id\":null,\"is_direct_hours\":true,\"end\":\"2023-08-08T16:03:00+02:00\",\"id\":11627036,\"locked\":false,\"external_references\":[]}]}");
//        when(restTemplate.exchange(eq("null/time-entries?date=2023-08-09"), isA(HttpMethod.class), isA(HttpEntity.class), eq(String.class))).thenReturn(responseEntityWoensdag);
//        when(responseEntityWoensdag.getBody()).thenReturn("{\"time_entries\":[{\"date\":\"2023-08-09\",\"note\":null,\"hours\":9.1897,\"purpose\":\"work\",\"start\":\"2023-08-09T06:16:54+02:00\",\"task_id\":205032,\"ongoing\":false,\"user_id\":7830,\"project_id\":null,\"is_direct_hours\":true,\"end\":\"2023-08-09T15:28:17+02:00\",\"id\":11630840,\"locked\":false,\"external_references\":[]}]}");
//        when(restTemplate.exchange(eq("null/time-entries?date=2023-08-10"), isA(HttpMethod.class), isA(HttpEntity.class), eq(String.class))).thenReturn(responseEntityDonderdag);
//        when(responseEntityDonderdag.getBody()).thenReturn("{\"time_entries\":[{\"date\":\"2023-08-10\",\"note\":null,\"hours\":8.2858,\"purpose\":\"work\",\"start\":\"2023-08-10T06:30:00+02:00\",\"task_id\":205032,\"ongoing\":true,\"user_id\":7830,\"project_id\":null,\"is_direct_hours\":true,\"end\":null,\"id\":11637304,\"locked\":false,\"external_references\":[]}]}");
//
//        nieuweKeepingService.checkKeeping();
//
//        verify(keepingUpdateSender).stuur(keepingUpdateArgumentCaptor.capture());
//
//        var keepingUpdate = keepingUpdateArgumentCaptor.getValue();
//
//        assertEquals(205032, keepingUpdate.taskId());
//        assertEquals(570, keepingUpdate.dag());
//        assertEquals(1957, keepingUpdate.week());
//        assertEquals(true, keepingUpdate.lopend());
//    }
//}