package nl.heidotting.jarvis.keepingbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.ClientParameters;
import com.rabbitmq.http.client.domain.ExchangeInfo;
import com.rabbitmq.http.client.domain.OutboundMessage;
import com.rabbitmq.http.client.domain.QueueInfo;
import lombok.SneakyThrows;
import nl.heidotting.jarvis.shared.mqtt.messages.MaandAfgesloten;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.Times;
import org.mockserver.mock.Expectation;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.Duration;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;

import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static nl.heidotting.jarvis.shared.mqtt.Queues.STUUR_MAAND_AFGESLOTEN_QUEUE;
import static nl.heidotting.jarvis.shared.mqtt.Topics.MAAND_AFGESLOTEN_EXCHANGE;
import static nl.heidotting.jarvis.shared.mqtt.Topics.STUUR_MAAND_AFGESLOTEN_EXCHANGE;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MaandAfgeslotenTest {
    private static RabbitMQContainer rabbitmq;
    public static final DockerImageName MOCKSERVER_IMAGE = DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion());

    public static MockServerContainer mockServer;

    static {
        rabbitmq = new RabbitMQContainer();
        rabbitmq.start();
        mockServer = new MockServerContainer(MOCKSERVER_IMAGE);
        mockServer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.rabbitmq.host", rabbitmq::getHost);
        registry.add("spring.rabbitmq.port", rabbitmq::getAmqpPort);
        registry.add("spring.rabbitmq.username", rabbitmq::getAdminUsername);
        registry.add("spring.rabbitmq.password", rabbitmq::getAdminPassword);
        registry.add("spring.rabbitmq.ssl.enabled", () -> false);
        registry.add("keeping.base.url", () -> mockServer.getEndpoint());
        registry.add("check.keeping.interval", () -> 5000);
        registry.add("check.keeping.initial.delay", () -> 120000);
    }

    @SneakyThrows
    @Test
    @DisplayName("Test de maandafsluiting")
    public void maandAfgeslotenTest() {
        Client c = new Client(
                new ClientParameters()
                        .url("http://" + rabbitmq.getHost() + ":" + rabbitmq.getHttpPort() + "/api/")
                        .username(rabbitmq.getAdminUsername())
                        .password(rabbitmq.getAdminPassword())
        );

        c.declareExchange("/", MAAND_AFGESLOTEN_EXCHANGE, new ExchangeInfo("fanout", true, false));
        c.declareExchange("/", STUUR_MAAND_AFGESLOTEN_EXCHANGE, new ExchangeInfo("fanout", true, false));
        c.declareQueue("/", STUUR_MAAND_AFGESLOTEN_QUEUE + "_JUNIT", new QueueInfo(false, true, false));
        c.bindQueue("/", STUUR_MAAND_AFGESLOTEN_QUEUE + "_JUNIT", STUUR_MAAND_AFGESLOTEN_EXCHANGE, null);

        var outboundMessage = new OutboundMessage();
        outboundMessage.payload(new ObjectMapper().findAndRegisterModules()
                .writeValueAsString(
                        new MaandAfgesloten(
                                Duration.of(50L, ChronoUnit.HOURS),
                                Duration.of(150L, ChronoUnit.HOURS),
                                "Taakomschrijving",
                                12345L)
                )
        );

        var reportMetTaakIds = "{\n" +
                "\"report\": {\n" +
                "\"from\": \"2022-11-01\",\n" +
                "\"to\": \"2022-12-31\",\n" +
                "\"row_type\": \"month\",\n" +
                "\"rows\": [\n" +
                "{\n" +
                "\"id\": \"2022-12-01..2022-12-31\",\n" +
                "\"description\": \"December 2022\",\n" +
                "\"url\": null,\n" +
                "\"hours\": 123.7189,\n" +
                "\"direct_hours\": null\n" +
                "},\n" +
                "{\n" +
                "\"id\": \"2022-11-01..2022-11-30\",\n" +
                "\"description\": \"November 2022\",\n" +
                "\"url\": null,\n" +
                "\"hours\": 141.3967,\n" +
                "\"direct_hours\": null\n" +
                "}\n" +
                "],\n" +
                "\"total_hours\": 265.1156,\n" +
                "\"total_direct_hours\": null\n" +
                "}\n" +
                "}";

        MockServerClient mockServerClient = new MockServerClient(mockServer.getHost(), mockServer.getServerPort());
        var einddatum = YearMonth.now().atEndOfMonth();

        var expectations = asList(mockServerClient
                .when(request().withPath("/report")
                                .withQueryStringParameter("from", "2022-11-01")
                                .withQueryStringParameter("to", einddatum.toString())
                                .withQueryStringParameter("row_type", "month")
                                .withQueryStringParameter("task_ids[]", "12345"),
                        Times.exactly(1)
                )
                .respond(response().withBody(reportMetTaakIds)));

        c.publish("/", MAAND_AFGESLOTEN_EXCHANGE, "", outboundMessage);

        await().atMost(100, SECONDS).until(() -> {
            var message = c.get("/", STUUR_MAAND_AFGESLOTEN_QUEUE + "_JUNIT");
            if (message != null) {
                assertTrue(message.getPayload().contains("\"urenKeeping\":265.11560000000003"));
                assertTrue(message.getPayload().contains("\"urenMoneybird\":150.0"));
                assertTrue(message.getPayload().contains("\"verschil\":115.11560000000003"));
                assertTrue(message.getPayload().contains("\"taakOmschrijving\":\"Taakomschrijving\""));

                expectations.stream()
                        .map(Expectation::getId)
                        .forEach(mockServerClient::verify);
            }
            return message != null;
        });
    }
}
