//package nl.heidotting.jarvis.keepingbackend;
//
//import com.rabbitmq.http.client.Client;
//import com.rabbitmq.http.client.ClientParameters;
//import com.rabbitmq.http.client.domain.ExchangeInfo;
//import com.rabbitmq.http.client.domain.QueueInfo;
//import lombok.SneakyThrows;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockserver.client.MockServerClient;
//import org.mockserver.matchers.Times;
//import org.mockserver.mock.Expectation;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.test.context.DynamicPropertyRegistry;
//import org.springframework.test.context.DynamicPropertySource;
//import org.springframework.test.context.junit.jupiter.SpringExtension;
//import org.testcontainers.containers.MockServerContainer;
//import org.testcontainers.containers.RabbitMQContainer;
//import org.testcontainers.junit.jupiter.Testcontainers;
//import org.testcontainers.utility.DockerImageName;
//
//import java.time.DayOfWeek;
//import java.time.LocalDate;
//
//import static java.util.Arrays.asList;
//import static java.util.concurrent.TimeUnit.SECONDS;
//import static nl.heidotting.jarvis.shared.mqtt.Queues.LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE;
//import static nl.heidotting.jarvis.shared.mqtt.Queues.UREN_NOG_TE_GAAN_QUEUE;
//import static nl.heidotting.jarvis.shared.mqtt.Topics.LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE;
//import static nl.heidotting.jarvis.shared.mqtt.Topics.UREN_NOG_TE_GAAN_EXCHANGE;
//import static org.junit.jupiter.api.Assertions.assertTrue;
//import static org.mockserver.model.HttpRequest.request;
//import static org.mockserver.model.HttpResponse.response;
//import static org.testcontainers.shaded.org.awaitility.Awaitility.await;
//
//@Testcontainers
//@ExtendWith(SpringExtension.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//public class ApplicatieTest {
//    private final String NOG_TE_GAAN_EXCHANGE = UREN_NOG_TE_GAAN_EXCHANGE;
//    private final String URENREGISTRATIE_AFGESLOTEN_EXCHANGE = LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE;
//    private final String NOG_TE_GAAN_QUEUE = UREN_NOG_TE_GAAN_QUEUE + "_JUNIT";
//    private final String URENREGISTRATIE_AFGESLOTEN_QUEUE = LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE + "_JUNIT";
//
//    private static RabbitMQContainer rabbitmq;
//    public static final DockerImageName MOCKSERVER_IMAGE = DockerImageName
//            .parse("mockserver/mockserver")
//            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion());
//
//    public static MockServerContainer mockServer;
//
//    static {
//        rabbitmq = new RabbitMQContainer();
//        rabbitmq.start();
//        mockServer = new MockServerContainer(MOCKSERVER_IMAGE);
//        mockServer.start();
//    }
//
//    @DynamicPropertySource
//    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
//        registry.add("spring.rabbitmq.host", rabbitmq::getHost);
//        registry.add("spring.rabbitmq.port", rabbitmq::getAmqpPort);
//        registry.add("spring.rabbitmq.username", rabbitmq::getAdminUsername);
//        registry.add("spring.rabbitmq.password", rabbitmq::getAdminPassword);
//        registry.add("spring.rabbitmq.ssl.enabled", () -> false);
//        registry.add("keeping.base.url", () -> mockServer.getEndpoint());
//        registry.add("check.keeping.interval", () -> 5000);
//    }
//
//    @SneakyThrows
//    @Test
//    @DisplayName("Test dat UrenNogTeGaan correct wordt verstuurd als een tijdregistratie wordt gestopt")
//    public void urenNogTeGaan() {
//        Client c = new Client(
//                new ClientParameters()
//                        .url("http://" + rabbitmq.getHost() + ":" + rabbitmq.getHttpPort() + "/api/")
//                        .username(rabbitmq.getAdminUsername())
//                        .password(rabbitmq.getAdminPassword())
//        );
//
//        c.declareExchange("/", NOG_TE_GAAN_EXCHANGE, new ExchangeInfo("fanout", true, false));
//        c.declareExchange("/", URENREGISTRATIE_AFGESLOTEN_EXCHANGE, new ExchangeInfo("fanout", true, false));
//        c.declareQueue("/", NOG_TE_GAAN_QUEUE, new QueueInfo(false, true, false));
//        c.declareQueue("/", URENREGISTRATIE_AFGESLOTEN_QUEUE, new QueueInfo(false, true, false));
//        c.bindQueue("/", NOG_TE_GAAN_QUEUE, NOG_TE_GAAN_EXCHANGE, null);
//        c.bindQueue("/", URENREGISTRATIE_AFGESLOTEN_QUEUE, URENREGISTRATIE_AFGESLOTEN_EXCHANGE, null);
//
//        var timeEntriesResponseOngoingTrue = "{\"time_entries\": [\n" +
//                "{\"ongoing\": true}\n" +
//                "]}";
//        var timeEntriesResponseOngoingFalse = "{\"time_entries\": [\n" +
//                "{\"ongoing\": false}\n" +
//                "]}";
//        var weekResponse = "{\n" +
//                "\"report\": {\n" +
//                "\"rows\": [\n" +
//                "{\n\"hours\": 29.8547\n}\n]\n" +
//                "}\n" +
//                "}";
//        var timeEntriesResponseMetTijd = "{\"time_entries\": [\n" +
//                "{\"task_id\": 12345,\n" +
//                "\"start\": \"2022-10-14T06:18:42+02:00\",\n" +
//                "\"end\": \"2022-10-14T11:19:29+02:00\"}" +
//                "]}";
//        var tasksResponse = "{\n" +
//                "\"task\": {\n" +
//                "\"id\": 12345,\n" +
//                "\"name\": \"Mooie Taakomschrijving\"\n" +
//                "}\n" +
//                "}";
//        MockServerClient mockServerClient = new MockServerClient(mockServer.getHost(), mockServer.getServerPort());
//        var expectations = asList(mockServerClient
//                .when(request().withPath("/time-entries"),
//                        Times.exactly(1)
//                )//.withQueryStringParameter("name", "peter"))
//                .respond(response().withBody(timeEntriesResponseOngoingTrue)));
//        var expectations2 = asList(mockServerClient
//                .when(request().withPath("/time-entries"),
//                        Times.exactly(1)
//                )
//                .respond(response().withBody(timeEntriesResponseOngoingFalse)));
//
//        var vandaag = LocalDate.now();
//        var maandag = LocalDate.now();
//        while (maandag.getDayOfWeek() != DayOfWeek.MONDAY) {
//            maandag = maandag.minusDays(1);
//        }
//
//        var expectations3 = asList(mockServerClient
//                .when(request().withPath("/report")
//                                .withQueryStringParameter("row_type", "week")
//                                .withQueryStringParameter("from", maandag.toString())
//                                .withQueryStringParameter("to", vandaag.toString())
//                        ,
//                        Times.exactly(1)
//                )
//                .respond(response().withBody(weekResponse)));
//
//        var expectations4 = asList(mockServerClient
//                .when(request().withPath("/time-entries"),
//                        Times.exactly(1)
//                )
//                .respond(response().withBody(timeEntriesResponseMetTijd)));
//        var expectations5 = asList(mockServerClient
//                .when(request().withPath("/tasks/12345"),
//                        Times.exactly(1)
//                )
//                .respond(response().withBody(tasksResponse)));
//
//        await().atMost(100, SECONDS).until(() -> {
//            var message = c.get("/", NOG_TE_GAAN_QUEUE);
//
//            if (message != null) {
//                assertTrue(message.getPayload().contains("\"urenDezeWeek\":29.8547"));
//                assertTrue(message.getPayload().contains("\"nogTeGaanVoor32\":2.1453"));
//                assertTrue(message.getPayload().contains("\"nogTeGaanVoor36\":6.1453"));
//                assertTrue(message.getPayload().contains("\"nogTeGaanVoor40\":10.1453"));
//
//                expectations.stream()
//                        .map(Expectation::getId)
//                        .forEach(mockServerClient::verify);
//                expectations2.stream()
//                        .map(Expectation::getId)
//                        .forEach(mockServerClient::verify);
//                expectations3.stream()
//                        .map(Expectation::getId)
//                        .forEach(mockServerClient::verify);
//                expectations4.stream()
//                        .map(Expectation::getId)
//                        .forEach(mockServerClient::verify);
//                expectations5.stream()
//                        .map(Expectation::getId)
//                        .forEach(mockServerClient::verify);
//                await().atMost(100, SECONDS).until(() -> {
//                    var message2 = c.get("/", URENREGISTRATIE_AFGESLOTEN_QUEUE);
//
//                    if (message2 != null) {
//                        assertTrue(message2.getPayload().contains("\"startTijd\":[2022,10,14,6,18,42]"));
//                        assertTrue(message2.getPayload().contains("\"eindTijd\":[2022,10,14,11,19,29]"));
//                        assertTrue(message2.getPayload().contains("\"project\":\"Mooie Taakomschrijving\""));
//                        assertTrue(message2.getPayload().contains("\"taakId\":12345"));
//
//                    }
//                    return message2 != null;
//                });
//            }
//            return message != null;
//        });
//    }
//}
