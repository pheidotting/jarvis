package nl.heidotting.jarvis.keepingbackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.ClientParameters;
import com.rabbitmq.http.client.domain.ExchangeInfo;
import com.rabbitmq.http.client.domain.OutboundMessage;
import com.rabbitmq.http.client.domain.QueueInfo;
import lombok.SneakyThrows;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingRequest;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.MatchType;
import org.mockserver.matchers.Times;
import org.mockserver.mock.Expectation;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static nl.heidotting.jarvis.shared.mqtt.Queues.START_STOP_URENREGISTRATIE_RESPONSE_QUEUE;
import static nl.heidotting.jarvis.shared.mqtt.Topics.START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE;
import static nl.heidotting.jarvis.shared.mqtt.Topics.START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.JsonBody.json;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class StartUrenregistratieMetFoutmeldingTest {
    private final String START_URENREGISTRATIE_REQUEST_EXCHANGE = START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE;
    private final String START_URENREGISTRATIE_RESPONSE_EXCHANGE = START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE;
    private final String START_URENREGISTRATIE_RESPONSE_QUEUE = START_STOP_URENREGISTRATIE_RESPONSE_QUEUE + "_JUNIT";

    private static RabbitMQContainer rabbitmq;
    public static final DockerImageName MOCKSERVER_IMAGE = DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion());

    public static MockServerContainer mockServer;

    static {
        rabbitmq = new RabbitMQContainer();
        rabbitmq.start();
        mockServer = new MockServerContainer(MOCKSERVER_IMAGE);
        mockServer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.rabbitmq.host", rabbitmq::getHost);
        registry.add("spring.rabbitmq.port", rabbitmq::getAmqpPort);
        registry.add("spring.rabbitmq.username", rabbitmq::getAdminUsername);
        registry.add("spring.rabbitmq.password", rabbitmq::getAdminPassword);
        registry.add("spring.rabbitmq.ssl.enabled", () -> false);
        registry.add("keeping.base.url", () -> mockServer.getEndpoint());
        registry.add("check.keeping.interval", () -> 5000);
        registry.add("check.keeping.initial.delay", () -> 120000);
    }

    @SneakyThrows
    @Test
    @DisplayName("Test het starten van een nieuwe UrenRegistratie")
    public void testStartUrenRegistratie() {
        Client c = new Client(
                new ClientParameters()
                        .url("http://" + rabbitmq.getHost() + ":" + rabbitmq.getHttpPort() + "/api/")
                        .username(rabbitmq.getAdminUsername())
                        .password(rabbitmq.getAdminPassword())
        );

        c.declareExchange("/", START_URENREGISTRATIE_REQUEST_EXCHANGE, new ExchangeInfo("fanout", true, false));
        c.declareExchange("/", START_URENREGISTRATIE_RESPONSE_EXCHANGE, new ExchangeInfo("fanout", true, false));
        c.declareQueue("/", START_URENREGISTRATIE_RESPONSE_QUEUE, new QueueInfo(false, true, false));
        c.bindQueue("/", START_URENREGISTRATIE_RESPONSE_QUEUE, START_URENREGISTRATIE_RESPONSE_EXCHANGE, null);

        var outboundMessage = new OutboundMessage();
        outboundMessage.payload(new ObjectMapper().findAndRegisterModules()
                .writeValueAsString(
                        new StartStopUrenregistratieKeepingRequest(true)
                )
        );

        var taskResponse = "{\n" +
                "\"tasks\": [\n" +
                "{\n" +
                "\"id\": 205032,\n" +
                "\"name\": \"Justid\",\n" +
                "\"code\": \"J\",\n" +
                "\"direct\": null,\n" +
                "\"state\": \"active\"\n" +
                "},\n" +
                "{\n" +
                "\"id\": 74549,\n" +
                "\"name\": \"Verlof\",\n" +
                "\"code\": \"V\",\n" +
                "\"direct\": null,\n" +
                "\"state\": \"active\"\n" +
                "}\n" +
                "],\n" +
                "\"meta\": {\n" +
                "\"total\": 2,\n" +
                "\"per_page\": 25,\n" +
                "\"current_page\": 1,\n" +
                "\"last_page\": 1\n" +
                "}\n" +
                "}";
        var startRegistratieBody = "{\"task_id\": 205032}";
        var legeTimeEntriesResponse = "{\n" +
                "\"time_entries\": []}";

        MockServerClient mockServerClient = new MockServerClient(mockServer.getHost(), mockServer.getServerPort());

        var expectations = asList(mockServerClient
                .when(request().withPath("/tasks"),
                        Times.exactly(1)
                )
                .respond(response().withBody(taskResponse)));
        var expectations2 = asList(mockServerClient
                .when(request().withPath("/time-entries"),
                        Times.exactly(1)
                )
                .respond(response().withBody(legeTimeEntriesResponse)));
        var expectations3 = asList(mockServerClient
                .when(request().withPath("/time-entries")
                                .withMethod("POST")
                                .withBody(json(startRegistratieBody, MatchType.ONLY_MATCHING_FIELDS)),
                        Times.exactly(1)
                )
                .respond(response()));
        var expectations4 = asList(mockServerClient
                .when(request().withPath("/time-entries"),
                        Times.exactly(1)
                )
                .respond(response().withBody(legeTimeEntriesResponse)));

        c.publish("/", START_URENREGISTRATIE_REQUEST_EXCHANGE, "", outboundMessage);

        await().atMost(100, SECONDS).until(() -> {
            var message = c.get("/", START_URENREGISTRATIE_RESPONSE_QUEUE);
            if (message != null) {
                var startStopUrenregistratieKeepingResponse = new ObjectMapper().findAndRegisterModules().readValue(message.getPayload(), StartStopUrenregistratieKeepingResponse.class);
                System.out.println(startStopUrenregistratieKeepingResponse);
                assertNull(startStopUrenregistratieKeepingResponse.startTijd());
                assertEquals("Starten van UrenRegistratie is niet gelukt, waarschijnlijk was er al 1 actief", startStopUrenregistratieKeepingResponse.melding());

                expectations.stream()
                        .map(Expectation::getId)
                        .forEach(mockServerClient::verify);
                expectations2.stream()
                        .map(Expectation::getId)
                        .forEach(mockServerClient::verify);
                expectations3.stream()
                        .map(Expectation::getId)
                        .forEach(mockServerClient::verify);
                expectations4.stream()
                        .map(Expectation::getId)
                        .forEach(mockServerClient::verify);
            }
            return message != null;
        });
    }
}
