package nl.heidotting.jarvis.keepingbackend.keeping.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.util.concurrent.RateLimiter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Slf4j
public abstract class AbstractService {
    protected abstract String getBaseUrl();

    private RateLimiter rateLimiter = RateLimiter.create(0.5);
    @Autowired
    private RestTemplate restTemplate;

    @SneakyThrows
    protected JSONObject roepKeepingAan(HttpMethod method, Object body, String url) {
        rateLimiter.acquire();
        if (method == HttpMethod.PATCH) {
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            restTemplate.setRequestFactory(requestFactory);
        }
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImJkN2Q4ODllZTJmNGIyZThmYThmMDE2ZDY4ZTI0NGRjMDRiOTU2YThiNzQ5Mzg4NzcwZGEyY2VkMjRhZmE3MDkxMWM0YzlhMzgyZDg2MDZiIn0.eyJhdWQiOiIzIiwianRpIjoiYmQ3ZDg4OWVlMmY0YjJlOGZhOGYwMTZkNjhlMjQ0ZGMwNGI5NTZhOGI3NDkzODg3NzBkYTJjZWQyNGFmYTcwOTExYzRjOWEzODJkODYwNmIiLCJpYXQiOjE2OTI4MDI0MDcsIm5iZiI6MTY5MjgwMjQwNywiZXhwIjoxNzI0NDI0ODA3LCJzdWIiOiI3MDg0Iiwic2NvcGVzIjpbInRpbWUiLCJyZXBvcnRpbmciLCJ0ZWFtIiwicHJvamVjdF9tYW5hZ2VtZW50Il19.sPbr2Xrjswh1FjidLJlTlbSC4Z2q5_edyZYcY6RwQiMWcIYfGHgHQ55PgW0gf5qhtHNH6hyp0tiMQaXamuaCfpCG6L6rJxvdqk0xbopFDGNonhvorYshrGugrirs7fPliNh3iYd8Tfoi4yxHMZHxn5Z_whlUnRfKZCELt76_IWr9eooSsPFdJ26ZbRY8DMlwX7NY8Mpxz4Rh_0gWdvecZ77EHqe2k_dTNGbSt_mGDd6_ytI3Z51n5C8vXfbdlYChKXpwykRqoFoY2yc2gI4pk5n3tY0A_Ptf_4QgY3VJoWwaz2VE5NinxMCurRSRGoDS1SIYE9zJ-n-BNQjRXkrAKet881IFw_T9frL3d6zbrrlSUD2w-QuJoNjqLceo035_coWC_K4VFjsmdpLap99txZjE6HwcLaLMHc5Sn2Si264Ukx1cqXbmiu8wwJgr4gDRT3iLgG8ij_RcJNMsTPViP-qOhf9VuiR-rC0jevXTXytVmBGWInbD-y2bpNdpt5jtS4cK-dtL6-xL5JJIxXYbWH6FGNuWr9qs8uw3rLj8uArjURlaQUJvryTbhsivy3V_hwpIDjea-hX17rP-aL9xWG0HS6rftS9hxzVRRGLpE_oyXGQPkfc7_CCBI6d04-zr6iUvXoqPWcfdPVRvf8eZaKg_HtQTIatM-29D6MYXXYo");

        var entity = new HttpEntity<>(body, headers);
        log.info("URL {} - {}", method, getBaseUrl() + "/" + url);
        if (body != null) {
            log.info(new ObjectMapper().findAndRegisterModules().writeValueAsString(body));
        }
        var response = restTemplate.exchange(getBaseUrl() + "/" + url, method, entity, String.class).getBody();
        if (response != null) {
            return new JSONObject(response);
        } else {
            return null;
        }
    }
}
