package nl.heidotting.jarvis.keepingbackend;

import nl.heidotting.jarvis.shared.services.AbstractStarter;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Configuration
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"nl.heidotting.jarvis"})
public class JarvisKeeping extends AbstractStarter {
    public static void main(String[] args) {
        SpringApplication.run(JarvisKeeping.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
