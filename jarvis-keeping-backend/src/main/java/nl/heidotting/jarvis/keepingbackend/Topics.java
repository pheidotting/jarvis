package nl.heidotting.jarvis.keepingbackend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Topics.*;

@Slf4j
@Configuration
public class Topics {
    @Bean
    public FanoutExchange urenNogTeGaanExchange() {
        log.info("Aanmaken {}", UREN_NOG_TE_GAAN_EXCHANGE);
        return new FanoutExchange(UREN_NOG_TE_GAAN_EXCHANGE);
    }

    @Bean
    public FanoutExchange lopendeUrenregistratieAfgeslotenExchange() {
        log.info("Aanmaken {}", LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE);
        return new FanoutExchange(LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE);
    }

    @Bean
    public FanoutExchange maandAfgeslotenExchange() {
        log.info("Aanmaken {}", MAAND_AFGESLOTEN_EXCHANGE);
        return new FanoutExchange(MAAND_AFGESLOTEN_EXCHANGE);
    }

    @Bean
    public FanoutExchange stuurMaandAfgeslotenExchange() {
        log.info("Aanmaken {}", STUUR_MAAND_AFGESLOTEN_EXCHANGE);
        return new FanoutExchange(STUUR_MAAND_AFGESLOTEN_EXCHANGE);
    }

    @Bean
    public FanoutExchange startStopUrenregistratieRequestExchange() {
        log.info("Aanmaken {}", START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE);
        return new FanoutExchange(START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE);
    }

    @Bean
    public FanoutExchange startstopurenregistratieResponseexchange() {
        log.info("Aanmaken {}", START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE);
        return new FanoutExchange(START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE);
    }
}
