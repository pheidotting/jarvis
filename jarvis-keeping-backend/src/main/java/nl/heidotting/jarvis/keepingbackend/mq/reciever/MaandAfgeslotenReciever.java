package nl.heidotting.jarvis.keepingbackend.mq.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.keepingbackend.keeping.service.MaandAfgeslotenService;
import nl.heidotting.jarvis.shared.mqtt.messages.MaandAfgesloten;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.MAAND_AFGESLOTEN_QUEUE;

@Slf4j
@Service
public class MaandAfgeslotenReciever {
    @Autowired
    private MaandAfgeslotenService maandAfgeslotenService;

    @RabbitListener(queues = "keeping." + MAAND_AFGESLOTEN_QUEUE)
    public void recievedMessage(MaandAfgesloten maandAfgesloten) {
        log.info("maandAfgesloten {}", maandAfgesloten);
        maandAfgeslotenService.verwerkMaandAfsluiting(maandAfgesloten.taakId(), maandAfgesloten.taakOmschrijving(), maandAfgesloten.totaal());
    }
}
