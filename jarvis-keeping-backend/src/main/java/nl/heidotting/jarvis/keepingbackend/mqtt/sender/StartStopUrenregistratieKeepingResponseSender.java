package nl.heidotting.jarvis.keepingbackend.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingResponse;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE;

@Slf4j
@Component
public class StartStopUrenregistratieKeepingResponseSender extends AbstractPublisher<StartStopUrenregistratieKeepingResponse> {
    @Override
    protected String topic() {
        return START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
