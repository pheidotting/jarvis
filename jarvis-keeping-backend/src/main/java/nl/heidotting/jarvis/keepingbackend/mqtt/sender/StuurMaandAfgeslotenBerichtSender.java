package nl.heidotting.jarvis.keepingbackend.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.StuurMaandAfgeslotenBericht;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.STUUR_MAAND_AFGESLOTEN_EXCHANGE;

@Slf4j
@Component
public class StuurMaandAfgeslotenBerichtSender extends AbstractPublisher<StuurMaandAfgeslotenBericht> {
    @Override
    protected String topic() {
        return STUUR_MAAND_AFGESLOTEN_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
