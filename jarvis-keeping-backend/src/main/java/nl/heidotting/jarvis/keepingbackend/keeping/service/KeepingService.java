package nl.heidotting.jarvis.keepingbackend.keeping.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.HashMap;
import java.util.Optional;

import static org.springframework.http.HttpMethod.*;

@Slf4j
@Service
@RequiredArgsConstructor
public class KeepingService extends AbstractService {

    @Value("${keeping.base.url}")
    private String baseUrl;

    @Override
    protected String getBaseUrl() {
        return this.baseUrl;
    }


    public float getUrenVandaag() {
        var vandaag = LocalDate.now();
        var report = roepKeepingAan(GET, null, "report?from=" + vandaag + "&to=" + vandaag + "&row_type=day").getJSONObject("report");
        return report.getFloat("total_hours");
    }

    public Optional<HashMap> leesJongsteUrenRegistratie() {
        return roepKeepingAan(GET, null, "time-entries").getJSONArray("time_entries").toList().stream()
                .map(HashMap.class::cast)
                .sorted((o1, o2) -> LocalDateTime.parse(((String) o2.get("start")).substring(0, 19))
                        .compareTo(LocalDateTime.parse(((String) o1.get("start")).substring(0, 19))))
                .findFirst();
    }

    public Optional<HashMap> leesVroegsteUrenRegistratie() {
        return roepKeepingAan(GET, null, "time-entries").getJSONArray("time_entries").toList().stream()
                .map(HashMap.class::cast)
                .sorted((o1, o2) -> LocalDateTime.parse(((String) o1.get("start")).substring(0, 19))
                        .compareTo(LocalDateTime.parse(((String) o2.get("start")).substring(0, 19))))
                .findFirst();
    }

    private boolean isErEenLopendeRegistratie() {
        return roepKeepingAan(GET, null, "time-entries").getJSONArray("time_entries").toList().stream()
                .map(HashMap.class::cast)
                .map(hashMap -> hashMap.get("ongoing"))
                .map(Boolean.class::cast)
                .anyMatch(aBoolean -> aBoolean);
    }

    public Double haalAlleUrenVoorTaak(Long taakId) {
        var allerEersteDag = LocalDate.of(2022, 11, 1);
        var laatsteDag = YearMonth.now().atEndOfMonth();

        var reportTotaal = roepKeepingAan(GET, null, "report?from=" + allerEersteDag + "&to=" + laatsteDag + "&row_type=month&task_ids[]=" + taakId).getJSONObject("report");
        var rowsTotaal = reportTotaal.getJSONArray("rows");

        var urenTotaal = 0.0;
        for (int i = 0; i < rowsTotaal.length(); i++) {
            var row = rowsTotaal.getJSONObject(i);
            urenTotaal += row.getDouble("hours");
        }

        return urenTotaal;
    }

    public Integer getOpenTaak() {
        return roepKeepingAan(GET, null, "tasks").getJSONArray("tasks")
                .toList().stream()
                .map(HashMap.class::cast)
                .filter(hashMap -> !"verlof".equalsIgnoreCase((String) hashMap.get("name")))
                .map(hashMap -> (Integer) hashMap.get("id"))
                .findFirst().get();
    }

    public void startUrenregistratie(Integer taakId) {
        if (!isErEenLopendeRegistratie()) {
            roepKeepingAan(POST, "{\"task_id\": " + taakId + "}", "time-entries");
        }
    }

    public void stopUrenregistratie(Integer id) {
        roepKeepingAan(PATCH, null, "time-entries/" + id + "/stop");
    }
}
