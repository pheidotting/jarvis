package nl.heidotting.jarvis.keepingbackend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Queues.MAAND_AFGESLOTEN_QUEUE;
import static nl.heidotting.jarvis.shared.mqtt.Queues.START_STOP_URENREGISTRATIE_REQUEST_QUEUE;

@Slf4j
@Configuration
public class Queues {
    @Bean
    public Queue maandAfgeslotenQueue() {
        log.info("Aanmaken keeping.{}", MAAND_AFGESLOTEN_QUEUE);
        return new Queue("keeping." + MAAND_AFGESLOTEN_QUEUE);
    }

    @Bean
    public Queue startStopUrenregistratieRequestQueue() {
        log.info("Aanmaken keeping.{}", START_STOP_URENREGISTRATIE_REQUEST_QUEUE);
        return new Queue("keeping." + START_STOP_URENREGISTRATIE_REQUEST_QUEUE);
    }
}
