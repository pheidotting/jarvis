package nl.heidotting.jarvis.keepingbackend.mq.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.keepingbackend.keeping.service.StartStopUrenregistratieService;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingRequest;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Queues.START_STOP_URENREGISTRATIE_REQUEST_QUEUE;

@Slf4j
@Component
public class StartStopUrenregistratieKeepingReciever {
    @Autowired
    private StartStopUrenregistratieService startStopUrenregistratieService;

    @RabbitListener(queues = "keeping." + START_STOP_URENREGISTRATIE_REQUEST_QUEUE)
    public void recievedMessage(StartStopUrenregistratieKeepingRequest startStopUrenregistratieKeepingRequest) {
        log.info("StartStopUrenregistratieKeeping {}", startStopUrenregistratieKeepingRequest);
        startStopUrenregistratieService.startStop(startStopUrenregistratieKeepingRequest.start());
    }
}
