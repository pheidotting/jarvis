package nl.heidotting.jarvis.keepingbackend.keeping.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.keepingbackend.mqtt.sender.StartStopUrenregistratieKeepingResponseSender;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;

import static java.math.RoundingMode.HALF_UP;

@Slf4j
@Service
public class StartStopUrenregistratieService {
    @Autowired
    private KeepingService keepingService;
    @Autowired
    private StartStopUrenregistratieKeepingResponseSender startStopUrenregistratieKeepingResponseSender;

    public void startStop(boolean start) {
        log.info("{} een Urenregistratie", start ? "Start" : "Stop");
        if (start) {
            start();
        } else {
            stop();
        }
    }

    public void start() {
        var taakId = keepingService.getOpenTaak();
        keepingService.startUrenregistratie(taakId);
        String melding = null;
        String starttijd = null;
        var registratie = keepingService.leesJongsteUrenRegistratie();
        if (registratie.isEmpty()) {
            melding = "Starten van UrenRegistratie is niet gelukt, waarschijnlijk was er al 1 actief";
        }
        if (registratie.isPresent()) {
            starttijd = LocalDateTime.parse(((String) registratie.get().get("start")).substring(0, 19)).toLocalTime().toString();
        }
        startStopUrenregistratieKeepingResponseSender.stuur(new StartStopUrenregistratieKeepingResponse(starttijd, melding));
    }

    public void stop() {
        var laatsteRegistratie = keepingService.leesJongsteUrenRegistratie();
        String melding;
        if (laatsteRegistratie.isEmpty()) {
            melding = "Er is geen registratie aanwezig om te stoppen";
        } else {
            keepingService.stopUrenregistratie((Integer) laatsteRegistratie.get().get("id"));
            laatsteRegistratie = keepingService.leesJongsteUrenRegistratie();
            var registratie = laatsteRegistratie.get();

            var starttijd = LocalDateTime.parse(((String) registratie.get("start")).substring(0, 19)).toLocalTime();
            var eindtijd = LocalDateTime.parse(((String) registratie.get("end")).substring(0, 19)).toLocalTime();

            var duration = Duration.between(starttijd, eindtijd);

            var uur = duration.toHours();
            var minuten = duration.minusHours(uur).toMinutes() + "";

            var tekst = new StringBuilder("De registratie die gestart is om ");
            tekst.append(starttijd);
            tekst.append(" is nu gestopt. Dat waren ");
            if (uur > 0) {
                tekst.append(uur);
                tekst.append(" uur en ");
            }
            tekst.append(minuten);
            tekst.append(" minuten.");

            var vroegsteRegistratie = keepingService.leesVroegsteUrenRegistratie().get();
            if (!vroegsteRegistratie.get("id").equals(laatsteRegistratie.get().get("id"))) {
                var urenVandaag = new BigDecimal(keepingService.getUrenVandaag()).setScale(1, HALF_UP);
                tekst.append(" Vandaag heb je in totaal ");
                tekst.append(urenVandaag);
                tekst.append(" uur gemaakt.");
            }

            melding = tekst.toString();
        }

        startStopUrenregistratieKeepingResponseSender.stuur(new StartStopUrenregistratieKeepingResponse(null, melding));
    }
}
