package nl.heidotting.jarvis.keepingbackend;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class Bindings {
    @Bean
    Binding maandAfgesloten(Queue maandAfgeslotenQueue, FanoutExchange maandAfgeslotenExchange) {
        log.info("Binding tussen {} en {} aanmaken", maandAfgeslotenQueue.getName(), maandAfgeslotenExchange.getName());
        return BindingBuilder.bind(maandAfgeslotenQueue).to(maandAfgeslotenExchange);
    }

    @Bean
    Binding startStopUrenregistratieRequest(Queue startStopUrenregistratieRequestQueue, FanoutExchange startStopUrenregistratieRequestExchange) {
        log.info("Binding tussen {} en {} aanmaken", startStopUrenregistratieRequestQueue.getName(), startStopUrenregistratieRequestExchange.getName());
        return BindingBuilder.bind(startStopUrenregistratieRequestQueue).to(startStopUrenregistratieRequestExchange);
    }
}
