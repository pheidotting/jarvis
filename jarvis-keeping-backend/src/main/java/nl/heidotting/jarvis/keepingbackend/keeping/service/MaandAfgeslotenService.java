package nl.heidotting.jarvis.keepingbackend.keeping.service;

import nl.heidotting.jarvis.keepingbackend.mqtt.sender.StuurMaandAfgeslotenBerichtSender;
import nl.heidotting.jarvis.shared.mqtt.messages.StuurMaandAfgeslotenBericht;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Service
public class MaandAfgeslotenService {
    @Autowired
    private KeepingService keepingService;
    @Autowired
    private StuurMaandAfgeslotenBerichtSender stuurMaandAfgeslotenBerichtSender;

    public void verwerkMaandAfsluiting(Long taakId, String taakOmschrijving, Duration urenMoneybird) {
        var urenKeeping = keepingService.haalAlleUrenVoorTaak(taakId);

        var verschil = urenKeeping - (urenMoneybird.toMinutes() / 60);
        var longMoneybird = (urenMoneybird.toMinutes() / 60) + 0.0;

        stuurMaandAfgeslotenBerichtSender.stuur(new StuurMaandAfgeslotenBericht(urenKeeping,longMoneybird,verschil,taakOmschrijving));

    }
}
