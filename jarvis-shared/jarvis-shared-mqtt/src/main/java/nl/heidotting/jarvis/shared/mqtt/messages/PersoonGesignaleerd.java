package nl.heidotting.jarvis.shared.mqtt.messages;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PersoonGesignaleerd {
    private String naam;
    private String camera;
    @ToString.Exclude
    private byte[] afbeelding;
}
