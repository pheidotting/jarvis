package nl.heidotting.jarvis.shared.mqtt.messages;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@ToString
public class UrenNogTeGaan {
    private float urenDezeWeek;
    private float nogTeGaanVoor32;
    private float nogTeGaanVoor36;
    private float nogTeGaanVoor40;
}
