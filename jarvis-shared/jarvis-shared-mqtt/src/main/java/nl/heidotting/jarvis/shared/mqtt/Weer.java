package nl.heidotting.jarvis.shared.mqtt;

import lombok.Data;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Data
public class Weer {
        private LocalDateTime laatstekeerOpgehaald;
        private String alarm;
        private String verwachting;
        private String temp;
        private String gevoeltemp;
        private String mintemp;
        private String maxtemp;
        private String neerslagkans;
        private String zon;
        private LocalTime zonop;
        private LocalTime zononder;
        private String mintempmorgen;
        private String maxtempmorgen;
        private String neerslagkansmorgen;
        private String zonmorgen;
}
