package nl.heidotting.jarvis.shared.mqtt.messages;

import java.io.Serializable;

public record Notificatie(String tekst) implements Serializable {
}
