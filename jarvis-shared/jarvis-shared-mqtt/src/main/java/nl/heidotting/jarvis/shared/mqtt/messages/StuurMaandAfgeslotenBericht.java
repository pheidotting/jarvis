package nl.heidotting.jarvis.shared.mqtt.messages;

public record StuurMaandAfgeslotenBericht(
        Double urenKeeping,
        Double urenMoneybird,
        Double verschil,
        String taakOmschrijving

) {
}
