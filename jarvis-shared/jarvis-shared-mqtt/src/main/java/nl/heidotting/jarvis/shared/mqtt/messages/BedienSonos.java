package nl.heidotting.jarvis.shared.mqtt.messages;

public record BedienSonos(
        String speaker,
        String commando
) {
}
