package nl.heidotting.jarvis.shared.mqtt.messages;

public record VolumeGewijzigdDoorZwarteLijstLied(
        int volume,
        String artist,
        String title
) {
}
