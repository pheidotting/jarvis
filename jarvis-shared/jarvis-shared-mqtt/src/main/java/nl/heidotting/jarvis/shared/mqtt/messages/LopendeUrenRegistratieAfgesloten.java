package nl.heidotting.jarvis.shared.mqtt.messages;

import java.time.LocalDateTime;

public record LopendeUrenRegistratieAfgesloten(LocalDateTime startTijd, LocalDateTime eindTijd, String project,Long taakId) {
}
