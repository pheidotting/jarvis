package nl.heidotting.jarvis.shared.mqtt.messages;

import java.time.Month;

public record OpvragenUrensheet(
        Month maand
) {
}
