package nl.heidotting.jarvis.shared.mqtt;

public class Queues {
    public static final String JARVIS_CAMERA_UPLOAD_MOVIE_QUEUE = "jarvis.camera.upload.movie.queue";
    public static final String JARVIS_CAMERA_UPLOAD_PICTURE_QUEUE = "jarvis.camera.upload.picture.queue";
    public static final String PERSOON_GESIGNALEERD_QUEUE = "jarvis.persoon.gesignaleerd.queue";
    public static final String UREN_NOG_TE_GAAN_QUEUE = "jarvis.uren.nog.te.gaan.queue";
    public static final String LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE = "jarvis.lopende.uren.registratie.afgesloten.queue";

    public static final String VERWERK_GEZICHT_QUEUE = "jarvis.verwerk.gezicht.queue";
    public static final String GEZICHT_VERWERKT_QUEUE = "jarvis.gezicht.verwerkt.queue";

    public static final String WEER_QUEUE = "jarvis.weer.queue";
    public static final String OPVRAGEN_WEER_QUEUE = "jarvis.opvragen.weer.queue";

    public static final String ZON_OP_QUEUE = "jarvis.zon.op.queue";
    public static final String ZON_ONDER_QUEUE = "jarvis.zon.onder.queue";

    public static final String MAAND_AFGESLOTEN_QUEUE = "jarvis.maand.afgesloten.queue";
    public static final String STUUR_MAAND_AFGESLOTEN_QUEUE = "jarvis.stuur.maand.afgesloten.queue";

    public static final String OPVRAGEN_URENSHEET_QUEUE = "jarvis.opvragen.urensheet.queue";
    public static final String OPVRAGEN_URENSHEET_RESPONSE_QUEUE = "jarvis.opvragen.urensheet.response.queue";

    public static final String START_STOP_URENREGISTRATIE_REQUEST_QUEUE = "jarvis.start.stop.urenregistratie.request.queue";
    public static final String START_STOP_URENREGISTRATIE_RESPONSE_QUEUE = "jarvis.start.stop.urenregistratie.response.queue";

    public static final String VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_QUEUE = "jarvis.volume.gewijzigd.door.zwarte.lijst.lied.queue";

    public static final String BEDIEN_SONOS_QUEUE = "jarvis.bedien.sonos.queue";

    public static final String LAMPEN_LAMPEN_AAN_QUEUE = "jarvis.lampen.lampen.aan.queue";
    public static final String LAMPEN_LAMPEN_UIT_QUEUE = "jarvis.lampen.lampen.uit.queue";
    public static final String LAMPEN_LAMPEN_UIT_MET_TIMEOUT_QUEUE = "jarvis.lampen.lampen.uit.met.timeout.queue";
    public static final String LAMPEN_LAMPEN_UIT_MET_LANGE_TIMEOUT_QUEUE = "jarvis.lampen.lampen.uit.met.lange.timeout.queue";

    public static final String ACCUPERCENTAGE_GEZAKT_QUEUE = "accupercentage.gezakt.queue";

    public static final String STOFZUIGEN_QUEUE = "stofzuigen.queue";
    public static final String ZIGBEE_MOTION_DETECTION_QUEUE = "zigbee.motion.detection.queue";
    public static final String ZIGBEE_OPEN_DICHT_DETECTION_QUEUE = "zigbee.open.dicht.detection.queue";
    public static final String SENSOR_OPEN_OF_DICHT_QUEUE = "zigbee.open.dicht.detection.queue";
    public static final String NOTIFICATIE_QUEUE = "jarvis.notificatie.queue";
    public static final String LICHT_WIJZIGING_OF_BEWEGING_GEDETECTEERD_QUEUE = "jarvis.licht.wijziging.of.beweging.gedetecteerd.queue";
    public static final String JARVIS_LAMPEN_IKEA_LOGEERKAMER_STATUS = "jarvis.lampen.ikea.logeerkamer.status.queue";

    public static final String LAMP_AAN_UIT_GESCHAKELD_QUEUE = "jarvis.lamp.aan.uit.geschakeld.queue";

    public static final String JARVIS_SMARTMETER_ENERGY = "smartmeter.energy";
    public static final String JARVIS_SMARTMETER_GAS = "smartmeter.gas";
    public static final String JARVIS_SMARTMETER_USAGE = "smartmeter.usage";
}
