package nl.heidotting.jarvis.shared.mqtt;

public class Topics {
    public static String JARVIS_CAMERA_UPLOAD_MOVIE_EXCHANGE= "jarvis.camera.upload.movie.exchange";
    public static String JARVIS_CAMERA_UPLOAD_PICTURE_EXCHANGE = "jarvis.camera.upload.picture.exchange";

    public static String PERSOON_GESIGNALEERD_EXCHANGE = "jarvis.persoon.gesignaleerd.exchange";

    public static String UREN_NOG_TE_GAAN_EXCHANGE = "keeping.uren.nog.te.gaan";
    public static String LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE = "keeping.lopende.urenregistratie.afgesloten";

    public static String VERWERK_GEZICHT_EXCHANGE = "jarvis.verwerk.gezicht.exchange";
    public static String GEZICHT_VERWERKT_EXCHANGE = "jarvis.gezicht.verwerkt.exchange";

    public static final String WEER_EXCHANGE = "jarvis.weer.exchange";
    public static final String OPVRAGEN_WEER_EXCHANGE = "jarvis.opvragen.weer.exchange";

    public static final String ZON_OP_EXCHANGE = "jarvis.zon.op.exchange";
    public static final String ZON_ONDER_EXCHANGE = "jarvis.zon.onder.exchange";

    public static final String MAAND_AFGESLOTEN_EXCHANGE = "jarvis.maand.afgesloten.exchange";
    public static final String STUUR_MAAND_AFGESLOTEN_EXCHANGE = "jarvis.stuur.maand.afgesloten.exchange";

    public static final String OPVRAGEN_URENSHEET_EXCHANGE = "jarvis.opvragen.urensheet.exchange";
    public static final String OPVRAGEN_URENSHEET_RESPONSE_EXCHANGE = "jarvis.opvragen.urensheet.response.exchange";

    public static final String START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE = "jarvis.start.stop.urenregistratie.request.exchange";
    public static final String START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE = "jarvis.start.stop.urenregistratie.response.exchange";

    public static final String VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_EXCHANGE = "jarvis.volume.gewijzigd.door.zwarte.lijst.lied.exchange";

    public static final String BEDIEN_SONOS_EXCHANGE = "jarvis.bedien.sonos.exchange";

    public static final String LAMPEN_LAMPEN_AAN_EXCHANGE = "jarvis.lampen.lampen.aan.exchange";
    public static final String LAMP_AAN_UIT_GESCHAKELD_EXCHANGE = "lamp.aan.of.uit.geschakeld";
    public static final String LAMPEN_LAMPEN_UIT_EXCHANGE = "jarvis.lampen.lampen.uit.exchange";
    public static final String LAMPEN_LAMPEN_UIT_MET_TIMEOUT_EXCHANGE = "jarvis.lampen.lampen.uit.met.timeout.exchange";
    public static final String LAMPEN_LAMPEN_UIT_MET_LANGE_TIMEOUT_EXCHANGE = "jarvis.lampen.lampen.uit.met.lange.timeout.exchange";

    public static final String ACCUPERCENTAGE_GEZAKT_EXCHANGE = "jarvis.accupercentage.gezakt.exchange";

    public static final String STOFZUIGEN_EXCHANGE = "stofzuigen.exchange";
    public static String LICHT_WIJZIGING_OF_BEWEGING_GEDETECTEERD_EXCHANGE = "jarvis.licht.wijziging.of.beweging.gedetecteerd.exchange";
    public static String SENSOR_OPEN_OF_DICHT_EXCHANGE = "jarvis.sensor.open.of.dicht.exchange";
    public static String NOTIFICATIE_EXCHANGE = "jarvis.notificatie.exchange";

    public static final String LAMP_LOGEERKAMER_MUTATIE_EXCHANGE = "lamp.logeerkamer.mutatie.exchange";

    public static final String LAMP_VOORDEUR_MUTATIE_EXCHANGE = "lamp.voordeur.mutatie.exchange";

    public static final String BIJ_DE_BANK_MUTATIE_EXCHANGE = "bij.de.bank.mutatie.exchange";
    public static final String BIJ_DE_PUI_MUTATIE_EXCHANGE = "bij.de.pui.mutatie.exchange";
    public static final String BIJKEUKEN_LAMPEN_MUTATIE_EXCHANGE = "bijkeuken.lamp.mutatie.exchange";
    public static final String BIJKEUKEN_BUITEN_LAMPEN_MUTATIE_EXCHANGE = "bijkeuken.buiten.lamp.mutatie.exchange";
    public static final String EETKAMER_MUTATIE_EXCHANGE = "eetkamer.mutatie.exchange";
    public static final String NAAST_DE_TV_MUTATIE_EXCHANGE = "naast.de.tv.mutatie.exchange";
}
