package nl.heidotting.jarvis.shared.mqtt.messages;

import lombok.*;

public record KeepingUpdate(
        long dag,
        long week,
        boolean lopend,
        long taskId) {
}
