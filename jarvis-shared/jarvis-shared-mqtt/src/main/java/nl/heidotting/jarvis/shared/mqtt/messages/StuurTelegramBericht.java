package nl.heidotting.jarvis.shared.mqtt.messages;

public record StuurTelegramBericht(
        String tekst,
        byte[] bestand) {
}
