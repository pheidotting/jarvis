package nl.heidotting.jarvis.shared.mqtt.messages;

public record GezichtsherkenningAanUit(
        boolean aanOfUit
) {
}
