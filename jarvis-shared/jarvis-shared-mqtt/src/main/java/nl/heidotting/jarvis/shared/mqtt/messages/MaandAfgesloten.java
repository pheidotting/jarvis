package nl.heidotting.jarvis.shared.mqtt.messages;

import java.time.Duration;

public record MaandAfgesloten(
        Duration dezeMaand,
        Duration totaal,
        String taakOmschrijving,
        Long taakId
) {
}
