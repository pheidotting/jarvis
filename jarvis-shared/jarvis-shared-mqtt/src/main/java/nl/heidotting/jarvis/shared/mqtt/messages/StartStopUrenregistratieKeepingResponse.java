package nl.heidotting.jarvis.shared.mqtt.messages;

public record StartStopUrenregistratieKeepingResponse(
        String startTijd,
        String melding
) {
}
