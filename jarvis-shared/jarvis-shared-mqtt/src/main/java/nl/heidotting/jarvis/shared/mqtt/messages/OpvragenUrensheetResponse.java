package nl.heidotting.jarvis.shared.mqtt.messages;

public record OpvragenUrensheetResponse(
        String projectId,
        byte[] fileData
) {
}
