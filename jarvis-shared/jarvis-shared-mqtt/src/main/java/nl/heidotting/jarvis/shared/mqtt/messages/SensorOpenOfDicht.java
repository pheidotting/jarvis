package nl.heidotting.jarvis.shared.mqtt.messages;

import java.io.Serializable;

public record SensorOpenOfDicht(String id) implements Serializable {
}
