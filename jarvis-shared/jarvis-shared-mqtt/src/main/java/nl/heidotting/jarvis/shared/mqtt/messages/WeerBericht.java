package nl.heidotting.jarvis.shared.mqtt.messages;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import nl.heidotting.jarvis.shared.mqtt.Weer;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class WeerBericht {
    private Weer Weer;
    private boolean opVerzoek;
}
