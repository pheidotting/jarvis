package nl.heidotting.jarvis.shared.mqtt.messages;

import java.io.Serializable;

public record LichtWijzigingOfBewegingGedetecteerd() implements Serializable {
}
