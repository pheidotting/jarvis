//package nl.heidotting.jarvis.shared.mqtt;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.rabbit.core.RabbitTemplate;
//import org.springframework.beans.factory.annotation.Autowired;
//
//@Slf4j
//public abstract class AbstractPublisher<T> {
//    @Autowired
//    protected RabbitTemplate rabbitTemplate;
//
//    protected abstract String topic();
//
//    protected abstract String routingKey();
//
//    public void stuur(T t){
//        log.info("Topic : {}\nBericht : {}",topic(),t);
//        rabbitTemplate.convertAndSend(topic(), routingKey(), t);
//    }
//}
