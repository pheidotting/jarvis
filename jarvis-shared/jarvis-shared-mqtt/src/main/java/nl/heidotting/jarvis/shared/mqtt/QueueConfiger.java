//package nl.heidotting.jarvis.shared.mqtt;
//
//import lombok.extern.slf4j.Slf4j;
//import org.slf4j.Logger;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.FanoutExchange;
//import org.springframework.amqp.core.Queue;
//
//import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;
//
//@Slf4j
//public class QueueConfiger {
//    private String queueNaam;
//    private String exchangeNaam;
//    private String routingKey;
//    private Logger logger;
//    private boolean metBinding;
//    private Queue queue = null;
//    private FanoutExchange fanoutExchange = null;
//    private Binding binding = null;
//
//    public QueueConfiger withLogger(Logger logger) {
//        this.logger = logger;
//        return this;
//    }
//
//    public QueueConfiger withQueue(String queueNaam) {
//        this.queueNaam = queueNaam;
//        return this;
//    }
//
//    public QueueConfiger withFanoutExchange(String exchangeNaam) {
//        this.exchangeNaam = exchangeNaam;
//        return this;
//    }
//
//    public QueueConfiger withRoutingKey(String routingKey) {
//        this.routingKey = routingKey;
//        return this;
//    }
//
//    public QueueConfiger withBinding() {
//        this.metBinding = true;
//        return this;
//    }
//
//    public QueueConfiger build() {
//        if (this.exchangeNaam == null) {
//            this.exchangeNaam = "amq.topic";
//        }
//        if (logger == null) {
//            logger = log;
//        }
//        if (this.queueNaam != null) {
//            logger.info("Queue {} aanmaken", queueNaam);
//            this.queue = new Queue(queueNaam);
//        }
//        if (!"amq.topic".equals(exchangeNaam)) {
//            logger.info("FanoutExchange {} aanmaken", this.exchangeNaam);
//            this.fanoutExchange = new FanoutExchange(this.exchangeNaam);
//        }
//        if (this.metBinding) {
//            if ("".equals(this.routingKey)) {
//                logger.info("Binding aanmaken tussen {} en {}", this.exchangeNaam, this.queueNaam);
//            } else {
//                logger.info("Binding aanmaken tussen {} en {} met routingkey {}", this.exchangeNaam, this.queueNaam, this.routingKey);
//            }
//            this.binding = new Binding(this.queueNaam, QUEUE, this.exchangeNaam, this.routingKey, null);
//        }
//        return this;
//    }
//
//    public Queue getQueue() {
//        return this.queue;
//    }
//
//    public FanoutExchange getFanoutExchange() {
//        return this.fanoutExchange;
//    }
//
//    public Binding getBinding() {
//        return this.binding;
//    }
//}
