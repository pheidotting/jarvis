//package nl.heidotting.jarvis.shared.mqtt;
//
//import lombok.extern.slf4j.Slf4j;
//import org.springframework.amqp.core.Binding;
//import org.springframework.amqp.core.FanoutExchange;
//import org.springframework.amqp.core.Queue;
//
//@Slf4j
//public abstract class AbstractQueueConfig {
//    protected Queue log(Queue queue) {
//        log.info("Queue {} aanmaken", queue.getName());
//        return queue;
//    }
//
//    protected FanoutExchange log(FanoutExchange fanoutExchange) {
//        log.info("FanoutExchange {} aanmaken", fanoutExchange.getName());
//        return fanoutExchange;
//    }
//
//    protected Binding log(Binding binding) {
//        if ("".equals(binding.getRoutingKey())) {
//            log.info("Binding aanmaken tussen {} en {}", binding.getExchange(), binding.getDestination());
//        } else {
//            log.info("Binding aanmaken tussen {} en {} met routingkey {}", binding.getExchange(), binding.getDestination(), binding.getRoutingKey());
//        }
//        return binding;
//    }
//}
