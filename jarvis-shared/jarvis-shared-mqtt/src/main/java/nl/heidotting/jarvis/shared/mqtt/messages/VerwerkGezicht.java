package nl.heidotting.jarvis.shared.mqtt.messages;

public record VerwerkGezicht (String naam, byte[] foto){
}
