package nl.heidotting.jarvis.shared.mqtt.messages;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class IncomingFileEvent {
    private String username;
    private String filename;
    @ToString.Exclude
    private byte[] fileData;
}
