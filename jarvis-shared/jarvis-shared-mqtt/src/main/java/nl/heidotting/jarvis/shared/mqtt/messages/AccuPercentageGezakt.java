package nl.heidotting.jarvis.shared.mqtt.messages;

public record AccuPercentageGezakt(
        Integer batPct,
        Integer percentage,
        String name,
        String cycle,
        String phase) {
}
