package nl.heidotting.jarvis.shared.mqtt.messages;

public record StartStopUrenregistratieKeepingRequest(
        boolean start
) {
}
