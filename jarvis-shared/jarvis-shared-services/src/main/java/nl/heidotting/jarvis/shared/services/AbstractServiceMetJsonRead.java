package nl.heidotting.jarvis.shared.services;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public abstract class AbstractServiceMetJsonRead {
    private String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }

    protected JSONObject readJsonFromUrl(String url, String method) throws IOException, JSONException {
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod(method == null ? "GET" : method);
        connection.setDoOutput(true);
        connection.setDoInput(true);

        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONObject json;
            try {
                json = new JSONObject(jsonText);
            } catch (JSONException e) {
                json = new JSONObject(jsonText.substring(1, jsonText.length() - 2));
            }
            return json;
        } finally {
            connection.getInputStream().close();
        }
    }
}
