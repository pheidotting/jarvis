package nl.heidotting.jarvis.shared.services;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.Weer;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Slf4j
@Service
public class WeerService extends AbstractServiceMetJsonRead {
    private LocalDateTime laatstekeerOpgehaald;
    private Weer weer;

    public Weer getWeer() {
        if (weer == null) {
            weer = ophalenWeergegevens();
        }
        return weer;
    }

    public Weer ophalenWeergegevens() {
        try {
            LocalDateTime tweeUurGeleden = LocalDateTime.now().minusHours(2);
            if (laatstekeerOpgehaald == null || laatstekeerOpgehaald.isBefore(tweeUurGeleden)) {
                weer = new Weer();
                JSONObject hoofd = readJsonFromUrl("http://weerlive.nl/api/json-data-10min.php?key=39e08ad01d&locatie=Klazienaveen", null);
                JSONObject jsonWeer = hoofd.getJSONArray("liveweer").getJSONObject(0);

                if (jsonWeer.getString("alarm") != null && "1".equals(jsonWeer.getString("alarm"))) {
                    weer.setAlarm(jsonWeer.getString("alarmtxt"));
                }
                weer.setVerwachting(jsonWeer.getString("verw").replace("(", "").replace(")", ""));
                weer.setTemp(jsonWeer.getString("temp").replace(".", ","));
                weer.setGevoeltemp(jsonWeer.getString("gtemp").replace(".", ","));
                weer.setMintemp(jsonWeer.getString("d0tmin").replace(".", ","));
                weer.setMaxtemp(jsonWeer.getString("d0tmax").replace(".", ","));
                weer.setNeerslagkans(jsonWeer.getString("d0neerslag"));
                weer.setZon(jsonWeer.getString("d0zon"));
                weer.setZonop(LocalTime.parse(jsonWeer.getString("sup")));
                weer.setZononder(LocalTime.parse(jsonWeer.getString("sunder")));
                weer.setMintempmorgen(jsonWeer.getString("d1tmin").replace(".", ","));
                weer.setMaxtempmorgen(jsonWeer.getString("d1tmax").replace(".", ","));
                weer.setNeerslagkansmorgen(jsonWeer.getString("d1neerslag"));
                weer.setZonmorgen(jsonWeer.getString("d1zon"));

                laatstekeerOpgehaald = LocalDateTime.now();
            }

//            tekst.append("De weersverwachting voor vandaag : ");
//            tekst.append(verwachting);
//            tekst.append(". Op dit moment is het ");
//            tekst.append(temp);
//            tekst.append(" graden buiten");
//            if (!temp.equals(gevoeltemp)) {
//                tekst.append(", met een gevoelstemperatuur van ");
//                tekst.append(gevoeltemp);
//            }
//            tekst.append(". Het wordt vandaag minimaal ");
//            tekst.append(mintemp);
//            tekst.append(" graden en maximaal ");
//            tekst.append(maxtemp);
//            tekst.append(" graden. De kans op regen is vandaag ");
//            tekst.append(neerslagkans);
//            tekst.append(" procent en de kans op zon is ");
//            tekst.append(zon);
//            tekst.append(" procent. Morgen verwachten we een minimumtemperatuur van ");
//            tekst.append(mintempmorgen);
//            tekst.append(" graden en maximaal ");
//            tekst.append(maxtempmorgen);
//            tekst.append(" graden, de neerslagkans voor morgen is ");
//            tekst.append(neerslagkansmorgen);
//            tekst.append(" procent en de zonnekans is ");
//            tekst.append(zonmorgen);
//            tekst.append(" procent. ");
//            if (alarm != null) {
//                tekst.append("Wel geldt er voor vandaag een weersalarm als volgt : ");
//                tekst.append(alarm);
//            }
        } catch (Exception e) {
            log.error("Geen weer vandaag {}", e.getMessage());
        }
//        return tekst.toString();
        return weer;
    }

//    public LocalTime getZonop() {
//        if (zonop == null) {
//            this.ophalenWeergegevens();
//        }
//        return zonop;
//    }
//
//    public LocalTime getZononder() {
//        if (zononder == null) {
//            this.ophalenWeergegevens();
//        }
//        return zononder;
//    }
}
