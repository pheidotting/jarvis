package nl.heidotting.jarvis.telegram.service;

import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenAan;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUit;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUitMetLangeTimeout;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUitMetTimeout;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenAanSender;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenUitMetLangeTimeoutSender;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenUitMetTimeoutSender;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenUitSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VerwerkLampenCommandoService {
    @Autowired
    private LampenAanSender lampenAanSender;
    @Autowired
    private LampenUitSender lampenUitSender;
    @Autowired
    private LampenUitMetTimeoutSender lampenUitMetTimeoutSender;
    @Autowired
    private LampenUitMetLangeTimeoutSender lampenUitMetLangeTimeoutSender;

    public void verwerk(String commando) {
        if ("lampen aan".equalsIgnoreCase(commando)) {
            lampenAanSender.stuur(new AlleLampenAan());
        } else if ("lampen uit".equalsIgnoreCase(commando)) {
            lampenUitSender.stuur(new AlleLampenUit());
        } else if ("lampen uit met timeout".equalsIgnoreCase(commando)) {
            lampenUitMetTimeoutSender.stuur(new AlleLampenUitMetTimeout());
        } else if ("lampen uit met lange timeout".equalsIgnoreCase(commando)) {
            lampenUitMetLangeTimeoutSender.stuur(new AlleLampenUitMetLangeTimeout());
        }
    }
}
