package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.AccuPercentageGezakt;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.ACCUPERCENTAGE_GEZAKT_QUEUE;

@Slf4j
@Service
public class AccuPercentageGezaktReciever {
    @Autowired
    private TelegramService telegramService;

    @RabbitListener(queues = "telegram." + ACCUPERCENTAGE_GEZAKT_QUEUE)
    public void recievedMessage(AccuPercentageGezakt accuPercentageGezakt) {
        var tekst = new StringBuilder("Het accupercentage van ");
        tekst.append(accuPercentageGezakt.name());
        tekst.append(" is gezakt, huidige cycle: ");
        tekst.append(accuPercentageGezakt.cycle());
        tekst.append(", huidige phase ");
        tekst.append(accuPercentageGezakt.phase());
        tekst.append(", percentage was ");
        tekst.append(accuPercentageGezakt.percentage());
        tekst.append(" en is nu ");
        tekst.append(accuPercentageGezakt.batPct());

        telegramService.sendMessageToChannel(tekst.toString());
    }
}
