package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.GezichtVerwerkt;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.GEZICHT_VERWERKT_QUEUE;

@Slf4j
@Service
public class GezichtVerwerktReciever {
    @Autowired
    private TelegramService telegramService;

    @RabbitListener(queues = "telegram." + GEZICHT_VERWERKT_QUEUE)
    public void recievedMessage(GezichtVerwerkt gezichtVerwerkt) {
        telegramService.sendMessageToChannel(gezichtVerwerkt.resultaat()==null?"Geen antwoord": gezichtVerwerkt.resultaat());
    }
}

