package nl.heidotting.jarvis.telegram.service;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static java.math.RoundingMode.HALF_UP;

@Service
public class UrenNogTeGaanService {
    @Autowired
    private TelegramService telegramService;

    @SneakyThrows
    public void verwerkUrenNogTeGaan(float urenDezeWeek, float urenNogTeGaanVoor32, float urenNogTeGaanVoor36, float urenNogTeGaanVoor40) {
        var dezeWeek = new BigDecimal(urenDezeWeek).setScale(1, HALF_UP);
        var nogTeGaanVoor32 = new BigDecimal(urenNogTeGaanVoor32).setScale(1, HALF_UP);
        var nogTeGaanVoor36 = new BigDecimal(urenNogTeGaanVoor36).setScale(1, HALF_UP);
        var nogTeGaanVoor40 = new BigDecimal(urenNogTeGaanVoor40).setScale(1, HALF_UP);

        var tekst = new StringBuilder("Deze week heb je ");
        tekst.append(dezeWeek);
        tekst.append(" uur gemaakt, nog te gaan:");
        tekst.append("\n");
        tekst.append("Voor 32 uur: ");
        tekst.append(nogTeGaanVoor32);
        tekst.append("\n");
        tekst.append("Voor 36 uur: ");
        tekst.append(nogTeGaanVoor36);
        tekst.append("\n");
        tekst.append("Voor 40 uur: ");
        tekst.append(nogTeGaanVoor40);

        telegramService.sendMessageToChannel(tekst.toString());
    }
}
