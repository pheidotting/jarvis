package nl.heidotting.jarvis.telegram.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.Stofzuigen;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.STOFZUIGEN_EXCHANGE;

@Slf4j
@Component
public class StofzuigenSender extends AbstractPublisher<Stofzuigen> {
    @Override
    protected String topic() {
        return STOFZUIGEN_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
