package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.UrenNogTeGaan;
import nl.heidotting.jarvis.telegram.service.UrenNogTeGaanService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.UREN_NOG_TE_GAAN_QUEUE;

@Slf4j
@Service
public class UrenNogTeGaanReciever {
    @Autowired
    private UrenNogTeGaanService urenNogTeGaanService;

    @RabbitListener(queues = "telegram." + UREN_NOG_TE_GAAN_QUEUE)
    public void recievedMessage(UrenNogTeGaan urenNogTeGaan) {
        urenNogTeGaanService.verwerkUrenNogTeGaan(
                urenNogTeGaan.getUrenDezeWeek(),
                urenNogTeGaan.getNogTeGaanVoor32(),
                urenNogTeGaan.getNogTeGaanVoor36(),
                urenNogTeGaan.getNogTeGaanVoor40()
        );
    }
}

