package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.StuurMaandAfgeslotenBericht;
import nl.heidotting.jarvis.telegram.service.StuurMaandAfgeslotenBerichtService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.STUUR_MAAND_AFGESLOTEN_QUEUE;

@Slf4j
@Service
public class StuurMaandAfgeslotenBerichtReciever {
    @Autowired
    private StuurMaandAfgeslotenBerichtService stuurMaandAfgeslotenBerichtService;

    @RabbitListener(queues = "telegram." + STUUR_MAAND_AFGESLOTEN_QUEUE)
    public void recievedMessage(StuurMaandAfgeslotenBericht stuurMaandAfgeslotenBericht) {
        stuurMaandAfgeslotenBerichtService.verwerk(
                stuurMaandAfgeslotenBericht.urenKeeping(),
                stuurMaandAfgeslotenBericht.urenMoneybird(),
                stuurMaandAfgeslotenBericht.verschil(),
                stuurMaandAfgeslotenBericht.taakOmschrijving()
        );
    }
}

