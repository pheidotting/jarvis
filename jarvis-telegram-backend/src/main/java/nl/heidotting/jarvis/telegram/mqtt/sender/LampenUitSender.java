package nl.heidotting.jarvis.telegram.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUit;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.LAMPEN_LAMPEN_UIT_EXCHANGE;

@Slf4j
@Component
public class LampenUitSender extends AbstractPublisher<AlleLampenUit> {
    @Override
    protected String topic() {
        return LAMPEN_LAMPEN_UIT_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
