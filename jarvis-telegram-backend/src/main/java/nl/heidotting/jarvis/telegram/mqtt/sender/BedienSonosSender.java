package nl.heidotting.jarvis.telegram.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.BedienSonos;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.BEDIEN_SONOS_EXCHANGE;

@Slf4j
@Component
public class BedienSonosSender extends AbstractPublisher<BedienSonos> {
    @Override
    protected String topic() {
        return BEDIEN_SONOS_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
