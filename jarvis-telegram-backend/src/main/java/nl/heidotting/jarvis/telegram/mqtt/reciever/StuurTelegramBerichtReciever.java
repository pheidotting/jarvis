//package nl.heidotting.jarvis.telegram.mqtt.reciever;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import lombok.extern.slf4j.Slf4j;
//import nl.heidotting.jarvis.shared.mqtt.AbstractReciever;
//import nl.heidotting.jarvis.shared.mqtt.messages.StuurTelegramBericht;
//import nl.heidotting.jarvis.telegram.service.StuurTelegramBerichtService;
//import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
//import org.eclipse.paho.client.mqttv3.MqttMessage;
//import org.slf4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Slf4j
//@Service
//public class StuurTelegramBerichtReciever extends AbstractReciever<StuurTelegramBericht> {
//    @Autowired
//    private StuurTelegramBerichtService stuurTelegramBerichtService;
//
//    @Override
//    public String topic() {
//        return "jarvis/telegram/stuurbericht";
//    }
//
//    @Override
//    protected void verwerk(StuurTelegramBericht stuurTelegramBericht) {
//        stuurTelegramBerichtService.stuurTelegramBericht(stuurTelegramBericht.tekst(), stuurTelegramBericht.bestand());
//    }
//
//    @Override
//    protected Class<StuurTelegramBericht> clazz() {
//        return StuurTelegramBericht.class;
//    }
//
//    @Override
//    protected Logger getLogger() {
//        return log;
//    }
//}
//
