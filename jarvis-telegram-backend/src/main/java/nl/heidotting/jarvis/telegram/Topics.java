package nl.heidotting.jarvis.telegram;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Topics.*;

@Slf4j
@Configuration
public class Topics {
    @Bean
    public FanoutExchange jarvisCameraUploadPictureExchange() {
        log.info("Aanmaken {}", JARVIS_CAMERA_UPLOAD_PICTURE_EXCHANGE);
        return new FanoutExchange(JARVIS_CAMERA_UPLOAD_PICTURE_EXCHANGE);
    }

    @Bean
    public FanoutExchange persoonGesignaleerdExchange() {
        log.info("Aanmaken {}", PERSOON_GESIGNALEERD_EXCHANGE);
        return new FanoutExchange(PERSOON_GESIGNALEERD_EXCHANGE);
    }

    @Bean
    public FanoutExchange urenNogTeGaanExchange() {
        log.info("Aanmaken {}", UREN_NOG_TE_GAAN_EXCHANGE);
        return new FanoutExchange(UREN_NOG_TE_GAAN_EXCHANGE);
    }

    @Bean
    public FanoutExchange verwerkGezichtExchange() {
        log.info("Aanmaken {}", VERWERK_GEZICHT_EXCHANGE);
        return new FanoutExchange(VERWERK_GEZICHT_EXCHANGE);
    }

    @Bean
    public FanoutExchange gezichtVerwerktExchange() {
        log.info("Aanmaken {}", GEZICHT_VERWERKT_EXCHANGE);
        return new FanoutExchange(GEZICHT_VERWERKT_EXCHANGE);
    }

    @Bean
    public FanoutExchange stuurMaandAfgeslotenExchange() {
        log.info("Aanmaken {}", STUUR_MAAND_AFGESLOTEN_EXCHANGE);
        return new FanoutExchange(STUUR_MAAND_AFGESLOTEN_EXCHANGE);
    }

    @Bean
    public FanoutExchange opvragenUrensheetExchange() {
        log.info("Aanmaken {}", OPVRAGEN_URENSHEET_EXCHANGE);
        return new FanoutExchange(OPVRAGEN_URENSHEET_EXCHANGE);
    }

    @Bean
    public FanoutExchange opvragenUrensheetResponseExchange() {
        log.info("Aanmaken {}", OPVRAGEN_URENSHEET_RESPONSE_EXCHANGE);
        return new FanoutExchange(OPVRAGEN_URENSHEET_RESPONSE_EXCHANGE);
    }

    @Bean
    public FanoutExchange startStopUrenregistratieRequestExchange() {
        log.info("Aanmaken {}", START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE);
        return new FanoutExchange(START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE);
    }

    @Bean
    public FanoutExchange startStopUrenregistratieResponseExchange() {
        log.info("Aanmaken {}", START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE);
        return new FanoutExchange(START_STOP_URENREGISTRATIE_RESPONSE_EXCHANGE);
    }

    @Bean
    public FanoutExchange volumeGewijzigdDoorZwarteLijstLiedExchange() {
        log.info("Aanmaken {}", VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_EXCHANGE);
        return new FanoutExchange(VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_EXCHANGE);
    }

    @Bean
    public FanoutExchange bedienSonosExchange() {
        log.info("Aanmaken {}", BEDIEN_SONOS_EXCHANGE);
        return new FanoutExchange(BEDIEN_SONOS_EXCHANGE);
    }

    @Bean
    public FanoutExchange accupercentageGezaktExchange() {
        log.info("Aanmaken {}", ACCUPERCENTAGE_GEZAKT_EXCHANGE);
        return new FanoutExchange(ACCUPERCENTAGE_GEZAKT_EXCHANGE);
    }

    @Bean
    public FanoutExchange stofzuigenExchange() {
        log.info("Aanmaken {}", STOFZUIGEN_EXCHANGE);
        return new FanoutExchange(STOFZUIGEN_EXCHANGE);
    }

    @Bean
    public FanoutExchange notificatieExchange() {
        log.info("{} aanmaken", NOTIFICATIE_EXCHANGE);
        return new FanoutExchange(NOTIFICATIE_EXCHANGE);
    }
}
