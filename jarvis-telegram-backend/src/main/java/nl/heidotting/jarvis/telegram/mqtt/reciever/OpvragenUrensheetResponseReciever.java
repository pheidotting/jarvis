package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.OpvragenUrensheetResponse;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.OPVRAGEN_URENSHEET_RESPONSE_QUEUE;

@Slf4j
@Service
public class OpvragenUrensheetResponseReciever {
    @Autowired
    private TelegramService telegramService;

    @RabbitListener(queues = "telegram." + OPVRAGEN_URENSHEET_RESPONSE_QUEUE)
    public void recievedMessage(OpvragenUrensheetResponse opvragenUrensheetResponse) {
        log.info("Urensheet ontvangen voor projectid {}", opvragenUrensheetResponse.projectId());
        telegramService.sendDocument(opvragenUrensheetResponse.projectId(), opvragenUrensheetResponse.fileData());
    }
}

