package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.PersoonGesignaleerd;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.PERSOON_GESIGNALEERD_QUEUE;

@Slf4j
@Service
public class PersoonGesignaleerdReciever {
    @Autowired
    private TelegramService telegramService;

    @RabbitListener(queues = "telegram." + PERSOON_GESIGNALEERD_QUEUE)
    public void recievedMessage(PersoonGesignaleerd persoonGesignaleerd) {
        telegramService.sendPhotoToChannel(persoonGesignaleerd.getNaam() + " gesignaleerd op camera " + persoonGesignaleerd.getCamera(), persoonGesignaleerd.getAfbeelding());
    }
}

