package nl.heidotting.jarvis.telegram;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Queues.*;

@Slf4j
@Configuration
public class Queues {
    @Bean
    public Queue jarvisCameraUploadPictureQueue() {
        log.info("Aanmaken {}", "telegram." + JARVIS_CAMERA_UPLOAD_PICTURE_QUEUE);
        return new Queue("telegram." + JARVIS_CAMERA_UPLOAD_PICTURE_QUEUE);
    }

    @Bean
    public Queue persoonGesignaleerdQueue() {
        log.info("Aanmaken {}", "telegram." + PERSOON_GESIGNALEERD_QUEUE);
        return new Queue("telegram." + PERSOON_GESIGNALEERD_QUEUE);
    }

    @Bean
    public Queue urenNogTeGaanQueue() {
        log.info("Aanmaken {}", "telegram." + UREN_NOG_TE_GAAN_QUEUE);
        return new Queue("telegram." + UREN_NOG_TE_GAAN_QUEUE);
    }
    @Bean
    public Queue gezichtVerwerktQueue() {
        log.info("Aanmaken {}", "telegram." + GEZICHT_VERWERKT_QUEUE);
        return new Queue("telegram." + GEZICHT_VERWERKT_QUEUE);
    }

    @Bean
    public Queue stuurMaandAfgeslotenQueue() {
        log.info("Aanmaken {}", "telegram." + STUUR_MAAND_AFGESLOTEN_QUEUE);
        return new Queue("telegram." + STUUR_MAAND_AFGESLOTEN_QUEUE);
    }

    @Bean
    public Queue opvragenUrensheetResponseQueue() {
        log.info("Aanmaken {}", "telegram." + OPVRAGEN_URENSHEET_RESPONSE_QUEUE);
        return new Queue("telegram." + OPVRAGEN_URENSHEET_RESPONSE_QUEUE);
    }

    @Bean
    public Queue startStopUrenregistratieResponseQueue() {
        log.info("Aanmaken {}", "telegram." + START_STOP_URENREGISTRATIE_RESPONSE_QUEUE);
        return new Queue("telegram." + START_STOP_URENREGISTRATIE_RESPONSE_QUEUE);
    }

    @Bean
    public Queue volumeGewijzigdDoorZwarteLijstLiedQueue() {
        log.info("Aanmaken {}", "telegram." + VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_QUEUE);
        return new Queue("telegram." + VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_QUEUE);
    }

    @Bean
    public Queue accupercentageGezaktLiedQueue() {
        log.info("Aanmaken {}", "telegram." + ACCUPERCENTAGE_GEZAKT_QUEUE);
        return new Queue("telegram." + ACCUPERCENTAGE_GEZAKT_QUEUE);
    }

    @Bean
    public Queue notificatieQueue() {
        log.info("Aanmaken {}", "telegram." + NOTIFICATIE_QUEUE);
        return new Queue("telegram." + NOTIFICATIE_QUEUE);
    }
}
