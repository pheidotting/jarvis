package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingResponse;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.START_STOP_URENREGISTRATIE_RESPONSE_QUEUE;

@Slf4j
@Service
public class StartStopUrenregistratieResponseReciever {
    @Autowired
    private TelegramService telegramService;

    @RabbitListener(queues = "telegram." + START_STOP_URENREGISTRATIE_RESPONSE_QUEUE)
    public void recievedMessage(StartStopUrenregistratieKeepingResponse startStopUrenregistratieKeepingResponse) {
        var builder = new StringBuilder();
        if (startStopUrenregistratieKeepingResponse.startTijd() != null) {
            builder.append("Urenregistratie gestart om ");
            builder.append(startStopUrenregistratieKeepingResponse.startTijd());
        } else {
            builder.append(startStopUrenregistratieKeepingResponse.melding());
        }
        telegramService.sendMessageToChannel(builder.toString());
    }
}

