package nl.heidotting.jarvis.telegram.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StuurMaandAfgeslotenBerichtService {
    @Autowired
    private StuurTelegramBerichtService stuurTelegramBerichtService;

    public void verwerk(Double urenKeeping,
                        Double urenMoneybird,
                        Double verschil,
                        String taakOmschrijving) {

        stuurTelegramBerichtService.stuurTelegramBericht(
                new StringBuilder()
                        .append("De maand is weer voorbij, je hebt voor ")
                        .append(taakOmschrijving)
                        .append(" ")
                        .append(urenMoneybird)
                        .append(" uur in Moneybird geschreven en ")
                        .append(urenKeeping)
                        .append(" uur in Keeping, dat is een verschil van ")
                        .append(verschil)
                        .toString()
                , null);
    }
}
