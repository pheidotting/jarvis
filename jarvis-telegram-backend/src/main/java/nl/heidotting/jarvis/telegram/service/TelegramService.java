package nl.heidotting.jarvis.telegram.service;

import com.google.common.io.ByteSource;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.IncomingFileEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.*;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.media.InputMediaPhoto;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.google.common.collect.Lists.newArrayList;

@Slf4j
@Service
@NoArgsConstructor
@AllArgsConstructor
public class TelegramService extends TelegramLongPollingBot {
    @Autowired
    private VerwerkTelegramCommandoService verwerkTelegramCommandoService;

    @Getter
    private List<IncomingFileEvent> incomingFileEvents = newArrayList();

    @SneakyThrows
    @PostConstruct
    public void init() {
        var telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
        telegramBotsApi.registerBot(new TelegramService(verwerkTelegramCommandoService, newArrayList()));

        verwerkTelegramCommandoService.setTelegramService(this);
    }

    @Override
    public String getBotToken() {
        return "5438460043:AAFWzORaBpCKb0PArU6_a_rDbOyv33Kn0_M";
    }

    @Override
    public void onUpdateReceived(Update update) {
        verwerkTelegramCommandoService.verwerkUpdate(update);
    }

    @Override
    public String getBotUsername() {
        return "Boogschutter 26";
    }

    @SneakyThrows
    public void sendMediaGroup() {
        log.info("Size : {}", incomingFileEvents.size());
        if (incomingFileEvents.size() > 7) {
            var mediaGroup = new SendMediaGroup();
            mediaGroup.setMedias(
                    incomingFileEvents.stream()
                            .map(mapIncomingFileEventNaarInputMediaPhoto())
                            .collect(Collectors.toList()));
            mediaGroup.setChatId("5593371273");
            mediaGroup.enableNotification();
            log.info("Media group versturen");
            execute(mediaGroup);

            incomingFileEvents.clear();
        }
    }

    private Function<IncomingFileEvent, InputMediaPhoto> mapIncomingFileEventNaarInputMediaPhoto() {
        return incomingFileEvent -> {
            var inputMedia = new InputMediaPhoto();
            inputMedia.setCaption(incomingFileEvent.getUsername());
            try {
                inputMedia.setMedia(ByteSource.wrap(incomingFileEvent.getFileData()).openStream(),
                        incomingFileEvent.getFilename()
                );
            } catch (IOException e) {
                log.error("Fout bij zetten van media {}", e);
            }

            return inputMedia;
        };
    }

    @SneakyThrows
    public void sendPhotoToChannel(String tekst, byte[] image) {
        if (image != null) {
            var sendPhoto = new SendPhoto();
            sendPhoto.setCaption(tekst);
            sendPhoto.setChatId("5593371273");
            sendPhoto.enableNotification();
            sendPhoto.setPhoto(new InputFile(ByteSource.wrap(image).openStream(), tekst));
            execute(sendPhoto);
        }
    }

    @SneakyThrows
    public void sendDocument(String projectId, byte[] doc) {
        if (doc != null) {
            var sendDocument = new SendDocument();
            sendDocument.setCaption(projectId + ".xlsx");
            sendDocument.setChatId("5593371273");
            sendDocument.enableNotification();
            sendDocument.setDocument(new InputFile(ByteSource.wrap(doc).openStream(), projectId + ".xlsx"));
            execute(sendDocument);
        }
    }

    @SneakyThrows
    public void sendVideoToChannel(String tekst, byte[] video) {
        if (video != null) {
            var sendVideo = new SendVideo();
            sendVideo.setCaption(tekst);
            sendVideo.setChatId("5593371273");
            sendVideo.setDuration(100);
            sendVideo.enableNotification();
            sendVideo.setVideo(new InputFile(ByteSource.wrap(video).openStream(), tekst));
            execute(sendVideo);
        }
    }

    @SneakyThrows
    public void sendMessageToChannel(String tekst) {
        if (tekst != null) {
            var sendMessage = new SendMessage();
            sendMessage.enableMarkdown(true);
            sendMessage.setChatId("5593371273");
            sendMessage.enableNotification();
            sendMessage.setText(tekst);
            execute(sendMessage);
        }
    }
}
