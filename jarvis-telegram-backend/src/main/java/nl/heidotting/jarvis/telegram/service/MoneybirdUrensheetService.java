package nl.heidotting.jarvis.telegram.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.OpvragenUrensheet;
import nl.heidotting.jarvis.telegram.mqtt.sender.OpvragenUrensheetSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.regex.Pattern;

@Slf4j
@Service
public class MoneybirdUrensheetService {
    @Autowired
    private OpvragenUrensheetSender opvragenUrensheetSender;

    private static Pattern DATE_PATTERN = Pattern.compile("^\\d{2}-\\d{2}-\\d{4}$");

    public void verwerk(String command) {
        log.info("Verwerk {}", command);
        var maandString = command.replace("urensheet", "").trim();

        Month maand;
        switch (maandString) {
            case "jan", "januari":
                maand = Month.JANUARY;
                break;
            case "feb", "februari":
                maand = Month.FEBRUARY;
                break;
            case "mrt", "maart":
                maand = Month.MARCH;
                break;
            case "apr", "april":
                maand = Month.APRIL;
                break;
            case "mei":
                maand = Month.MAY;
                break;
            case "jun", "juni":
                maand = Month.JUNE;
                break;
            case "jul", "juli":
                maand = Month.JULY;
                break;
            case "aug", "augustus":
                maand = Month.AUGUST;
                break;
            case "sep", "september":
                maand = Month.SEPTEMBER;
                break;
            case "okt", "oktober":
                maand = Month.OCTOBER;
                break;
            case "nov", "november":
                maand = Month.NOVEMBER;
                break;
            case "dec", "december":
                maand = Month.DECEMBER;
                break;
            default:
                maand = LocalDate.now().getMonth();
        }

        opvragenUrensheetSender.stuur(new OpvragenUrensheet(maand));
    }
}
