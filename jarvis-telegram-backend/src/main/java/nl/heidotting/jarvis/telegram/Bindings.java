package nl.heidotting.jarvis.telegram;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class Bindings {
    @Bean
    Binding jarvisCameraUploadPicture(Queue jarvisCameraUploadPictureQueue, FanoutExchange jarvisCameraUploadPictureExchange) {
        log.info("Binding tussen {} en {} aanmaken", jarvisCameraUploadPictureQueue.getName(), jarvisCameraUploadPictureExchange.getName());
        return BindingBuilder.bind(jarvisCameraUploadPictureQueue).to(jarvisCameraUploadPictureExchange);
    }

    @Bean
    Binding persoonGesignaleerd(Queue persoonGesignaleerdQueue, FanoutExchange persoonGesignaleerdExchange) {
        log.info("Binding tussen {} en {} aanmaken", persoonGesignaleerdQueue.getName(), persoonGesignaleerdExchange.getName());
        return BindingBuilder.bind(persoonGesignaleerdQueue).to(persoonGesignaleerdExchange);
    }

    @Bean
    Binding urenNogTeGaan(Queue urenNogTeGaanQueue, FanoutExchange urenNogTeGaanExchange) {
        log.info("Binding tussen {} en {} aanmaken", urenNogTeGaanQueue.getName(), urenNogTeGaanExchange.getName());
        return BindingBuilder.bind(urenNogTeGaanQueue).to(urenNogTeGaanExchange);
    }

    @Bean
    Binding gezichtVerwerkt(Queue gezichtVerwerktQueue, FanoutExchange gezichtVerwerktExchange) {
        log.info("Binding tussen {} en {} aanmaken", gezichtVerwerktQueue.getName(), gezichtVerwerktExchange.getName());
        return BindingBuilder.bind(gezichtVerwerktQueue).to(gezichtVerwerktExchange);
    }

    @Bean
    Binding stuurMaandAfgesloten(Queue stuurMaandAfgeslotenQueue, FanoutExchange stuurMaandAfgeslotenExchange) {
        log.info("Binding tussen {} en {} aanmaken", stuurMaandAfgeslotenQueue.getName(), stuurMaandAfgeslotenExchange.getName());
        return BindingBuilder.bind(stuurMaandAfgeslotenQueue).to(stuurMaandAfgeslotenExchange);
    }

    @Bean
    Binding opvragenUrensheetResponse(Queue opvragenUrensheetResponseQueue, FanoutExchange opvragenUrensheetResponseExchange) {
        log.info("Binding tussen {} en {} aanmaken", opvragenUrensheetResponseQueue.getName(), opvragenUrensheetResponseExchange.getName());
        return BindingBuilder.bind(opvragenUrensheetResponseQueue).to(opvragenUrensheetResponseExchange);
    }

    @Bean
    Binding startStopUrenregistratieResponse(Queue startStopUrenregistratieResponseQueue, FanoutExchange startStopUrenregistratieResponseExchange) {
        log.info("Binding tussen {} en {} aanmaken", startStopUrenregistratieResponseQueue.getName(), startStopUrenregistratieResponseExchange.getName());
        return BindingBuilder.bind(startStopUrenregistratieResponseQueue).to(startStopUrenregistratieResponseExchange);
    }

    @Bean
    Binding volumeGewijzigdDoorZwarteLijstLied(Queue volumeGewijzigdDoorZwarteLijstLiedQueue, FanoutExchange volumeGewijzigdDoorZwarteLijstLiedExchange) {
        log.info("Binding tussen {} en {} aanmaken", volumeGewijzigdDoorZwarteLijstLiedQueue.getName(), volumeGewijzigdDoorZwarteLijstLiedExchange.getName());
        return BindingBuilder.bind(volumeGewijzigdDoorZwarteLijstLiedQueue).to(volumeGewijzigdDoorZwarteLijstLiedExchange);
    }

    @Bean
    Binding accupercentageGezaktLied(Queue accupercentageGezaktLiedQueue, FanoutExchange accupercentageGezaktExchange) {
        log.info("Binding tussen {} en {} aanmaken", accupercentageGezaktLiedQueue.getName(), accupercentageGezaktExchange.getName());
        return BindingBuilder.bind(accupercentageGezaktLiedQueue).to(accupercentageGezaktExchange);
    }

    @Bean
    Binding notificatie(Queue notificatieQueue, FanoutExchange notificatieExchange) {
        log.info("Binding tussen {} en {} aanmaken", notificatieQueue.getName(), notificatieExchange.getName());
        return BindingBuilder.bind(notificatieQueue).to(notificatieExchange);
    }
}
