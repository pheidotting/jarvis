package nl.heidotting.jarvis.telegram.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.VerwerkGezicht;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.VERWERK_GEZICHT_EXCHANGE;

@Slf4j
@Component
public class VerwerkGezichtSender extends AbstractPublisher<VerwerkGezicht> {
    @Override
    protected String topic() {
        return VERWERK_GEZICHT_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
