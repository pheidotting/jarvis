package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.VolumeGewijzigdDoorZwarteLijstLied;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_QUEUE;

@Slf4j
@Service
public class VolumeGewijzigdDoorZwarteLijstLiedReciever {
    @Autowired
    private TelegramService telegramService;

    @RabbitListener(queues = "telegram." + VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_QUEUE)
    public void recievedMessage(VolumeGewijzigdDoorZwarteLijstLied volumeGewijzigdDoorZwarteLijstLied) {
        var tekst = new StringBuilder();
        tekst.append("Volume naar ");
        tekst.append(volumeGewijzigdDoorZwarteLijstLied.volume());
        tekst.append(" gezet i.v.m. ");
        if (volumeGewijzigdDoorZwarteLijstLied.volume() > 0) {
            tekst.append("einde ");
        }
        tekst.append("lied ");
        if (volumeGewijzigdDoorZwarteLijstLied.volume() == 0) {
            tekst.append("(");
            tekst.append(volumeGewijzigdDoorZwarteLijstLied.artist());
            tekst.append(" - ");
            tekst.append(volumeGewijzigdDoorZwarteLijstLied.title());
            tekst.append(") ");
        }
        tekst.append("wat je op de zwarte lijst hebt staan.");

        telegramService.sendMessageToChannel(tekst.toString());
    }
}
