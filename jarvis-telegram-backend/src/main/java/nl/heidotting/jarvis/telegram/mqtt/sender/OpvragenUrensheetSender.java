package nl.heidotting.jarvis.telegram.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.OpvragenUrensheet;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.OPVRAGEN_URENSHEET_EXCHANGE;

@Slf4j
@Component
public class OpvragenUrensheetSender extends AbstractPublisher<OpvragenUrensheet> {
    @Override
    protected String topic() {
        return OPVRAGEN_URENSHEET_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
