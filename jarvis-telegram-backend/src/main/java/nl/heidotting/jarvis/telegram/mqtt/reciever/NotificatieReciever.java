package nl.heidotting.jarvis.telegram.mqtt.reciever;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.shared.mqtt.messages.Notificatie;
import nl.heidotting.jarvis.telegram.service.TelegramService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Queues.NOTIFICATIE_QUEUE;

@Component
@RequiredArgsConstructor
public class NotificatieReciever {
    private final TelegramService telegramService;

    @RabbitListener(queues = "telegram." + NOTIFICATIE_QUEUE)
    public void recievedMessage(Notificatie notificatie) {
        telegramService.sendMessageToChannel(notificatie.tekst());
    }
}
