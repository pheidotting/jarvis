//package nl.heidotting.jarvis.telegram.mqtt.reciever;
//
//import lombok.extern.slf4j.Slf4j;
//import nl.heidotting.jarvis.shared.mqtt.AbstractReciever;
//import nl.heidotting.jarvis.shared.mqtt.messages.IncomingFileEvent;
//import nl.heidotting.jarvis.shared.mqtt.messages.UrenNogTeGaan;
//import nl.heidotting.jarvis.telegram.service.TelegramService;
//import nl.heidotting.jarvis.telegram.service.UrenNogTeGaanService;
//import org.slf4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Slf4j
//@Service
//public class MovieUploadReciever extends AbstractReciever<IncomingFileEvent> {
//    @Autowired
//    private TelegramService telegramService;
//
//    @Override
//    public String topic() {
//        return "jarvis/camera/upload/movie";
//    }
//
//    @Override
//    protected void verwerk(IncomingFileEvent incomingFileEvent) {
//        telegramService.sendVideoToChannel(incomingFileEvent.getUsername(),incomingFileEvent.getFileData());
//    }
//
//    @Override
//    protected Class<IncomingFileEvent> clazz() {
//        return IncomingFileEvent.class;
//    }
//
//    @Override
//    protected Logger getLogger() {
//        return log;
//    }
//}
//
