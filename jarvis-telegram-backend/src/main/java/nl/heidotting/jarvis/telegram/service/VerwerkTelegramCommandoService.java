package nl.heidotting.jarvis.telegram.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.messages.BedienSonos;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingRequest;
import nl.heidotting.jarvis.shared.mqtt.messages.Stofzuigen;
import nl.heidotting.jarvis.shared.mqtt.messages.VerwerkGezicht;
import nl.heidotting.jarvis.telegram.mqtt.sender.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.GetFile;
import org.telegram.telegrambots.meta.api.objects.PhotoSize;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.io.File;
import java.net.URL;
import java.util.UUID;


@Slf4j
@Service
public class VerwerkTelegramCommandoService {
    private final String baseDir = "/home/pi/motioneye";

    @Autowired
    private VerwerkGezichtSender verwerkGezichtSender;
    @Autowired
    private GezichtsherkenningAanUitSender gezichtsherkenningAanUitSender;
    @Autowired
    private MoneybirdUrensheetService moneybirdUrensheetService;
    @Autowired
    private StartStopUrenregistratieKeepingSender startStopUrenregistratieKeepingSender;
    @Autowired
    private BedienSonosSender bedienSonosSender;
    @Autowired
    private VerwerkLampenCommandoService verwerkLampenCommandoService;
    @Autowired
    private StofzuigenSender stofzuigenSender;

    private TelegramService telegramService;

    public void setTelegramService(TelegramService telegramService) {
        this.telegramService = telegramService;
    }

    @SneakyThrows
    public void verwerkUpdate(Update update) {
        if (update.getMessage().getPhoto() == null) {
            log.info("Tekst {}", update.getMessage().getText());

            var s = update.getMessage().getText().toLowerCase().trim();
            switch (s) {
                case "ping" -> telegramService.sendMessageToChannel("pong");
                case "gezichtsherkenning aan" -> gezichtsherkenningAanUitSender.stuur(s.replace("gezichtsherkkenning", "").trim().equalsIgnoreCase("aan"));
                case "gezichtsherkenning uit" -> gezichtsherkenningAanUitSender.stuur(s.replace("gezichtsherkkenning", "").trim().equalsIgnoreCase("aan"));
                case "start werk" -> startStopUrenregistratieKeepingSender.stuur(new StartStopUrenregistratieKeepingRequest(true));
                case "stop werk" -> startStopUrenregistratieKeepingSender.stuur(new StartStopUrenregistratieKeepingRequest(false));
                default -> verdereVerwerking(s);
            }
            ;

        } else {
            var fileId = update.getMessage().getPhoto()
                    .stream().sorted((o1, o2) -> o2.getFileSize().compareTo(o1.getFileSize()))
                    .map(PhotoSize::getFileId)
                    .toList().get(0);

            var getFile = new GetFile();
            getFile.setFileId(fileId);
            var file = telegramService.execute(getFile);

            var localFile = new File(baseDir + File.separator + "inleren" + File.separator + update.getMessage().getCaption() + File.separator + UUID.randomUUID().toString());
            var is = new URL("https://api.telegram.org/file/bot5438460043:AAFWzORaBpCKb0PArU6_a_rDbOyv33Kn0_M/" + file.getFilePath()).openStream();

            verwerkGezichtSender.stuur(new VerwerkGezicht(update.getMessage().getCaption(), is.readAllBytes()));
        }
    }

    private void verdereVerwerking(String commando) {
        final var KUTMUZIEK = "kutmuziek";
        final var LEUKE_MUZIEK = "leuke muziek";
        final var START_MUZIEK = "start muziek";
        final var STOP_MUZIEK = "stop muziek";
        final var MUTE = "mute";
        final var UNMUTE = "unmute";
        if (commando.startsWith("urensheet")) {
            moneybirdUrensheetService.verwerk(commando);
        } else if (commando.startsWith(KUTMUZIEK)) {
            var bedienSonos = new BedienSonos(commando.replace(KUTMUZIEK, "").trim(), KUTMUZIEK);
            bedienSonosSender.stuur(bedienSonos);
        } else if (commando.startsWith(LEUKE_MUZIEK)) {
            var bedienSonos = new BedienSonos(commando.replace(LEUKE_MUZIEK, "").trim(), LEUKE_MUZIEK);
            bedienSonosSender.stuur(bedienSonos);
        } else if (commando.startsWith(START_MUZIEK)) {
            var bedienSonos = new BedienSonos(commando.replace(START_MUZIEK, "").trim(), START_MUZIEK);
            bedienSonosSender.stuur(bedienSonos);
        } else if (commando.startsWith(STOP_MUZIEK)) {
            var bedienSonos = new BedienSonos(commando.replace(STOP_MUZIEK, "").trim(), STOP_MUZIEK);
            bedienSonosSender.stuur(bedienSonos);
        } else if (commando.startsWith(MUTE)) {
            var bedienSonos = new BedienSonos(commando.replace(MUTE, "").trim(), MUTE);
            bedienSonosSender.stuur(bedienSonos);
        } else if (commando.startsWith(UNMUTE)) {
            var bedienSonos = new BedienSonos(commando.replace(UNMUTE, "").trim(), UNMUTE);
            bedienSonosSender.stuur(bedienSonos);
        } else if (commando.startsWith("lampen")) {
            verwerkLampenCommandoService.verwerk(commando);
        } else if (commando.startsWith("stofzuigen")) {
            stofzuigenSender.stuur(new Stofzuigen());
        } else {
            telegramService.sendMessageToChannel("Opdracht niet herkend");
        }
    }
}
