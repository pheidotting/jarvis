package nl.heidotting.jarvis.telegram.mqtt.sender;

import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.StartStopUrenregistratieKeepingRequest;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE;

@Component
public class StartStopUrenregistratieKeepingSender extends AbstractPublisher<StartStopUrenregistratieKeepingRequest> {
    @Override
    protected String topic() {
        return START_STOP_URENREGISTRATIE_REQUEST_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
