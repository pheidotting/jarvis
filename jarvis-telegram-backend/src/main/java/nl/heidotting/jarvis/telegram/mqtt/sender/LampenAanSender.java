package nl.heidotting.jarvis.telegram.mqtt.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenAan;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.LAMPEN_LAMPEN_AAN_EXCHANGE;

@Slf4j
@Component
public class LampenAanSender extends AbstractPublisher<AlleLampenAan> {
    @Override
    protected String topic() {
        return LAMPEN_LAMPEN_AAN_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
