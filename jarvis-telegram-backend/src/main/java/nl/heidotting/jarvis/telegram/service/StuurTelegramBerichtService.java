package nl.heidotting.jarvis.telegram.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StuurTelegramBerichtService {
    @Autowired
    private TelegramService telegramService;

    public void stuurTelegramBericht(String tekst, byte[] bestand) {
        if (tekst != null && bestand == null) {
            telegramService.sendMessageToChannel(tekst);
        } else if (bestand != null) {
            telegramService.sendPhotoToChannel(tekst, bestand);
        }
    }
}
