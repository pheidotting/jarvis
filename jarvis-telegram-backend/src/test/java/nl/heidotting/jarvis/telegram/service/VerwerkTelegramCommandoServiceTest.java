//package nl.heidotting.jarvis.telegram.service;
//
//import nl.heidotting.jarvis.telegram.mqtt.sender.GezichtsherkenningAanUitSender;
//import nl.heidotting.jarvis.telegram.mqtt.sender.SonosMQTTSender;
//import nl.heidotting.jarvis.telegram.mqtt.sender.VerwerkGezichtSender;
//import org.junit.jupiter.api.BeforeAll;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.DisplayName;
//import org.junit.jupiter.api.Test;
//import org.mockito.Mock;
//import org.telegram.telegrambots.meta.api.objects.Message;
//import org.telegram.telegrambots.meta.api.objects.Update;
//
//import static org.mockito.Mockito.*;
//import static org.mockito.Mockito.verifyNoMoreInteractions;
//import static org.mockito.MockitoAnnotations.initMocks;
//
//class VerwerkTelegramCommandoServiceTest {
//    @Mock
//    private SonosMQTTSender sonosMQTTSender;
//    @Mock
//    private VerwerkGezichtSender verwerkGezichtSender;
//    @Mock
//    private GezichtsherkenningAanUitSender gezichtsherkenningAanUitSender;
//    @Mock
//    private TelegramService telegramService;
//
//    private VerwerkTelegramCommandoService verwerkTelegramCommandoService = new VerwerkTelegramCommandoService();
//
//    @BeforeEach
//    public void init() {
//        initMocks(this);
//
//        verwerkTelegramCommandoService.setTelegramService(telegramService);
//    }
//
//    @Test
//    @DisplayName("kantoor aan: Zet de Sonos op kantoor aan")
//    void verwerkUpdate() {
//        var message = new Message();
//        message.setText("kantoor aan");
//        var update = new Update();
//        update.setMessage(message);
//
//        verwerkTelegramCommandoService.verwerkUpdate(update);
//
//        verify(sonosMQTTSender).stuur();
//
//    }
//    @Test
//    @DisplayName("Ping Pong")
//    void pingPong() {
//        var message = new Message();
//        message.setText("ping");
//        var update = new Update();
//        update.setMessage(message);
//
//        verwerkTelegramCommandoService.verwerkUpdate(update);
//
//        verify(telegramService).sendMessageToChannel("pong");
//
//        verifyNoMoreInteractions(sonosMQTTSender);
//        verifyNoMoreInteractions(verwerkGezichtSender);
//        verifyNoMoreInteractions(gezichtsherkenningAanUitSender);
//        verifyNoMoreInteractions(telegramService);
//    }
//}