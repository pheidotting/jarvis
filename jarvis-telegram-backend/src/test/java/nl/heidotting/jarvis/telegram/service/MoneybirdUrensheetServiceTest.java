//package nl.heidotting.jarvis.telegram.service;
//
//import nl.heidotting.jarvis.shared.mqtt.messages.OpvragenUrensheet;
//import nl.heidotting.jarvis.telegram.mqtt.sender.OpvragenUrensheetSender;
//import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.extension.ExtendWith;
//import org.mockito.ArgumentCaptor;
//import org.mockito.Captor;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.junit.MockitoJUnitRunner;
//import org.mockito.junit.jupiter.MockitoExtension;
//
//import java.time.LocalDate;
//
//import static org.junit.jupiter.api.Assertions.*;
//import static org.mockito.Mockito.*;
//
//@ExtendWith(MockitoExtension.class)
//class MoneybirdUrensheetServiceTest {
//    @Mock
//    private OpvragenUrensheetSender opvragenUrensheetSender;
//
//    @InjectMocks
//    private MoneybirdUrensheetService moneybirdUrensheetService=new MoneybirdUrensheetService();
//
//    @Captor
//    ArgumentCaptor<OpvragenUrensheet> datumCaptor;
//
//    @Test
//    void verwerkZonderDatum() {
//
//        moneybirdUrensheetService.verwerk("urensheet");
//
//        verify(opvragenUrensheetSender).stuur(datumCaptor.capture());
//        verifyNoMoreInteractions(opvragenUrensheetSender);
//
//        assertNull(datumCaptor.getValue().eindDatum());
//    }
//
//    @Test
//    void verwerkMetDatum() {
//
//        moneybirdUrensheetService.verwerk("urensheet 01-10-2022");
//
//        verify(opvragenUrensheetSender).stuur(datumCaptor.capture());
//        verifyNoMoreInteractions(opvragenUrensheetSender);
//
//        assertEquals(LocalDate.of(2022,10,1),datumCaptor.getValue().eindDatum());
//    }
//
//    @Test
//    void verwerkMetFoutieveDatum() {
//        moneybirdUrensheetService.verwerk("urensheet abc");
//
//        verify(opvragenUrensheetSender).stuur(datumCaptor.capture());
//        verifyNoMoreInteractions(opvragenUrensheetSender);
//
//        assertNull(datumCaptor.getValue().eindDatum());
//    }
//}