package nl.heidotting.jarvis.telegram.service;

import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenAan;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUit;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUitMetLangeTimeout;
import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUitMetTimeout;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenAanSender;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenUitMetLangeTimeoutSender;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenUitMetTimeoutSender;
import nl.heidotting.jarvis.telegram.mqtt.sender.LampenUitSender;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VerwerkLampenCommandoServiceTest {
    @InjectMocks
    private VerwerkLampenCommandoService verwerkLampenCommandoService = new VerwerkLampenCommandoService();

    @Mock
    private LampenAanSender lampenAanSender;
    @Mock
    private LampenUitSender lampenUitSender;
    @Mock
    private LampenUitMetTimeoutSender lampenUitMetTimeoutSender;
    @Mock
    private LampenUitMetLangeTimeoutSender lampenUitMetLangeTimeoutSender;

    @Test
    @DisplayName("Lampen Aan")
    public void testLampenAan() {
        verwerkLampenCommandoService.verwerk("lampen aan");

        verify(lampenAanSender).stuur(new AlleLampenAan());
        verifyNoMoreInteractions(lampenAanSender);
        verifyNoInteractions(lampenUitSender, lampenUitMetTimeoutSender, lampenUitMetLangeTimeoutSender);
    }

    @Test
    @DisplayName("Lampen Uit")
    public void testLampenUit() {
        verwerkLampenCommandoService.verwerk("lampen uit");

        verify(lampenUitSender).stuur(new AlleLampenUit());
        verifyNoMoreInteractions(lampenUitSender);
        verifyNoInteractions(lampenAanSender, lampenUitMetTimeoutSender, lampenUitMetLangeTimeoutSender);
    }

    @Test
    @DisplayName("Lampen Uit met timeout")
    public void testLampenUitMetTimeout() {
        verwerkLampenCommandoService.verwerk("lampen uit met timeout");

        verify(lampenUitMetTimeoutSender).stuur(new AlleLampenUitMetTimeout());
        verifyNoMoreInteractions(lampenUitMetTimeoutSender);
        verifyNoInteractions(lampenAanSender, lampenUitSender, lampenUitMetLangeTimeoutSender);
    }

    @Test
    @DisplayName("Lampen Uit met lange timeout")
    public void testLampenUitMetLangeTimeout() {
        verwerkLampenCommandoService.verwerk("lampen uit met lange timeout");

        verify(lampenUitMetLangeTimeoutSender).stuur(new AlleLampenUitMetLangeTimeout());
        verifyNoMoreInteractions(lampenUitMetLangeTimeoutSender);
        verifyNoInteractions(lampenAanSender, lampenUitSender, lampenUitMetTimeoutSender);
    }
}