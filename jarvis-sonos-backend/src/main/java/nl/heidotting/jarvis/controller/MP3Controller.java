package nl.heidotting.jarvis.controller;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.service.TTSService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
public class MP3Controller {
    @Autowired
    private TTSService ttsService;

    @PostConstruct
    public void init() {
        log.info(" sadoijeowijf ajwfe ewjfoi jweaoioea wfjaowi jeawojfwa fio");
    }

    @GetMapping(value = "/ochtendbegroeting.mp3")
    public ResponseEntity<byte[]> ochtendbegroeting() {
        log.info("ochtendbegroeting.mp3 opvragen");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE); // (3) Content-Type: application/octet-stream

        return ResponseEntity.ok().headers(httpHeaders).body(ttsService.maakOchtendBegroeting(false)); // (5) Return Response
    }

    @GetMapping(value = "/urennogtegaan.mp3")
    public ResponseEntity<byte[]> urennogtegaan() {
        log.info("urennogtegaan.mp3 opvragen");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE); // (3) Content-Type: application/octet-stream

        return ResponseEntity.ok().headers(httpHeaders).body(ttsService.getUrenNogTeGaanBericht()); // (5) Return Response
    }
    @GetMapping(value = "/iemandGesignaleerd.mp3")
    public ResponseEntity<byte[]> iemandGesignaleerd() {
        log.info("iemandGesignaleerd.mp3 opvragen");
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.set(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM_VALUE); // (3) Content-Type: application/octet-stream

        return ResponseEntity.ok().headers(httpHeaders).body(ttsService.getIemandGesignaleerdBericht()); // (5) Return Response
    }
}
