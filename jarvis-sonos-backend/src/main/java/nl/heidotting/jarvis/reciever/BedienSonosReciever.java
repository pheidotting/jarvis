package nl.heidotting.jarvis.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.service.BedienSonosService;
import nl.heidotting.jarvis.shared.mqtt.messages.BedienSonos;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Queues.BEDIEN_SONOS_QUEUE;

@Slf4j
@Configuration
public class BedienSonosReciever {
    @Autowired
    private BedienSonosService bedienSonosService;

    @RabbitListener(queues = "sonos." + BEDIEN_SONOS_QUEUE)
    public void recievedMessage(BedienSonos bedienSonos) {
        log.info("Ontvangen : {}", bedienSonos);
        bedienSonosService.voerCommandoUit(bedienSonos.speaker(), bedienSonos.commando());
    }
}
