package nl.heidotting.jarvis.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.service.UrenNogTeGaanService;
import nl.heidotting.jarvis.shared.mqtt.messages.UrenNogTeGaan;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Queues.UREN_NOG_TE_GAAN_QUEUE;

@Slf4j
@Configuration
public class UrenNogTeGaanReciever {
    @Autowired
    private UrenNogTeGaanService urenNogTeGaanService;

    @RabbitListener(queues = "sonos." + UREN_NOG_TE_GAAN_QUEUE)
    public void recievedMessage(UrenNogTeGaan urenNogTeGaan) {
        log.info("Ontvangen : {}", urenNogTeGaan);

        try {
            urenNogTeGaanService.verwerkUrenNogTeGaan(
                    urenNogTeGaan.getUrenDezeWeek(),
                    urenNogTeGaan.getNogTeGaanVoor32(),
                    urenNogTeGaan.getNogTeGaanVoor36(),
                    urenNogTeGaan.getNogTeGaanVoor40()
            );
        } catch (Exception e) {
            log.error("Fout ontstaan: {}", e);
            e.printStackTrace();
        }
    }
}


