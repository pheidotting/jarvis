package nl.heidotting.jarvis.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.service.WoonkamerSonosService;
import nl.heidotting.jarvis.sonos.model.SonosState;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WoonkamerReciever {
    @Autowired
    private WoonkamerSonosService woonkamerSonosService;

    @RabbitListener(queues = "sonos.woonkamer")
    public void recievedMessage(SonosState sonosState) {
        woonkamerSonosService.verwerkVerandering(sonosState);
    }
}

