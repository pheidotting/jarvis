package nl.heidotting.jarvis.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.service.KantoorSonosService;
import nl.heidotting.jarvis.sonos.model.SonosState;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class KantoorReciever {
    @Autowired
    private KantoorSonosService kantoorSonosService;

    @RabbitListener(queues = "sonos.kantoor")
    public void recievedMessage(SonosState sonosState) {
        kantoorSonosService.verwerkVerandering(sonosState);
    }

}

