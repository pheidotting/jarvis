package nl.heidotting.jarvis.reciever;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sender.KantoorSender;
import nl.heidotting.jarvis.service.TTSService;
import nl.heidotting.jarvis.shared.mqtt.messages.IncomingFileEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.MessageHandler;

@Slf4j
@Configuration
public class ImageUploadReciever {
    @Autowired
    private TTSService ttsService;
    @Autowired
    private KantoorSender kantoorSender;

    @Bean
    @ServiceActivator(inputChannel = "uploadPictureChannel")
    public MessageHandler recieve() {
        return message -> {
//            IncomingFileEvent incomingFileEvent = map(message.getPayload().toString());
//            ttsService.maakIemandGesignaleerdBericht(incomingFileEvent.getUsername());
//            kantoorSender.speelMp3("iemandGesignaleerd", true);
        };
    }

    @SneakyThrows
    public IncomingFileEvent map(String value) {
        return new ObjectMapper().findAndRegisterModules().readValue(value, IncomingFileEvent.class);
    }
}

