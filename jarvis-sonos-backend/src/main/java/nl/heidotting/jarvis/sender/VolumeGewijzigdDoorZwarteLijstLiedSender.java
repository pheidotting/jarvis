package nl.heidotting.jarvis.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.VolumeGewijzigdDoorZwarteLijstLied;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_EXCHANGE;

@Slf4j
@Component
public class VolumeGewijzigdDoorZwarteLijstLiedSender extends AbstractPublisher<VolumeGewijzigdDoorZwarteLijstLied> {
    @Override
    protected String topic() {
        return VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
