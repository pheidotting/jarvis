package nl.heidotting.jarvis.sender;

import nl.heidotting.jarvis.sonos.speaker.Kantoor;
import org.springframework.stereotype.Service;


@Service
public class KantoorSender extends AbstractSpeakerSender {
    @Override
    protected String topic() {
        return new Kantoor().controlTopic();
    }

    @Override
    protected String routingKey() {
        return new Kantoor().controlRoutingkey();
    }
}
