package nl.heidotting.jarvis.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.speaker.Kantoor;
import nl.heidotting.jarvis.sonos.speaker.Woonkamer;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class WoonkamerSender extends AbstractSpeakerSender {
    @Override
    protected String topic() {
        return new Woonkamer().controlTopic();
    }

    @Override
    protected String routingKey() {
        return new Kantoor().controlRoutingkey();
    }
}
