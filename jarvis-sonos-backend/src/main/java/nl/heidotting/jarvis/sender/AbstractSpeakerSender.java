package nl.heidotting.jarvis.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.sonos.commands.*;

import java.math.BigDecimal;

@Slf4j
public abstract class AbstractSpeakerSender extends AbstractPublisher {

    public void stuurStopCommand() {
        log.info("Stop Commando");
        stuur(new StartStopCommando(false));
    }

    public void stuurStartCommand() {
        log.info("Start Commando");
        stuur(new StartStopCommando(true));
    }

    public void setVolume(BigDecimal volume) {
        log.info("Zet volume naar {}", volume);
        stuur(new VolumeCommand(volume.intValue()));
    }

    public void speelMp3(String welkemp3, boolean onlyWhenPlaying) {
        stuur(new SpeelMp3Command(new Input("http://192.168.1.122:8082/" + welkemp3 + ".mp3", onlyWhenPlaying)));
    }

    public void setAvTransportUri(String uri) {
        stuur(new SetAvTransportUri(uri));
    }
}
