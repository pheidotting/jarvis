package nl.heidotting.jarvis.sonos.speaker;

public class Woonkamer extends Speaker{
    @Override
    public String queue() {
        return "sonos.woonkamer";
    }

    @Override
    public String controlTopic() {
        return "sonos.woonkamer.control";
    }

    @Override
    public String routingkey() {
        return "sonos.RINCON_347E5C3D0A5C01400";
    }

    @Override
    public String controlRoutingkey() {
        return "sonos.RINCON_347E5C3D0A5C01400.control";
    }
}
