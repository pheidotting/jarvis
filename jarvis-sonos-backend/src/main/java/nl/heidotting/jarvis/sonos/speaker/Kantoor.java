package nl.heidotting.jarvis.sonos.speaker;

public class Kantoor extends Speaker{
    @Override
    public String queue() {
        return "sonos.kantoor";
    }

    @Override
    public String controlTopic() {
        return "sonos.kantoor.control";
    }

    @Override
    public String routingkey() {
        return "sonos.RINCON_542A1B5CC4F601400";
    }

    @Override
    public String controlRoutingkey() {
        return "sonos.RINCON_542A1B5CC4F601400.control";
    }
}
