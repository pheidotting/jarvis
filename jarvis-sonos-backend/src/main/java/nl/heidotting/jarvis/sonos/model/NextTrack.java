package nl.heidotting.jarvis.sonos.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class NextTrack {
    @JsonProperty("UpnpClass")
    private String upnpClass;
    @JsonProperty("ItemId")
    private String itemId;
    @JsonProperty("ParentId")
    private String parentId;

}