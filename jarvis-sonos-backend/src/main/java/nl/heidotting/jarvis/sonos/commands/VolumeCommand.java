package nl.heidotting.jarvis.sonos.commands;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class VolumeCommand {
    private String command = "volume";
    private int input;

    public VolumeCommand(int volume) {
        this.input = volume;
    }
}
