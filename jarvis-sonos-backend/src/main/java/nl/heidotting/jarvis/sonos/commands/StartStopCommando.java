package nl.heidotting.jarvis.sonos.commands;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class StartStopCommando {
    private transient final String PLAY = "play";
    private transient final String STOP = "stop";
    private String command;

    public StartStopCommando(boolean start) {
        this.command = start ? PLAY : STOP;
    }
}
