package nl.heidotting.jarvis.sonos.speaker;

public abstract class Speaker {
    public abstract String queue();
    public abstract String controlTopic();
    public abstract String routingkey();
    public abstract String controlRoutingkey();
}
