package nl.heidotting.jarvis.sonos.commands;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class SetAvTransportUri {
    private String command = "setavtransporturi";
    private final String input;
}
