package nl.heidotting.jarvis.sonos.commands;

import lombok.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class Input {
    private String trackUri;
    private int volume;
    private boolean onlyWhenPlaying;
    private int timeout;
    private int delayMs;

    public Input(String trackUri) {
        this.trackUri = trackUri;
        this.volume = 30;
        this.onlyWhenPlaying = true;
        this.timeout = 0;
        this.delayMs = 700;
    }

    public Input(String trackUri,  boolean onlyWhenPlaying) {
        this.trackUri = trackUri;
        this.volume = 30;
        this.onlyWhenPlaying = onlyWhenPlaying;
        this.timeout = 0;
        this.delayMs = 700;
    }

    public Input(String trackUri, int volume, boolean onlyWhenPlaying) {
        this.trackUri = trackUri;
        this.volume = volume;
        this.onlyWhenPlaying = onlyWhenPlaying;
        this.timeout = 0;
        this.delayMs = 700;
    }
}
