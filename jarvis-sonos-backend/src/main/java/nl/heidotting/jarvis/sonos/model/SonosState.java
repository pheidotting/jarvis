package nl.heidotting.jarvis.sonos.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SonosState {
    private String uuid;
    private String model;
    private String name;
    private String groupName;
    private String coordinatorUuid;
    private Volume volume;
    private Mute mute;
    private int bass;
    private int treble;
    private long ts;
    private Track currentTrack;
    private MetaData enqueuedMetadata;
    private String playmode;
    private String transportState;
    private NextTrack nextTrack;
}