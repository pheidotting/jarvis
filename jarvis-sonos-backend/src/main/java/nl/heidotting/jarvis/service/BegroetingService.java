package nl.heidotting.jarvis.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Slf4j
@Service
public class BegroetingService {
    @Autowired
    private OphalenWeerService ophalenWeerService;
    @Autowired
    private OudPapierService oudPapierService;
    @Autowired
    private ContainerService containerService;

    private LocalDateTime nu;

    public String getBegroeting() {
        StringBuilder begroeting = new StringBuilder("Goede");
        begroeting.append(getDagdeel());
        begroeting.append(", het is vandaag ");
        begroeting.append(getDagVanDeWeek());
        begroeting.append(" ");
        begroeting.append(getLocalDateTime().getDayOfMonth());
        begroeting.append(" ");
        begroeting.append(getMaand());
        begroeting.append(". ");

        begroeting.append(oudPapierService.wordtOudPapierVandaagOpgehaald());
        try {
            begroeting.append(containerService.getTekstVoorBegroeting());
        } catch (Exception e) {
            log.info("Gezeik met het ophalen van de containerdata");
            log.info(e.getMessage());
        }
        begroeting.append(ophalenWeerService.ophalenWeergegevens());

        nu = null;

        log.info(begroeting.toString());
        return begroeting.toString();
    }

    private LocalDateTime getLocalDateTime() {
        if (nu == null) {
            nu = LocalDateTime.now();
        }
        return nu;
    }

    protected String getDagdeel() {
        if (getLocalDateTime().toLocalTime().isBefore(LocalTime.of(6, 0))) {
            return "nacht";
        } else if (getLocalDateTime().toLocalTime().isBefore(LocalTime.of(12, 0))) {
            return "morgen";
        } else if (getLocalDateTime().toLocalTime().isBefore(LocalTime.of(18, 0))) {
            return "middag";
        } else {
            return "navond";
        }
    }

    protected String getDagVanDeWeek() {
        String dag;
        switch (getLocalDateTime().getDayOfWeek().getValue()) {
            case 1:
                dag = "Maandag";
                break;
            case 2:
                dag = "Dinsdag";
                break;
            case 3:
                dag = "Woensdag";
                break;
            case 4:
                dag = "Donderdag";
                break;
            case 5:
                dag = "Vrijdag";
                break;
            case 6:
                dag = "Zaterdag";
                break;
            default:
                dag = "Zondag";
                break;
        }
        return dag;
    }

    protected String getMaand() {
        String maand;
        switch (getLocalDateTime().getMonthValue()) {
            case 1:
                maand = "Januari";
                break;
            case 2:
                maand = "Februari";
                break;
            case 3:
                maand = "Maart";
                break;
            case 4:
                maand = "April";
                break;
            case 5:
                maand = "Mei";
                break;
            case 6:
                maand = "Juni";
                break;
            case 7:
                maand = "Juli";
                break;
            case 8:
                maand = "Augustus";
                break;
            case 9:
                maand = "September";
                break;
            case 10:
                maand = "Oktober";
                break;
            case 11:
                maand = "November";
                break;
            default:
                maand = "December";
                break;
        }
        return maand;
    }

    public void setNu(LocalDateTime nu) {
        this.nu = nu;
    }
}
