package nl.heidotting.jarvis.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

import static java.util.concurrent.TimeUnit.MINUTES;
import static org.awaitility.Awaitility.await;

@Slf4j
@Service
public class BedienSonosService {
    @Autowired
    private GetSonosServiceService getSonosServiceService;

    public void voerCommandoUit(String speaker, String commando) {
        var sonosService = getSonosServiceService.getSonosService(speaker, commando.trim().startsWith("start"));

        if (sonosService.isPresent()) {
            log.info("Speaker is {}, bijbehorende Service is {}", speaker == null ? "niet meegegeven" : speaker, sonosService.get());

            if ("kutmuziek".equalsIgnoreCase(commando.trim())) {
                verwerkKutmuziekOpdracht(sonosService.get());
            } else if ("leuke muziek".equalsIgnoreCase(commando.trim())) {
                verwerkLeukeMmuziekOpdracht(sonosService.get());
            } else if ("stop muziek".equalsIgnoreCase(commando.trim())) {
                verwerkStopMmuziekOpdracht(sonosService.get());
            } else if ("start muziek".equalsIgnoreCase(commando.trim())) {
                verwerkStartMmuziekOpdracht(sonosService.get());
            } else if ("mute".equalsIgnoreCase(commando.trim())) {
                mute(sonosService.get());
            } else if ("unmute".equalsIgnoreCase(commando.trim())) {
                unmute(sonosService.get());
            }
        } else {
            log.error("Geen SonosService gevonden!");
        }
    }

    private void mute(SonosService sonosService) {
        sonosService.mute();
    }

    private void unmute(SonosService sonosService) {
        sonosService.unmute();
    }

    private void verwerkStartMmuziekOpdracht(SonosService sonosService) {
        log.info("start muziek, playing? {}", sonosService.isPlaying());
        if (!sonosService.isPlaying()) {
            sonosService.stuurStartOpdracht();
            sonosService.stuurSetVolumeOpdracht(new BigDecimal(15));
        }
    }

    private void verwerkStopMmuziekOpdracht(SonosService sonosService) {
        log.info("stop muziek, playing? {}", sonosService.isPlaying());
        if (sonosService.isPlaying()) {
            sonosService.stuurStopOpdracht();
        }
    }

    private void verwerkLeukeMmuziekOpdracht(SonosService sonosService) {
        log.info("leuke muziek, playing? {}", sonosService.isPlaying());
        if (sonosService.isPlaying()) {
            var artist = sonosService.getArtist();
            var title = sonosService.getTitle();
            var volume = sonosService.getVolume();

            sonosService.stuurSetVolumeOpdracht(new BigDecimal(35));

            await().atMost(5, MINUTES).until(() -> !artist.equals(sonosService.getArtist()) && !title.equals(sonosService.getArtist()));

            sonosService.stuurSetVolumeOpdracht(new BigDecimal(volume));
        }
    }

    private void verwerkKutmuziekOpdracht(SonosService sonosService) {
        log.info("k**muziek, playing? {}", sonosService.isPlaying());
        if (sonosService.isPlaying()) {
            var artist = sonosService.getArtist();
            var title = sonosService.getTitle();
            var volume = sonosService.getVolume();

            sonosService.stuurSetVolumeOpdracht(BigDecimal.ZERO);

            await().atMost(5, MINUTES).until(() -> !artist.equals(sonosService.getArtist()) && !title.equals(sonosService.getArtist()));

            sonosService.stuurSetVolumeOpdracht(new BigDecimal(volume));
        }
    }
}
