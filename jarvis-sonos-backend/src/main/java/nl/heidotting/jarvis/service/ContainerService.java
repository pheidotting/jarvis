package nl.heidotting.jarvis.service;

import lombok.Getter;
import lombok.Setter;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.stream.Collectors.toList;

@Service
public class ContainerService extends AbstractServiceMetJsonRead {
    private final static String KEY_LAATSTE_VULLING = "laatsteVullingHashmap";
    private HashMap<String, List<LocalDate>> ophaalDatums;

    public String getTekstVoorBegroeting() {
        String welke = welkeContainerVandaag();
        if (welke != null) {
            if (welke.equals("GREEN")) {
                welke = "groene";
            } else if (welke.equals("GREY")) {
                welke = "grijze";
            } else if (welke.equals("PLASTIC")) {
                welke = "plastic";
            }
            return "Denk je er aan dat vandaag de " + welke + " container wordt geleegd? ";
        } else {
            return "";
        }
    }

    private String welkeContainerVandaag() {
        return getOphaalDatums().keySet().stream()
                .filter(key -> ophaalDatums.get(key).contains(LocalDate.now()))
                .findFirst()
                .orElse(null);
    }

    private void vulHashMap() {
        BodyVoorWasteApi body = new BodyVoorWasteApi();
        body.setCompanyCode("adc418da-d19b-11e5-ab30-625662870761");
        body.setStartDate("2021-12-21");
        body.setEndDate("2025-01-09");
        body.setCommunity("Emmen");
        body.setUniqueAddressID("3000040623");

        HttpEntity<BodyVoorWasteApi> request = new HttpEntity<>(body);

        RestTemplate restTemplate = new RestTemplate();
        String responseString = restTemplate.postForObject("http://wasteapi.ximmio.com/api/GetCalendar", request, String.class);
        JSONArray dataList = new JSONObject(responseString).getJSONArray("dataList");

        ophaalDatums = new HashMap<>();
        dataList.toList().forEach(o -> {
            String welkeContainer = ((HashMap<String, String>) o).get("_pickupTypeText");
            List<LocalDate> datums = ((HashMap<String, List<String>>) o).get("pickupDates").stream()
                    .map(s -> LocalDateTime.parse(s).toLocalDate())
                    .filter(localDate -> !localDate.isBefore(LocalDate.now()))
                    .sorted()
                    .collect(toList());

            ophaalDatums.put(welkeContainer, datums);
        });
        ophaalDatums.put(KEY_LAATSTE_VULLING, newArrayList(LocalDate.now()));
    }

    public HashMap<String, List<LocalDate>> getOphaalDatums() {
        if (ophaalDatums == null || ophaalDatums.get(KEY_LAATSTE_VULLING).get(0).isBefore(LocalDate.now())) {
            vulHashMap();
        }
        HashMap<String, List<LocalDate>> result = (HashMap<String, List<LocalDate>>) ophaalDatums.clone();
        result.remove(KEY_LAATSTE_VULLING);
        return result;
    }

    @Getter
    @Setter
    private static class BodyVoorWasteApi {
        private String companyCode;
        private String startDate;
        private String endDate;
        private String community;
        private String uniqueAddressID;
    }
}
