package nl.heidotting.jarvis.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sender.AbstractSpeakerSender;
import nl.heidotting.jarvis.sender.VolumeGewijzigdDoorZwarteLijstLiedSender;
import nl.heidotting.jarvis.shared.mqtt.messages.VolumeGewijzigdDoorZwarteLijstLied;
import nl.heidotting.jarvis.sonos.model.SonosState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.function.Predicate;

import static com.google.common.collect.Lists.newArrayList;

@Slf4j
@Service
public class KantoorSonosService extends SonosService {
    private final static List<String> zwarteLijst = newArrayList(
            "Mart Hoogkamer",
            "Suzan & Freek",
            "Suzan en Freek",
            "Nick en Simon",
            "Nick & Simon",
            "Meau",
            "Flemming",
            "Snelle",
            "Jaap Reesema"
    );
    private LocalDate vandaagAlAanGeweest = LocalDate.now().minusDays(1);

    @Autowired
    private TTSService ttsService;
    @Autowired
    private VolumeGewijzigdDoorZwarteLijstLiedSender volumeGewijzigdDoorZwarteLijstLiedSender;

    private boolean gemute = false;

    @Override
    public AbstractSpeakerSender getSender() {
        return kantoorSender;
    }

    public void verwerkVeranderingVoorSpeaker(SonosState sonosState) {
        var masterVolume = sonosState.getVolume().getMaster();
        var title = getTitle();
        var artist = getArtist();

        log.info("Artist : {}, Title : {}", artist, title);
        if (zwarteLijst.stream().anyMatch(isEentjeVanDeZwarteLijst(artist, title)) && !gemute) {
            this.setVolume(masterVolume);
            this.gemute = true;
            log.info("k*tmuziek, volume lager");
            kantoorSender.setVolume(new BigDecimal(1));
            volumeGewijzigdDoorZwarteLijstLiedSender.stuur(new VolumeGewijzigdDoorZwarteLijstLied(0, artist, title));
        } else if (zwarteLijst.stream().noneMatch(isEentjeVanDeZwarteLijst(artist, title)) && gemute) {
            this.gemute = false;
            log.info("volume herstellen");
            kantoorSender.setVolume(new BigDecimal(this.getVolume()));
            volumeGewijzigdDoorZwarteLijstLiedSender.stuur(new VolumeGewijzigdDoorZwarteLijstLied(this.getVolume(), null, null));
        }

        if (vandaagAlAanGeweest.isBefore(LocalDate.now()) && LocalTime.now().isBefore(LocalTime.of(10, 0))) {
            log.info("ochtendbegroeting afspelen");
            ttsService.maakOchtendBegroeting(true);
            kantoorSender.speelMp3("ochtendbegroeting", true);
            vandaagAlAanGeweest = LocalDate.now();
        }
    }

    private Predicate isEentjeVanDeZwarteLijst(String artist, String title) {
        return (Predicate<String>) zwarteLijstItem -> artist.toLowerCase().contains(zwarteLijstItem.toLowerCase()) || title.toLowerCase().contains(zwarteLijstItem.toLowerCase());
    }

}
