package nl.heidotting.jarvis.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sender.AbstractSpeakerSender;
import nl.heidotting.jarvis.sonos.model.SonosState;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class WoonkamerSonosService extends SonosService {
    @Override
    public void verwerkVeranderingVoorSpeaker(SonosState sonosState) {
        var title = getTitle();
        var artist = getArtist();

        log.info("Artist : {}, Title : {}", artist, title);
    }

    @Override
    public AbstractSpeakerSender getSender() {
        return woonkamerSender;
    }
}
