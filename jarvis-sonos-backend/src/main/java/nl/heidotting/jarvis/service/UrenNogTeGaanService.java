package nl.heidotting.jarvis.service;

import nl.heidotting.jarvis.sender.KantoorSender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UrenNogTeGaanService {
    @Autowired
    private TTSService ttsService;
    @Autowired
    private KantoorSender kantoorSender;
    @Autowired
    private KantoorSonosService kantoorSonosService;

    public void verwerkUrenNogTeGaan(float urenDezeWeek, float urenNogTeGaanVoor32, float urenNogTeGaanVoor36, float urenNogTeGaanVoor40) {
        if (kantoorSonosService.isPlaying()) {

            var tekst = new StringBuilder("Deze week heb je ");
            tekst.append(formatFloat(urenDezeWeek));
            tekst.append(" uur gemaakt, nog te gaan voor 32 uur : ");
            tekst.append(formatFloat(urenNogTeGaanVoor32));
            tekst.append(" uur. Voor 36 uur : ");
            tekst.append(formatFloat(urenNogTeGaanVoor36));
            tekst.append(" uur. Voor 40 uur : ");
            tekst.append(formatFloat(urenNogTeGaanVoor40));

            ttsService.maakUrenNogTeGaanBericht(tekst.toString());

            kantoorSender.stuurStopCommand();

            kantoorSender.speelMp3("urennogtegaan", false);

            new Thread(() -> {
                try {
                    Thread.sleep(600000);
                    kantoorSonosService.speelQmusic();
                    Thread.sleep(5000);
                    kantoorSender.stuurStopCommand();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).start();
        }
    }

    protected String formatFloat(float uren) {
        var parts = Float.toString(uren).split("\\.");
        try {
            return parts[0] + "," + parts[1].substring(0, 1);
        } catch (Exception e) {
            return "FOUT BIJ OMZETTEN FLOAT NAAR STRING";
        }
    }
}
