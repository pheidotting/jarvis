package nl.heidotting.jarvis.service;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

@Slf4j
@Service
public class TTSService {
    @Autowired
    private BegroetingService begroetingService;

    private byte[] ochtendBegroeting;
    private byte[] urenNogTeGaan;
    private byte[] iemandGesignaleerd;

    @SneakyThrows
    public byte[] maakOchtendBegroeting(boolean reset) {
        if (ochtendBegroeting == null || reset) {
            ochtendBegroeting = speech(begroetingService.getBegroeting());
        }
        return ochtendBegroeting;
    }

    @SneakyThrows
    public void maakUrenNogTeGaanBericht(String tekst) {
        urenNogTeGaan = speech(tekst);
    }

    public byte[] getUrenNogTeGaanBericht() {
        return urenNogTeGaan;
    }

    @SneakyThrows
    public void maakIemandGesignaleerdBericht(String tekst){
        iemandGesignaleerd=speech(tekst);
    }

    public byte[] getIemandGesignaleerdBericht() {
        return iemandGesignaleerd;
    }

    public byte[] speech(String tekst) throws Exception {
        URL url = new URL("http://api.voicerss.org/");
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("POST");
        conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        conn.setConnectTimeout(60000);
        conn.setDoOutput(true);
        DataOutputStream outStream = new DataOutputStream(conn.getOutputStream());
        BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outStream, "UTF-8"));
        writer.write(this.buildParameters(tekst));
        writer.close();
        outStream.close();
        if (conn.getResponseCode() != 200) {
            throw new Exception(conn.getResponseMessage());
        } else {
            ByteArrayOutputStream outArray = new ByteArrayOutputStream();
            InputStream inStream = conn.getInputStream();
            byte[] buffer = new byte[4096];
            boolean var9 = true;

            int n;
            while ((n = inStream.read(buffer)) > 0) {
                outArray.write(buffer, 0, n);
            }

            byte[] response = outArray.toByteArray();
            inStream.close();
            String responseString = new String(response, "UTF-8");
            if (responseString.indexOf("ERROR") == 0) {
                if(responseString.contains("The subscription is expired or requests count limitation is exceeded!")){
                    log.info(responseString);
                    return null;
                }else {
                    throw new Exception(responseString);
                }
            } else {
//                return params.getBase64() ? responseString : response;
                return response;
            }
        }
    }

    private String buildParameters(String tekst) {
        StringBuilder sb = new StringBuilder();
        sb.append("key=38679ce86328478aa86ba2fe3841bc6c");
        sb.append("&src=" + tekst);
        sb.append("&hl=nl-nl");
        sb.append("&v=");
        sb.append("&r=0");
        sb.append("&c=MP3");
        sb.append("&f=44khz_16bit_stereo");
        sb.append("&ssml=false");
        sb.append("&b64=false");
        return sb.toString();
    }
}