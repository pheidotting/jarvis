package nl.heidotting.jarvis.service;

import org.springframework.stereotype.Service;

import java.time.LocalDate;

import static java.time.DayOfWeek.MONDAY;

@Service
public class OudPapierService {
    public String wordtOudPapierVandaagOpgehaald() {
        if (LocalDate.now().equals(tweedeMaandagVanDeMaand())) {
            return "Het is vandaag de tweede maandag van de maand, denk je aan het oud papier? ";
        } else {
            return "";
        }
    }

    private LocalDate tweedeMaandagVanDeMaand() {
        LocalDate datum = LocalDate.of(
                LocalDate.now().getYear(),
                LocalDate.now().getMonth(),
                1);

        while (!datum.getDayOfWeek().equals(MONDAY)) {
            datum = datum.plusDays(1);
        }
        return datum.plusDays(7);
    }
}
