package nl.heidotting.jarvis.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

@Service
public class GetSonosServiceService {
    @Autowired
    private KantoorSonosService kantoorSonosService;
    @Autowired
    private WoonkamerSonosService woonkamerSonosService;

    public Optional<SonosService> getSonosService(String speaker, boolean startCommando) {
        if (speaker == null ||
                "".equalsIgnoreCase(speaker.trim()) ||
                "kantoor".equalsIgnoreCase(speaker.trim()) ||
                "boven".equalsIgnoreCase(speaker.trim())
        ) {
            if (kantoorSonosService.isPlaying() || startCommando)
                return of(kantoorSonosService);
            if (speaker == null && (woonkamerSonosService.isPlaying() || startCommando))
                return of(woonkamerSonosService);
            return ofNullable(null);
        } else {
            if (woonkamerSonosService.isPlaying() || startCommando)
                return of(woonkamerSonosService);
            return ofNullable(null);
        }
    }

}
