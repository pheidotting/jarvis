package nl.heidotting.jarvis.service;

import lombok.Getter;
import lombok.Setter;
import nl.heidotting.jarvis.sender.AbstractSpeakerSender;
import nl.heidotting.jarvis.sender.KantoorSender;
import nl.heidotting.jarvis.sender.WoonkamerSender;
import nl.heidotting.jarvis.sonos.model.SonosState;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.Duration;

public abstract class SonosService {
    @Autowired
    protected KantoorSender kantoorSender;
    @Autowired
    protected WoonkamerSender woonkamerSender;

    @Setter
    @Getter
    private Integer volume;
    @Getter
    private Duration duration;
    @Getter
    private boolean isPlaying;
    @Getter
    private String artist;
    @Getter
    private String title;
    private int masterVolume;
    private int volumeVoorMute;

    public abstract void verwerkVeranderingVoorSpeaker(SonosState sonosState);

    public abstract AbstractSpeakerSender getSender();

    public void verwerkVerandering(SonosState sonosState) {
        duration = null;
        var transportState = sonosState.getTransportState();
        this.isPlaying = isPlaying(transportState);
        this.masterVolume = sonosState.getVolume().getMaster();
        var isQmusic = "qmusic".equalsIgnoreCase(sonosState.getEnqueuedMetadata().getTitle());
        artist = isQmusic ? sonosState.getCurrentTrack().getTitle() : sonosState.getCurrentTrack().getArtist();
        title = isQmusic ? sonosState.getCurrentTrack().getArtist() : sonosState.getCurrentTrack().getTitle();
        if (sonosState.getCurrentTrack().getDuration() != null) {
            duration = Duration.parse(sonosState.getCurrentTrack().getDuration());
        }

        if (isPlaying(transportState) && !"ad break".equalsIgnoreCase(title) && !"qmusic nieuws".equalsIgnoreCase(title)) {
            if (artist != null && title != null) {
                verwerkVeranderingVoorSpeaker(sonosState);
            }
        }
    }

    public void speelQmusic() {
        getSender().setAvTransportUri("radio:s87683");
    }

    protected boolean isPlaying(String transportState) {
        return "playing".equalsIgnoreCase(transportState);
    }

    public void stuurSetVolumeOpdracht(BigDecimal volume) {
        getSender().setVolume(volume);
    }

    public void stuurStopOpdracht() {
        getSender().stuurStopCommand();
    }

    public void stuurStartOpdracht() {
        getSender().stuurStartCommand();
    }

    public void mute() {
        this.volumeVoorMute = this.masterVolume;
        getSender().setVolume(new BigDecimal(5));
    }

    public void unmute() {
        getSender().setVolume(new BigDecimal(this.volumeVoorMute));
    }
}
