package nl.heidotting.jarvis.service;

import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;

@Slf4j
@Service
public class OphalenWeerService extends AbstractServiceMetJsonRead {
    private LocalDateTime laatstekeerOpgehaald;
    private String alarm;
    private String verwachting;
    private String temp;
    private String gevoeltemp;
    private String mintemp;
    private String maxtemp;
    private String neerslagkans;
    private String zon;
    private LocalTime zonop;
    private LocalTime zononder;
    private String mintempmorgen;
    private String maxtempmorgen;
    private String neerslagkansmorgen;
    private String zonmorgen;

    public String ophalenWeergegevens() {
        StringBuilder tekst = new StringBuilder();

        try {
            LocalDateTime tweeUurGeleden = LocalDateTime.now().minusHours(2);
            if (laatstekeerOpgehaald == null || laatstekeerOpgehaald.isBefore(tweeUurGeleden)) {
                JSONObject hoofd = readJsonFromUrl("http://weerlive.nl/api/json-data-10min.php?key=39e08ad01d&locatie=Klazienaveen", null);
                JSONObject weer = hoofd.getJSONArray("liveweer").getJSONObject(0);

                if (weer.getString("alarm") != null && "1" .equals(weer.getString("alarm"))) {
                    alarm = weer.getString("alarmtxt");
                }
                verwachting = weer.getString("verw").replace("(", "").replace(")", "");
                temp = weer.getString("temp").replace(".", ",");
                gevoeltemp = weer.getString("gtemp").replace(".", ",");
                mintemp = weer.getString("d0tmin").replace(".", ",");
                maxtemp = weer.getString("d0tmax").replace(".", ",");
                neerslagkans = weer.getString("d0neerslag");
                zon = weer.getString("d0zon");
                zonop = LocalTime.parse(weer.getString("sup"));
                zononder = LocalTime.parse(weer.getString("sunder"));
                mintempmorgen = weer.getString("d1tmin").replace(".", ",");
                maxtempmorgen = weer.getString("d1tmax").replace(".", ",");
                neerslagkansmorgen = weer.getString("d1neerslag");
                zonmorgen = weer.getString("d1zon");

                laatstekeerOpgehaald = LocalDateTime.now();
            }

            tekst.append("De weersverwachting voor vandaag : ");
            tekst.append(verwachting);
            tekst.append(". Op dit moment is het ");
            tekst.append(temp);
            tekst.append(" graden buiten");
            if (!temp.equals(gevoeltemp)) {
                tekst.append(", met een gevoelstemperatuur van ");
                tekst.append(gevoeltemp);
            }
            tekst.append(". Het wordt vandaag minimaal ");
            tekst.append(mintemp);
            tekst.append(" graden en maximaal ");
            tekst.append(maxtemp);
            tekst.append(" graden. De kans op regen is vandaag ");
            tekst.append(neerslagkans);
            tekst.append(" procent en de kans op zon is ");
            tekst.append(zon);
            tekst.append(" procent. Morgen verwachten we een minimumtemperatuur van ");
            tekst.append(mintempmorgen);
            tekst.append(" graden en maximaal ");
            tekst.append(maxtempmorgen);
            tekst.append(" graden, de neerslagkans voor morgen is ");
            tekst.append(neerslagkansmorgen);
            tekst.append(" procent en de zonnekans is ");
            tekst.append(zonmorgen);
            tekst.append(" procent. ");
            if (alarm != null) {
                tekst.append("Wel geldt er voor vandaag een weersalarm als volgt : ");
                tekst.append(alarm);
            }
        } catch (Exception e) {
            log.error("Geen weer vandaag {}", e.getMessage());
        }
        return tekst.toString();
    }

    public LocalTime getZonop() {
        if (zonop == null) {
            this.ophalenWeergegevens();
        }
        return zonop;
    }

    public LocalTime getZononder() {
        if (zononder == null) {
            this.ophalenWeergegevens();
        }
        return zononder;
    }
}
