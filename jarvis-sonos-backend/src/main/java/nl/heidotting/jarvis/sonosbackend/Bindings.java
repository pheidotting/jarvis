package nl.heidotting.jarvis.sonosbackend;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.speaker.Kantoor;
import nl.heidotting.jarvis.sonos.speaker.Woonkamer;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.springframework.amqp.core.Binding.DestinationType.EXCHANGE;
import static org.springframework.amqp.core.Binding.DestinationType.QUEUE;

@Slf4j
@Configuration
public class Bindings {
    @Bean
    Binding bindSonosWoonkamer(Queue sonosWoonkamer) {
        log.info("Binding tussen {} en {} aanmaken", sonosWoonkamer.getName(), "amq.topic");
        return new Binding(new Woonkamer().queue(), QUEUE, "amq.topic", new Woonkamer().routingkey(), null);
    }

    @Bean
    Binding bindSonosWoonkamerControl(FanoutExchange sonosWoonkamerControl) {
        log.info("Binding tussen {} en {} aanmaken", sonosWoonkamerControl.getName(), "amq.topic");
        return new Binding("amq.topic", EXCHANGE, new Woonkamer().controlTopic(), new Woonkamer().controlRoutingkey(), null);
    }

    @Bean
    Binding bindSonosKantoor(Queue sonosKantoor) {
        log.info("Binding tussen {} en {} aanmaken", sonosKantoor.getName(), "amq.topic");
        return new Binding(new Kantoor().queue(), QUEUE, "amq.topic", new Kantoor().routingkey(), null);
    }

    @Bean
    Binding bindSonosKantoorControl(FanoutExchange sonosKantoorControl) {
        log.info("Binding tussen {} en {} aanmaken", sonosKantoorControl.getName(), "amq.topic");
        return new Binding("amq.topic", EXCHANGE, new Kantoor().controlTopic(), new Kantoor().controlRoutingkey(), null);
    }

    @Bean
    Binding urenNogTeGaan(Queue urenNogTeGaanQueue, FanoutExchange urenNogTeGaanExchange) {
        log.info("Binding tussen {} en {} aanmaken", urenNogTeGaanQueue.getName(), urenNogTeGaanExchange.getName());
        return BindingBuilder.bind(urenNogTeGaanQueue).to(urenNogTeGaanExchange);
    }

    @Bean
    Binding bedienSonos(Queue bedienSonosQueue, FanoutExchange bedienSonosExchange) {
        log.info("Binding tussen {} en {} aanmaken", bedienSonosQueue.getName(), bedienSonosExchange.getName());
        return BindingBuilder.bind(bedienSonosQueue).to(bedienSonosExchange);
    }
}
