package nl.heidotting.jarvis.sonosbackend;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.speaker.Kantoor;
import nl.heidotting.jarvis.sonos.speaker.Woonkamer;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Queues.BEDIEN_SONOS_QUEUE;
import static nl.heidotting.jarvis.shared.mqtt.Queues.UREN_NOG_TE_GAAN_QUEUE;

@Slf4j
@Configuration
public class Queues {
    @Bean
    public Queue sonosWoonkamer() {
        log.info("{} aanmaken", new Woonkamer().queue());
        return new Queue(new Woonkamer().queue());
    }

    @Bean
    public Queue sonosKantoor() {
        log.info("{} aanmaken", new Kantoor().queue());
        return new Queue(new Kantoor().queue());
    }

    @Bean
    public Queue urenNogTeGaanQueue() {
        log.info("Aanmaken {}", "sonos." + UREN_NOG_TE_GAAN_QUEUE);
        return new Queue("sonos." + UREN_NOG_TE_GAAN_QUEUE);
    }

    @Bean
    public Queue bedienSonosQueue() {
        log.info("Aanmaken {}", "sonos." + BEDIEN_SONOS_QUEUE);
        return new Queue("sonos." + BEDIEN_SONOS_QUEUE);
    }

}
