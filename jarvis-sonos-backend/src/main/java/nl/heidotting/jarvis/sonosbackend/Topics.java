package nl.heidotting.jarvis.sonosbackend;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.sonos.speaker.Kantoor;
import nl.heidotting.jarvis.sonos.speaker.Woonkamer;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Topics.*;

@Slf4j
@Configuration
public class Topics {
    @Bean
    public FanoutExchange sonosWoonkamerControl() {
        log.info("{} aanmaken", new Woonkamer().controlTopic());
        return new FanoutExchange(new Woonkamer().controlTopic());
    }

    @Bean
    public FanoutExchange sonosKantoorControl() {
        log.info("{} aanmaken", new Kantoor().controlTopic());
        return new FanoutExchange(new Kantoor().controlTopic());
    }

    @Bean
    public FanoutExchange urenNogTeGaanExchange() {
        log.info("Aanmaken {}", UREN_NOG_TE_GAAN_EXCHANGE);
        return new FanoutExchange(UREN_NOG_TE_GAAN_EXCHANGE);
    }

    @Bean
    public FanoutExchange volumeGewijzigdDoorZwarteLijstLiedExchange() {
        log.info("Aanmaken {}", VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_EXCHANGE);
        return new FanoutExchange(VOLUME_GEWIJZIGD_DOOR_ZWARTE_LIJST_LIED_EXCHANGE);
    }

    @Bean
    public FanoutExchange bedienSonosExchange() {
        log.info("Aanmaken {}", BEDIEN_SONOS_EXCHANGE);
        return new FanoutExchange(BEDIEN_SONOS_EXCHANGE);
    }
}
