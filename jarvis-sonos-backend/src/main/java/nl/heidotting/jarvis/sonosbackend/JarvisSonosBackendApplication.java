package nl.heidotting.jarvis.sonosbackend;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.services.AbstractStarter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication(scanBasePackages = {"nl.heidotting.jarvis"})
public class JarvisSonosBackendApplication extends AbstractStarter {

    public static void main(String[] args) {
        SpringApplication.run(JarvisSonosBackendApplication.class, args);
    }
}
