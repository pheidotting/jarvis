package nl.heidotting.jarvis.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UrenNogTeGaanServiceTest {
    private UrenNogTeGaanService urenNogTeGaanService = new UrenNogTeGaanService();

    @Test
    void testFormatFloat() {
        assertEquals("3,4", urenNogTeGaanService.formatFloat(3.4563F));
    }
}