package nl.heidotting.jarvis.service;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class GetSonosServiceServiceTest {
    @Mock
    private KantoorSonosService kantoorSonosService;
    @Mock
    private WoonkamerSonosService woonkamerSonosService;

    @InjectMocks
    private GetSonosServiceService getSonosServiceService = new GetSonosServiceService();

    @ParameterizedTest
    @NullSource
    @ValueSource(strings = {"", "kantoor", " kantoor", "kantoor ", "boven", " boven", "boven "})
    void kantoor(String speaker) {
        when(kantoorSonosService.isPlaying()).thenReturn(true);

        assertEquals(kantoorSonosService, getSonosServiceService.getSonosService(speaker, false).get());

        verify(kantoorSonosService).isPlaying();
        verifyNoMoreInteractions(kantoorSonosService);
        verifyNoInteractions(woonkamerSonosService);
    }

    @ParameterizedTest
    @NullSource
    void kantoorNotPlayingDusKamer(String speaker) {
        when(kantoorSonosService.isPlaying()).thenReturn(false);
        when(woonkamerSonosService.isPlaying()).thenReturn(true);

        assertEquals(woonkamerSonosService, getSonosServiceService.getSonosService(speaker, false).get());

        verify(kantoorSonosService).isPlaying();
        verify(woonkamerSonosService).isPlaying();
        verifyNoMoreInteractions(kantoorSonosService);
        verifyNoMoreInteractions(woonkamerSonosService);
    }

    @ParameterizedTest
    @NullSource
    void kantoorNotPlayingDusKamerBehalveAlsHetEenStartCommandoIs(String speaker) {
        when(kantoorSonosService.isPlaying()).thenReturn(false);

        assertEquals(kantoorSonosService, getSonosServiceService.getSonosService(speaker, true).get());

        verify(kantoorSonosService).isPlaying();
        verifyNoMoreInteractions(kantoorSonosService);
        verifyNoInteractions(woonkamerSonosService);
    }

    @ParameterizedTest
    @NullSource
    void kantoorNotPlayingDusKamerMaarKamerOokNotPlaying(String speaker) {
        when(kantoorSonosService.isPlaying()).thenReturn(false);
        when(woonkamerSonosService.isPlaying()).thenReturn(false);

        assertTrue(getSonosServiceService.getSonosService(speaker, false).isEmpty());

        verify(kantoorSonosService).isPlaying();
        verify(woonkamerSonosService).isPlaying();
        verifyNoMoreInteractions(kantoorSonosService);
        verifyNoMoreInteractions(woonkamerSonosService);
    }

    @ParameterizedTest
    @ValueSource(strings = {"kamer", " kamer", "kamer ", "woonkamer", " woonkamer", "woonkamer "})
    void kamer(String speaker) {
        when(woonkamerSonosService.isPlaying()).thenReturn(true);

        assertEquals(woonkamerSonosService, getSonosServiceService.getSonosService(speaker, false).get());

        verify(woonkamerSonosService).isPlaying();
        verifyNoMoreInteractions(woonkamerSonosService);
        verifyNoInteractions(kantoorSonosService);
    }

    @ParameterizedTest
    @ValueSource(strings = {"kamer", " kamer", "kamer ", "woonkamer", " woonkamer", "woonkamer "})
    void kamerNotPlaying(String speaker) {
        when(woonkamerSonosService.isPlaying()).thenReturn(false);

        assertTrue(getSonosServiceService.getSonosService(speaker, false).isEmpty());

        verify(woonkamerSonosService).isPlaying();
        verifyNoMoreInteractions(woonkamerSonosService);
        verifyNoInteractions(kantoorSonosService);
    }

    @ParameterizedTest
    @ValueSource(strings = {"kamer", " kamer", "kamer ", "woonkamer", " woonkamer", "woonkamer "})
    void kamerNotPlayingMaarStartCommando(String speaker) {
        when(woonkamerSonosService.isPlaying()).thenReturn(true);

        assertEquals(woonkamerSonosService, getSonosServiceService.getSonosService(speaker, false).get());

        verify(woonkamerSonosService).isPlaying();
        verifyNoMoreInteractions(woonkamerSonosService);
        verifyNoInteractions(kantoorSonosService);
    }
}