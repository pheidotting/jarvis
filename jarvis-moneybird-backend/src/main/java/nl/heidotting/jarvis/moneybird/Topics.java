package nl.heidotting.jarvis.moneybird;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Topics.*;

@Slf4j
@Configuration
public class Topics {
    @Bean
    public FanoutExchange lopendeUrenregistratieAfgeslotenExchange() {
        log.info("Aanmaken {}", LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE);
        return new FanoutExchange(LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE);
    }

    @Bean
    public FanoutExchange maandAfgeslotenExchange() {
        log.info("Aanmaken {}", MAAND_AFGESLOTEN_EXCHANGE);
        return new FanoutExchange(MAAND_AFGESLOTEN_EXCHANGE);
    }

    @Bean
    public FanoutExchange opvragenUrensheetExchange() {
        log.info("Aanmaken {}", OPVRAGEN_URENSHEET_EXCHANGE);
        return new FanoutExchange(OPVRAGEN_URENSHEET_EXCHANGE);
    }

    @Bean
    public FanoutExchange opvragenUrensheetResponseExchange() {
        log.info("Aanmaken {}", OPVRAGEN_URENSHEET_RESPONSE_EXCHANGE);
        return new FanoutExchange(OPVRAGEN_URENSHEET_RESPONSE_EXCHANGE);
    }
}
