package nl.heidotting.jarvis.moneybird.service;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.moneybird.mq.sender.MaandAfgeslotenSender;
import nl.heidotting.jarvis.shared.mqtt.messages.MaandAfgesloten;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.YearMonth;

import static java.time.LocalDate.now;

@Slf4j
@Service
public class OpslaanUrenRegistratieService {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private UrenService urenService;
    @Autowired
    private LaatsteWerkdagVanDeMaandService laatsteWerkdagVanDeMaandService;
    @Autowired
    private MaandAfgeslotenSender maandAfgeslotenSender;

    public void opslaanUrenRegistratie(LocalDateTime startTijd, LocalDateTime eindTijd, String projectNaam,Long taakId) {
        log.info("opslaanUrenRegistratie");
        var project = projectService.ophalenProject(projectNaam);
        log.info("Project gevonden : {}", project);
        var projectId = project.getLong("id");
        urenService.opslaanUrenRegistratie(startTijd, eindTijd, projectId, projectNaam);

        if (now().equals(laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.now()))) {
            var dezeMaandVoorProject = urenService.bepaalAantalUrenDezeMaandVoorProject(projectId);
            var totaalAantalUrenVoorProject = urenService.bepaalAantalUrenVoorProject(projectId);

            maandAfgeslotenSender.stuur(new MaandAfgesloten(dezeMaandVoorProject, totaalAantalUrenVoorProject,projectNaam,taakId));
        }
    }
}
