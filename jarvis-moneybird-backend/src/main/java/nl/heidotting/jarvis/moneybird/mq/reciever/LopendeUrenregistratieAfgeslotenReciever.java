package nl.heidotting.jarvis.moneybird.mq.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.moneybird.service.OpslaanUrenRegistratieService;
import nl.heidotting.jarvis.shared.mqtt.messages.LopendeUrenRegistratieAfgesloten;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE;

@Slf4j
@Service
public class LopendeUrenregistratieAfgeslotenReciever {
    @Autowired
    private OpslaanUrenRegistratieService opslaanUrenRegistratieService;

    @RabbitListener(queues = "moneybird." + LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE)
    public void recievedMessage(LopendeUrenRegistratieAfgesloten lopendeUrenRegistratieAfgesloten) {
        log.info("lopendeUrenRegistratieAfgesloten {}", lopendeUrenRegistratieAfgesloten);
        opslaanUrenRegistratieService.opslaanUrenRegistratie(
                lopendeUrenRegistratieAfgesloten.startTijd(),
                lopendeUrenRegistratieAfgesloten.eindTijd(),
                lopendeUrenRegistratieAfgesloten.project(),
                lopendeUrenRegistratieAfgesloten.taakId()
        );
    }
}

