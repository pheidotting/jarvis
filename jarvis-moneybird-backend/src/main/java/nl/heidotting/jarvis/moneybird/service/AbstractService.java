package nl.heidotting.jarvis.moneybird.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Collections;

@Slf4j
public abstract class AbstractService<T> {
    protected abstract String getBaseUrl();

    protected abstract String getToken();

    @SneakyThrows
    protected String roepMoneyBirdAan(HttpMethod method, T body, String url) {
        var restTemplate = new RestTemplate();
        if (method == HttpMethod.PATCH) {
            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
            restTemplate.setRequestFactory(requestFactory);
        }
        var headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(HttpHeaders.AUTHORIZATION, "Bearer " + getToken());

        var entity = new HttpEntity<>(body, headers);
        log.info("URL {} - {}", method, getBaseUrl() + url);
        log.info("De aan de API mee te geven body : {}", new ObjectMapper().findAndRegisterModules().writeValueAsString(body));
        return restTemplate.exchange(getBaseUrl() + url, method, entity, String.class).getBody();
    }
}
