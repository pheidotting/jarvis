package nl.heidotting.jarvis.moneybird.mq.reciever;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.moneybird.service.ExcelService;
import nl.heidotting.jarvis.moneybird.service.OpvragenUrensheetService;
import nl.heidotting.jarvis.shared.mqtt.messages.OpvragenUrensheet;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static nl.heidotting.jarvis.shared.mqtt.Queues.OPVRAGEN_URENSHEET_QUEUE;

@Slf4j
@Service
public class OpvragenUrensheetReciever {
    @Autowired
    private OpvragenUrensheetService opvragenUrensheetService;
    @Autowired
    private ExcelService excelService;

    @RabbitListener(queues = "moneybird." + OPVRAGEN_URENSHEET_QUEUE)
    public void recievedMessage(OpvragenUrensheet opvragenUrensheet) {
        log.info("Opvragen urensheet voor maand {}", opvragenUrensheet.maand());

        excelService.maakSheet(opvragenUrensheet.maand(), opvragenUrensheetService.maakUrensheet(opvragenUrensheet.maand()));
    }
}

