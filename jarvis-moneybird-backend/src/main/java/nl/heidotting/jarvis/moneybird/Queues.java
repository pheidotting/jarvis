package nl.heidotting.jarvis.moneybird;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Queues.LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE;
import static nl.heidotting.jarvis.shared.mqtt.Queues.OPVRAGEN_URENSHEET_QUEUE;

@Slf4j
@Configuration
public class Queues {
    @Bean
    public Queue lopendeUrenregistratieAfgeslotenQueue() {
        log.info("Aanmaken moneybird.{}", LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE);
        return new Queue("moneybird." + LOPENDE_URENREGISTRATIE_AFGESLOTEN_QUEUE);
    }
    @Bean
    public Queue opvragenUrensheetQueue() {
        log.info("Aanmaken moneybird.{}", OPVRAGEN_URENSHEET_QUEUE);
        return new Queue("moneybird." + OPVRAGEN_URENSHEET_QUEUE);
    }
}
