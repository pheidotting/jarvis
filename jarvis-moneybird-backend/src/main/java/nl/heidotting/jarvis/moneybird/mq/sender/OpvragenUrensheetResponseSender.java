package nl.heidotting.jarvis.moneybird.mq.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.OpvragenUrensheetResponse;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.OPVRAGEN_URENSHEET_RESPONSE_EXCHANGE;

@Slf4j
@Component
public class OpvragenUrensheetResponseSender extends AbstractPublisher<OpvragenUrensheetResponse> {
    @Override
    protected String topic() {
        return OPVRAGEN_URENSHEET_RESPONSE_EXCHANGE;
    }

    @Override
    protected String routingKey() {
        return null;
    }
}
