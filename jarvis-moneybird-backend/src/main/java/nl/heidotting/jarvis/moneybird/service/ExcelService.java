package nl.heidotting.jarvis.moneybird.service;

import lombok.RequiredArgsConstructor;
import nl.heidotting.jarvis.moneybird.mq.sender.OpvragenUrensheetResponseSender;
import nl.heidotting.jarvis.shared.mqtt.messages.OpvragenUrensheetResponse;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Comparator;
import java.util.Locale;
import java.util.Map;

import static java.time.format.TextStyle.FULL;

@Service
@RequiredArgsConstructor
public class ExcelService {
    @Autowired
    private OpvragenUrensheetResponseSender opvragenUrensheetResponseSender;
    @Autowired
    private ProjectService projectService;

    public void maakSheet(Month maand, Map<Long, Map<LocalDate, Duration>> uren) {
        var dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");

        uren.keySet()
                .forEach(project -> {
                    var workbook = new XSSFWorkbook();

                    var sheet = workbook.createSheet(maand.getDisplayName(FULL, Locale.UK));
                    sheet.setColumnWidth(0, 6000);
                    var uurtjes = uren.get(project);

                    var weekFields = WeekFields.of(Locale.getDefault());
                    final int[] weekNumber = {0};
                    int row[] = {1};

                    var styleBold = workbook.createCellStyle();
                    var fontBold = workbook.createFont();
                    fontBold.setFontName("Arial");
                    fontBold.setFontHeightInPoints((short) 12);
                    fontBold.setBold(true);
                    styleBold.setFont(fontBold);

                    Row headerRow = sheet.createRow(row[0]++);
                    Cell headerCel = headerRow.createCell(1);
                    headerCel.setCellValue("Patrick Heidotting");
                    headerCel.setCellStyle(styleBold);


                    var styleDefault = workbook.createCellStyle();
                    var fontDefault = workbook.createFont();
                    fontDefault.setFontName("Arial");
                    fontDefault.setFontHeightInPoints((short) 12);
                    styleDefault.setAlignment(HorizontalAlignment.LEFT);
                    styleDefault.setFont(fontDefault);

                    var minutenTotaal = uurtjes.keySet()
                            .stream()
                            .sorted(Comparator.naturalOrder())
                            .filter(localDate -> localDate.getDayOfWeek() != DayOfWeek.SATURDAY && localDate.getDayOfWeek() != DayOfWeek.SUNDAY)
                            .mapToLong(localDate -> {
                                var duration = uurtjes.get(localDate);
                                var uur = duration.toHours();
                                var minuten = duration.minusHours(uur).toMinutes() + "";

                                int weekNumberNw = localDate.get(weekFields.weekOfWeekBasedYear());
                                if (weekNumberNw != weekNumber[0]) {
                                    weekNumber[0] = weekNumberNw;

                                    Row weekRow = sheet.createRow(row[0]++);
                                    Cell weekCel = weekRow.createCell(0);
                                    weekCel.setCellValue("Week " + weekNumber[0]);
                                    weekCel.setCellStyle(styleBold);
                                }

                                var urenRegel = sheet.createRow(row[0]++);
                                var datumCel = urenRegel.createCell(0);
                                datumCel.setCellValue(localDate.format(dtf));
                                var urenCel = urenRegel.createCell(1);
                                urenCel.setCellStyle(styleDefault);
                                if ("0".equals(minuten)) {
                                    urenCel.setCellValue(uur);
                                } else {
                                    urenCel.setCellValue(uur + ("0".equals(minuten) ? "" : (":" + (minuten.length() == 2 ? minuten : "0" + minuten))));
                                }

                                return duration.toMinutes();
                            })
                            .sum();

                    var totaalUur = minutenTotaal / 60;
                    var minuten = minutenTotaal - (totaalUur * 60) + "";

                    var totaalRegel = sheet.createRow((row[0] + 2));
                    var totaalCelHeader = totaalRegel.createCell(0);
                    totaalCelHeader.setCellValue("Totaal :");
                    totaalCelHeader.setCellStyle(styleBold);
                    var urentotaalCelTotaalCel = totaalRegel.createCell(1);
                    urentotaalCelTotaalCel.setCellValue(totaalUur + ("0".equals(minuten) ? "" : (":" + (minuten.length() == 2 ? minuten : "0" + minuten))));
                    urentotaalCelTotaalCel.setCellStyle(styleBold);


                    var outputStream = new ByteArrayOutputStream();
                    try {
                        workbook.write(outputStream);
                        workbook.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    opvragenUrensheetResponseSender.stuur(new OpvragenUrensheetResponse(projectService.ophalenProjectOpId(project) + " " + maand.getDisplayName(FULL, Locale.UK), outputStream.toByteArray()));
                });

    }
}
