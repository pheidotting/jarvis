package nl.heidotting.jarvis.moneybird;

import nl.heidotting.jarvis.shared.services.AbstractStarter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication(scanBasePackages = {"nl.heidotting.jarvis.moneybird"})
public class JarvisMoneyBird extends AbstractStarter {
    public static void main(String[] args) {
        SpringApplication.run(JarvisMoneyBird.class, args);
    }
}
