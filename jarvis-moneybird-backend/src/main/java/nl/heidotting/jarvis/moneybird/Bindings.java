package nl.heidotting.jarvis.moneybird;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class Bindings {
    @Bean
    Binding lopendeUrenregistratieAfgesloten(Queue lopendeUrenregistratieAfgeslotenQueue, FanoutExchange lopendeUrenregistratieAfgeslotenExchange) {
        log.info("Binding tussen {} en {} aanmaken", lopendeUrenregistratieAfgeslotenQueue.getName(), lopendeUrenregistratieAfgeslotenExchange.getName());
        return BindingBuilder.bind(lopendeUrenregistratieAfgeslotenQueue).to(lopendeUrenregistratieAfgeslotenExchange);
    }
    @Bean
    Binding opvragenUrensheet(Queue opvragenUrensheetQueue, FanoutExchange opvragenUrensheetExchange) {
        log.info("Binding tussen {} en {} aanmaken", opvragenUrensheetQueue.getName(), opvragenUrensheetExchange.getName());
        return BindingBuilder.bind(opvragenUrensheetQueue).to(opvragenUrensheetExchange);
    }
}
