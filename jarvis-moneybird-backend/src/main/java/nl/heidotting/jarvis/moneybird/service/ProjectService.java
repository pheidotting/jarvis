package nl.heidotting.jarvis.moneybird.service;

import nl.heidotting.jarvis.moneybird.model.OpslaanProject;
import nl.heidotting.jarvis.moneybird.model.Project;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class ProjectService extends AbstractService<OpslaanProject> {
    @Value("${moneybird.base.url}")
    private String baseUrl;
    @Value("${moneybird.autorisation.token}")
    private String token;

    @Override
    protected String getBaseUrl() {
        return baseUrl;
    }

    @Override
    protected String getToken() {
        return token;
    }

    public String ophalenProjectOpId(Long projectId) {
        var body = new JSONObject(roepMoneyBirdAan(HttpMethod.GET, null, "projects/" + projectId));
        return body.getString("name");
    }

    public JSONObject ophalenProject(String projectNaam) {
        var body = new JSONArray(roepMoneyBirdAan(HttpMethod.GET, null, "projects"));

        JSONObject project = null;
        for (int i = 0; i < body.length(); i++) {
            var item = (JSONObject) body.get(i);
            if (projectNaam.equalsIgnoreCase(item.getString("name"))) {
                project = item;
            }
        }
        if (project == null) {
            var obj = new OpslaanProject(new Project(projectNaam));
            roepMoneyBirdAan(HttpMethod.POST, obj, "projects");
            return ophalenProject(projectNaam);
        }
        return project;
    }

}
