package nl.heidotting.jarvis.moneybird.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.function.Predicate;

import static com.google.common.collect.Lists.newArrayList;


@Service
public class OpvragenUrensheetService {
    @Autowired
    private UrenService urenService;

    public Map<Long, Map<LocalDate, Duration>> maakUrensheet(Month maand) {
        Map<Long, Map<LocalDate, Duration>> result = new HashMap<>();
        LocalDate beginDatum = LocalDate.of(LocalDate.now().getYear(), maand.getValue(), 1);
        LocalDate eindDatum = LocalDate.of(LocalDate.now().getYear(), maand.getValue(), maand.length(LocalDate.now().isLeapYear()));

        var uren = urenService.haalAlleUrenregistratiesTussenTweeDatums(beginDatum, eindDatum);

        var projecten = new HashSet<Long>();
        uren.toList()
                .stream().filter(new Predicate<Object>() {
                    @Override
                    public boolean test(Object o) {
                        return ((HashMap) o).get("project_id") != null;
                    }
                })
                .forEach(o -> projecten.add(Long.valueOf((String) ((HashMap) o).get("project_id"))));

        projecten.forEach(projectId -> {
            var gewerkteDatums = new HashMap<LocalDate, Duration>();
            var volledigeMaand = new HashMap<LocalDate, Duration>();
            newArrayList(uren)
                    .stream()
                    .map(JSONObject.class::cast)
                    .filter(jsonObject -> !"null".equals(jsonObject.get("project_id").toString()))
                    .filter(o -> o.getBigInteger("project_id").equals(BigInteger.valueOf(projectId)))
                    .forEach(entry -> {
                        var startTijd = LocalDateTime.parse(entry.getString("started_at").substring(0, 22));
                        var eindTijd = LocalDateTime.parse(entry.getString("ended_at").substring(0, 22));

                        var pause = entry.getLong("paused_duration");
                        var duur = Duration.ofMinutes(ChronoUnit.MINUTES.between(startTijd, eindTijd) - (pause / 60));

                        gewerkteDatums.put(startTijd.toLocalDate(), duur);
                    });

            for (int i = 1; i <= eindDatum.getDayOfMonth(); i++) {
                var datum = LocalDate.of(beginDatum.getYear(), beginDatum.getMonth(), i);
                var duur = gewerkteDatums.get(datum);
                if (duur == null) {
                    duur = Duration.ZERO;
                }
                volledigeMaand.put(datum, duur);
            }

            result.put(projectId, volledigeMaand);
        });

        return result;
    }

}

