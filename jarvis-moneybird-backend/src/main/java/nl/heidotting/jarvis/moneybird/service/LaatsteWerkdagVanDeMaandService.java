package nl.heidotting.jarvis.moneybird.service;

import org.springframework.stereotype.Service;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;

@Service
public class LaatsteWerkdagVanDeMaandService {
    public LocalDate bepaalLaatsteWerkdagVanDeMaand(YearMonth jaarMaand) {
        LocalDate laatsteDag = jaarMaand.atEndOfMonth();

        while (laatsteDag.getDayOfWeek() == DayOfWeek.SATURDAY
                || laatsteDag.getDayOfWeek() == DayOfWeek.SUNDAY) {
            laatsteDag = laatsteDag.minusDays(1);
        }

        return laatsteDag;
    }
}
