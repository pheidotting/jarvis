package nl.heidotting.jarvis.moneybird.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OpslaanTimeEntry implements OpslaanOfWijzigTimeEntry {
    private TimeEntryNieuw time_entry;
}
