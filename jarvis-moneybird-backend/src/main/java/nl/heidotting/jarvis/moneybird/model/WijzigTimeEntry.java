package nl.heidotting.jarvis.moneybird.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class WijzigTimeEntry implements OpslaanOfWijzigTimeEntry {
    private TimeEntry time_entry;
}
