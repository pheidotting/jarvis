package nl.heidotting.jarvis.moneybird.mq.sender;

import lombok.extern.slf4j.Slf4j;
import nl.heidotting.jarvis.shared.mqtt.AbstractPublisher;
import nl.heidotting.jarvis.shared.mqtt.messages.MaandAfgesloten;
import org.springframework.stereotype.Component;

import static nl.heidotting.jarvis.shared.mqtt.Topics.MAAND_AFGESLOTEN_EXCHANGE;

@Slf4j
    @Component
public class MaandAfgeslotenSender  extends AbstractPublisher<MaandAfgesloten> {
        @Override
        protected String topic() {
            return MAAND_AFGESLOTEN_EXCHANGE;
        }

        @Override
        protected String routingKey() {
            return null;
        }
    }
