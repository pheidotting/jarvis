package nl.heidotting.jarvis.moneybird;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.http.client.Client;
import com.rabbitmq.http.client.ClientParameters;
import com.rabbitmq.http.client.domain.ExchangeInfo;
import com.rabbitmq.http.client.domain.OutboundMessage;
import lombok.SneakyThrows;
import nl.heidotting.jarvis.shared.mqtt.messages.LopendeUrenRegistratieAfgesloten;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockserver.client.MockServerClient;
import org.mockserver.matchers.Times;
import org.mockserver.mock.Expectation;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.testcontainers.containers.MockServerContainer;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static java.util.Arrays.asList;
import static java.util.concurrent.TimeUnit.SECONDS;
import static nl.heidotting.jarvis.shared.mqtt.Topics.LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.testcontainers.shaded.org.awaitility.Awaitility.await;

@Testcontainers
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NieuweRegistratieTest {
    private static RabbitMQContainer rabbitmq;
    public static final DockerImageName MOCKSERVER_IMAGE = DockerImageName
            .parse("mockserver/mockserver")
            .withTag("mockserver-" + MockServerClient.class.getPackage().getImplementationVersion());

    public static MockServerContainer mockServer;

    static {
        rabbitmq = new RabbitMQContainer();
        rabbitmq.start();
        mockServer = new MockServerContainer(MOCKSERVER_IMAGE);
        mockServer.start();
    }

    @DynamicPropertySource
    public static void setDatasourceProperties(final DynamicPropertyRegistry registry) {
        registry.add("spring.rabbitmq.host", rabbitmq::getHost);
        registry.add("spring.rabbitmq.port", rabbitmq::getAmqpPort);
        registry.add("spring.rabbitmq.username", rabbitmq::getAdminUsername);
        registry.add("spring.rabbitmq.password", rabbitmq::getAdminPassword);
        registry.add("spring.rabbitmq.ssl.enabled", () -> false);
        registry.add("moneybird.base.url", () -> mockServer.getEndpoint() + "/");
    }

    @SneakyThrows
    @Test
    @DisplayName("Test nieuwe urenregistratie als nog niets aanwezig voor die dag")
    public void nieuweUrenRegistratie() {
        var vandaag = LocalDate.now();
        var c = new Client(
                new ClientParameters()
                        .url("http://" + rabbitmq.getHost() + ":" + rabbitmq.getHttpPort() + "/api/")
                        .username(rabbitmq.getAdminUsername())
                        .password(rabbitmq.getAdminPassword())
        );

        c.declareExchange("/", LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE, new ExchangeInfo("fanout", true, false));

        var projectsResponse = "[\n" +
                "{\n" +
                "\"id\": \"366712049429906585\",\n" +
                "\"name\": \"Project\",\n" +
                "\"state\": \"active\"\n" +
                "}\n" +
                "]";
        var timeentriesOpDatum = "[]";
        var mockServerClient = new MockServerClient(mockServer.getHost(), mockServer.getServerPort());
        var expectations = asList(mockServerClient
                .when(request().withPath("/projects"),
                        Times.exactly(1)
                )
                .respond(response().withBody(projectsResponse)));
        var dtfZonderStreepjes = DateTimeFormatter.ofPattern("yyyyMMdd");
        var expectations2 = asList(mockServerClient
                .when(request().withPath("/time_entries")
                                .withQueryStringParameter("filter", "day:" + vandaag.format(dtfZonderStreepjes)),
                        Times.exactly(1)
                )
                .respond(response().withBody(timeentriesOpDatum)));
        var dtfMetStreepjes = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        var body = "{\"time_entry\":{\"user_id\":365712881003529541,\"started_at\":\"" + vandaag.format(dtfMetStreepjes) + "T07:23+01:00\",\"ended_at\":\"" + vandaag.format(dtfMetStreepjes) + "T14:45+01:00\",\"description\":\"Gewerkt Project\",\"contact_id\":null,\"project_id\":366712049429906585,\"detail_id\":null,\"billable\":true,\"paused_duration\":0}}";
//        var body = "{\"time_entry\":{\"user_id\":365712881003529541,\"started_at\":\"" + vandaag.format(dtfMetStreepjes) + "T07:23";
//        var body = "\"user_id\":365712881003529541";
//                   "{\"time_entry\":{\"user_id\":365712881003529541,\"started_at\":\"2023-01-05                             T07:23Z\     ",\"ended_at\":\"2023-01-05                             T14:45Z     \",\"description\":\"Gewerkt Project\",\"contact_id\":null,\"project_id\":366712049429906585,\"detail_id\":null,\"billable\":true,\"paused_duration\":0}}"
        var expectations3 = asList(mockServerClient
                .when(request().withPath("/time_entries")
                                .withMethod("POST")
//                                .withBody(jsonPath(body))//, MatchType.ONLY_MATCHING_FIELDS))
                        ,
                        Times.exactly(1)
                )
                .respond(response()));

        var outboundMessage = new OutboundMessage();
        var startTijd = LocalDateTime.of(vandaag, LocalTime.of(7, 23));
        var eindTijd = LocalDateTime.of(vandaag, LocalTime.of(14, 45));

        outboundMessage.payload(new ObjectMapper().findAndRegisterModules().writeValueAsString(new LopendeUrenRegistratieAfgesloten(startTijd, eindTijd, "Project", 123L)));
        c.publish("/", LOPENDE_URENREGISTRATIE_AFGESLOTEN_EXCHANGE, "", outboundMessage);

        expectations.stream()
                .map(Expectation::getId)
                .forEach(expectationIds -> await().atMost(30, SECONDS).until(() -> {
                    try {
                        mockServerClient.verify(expectationIds);
                        return true;
                    } catch (AssertionError ae) {
                        return false;
                    }
                }));
        expectations2.stream()
                .map(Expectation::getId)
                .forEach(expectationIds -> await().atMost(30, SECONDS).until(() -> {
                    try {
                        mockServerClient.verify(expectationIds);
                        return true;
                    } catch (AssertionError ae) {
                        return false;
                    }
                }));
        expectations3.stream()
                .map(Expectation::getId)
                .forEach(expectationIds -> await().atMost(30, SECONDS).until(() -> {
                    try {
                        mockServerClient.verify(expectationIds);
                        return true;
                    } catch (AssertionError ae) {
                        return false;
                    }
                }));
    }
}
