package nl.heidotting.jarvis.moneybird.service;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.YearMonth;

import static org.junit.jupiter.api.Assertions.assertEquals;

class LaatsteWerkdagVanDeMaandServiceTest {

    private LaatsteWerkdagVanDeMaandService laatsteWerkdagVanDeMaandService = new LaatsteWerkdagVanDeMaandService();

    @Test
    @DisplayName("Januari 2022")
    void januari() {
        assertEquals(LocalDate.of(2022, 1, 31), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 1)));
    }

    @Test
    @DisplayName("Februari 2022")
    void februari() {
        assertEquals(LocalDate.of(2022, 2, 28), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 2)));
    }

    @Test
    @DisplayName("Maart 2022")
    void maart() {
        assertEquals(LocalDate.of(2022, 3, 31), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 3)));
    }

    @Test
    @DisplayName("April 2022")
    void april() {
        assertEquals(LocalDate.of(2022, 4, 29), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 4)));
    }

    @Test
    @DisplayName("Mei 2022")
    void mei() {
        assertEquals(LocalDate.of(2022, 5, 31), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 5)));
    }

    @Test
    @DisplayName("Juni 2022")
    void juni() {
        assertEquals(LocalDate.of(2022, 6, 30), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 6)));
    }

    @Test
    @DisplayName("Juli 2022")
    void juli() {
        assertEquals(LocalDate.of(2022, 7, 29), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 7)));
    }

    @Test
    @DisplayName("Augustus 2022")
    void augustus() {
        assertEquals(LocalDate.of(2022, 8, 31), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 8)));
    }

    @Test
    @DisplayName("September 2022")
    void september() {
        assertEquals(LocalDate.of(2022, 9, 30), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 9)));
    }

    @Test
    @DisplayName("Oktober 2022")
    void oktober() {
        assertEquals(LocalDate.of(2022, 10, 31), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 10)));
    }

    @Test
    @DisplayName("November 2022")
    void november() {
        assertEquals(LocalDate.of(2022, 11, 30), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 11)));
    }

    @Test
    @DisplayName("December 2022")
    void december() {
        assertEquals(LocalDate.of(2022, 12, 30), laatsteWerkdagVanDeMaandService.bepaalLaatsteWerkdagVanDeMaand(YearMonth.of(2022, 12)));
    }
}