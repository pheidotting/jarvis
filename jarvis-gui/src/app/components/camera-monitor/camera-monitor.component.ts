import {Component, Input, OnInit} from '@angular/core';
import {v4 as uuid} from 'uuid';
import {timer} from "rxjs";

@Component({
  selector: 'app-camera-monitor',
  templateUrl: './camera-monitor.component.html',
  styleUrls: ['./camera-monitor.component.css']
})
export class CameraMonitorComponent implements OnInit {
  @Input() naam: string = '';
  @Input() url: string = '';
  urlMetUuid: string = '';
  minuut = 300000;

  ngOnInit(): void {
    this.refresh();
  }

  refresh(): void {
    this.urlMetUuid = this.url + '&a=' + uuid();
    timer(this.minuut)
      .subscribe(() => {
        this.refresh();
      });
  }
}
