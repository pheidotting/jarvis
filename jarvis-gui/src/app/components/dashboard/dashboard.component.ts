import {Component, OnInit} from '@angular/core';
import {KeepingService} from "../../services/keeping.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  projecten: any;
  cameras: any[] = [];

  constructor(
    private keepingService: KeepingService
  ) {
    this.cameras.push({
      'naam': 'Zijkant links',
      'url': 'http://192.168.1.122/zm/cgi-bin/nph-zms?scale=100&mode=jpeg&maxfps=30&monitor=3&rand=1693727840&connkey=665625'
    });
    this.cameras.push({
      'naam': 'Voorkant',
      'url': 'http://192.168.1.122/zm/cgi-bin/nph-zms?scale=100&mode=jpeg&maxfps=30&monitor=1&rand=1693728232&connkey=879007'
    });
    this.cameras.push({
      'naam': 'Zijkant rechts',
      'url': 'http://192.168.1.122/zm/cgi-bin/nph-zms?scale=38&mode=jpeg&maxfps=30&monitor=2&rand=387076&connkey=479061'
    });
    this.cameras.push({
      'naam': 'Achterdeur',
      'url': 'http://192.168.1.122/zm/cgi-bin/nph-zms?scale=38&mode=jpeg&maxfps=30&monitor=4&rand=74143&connkey=663303'
    });
    this.cameras.push({
      'naam': 'Deurbel',
      'url': 'http://192.168.1.122/zm/cgi-bin/nph-zms?scale=25&mode=jpeg&maxfps=30&monitor=5&rand=556357&connkey=962903'
    });
  }

  ngOnInit(): void {
    this.keepingService.getProjecten().subscribe((projecten: any) => {
      this.projecten = projecten.tasks
        .filter((task: any) => {
          return task.name != 'Verlof';
        });
    })
  }
}
