import {Component, Input, OnInit} from '@angular/core';
import {Client} from "@stomp/stompjs";
import {KeepingService} from "../../api";

@Component({
  selector: 'app-keeping',
  templateUrl: './keeping.component.html',
  styleUrls: ['./keeping.component.css']
})
export class KeepingComponent implements OnInit {
  @Input() project: any;
  urenVandaag: number = 0;
  urenWeek: number = 0;
  lopendeRegistratie: boolean = false;

  constructor(
    private keepingService: KeepingService
  ) {
    this.keepingService.configuration.basePath = '/keeping/api';
  }

  ngOnInit(): void {
    const client = new Client({
      // brokerURL: 'ws://localhost:8080//ws/chat/uuhh/henk',
      // brokerURL: 'ws://192.168.1.109:8094/jarvis/websockets',
      brokerURL: 'ws://192.168.1.109:8093/jarvis/websockets',
      onConnect: () => {
        client.subscribe('/topic/keepingupdate/' + this.project.id, message => {
          let keepingUpdate = JSON.parse(message.body);
          this.lopendeRegistratie = keepingUpdate.lopend;
          this.urenWeek = Number((Math.round((keepingUpdate.week / 60) * 100) / 100).toFixed(1));
          this.urenVandaag = Number((Math.round((keepingUpdate.dag / 60) * 100) / 100).toFixed(1));
        })
      },
    });

    client.activate();
  }

  onClickStartOfStopRegistratie() {
    this.keepingService.startStopLopendeEntry().subscribe();
  }
}
