import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class KeepingService {
  private baseUrl: string = '';

  // private baseUrl: string = 'https://api.keeping.nl/v1/6592';

  constructor(
    private httpClient: HttpClient
  ) {

  }

  public getProjecten(): any {
    return this.httpClient.get(this.baseUrl + '/assets/tasks.json');
  }
}
