import {TestBed} from '@angular/core/testing';

import {KeepingService} from './keeping.service';

describe('KeepingService', () => {
  let service: KeepingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(KeepingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
