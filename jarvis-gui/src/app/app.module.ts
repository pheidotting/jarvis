import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {HttpClientModule} from "@angular/common/http";
import {KeepingComponent} from './components/keeping/keeping.component';
import {GaugeModule} from "angular-gauge";
import {CameraMonitorComponent} from './components/camera-monitor/camera-monitor.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    KeepingComponent,
    CameraMonitorComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    GaugeModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
