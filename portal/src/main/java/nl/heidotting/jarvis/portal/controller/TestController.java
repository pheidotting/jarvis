package nl.heidotting.jarvis.portal.controller;

import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;

@Controller("/test")
public class TestController {
    @Get("/hoi")
    public String hoi() {
        return "JoeJoe";
    }
}
