package nl.heidotting.jarvis.portal.websocket;

import io.micronaut.websocket.WebSocketBroadcaster;
import io.micronaut.websocket.WebSocketSession;
import io.micronaut.websocket.annotation.OnClose;
import io.micronaut.websocket.annotation.OnMessage;
import io.micronaut.websocket.annotation.OnOpen;
import io.micronaut.websocket.annotation.ServerWebSocket;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.Predicate;

@Slf4j
@ServerWebSocket("/jarvis/websockets")
public class KeepingWebsocket {
    private final static Logger LOGGER = LoggerFactory.getLogger(KeepingWebsocket.class);
    private WebSocketBroadcaster broadcaster;

    public KeepingWebsocket(WebSocketBroadcaster broadcaster) {
        this.broadcaster = broadcaster;
    }

    @OnOpen
    public Publisher<String> onOpen(WebSocketSession session) {
        log("onOpen", session);
//        if (topic.equals("all")) {
        return broadcaster.broadcast(String.format("[%s] Now making announcements!"), isValid("a"));
//        }
//        return broadcaster.broadcast(String.format("[%s] Joined %s!", username, topic), isValid(topic));
    }

    @OnMessage
    public Publisher<String> onMessage(
            String message,
            WebSocketSession session) {

        log("onMessage", session);
        return broadcaster.broadcast(String.format("[%s] %s", message), isValid("a"));
    }

    @OnClose
    public Publisher<String> onClose(
            WebSocketSession session) {

        log("onClose", session);
        return broadcaster.broadcast(String.format("[%s] Leaving %s!"), isValid("a"));
    }

    private void log(String event, WebSocketSession session) {
        LOGGER.info("* WebSocket: {} received for session {} from '{}' regarding '{}'",
                event, session.getId());
    }

    private Predicate<WebSocketSession> isValid(String topic) {
        return s -> topic.equals("all") //broadcast to all users
                || "all" .equals(s.getUriVariables().get("topic", String.class, null)) //"all" subscribes to every topic
                || topic.equalsIgnoreCase(s.getUriVariables().get("topic", String.class, null)); //intra-topic chat
    }
}