package nl.heidotting.jarvis;

import io.micronaut.runtime.Micronaut;

public class Application {

    public static void main(String[] args) {
        System.out.println("P O R T A L ! !");
        Micronaut.run(Application.class, args);
    }
}