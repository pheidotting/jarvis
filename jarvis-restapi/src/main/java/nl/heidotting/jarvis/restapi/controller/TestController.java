package nl.heidotting.jarvis.restapi.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Slf4j
@Controller
public class TestController {
    @RequestMapping(
            method = RequestMethod.GET,
            value = "/moi"
    )
    public ResponseEntity<String> moi() {
        log.info("moi");
        return ResponseEntity.ok("moimoi");
    }
}
