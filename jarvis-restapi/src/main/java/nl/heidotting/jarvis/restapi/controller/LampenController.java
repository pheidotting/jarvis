//package nl.heidotting.jarvis.restapi.controller;
//
//import lombok.extern.slf4j.Slf4j;
//import nl.heidotting.jarvis.restapi.LampenApiDelegate;
//import nl.heidotting.jarvis.restapi.mq.sender.LampenAanSender;
//import nl.heidotting.jarvis.restapi.mq.sender.LampenUitMetLangeTimeoutSender;
//import nl.heidotting.jarvis.restapi.mq.sender.LampenUitMetTimeoutSender;
//import nl.heidotting.jarvis.restapi.mq.sender.LampenUitSender;
//import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenAan;
//import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUit;
//import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUitMetLangeTimeout;
//import nl.heidotting.jarvis.shared.mqtt.messages.AlleLampenUitMetTimeout;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.stereotype.Controller;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestMethod;
//
//@Slf4j
//@Controller
//public class LampenController implements LampenApiDelegate {
//    @Autowired
//    private LampenAanSender lampenAanSender;
//    @Autowired
//    private LampenUitSender lampenUitSender;
//    @Autowired
//    private LampenUitMetTimeoutSender lampenUitMetTimeoutSender;
//    @Autowired
//    private LampenUitMetLangeTimeoutSender lampenUitMetLangeTimeoutSender;
//
//
//    @Override
//    public ResponseEntity<Void> schakelLampenAan() {
//        log.info("Lampen inschakelen");
//        lampenAanSender.stuur(new AlleLampenAan());
//
//        return ResponseEntity.ok().build();
//    }
//
//    @Override
//    public ResponseEntity<Void> schakelLampenUit() {
//        log.info("Lampen uitschakelen");
//        lampenUitSender.stuur(new AlleLampenUit());
//
//        return ResponseEntity.ok().build();
//    }
//
//    @Override
//    public ResponseEntity<Void> schakelLampenUitMetTimeout() {
//        log.info("Lampen uitschakelen met vertraging");
//        lampenUitMetTimeoutSender.stuur(new AlleLampenUitMetTimeout());
//
//        return ResponseEntity.ok().build();
//    }
//
//    @Override
//    public ResponseEntity<Void> schakelLampenUitMetLangeTimeout() {
//        log.info("Lampen uitschakelen met vertraging");
//        lampenUitMetLangeTimeoutSender.stuur(new AlleLampenUitMetLangeTimeout());
//
//        return ResponseEntity.ok().build();
//    }
//}
