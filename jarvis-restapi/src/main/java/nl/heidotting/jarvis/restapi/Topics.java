package nl.heidotting.jarvis.restapi;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static nl.heidotting.jarvis.shared.mqtt.Topics.*;

@Slf4j

@Configuration
public class Topics {
    @Bean
    public FanoutExchange alleLampenAanExchange() {
        log.info("Aanmaken {}", LAMPEN_LAMPEN_AAN_EXCHANGE);
        return new FanoutExchange(LAMPEN_LAMPEN_AAN_EXCHANGE);
    }

    @Bean
    public FanoutExchange alleLampenUitExchange() {
        log.info("Aanmaken {}", LAMPEN_LAMPEN_UIT_EXCHANGE);
        return new FanoutExchange(LAMPEN_LAMPEN_UIT_EXCHANGE);
    }

    @Bean
    public FanoutExchange alleLampenUitMetTimeoutExchange() {
        log.info("Aanmaken {}", LAMPEN_LAMPEN_UIT_MET_TIMEOUT_EXCHANGE);
        return new FanoutExchange(LAMPEN_LAMPEN_UIT_MET_TIMEOUT_EXCHANGE);
    }

    @Bean
    public FanoutExchange alleLampenUitMetLangeTimeoutExchange() {
        log.info("Aanmaken {}", LAMPEN_LAMPEN_UIT_MET_LANGE_TIMEOUT_EXCHANGE);
        return new FanoutExchange(LAMPEN_LAMPEN_UIT_MET_LANGE_TIMEOUT_EXCHANGE);
    }
}
