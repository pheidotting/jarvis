package nl.heidotting.jarvis.restapi;

import nl.heidotting.jarvis.shared.services.AbstractStarter;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;

@Configuration
@SpringBootApplication(scanBasePackages = {"nl.heidotting.jarvis"})
public class JarvisRestAPI extends AbstractStarter {
    public static void main(String[] args) {
        SpringApplication.run(JarvisRestAPI.class, args);
    }
}
